# -*- coding: utf-8 -*-
'''
Each class corresponds to a component of an optical system. Each has the methods forwardPropagate -- which returns the field u2 at the output of the component given the field u1 at the input -- and backPropagate -- returns the field at the input given the field at the output.

All the details of the optical setup are contained in this file so a script calling this module to simulate the setup will only have to define the grid and optics

###########
## Notes ##
###########
- uses Goodman/NumPy's convention for the Fourier transform
- the optics classes cannot accept arrays of waveLength, propagationDistance, focalLength, or clearAperture
- gs and omraf require a_targ and a_dmd to be normalized such that np.sum(a_targ**2) == np.sum(a_dmd**2) == 1 (done by prepareInputs)

###########
## TODO ##
###########
- can we rescale the DMD and trap planes in Fresnel and Helmholtz so we don't have to oversample so much in the DMD plane?
- exact propagation through a lens with a known thickness profile r(x,y)?
    - Numerically convolve input field with Helmholtz equation Green's function using FFT
    - how to handle refraction at interfaces?
- optimize and parallelize code (at the moment only the loops in computeHologram would benefit)
    -> https://scipy-cookbook.readthedocs.io/items/PerformancePython.html
    -> books by Johansson or Lanaro
    -> Numba, Cython, weave
    -> parallelization: https://ipyparallel.readthedocs.io/en/latest/, http://numba.pydata.org/numba-doc/latest/user/jit.html#parallel
- add roughness figure of merit
- shift the Fourier simulation space to the center of the target field's spatial frequency spectrum so we can sample a smaller region
- add a step to smooth the kinoform before *propagate in gs and omraf
    -> https://www.mathworks.com/matlabcentral/answers/320596-fft-and-smoothing-of-signal
    -> https://www.mathworks.com/help/signal/ref/sgolayfilt.html
    -> https://www.mathworks.com/help/curvefit/smooth.html
- add Lens class that has the exact profile of the aspheric lens (Saleh & Teich Eq. 2.4-5)
- Look into these functions:
    -> http://scikit-image.org/docs/dev/api/api.html
    -> http://scikit-image.org/docs/dev/api/skimage.transform.html#skimage.transform.resize
    -> http://scikit-image.org/docs/dev/api/skimage.measure.html#skimage.measure.compare_ssim
    -> http://scikit-image.org/docs/0.13.x/api/skimage.util.html#skimage.util.pad

Last updated: January 6, 2018 by Jeff Mohan (mohanj@phys.ethz.ch)
'''

###################################################################
## INITIALIZE MODULE WITH GLOBAL PARAMETERS OF THE OPTICAL SETUP ##
###################################################################

import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm
from scipy.special import erfinv, fresnel
from math import factorial
from scipy.interpolate import RectBivariateSpline, RegularGridInterpolator
from skimage.transform import rotate
from imageio import imread
from glob import glob
import sys, os, time, pickle, pdb, __main__

# location of this file and its dependancies
root = os.path.split(os.path.abspath(__file__))[0]

# Parameters of the setup with the Vialux DMD
waveLength          = 671e-9
focalLength         = 18e-3 # Fourier lens
dmdShape            = (768, 1024) # y runs along the 0th axis,
# ccdShape            = (960, 1280) # x runs along the 1st axis
ccdShape            = (1024, 1024) # x runs along the 1st axis
dmdPixel            = 13.68e-6 # https://www.vialux.de/en/dlp-chipsets.html
telescope           = 1 # (magnification of telescope with image in back focal plane of Fourier lens) * (-1)**(number of reflections from mirrors/beam splitters)
on                  = 1 # the DMD mirror is blazed (on) when the controller passes the value of on to it
# diffractionOrder    = 6 # which diffraction order from the fundamental DMD balzed grating is used
subDiffractionOrder = 1 # which diffraction order from the supergrating is used (relative to diffractionOrder)
# blazeAngle          = 12 # tilt angle of the DMD mirrors in on or off
# dmdAngle            = 45 # DMD is rotated 45 degrees counter-clockwise about the optical axis facing the DMD
# incidentAngle       = 12.01 # angle the incident optical axis makes with the DMD normal
# reflectedAngle      = 12.01 # angle the reflected optical axis makes with the DMD normal
ccdPixel            = 3.75e-6 # size of square CCD pixels
angle_CCDtoDMD      = 137   # parameters to transform CCD to
flipX               = True  # DMD coordinates according to:
flipY               = False # r_dmd = sigmaX**flipX * sigmaY**flipY * R(angle_CCDtoDMD) * r_ccd
dmdPixel            *= np.abs(telescope) # TODO

#############
## CLASSES ##
#############

def ft(u):
    '''
    Goodman/NumPy's Fourier transform (Saleh & Teich's inverse Fourier transform) with proper shifting (Schmidt Listing 2.1)
    '''
    return np.fft.fftshift( np.fft.fft2( np.fft.ifftshift(u), norm='ortho' ) )



def ift(Fu):
    '''
    Goodman/NumPy's inverse Fourier transform (Saleh & Teich's Fourier transform) with proper shifting (Schmidt Listing 2.2)
    '''
    return np.fft.ifftshift( np.fft.ifft2( np.fft.fftshift(Fu), norm='ortho' ) )



class Helmholtz:
    '''
    Free space propagation as the exact solution to the scalar Helmholtz equation. Uses the same grid in both the input and output plane
    '''
    def __init__(self, scanGridShape, propagationDistance, nyquistParameter=2, padFactor=0, ccd=False):
        print('Creating a Helmholtz object on a scan grid of {} DMD pixels...'.format(scanGridShape))
        t0 = time.time()
        self.grid = computeHelmholtzGrid(scanGridShape, propagationDistance, nyquistParameter, padFactor, ccd)
        x, y = self.grid
        nuY = np.fft.fftshift(np.fft.fftfreq( y.shape[0], y[1,0]-y[0,0] ))
        nuX = np.fft.fftshift(np.fft.fftfreq( x.shape[1], x[0,1]-x[0,0] ))
        nuX, nuY = np.meshgrid(nuX, nuY)
        self.H = np.exp( +2j*np.pi*propagationDistance*np.sqrt(waveLength**-2 - nuX**2 - nuY**2 + 0j) ) # transfer function
        print('Done! The grid shape is {} ({:e} sec.)\n'.format( x.shape, time.time()-t0 ))
    def forwardPropagate(self, u):
        return ift( self.H*ft( u ) )
    def backPropagate(self, u): # TODO check that this is ok for non-unitary evolution nu>1/waveLength (Yang, Dong, Gu, Zhuang, Ersoy 1994)
        return ift( self.H.conj()*ft( u ) )



class Fresnel:
    '''
    Free space propagation from back focal/input/object to the front focal/output/Fourier plane of a parabolic lens in Fresnel approximation: Fourier transform
    '''
    def __init__(self, scanGridShape, nyquistParameter=2, padFactor=0, ccd=False):
        print('Creating a Fresnel object on a scan grid of {} DMD pixels...'.format(scanGridShape))
        t0 = time.time()
        self.grid_dmd, self.grid_trap = computeFresnelGrid(scanGridShape, nyquistParameter, padFactor, ccd)
        print('Done! The grid shape is {} ({:e} sec.)\n'.format( self.grid_dmd[0].shape, time.time()-t0 ))
    def forwardPropagate(self, u):
        return ft(u)
    def backPropagate(self, u):
        return ift(u)



# TODO extend to dmdLensDistance, lensTrapDistance != focalLength
# class FresnelAperture:
#     '''
#     Free space propagation from back focal/input/object to the front focal/output/Fourier plane of a finite parabolic lens in Fresnel approximation
#
#     ssa, csa = fresnel(z)
#     ssa = integral(sin(pi/2 * t**2), t=0..z)
#     csa = integral(cos(pi/2 * t**2), t=0..z)
#     '''
#     # def __init__(self, scanGridShape, dmdLensDistance=focalLength, lensTrapDistance=focalLength, clearAperture=19e-3, nyquistParameter=2, padFactor=0, ccd=False):
#     def __init__(self, scanGridShape, clearAperture=19e-3, nyquistParameter=2, padFactor=0, ccd=False):
#         print('Creating a FresnelAperture object on a scan grid of {} DMD pixels...'.format(scanGridShape))
#         t0 = time.time()
#         lf = np.sqrt(waveLength*focalLength)
#         self.grid_dmd, self.grid_trap = computeFresnelGrid(scanGridShape, nyquistParameter, padFactor, ccd)
#         print('Done! The grid shape is {} ({:e} sec.)\n'.format( self.grid_dmd[0].shape, time.time()-t0 ))
#     def forwardPropagate(self, u):
#         return ft(u)
#     def backPropagate(self, u):
#         return ift(u)



class Fourier:
    '''
    Fourier configuration where, ideally, the object is in the back focal plane (dmdLensDistance=focalLength) and the trap/CCD is in the front focal plane (lensTrapDistance=focalLength) shown in Figure 5.5 (b) in Goodman. Uses full Helmholtz propagation.
    '''
    def __init__(self, scanGridShape, dmdLensDistance=focalLength, lensTrapDistance=focalLength, clearAperture=2.54e-2, nyquistParameter=2, padFactor=0, ccd=False):
        print('Creating a Fourier object on a scan grid of {} DMD pixels...'.format(scanGridShape))
        t0 = time.time()
        self.grid = computeFourierGrid(scanGridShape, dmdLensDistance, lensTrapDistance, nyquistParameter, padFactor, ccd)
        x, y = self.grid
        nuY = np.fft.fftshift(np.fft.fftfreq( y.shape[0], y[1,0]-y[0,0] ))
        nuX = np.fft.fftshift(np.fft.fftfreq( x.shape[1], x[0,1]-x[0,0] ))
        nuX, nuY = np.meshgrid(nuX, nuY)
        self.H1 = np.exp( +2j*np.pi* dmdLensDistance*np.sqrt(waveLength**-2 - nuX**2 - nuY**2 + 0j) ) # DMD -> lens
        self.H2 = np.exp( +2j*np.pi*lensTrapDistance*np.sqrt(waveLength**-2 - nuX**2 - nuY**2 + 0j) ) # lens -> trap
        self.L = np.exp( -1j*np.pi*(x**2 + y**2)/(waveLength*focalLength) )
        self.L[ x**2 + y**2 > (clearAperture/2)**2 ] = 0
        print('Done! The grid shape is {} ({:e} sec.)\n'.format( x.shape, time.time()-t0 ))
    def forwardPropagate(self, u):
        return ift( self.H2*ft( self.L*ift( self.H1*ft(u) ) ) )
    def backPropagate(self, u): # TODO is this ok for non-unitary evolution nu>1/waveLength (Yang, Dong, Gu, Zhuang, Ersoy 1994)?
        return ift( self.H1.conj()*ft( self.L.conj()*ift( self.H2.conj()*ft(u) ) ) )



# TODO add Aperture class like a mirror or beam splitter



# class OpticsList:
#     '''
#     Propagation through an optical system composed of an arbitrary number of any of the above optical components (Helmholtz, Lens, Fresnel, Fourier). The order that the light encounters the components during forward propagation should be the order of the orguments to OpticsList __init__ function
#     '''
#     def __init__(self, *opticsList): # TODO add grid to this class
#         self.opticsList = opticsList
#     def forwardPropagate(self, u):
#         for o in self.opticsList: u = o.forwardPropagate(u)
#         return u
#     def backPropagate(self, u):
#         for o in opticsList: u = o.backPropagate(u)
#         return u

#########################
## GRID INITIALIZATION ##
#########################

def computeHelmholtzGrid(scanGridShape, propagationDistance, nyquistParameter, padFactor, ccd):
    '''
    Returns the grid (x, y) centered at (0, 0) that sufficiently samples both the DMD and down-field planes for use with Helmholtz. scanGridShape is the number of DMD pixels to be covered by the grid in each dimension and can differ dmdShape. If using data from a wavefront scan performed by fourierTransform_GUI.py, scanGridShape should be (scan_size, scan_size) (returned by getScanData). Enhance the grid size by 2**padFactor along each dimension. If ccd=True, then use the CCD pixel size (3.75µm) as the smallest feature in the trap plane we want to sample.

    Returns: grid=(x,y)
    '''
    dy = dmdPixel/nyquistParameter # grid
    dx = dmdPixel/nyquistParameter # spacing
    Dy = propagationDistance*waveLength/dmdPixel + dmdPixel*scanGridShape[0] # lower bound of
    Dx = propagationDistance*waveLength/dmdPixel + dmdPixel*scanGridShape[1] # grid length
    if ccd: # ensure the CCD is being adequately sampled (1 grid point per CCD pixel)
        dy = min( dy, ccdPixel )
        dx = min( dx, ccdPixel )

    Dy = dy * 2**np.ceil( np.log2(Dy/dy) ) # number of grid points
    Dx = dx * 2**np.ceil( np.log2(Dx/dx) ) # to next power of 2
    Dy *= 2**padFactor # pad to avoid
    Dx *= 2**padFactor # aliasing

    Ny = int(Dy/dy/2) # number of grid points in both directions
    Nx = int(Dx/dx/2) # using int to eliminate rounding errors
    y, x = np.mgrid[ -Ny:Ny, -Nx:Nx ] # take care to not repeat left boundary on the right
    y = dy*y
    x = dx*x
    return x, y



# TODO add function to compute the grid (sampling requirements) from the properties (footprints in real and reciprocal space) of the target field to avoid sampling to large of a region in the trap plane
def computeFresnelGrid(scanGridShape, nyquistParameter, padFactor, ccd):
    '''
    Returns the grids centered at (0, 0) in both DMD and trap (Fourier) planes (x_dmd, y_dmd) and (x_trap, y_trap) defined by:
        dx_trap = waveLength*focalLength / Dx_dmd
        Dx_trap = waveLength*focalLength / dx_dmd
    for use with Fresnel. gridShape is the number of DMD pixels to be covered by the grid in each dimension and can differ dmdShape. If using data from a wavefront scan performed by fourierTransform_GUI.py, gridShape should be (scan_size, scan_size) (returned by getScanData). Enhance the grid size by 2**padFactor along each dimension. If ccd=True, then use the CCD pixel size (3.75um) as the smallest feature in the trap plane we want to sample.

    Returns: grid_dmd=(x_dmd, y_dmd), grid_trap=(x_trap, y_trap)
    '''
    dy_dmd = dmdPixel/nyquistParameter # DMD grid
    dx_dmd = dmdPixel/nyquistParameter # spacing
    Dy_dmd = nyquistParameter*dmdPixel*scanGridShape[0] # lower bound of
    Dx_dmd = nyquistParameter*dmdPixel*scanGridShape[1] # DMD grid length
    if ccd: # ensure the CCD is being adequately sampled in the trap plane (1 grid point per CCD pixel)
        Dy_dmd = max( Dy_dmd, waveLength*focalLength/ccdPixel )
        Dx_dmd = max( Dx_dmd, waveLength*focalLength/ccdPixel )

    Dy_dmd = dy_dmd * 2**np.ceil( np.log2(Dy_dmd/dy_dmd) ) # number of grid points
    Dx_dmd = dx_dmd * 2**np.ceil( np.log2(Dx_dmd/dx_dmd) ) # to next power of 2
    dy_dmd /= 2**padFactor # pad in both
    dx_dmd /= 2**padFactor # DMD and trap
    Dy_dmd *= 2**padFactor # planes to avoid
    Dx_dmd *= 2**padFactor # aliasing

    Ny = int(Dy_dmd/dy_dmd/2) # number of grid points in both directions
    Nx = int(Dx_dmd/dx_dmd/2) # using int to eliminate rounding errors
    y_dmd, x_dmd = np.mgrid[ -Ny:Ny, -Nx:Nx ] # take care to not repeat left boundary on the right
    y_dmd = dy_dmd*y_dmd
    x_dmd = dx_dmd*x_dmd

    # trap plane is related to DMD by FFT from Fresnel
    y_trap = waveLength*focalLength*np.fft.fftshift(np.fft.fftfreq( 2*Ny, dy_dmd ))
    x_trap = waveLength*focalLength*np.fft.fftshift(np.fft.fftfreq( 2*Nx, dx_dmd ))
    x_trap, y_trap = np.meshgrid(x_trap, y_trap)
    return (x_dmd, y_dmd), (x_trap, y_trap)



# TODO add function to compute the grid (sampling) from the properties of the target field
# TODO add scaling parameter from telescope between the DMD and trap
def computeFourierGrid(scanGridShape, dmdLensDistance, lensTrapDistance, nyquistParameter=2, padFactor=0, ccd=False):
    '''
    Returns the grid (x, y) centered at (0, 0) that sufficiently samples both the DMD and trap planes for use with Helmholtz. scanGridShape is the number of DMD pixels to be covered by the grid in each dimension and can differ dmdShape. If using data from a wavefront scan performed by fourierTransform_GUI.py, scanGridShape should be (scan_size, scan_size) (returned by getScanData). Enhance the grid size by 2**padFactor along each dimension. If ccd=True, then use the CCD pixel size (3.75um) as the smallest feature in the trap plane we want to sample.

    Returns: x, y
    '''
    dy_dmd = dmdPixel/nyquistParameter # DMD grid
    dx_dmd = dmdPixel/nyquistParameter # spacing
    Dy_lens = np.abs(subDiffractionOrder)*dmdLensDistance*waveLength/dmdPixel + dmdPixel*scanGridShape[0] # lower bound of
    Dx_lens = np.abs(subDiffractionOrder)*dmdLensDistance*waveLength/dmdPixel + dmdPixel*scanGridShape[1] # lens grid length

    dy_trap = waveLength*focalLength/(nyquistParameter*dmdPixel*scanGridShape[0]) # DMD and trap
    dx_trap = waveLength*focalLength/(nyquistParameter*dmdPixel*scanGridShape[1]) # field sizes and
    Dy_trap = waveLength*focalLength/dmdPixel                                     # bandwidths are
    Dx_trap = waveLength*focalLength/dmdPixel                                     # related by Fourier
    if ccd: # ensure the CCD is being adequately sampled (1 grid point per CCD pixel)
        dy_trap = min( dy_trap, ccdPixel )
        dx_trap = min( dx_trap, ccdPixel )

    dy = min( dy_dmd, dy_trap )  # DMD, lens, and trap
    dx = min( dx_dmd, dx_trap )  # grids are the
    Dy = max( Dy_lens, Dy_trap ) # same so sample
    Dx = max( Dx_lens, Dx_trap ) # both sufficiently

    Dy = dy * 2**np.ceil( np.log2(Dy/dy) ) # number of grid points
    Dx = dx * 2**np.ceil( np.log2(Dx/dx) ) # to next power of 2
    Dy *= 2**padFactor # pad to avoid
    Dx *= 2**padFactor # aliasing

    Ny = int(Dy/dy/2) # number of grid points in both directions
    Nx = int(Dx/dx/2) # using int to eliminate rounding errors
    y, x = np.mgrid[ -Ny:Ny, -Nx:Nx ] # take care to not repeat left boundary on the right
    y = dy*y
    x = dx*x
    return x, y



def computeScanIdx(grid_dmd, scanGridShape):
    '''
    Computes the slice on the DMD grid (x_dmd, y_dmd) that contains just grid pixels in the scan grid based on either where
        - a_dmd is 0 if the grid is not passed
        - the grid is outside the scan grid defined by scanGridShape if a_dmd is not passed

    Returns: scan
    '''

    x_dmd, y_dmd = grid_dmd
    if x_dmd.ndim == 1: x_dmd, y_dmd = np.meshgrid(x_dmd, y_dmd)
    y_scan = np.arange( -np.ceil(scanGridShape[0]/2), np.floor(scanGridShape[0]/2) )
    x_scan = np.arange( -np.ceil(scanGridShape[1]/2), np.floor(scanGridShape[1]/2) )
    bounds = dmdPixel*np.array([ y_scan[0]-0.5, y_scan[-1]+0.5,   # include the whole DMD
                                 x_scan[0]-0.5, x_scan[-1]+0.5 ]) # pixel on all sides
    scan = np.argwhere( (bounds[0]<=y_dmd) * (y_dmd<=bounds[1]) * (bounds[2]<=x_dmd) * (x_dmd<=bounds[3]) )
    scan = ( slice( scan[0,0], scan[-1,0]+1 ),
             slice( scan[0,1], scan[-1,1]+1 ) )
    return scan



def computeControlIdx(grid_trap, ctrl_scale=None, ctrl_length=None):
    '''
    Computes the slice on grid_trap=(x_trap, y_trap) that contains just grid pixels in the control region. ctrl_scale is a 2-tuple with the fraction of the simulation grid control grid covers in each dimension and ctrl_length is a 2-tuple with the absolute length in meters (assumes the grid is centered at the origin)

    Returns: ctrl
    '''
    x, y = grid_trap[0], grid_trap[1]
    if ctrl_length is not None:
        ctrl = np.argwhere( (-ctrl_length[0]/2<=y) * (y<=ctrl_length[0]/2)
                          * (-ctrl_length[1]/2<=x) * (x<=ctrl_length[1]/2) )
        ctrl = ( slice( ctrl[0,0], ctrl[-1,0]+1 ),
                 slice( ctrl[0,1], ctrl[-1,1]+1 ) )
    elif (ctrl_scale is not None) and (ctrl_length is None):
        ctrl = ( slice( int(x.shape[0]*(0.5-ctrl_scale[0]/2)), int(x.shape[0]*(0.5+ctrl_scale[0]/2)) ),
                 slice( int(x.shape[1]*(0.5-ctrl_scale[1]/2)), int(x.shape[1]*(0.5+ctrl_scale[1]/2)) ) )
    else:
        ctrl = ( slice(0, x.shape[0]), slice(0, x.shape[1]) )
    return ctrl



def computeOffsetControlIdx(grid_trap, supergratingPitch, supergratingAngle, ctrl_scale=None, ctrl_length=None):
    '''
    Computes the slice on grid_trap=(x_trap, y_trap) that contains just grid pixels in the control region. ctrl_scale is a 2-tuple with the fraction of the simulation grid control grid covers in each dimension and ctrl_length is a 2-tuple with the absolute length in meters (assumes the grid is centered at the origin)

    Returns: ctrl
    '''
    x, y = grid_trap[0], grid_trap[1]
    offsetX = subDiffractionOrder*waveLength*focalLength/(supergratingPitch*dmdPixel)*np.cos(np.deg2rad(supergratingAngle))
    offsetY = subDiffractionOrder*waveLength*focalLength/(supergratingPitch*dmdPixel)*np.sin(np.deg2rad(supergratingAngle))
    if ctrl_length is not None:
        ctrl = np.argwhere( (-ctrl_length[0]/2+offsetY<=y) * (y<=ctrl_length[0]/2+offsetY)
                          * (-ctrl_length[1]/2+offsetX<=x) * (x<=ctrl_length[1]/2+offsetX) )
        ctrl = ( slice( ctrl[0,0], ctrl[-1,0]+1 ),
                 slice( ctrl[0,1], ctrl[-1,1]+1 ) )
    elif (ctrl_scale is not None) and (ctrl_length is None):
        ctrl = ( slice( int(x.shape[0]*(0.5-ctrl_scale[0]/2)), int(x.shape[0]*(0.5+ctrl_scale[0]/2)) ),
                 slice( int(x.shape[1]*(0.5-ctrl_scale[1]/2)), int(x.shape[1]*(0.5+ctrl_scale[1]/2)) ) )
    else:
        raise ValueError('Pass either ctrl_scale or ctrl_length with grid_trap')
    return ctrl



def DMDtoCCD(grid_trap, trap, order=3):
    '''
    Given the grid in the trap plane in DMD coordinates,
        r_ccd = R(-phi) * sigmaX**flipX * sigmaY**flipY * r_dmd
    Visualize with
        pp.pcolormesh(*grid_trap, trap)
        pp.axis([xmin, xmax, ymin, ymax])
        pp.gca().invert_yaxis()
    to mimic y axis inversion of imshow used by the GUI and beam profiler

    Returns: trap
    '''
    if isinstance(trap,tuple) or isinstance(trap,list):
        trap = list(trap) # don't use the same memory space
    if isinstance(trap,list):
        for i in range(len(trap)):
            if flipX: trap[i] = np.fliplr(trap[i])
            if flipY: trap[i] = np.flipud(trap[i])
            if angle_CCDtoDMD !=0: trap[i] = rotate(trap[i], angle_CCDtoDMD, order=order)
    else:
        if flipX: trap = np.fliplr(trap)
        if flipY: trap = np.flipud(trap)
        if angle_CCDtoDMD !=0: trap = rotate(trap, angle_CCDtoDMD, order=order)

    return trap



def CCDtoDMD(ccd, order=3):
    '''
    Given a picture from the CCD, transform to DMD coordinates:
        r_dmd = sigmaX**flipX * sigmaY**flipY * R(phi) * r_ccd

    Returns: dmd
    '''
    if isinstance(ccd,tuple) or isinstance(ccd,list):
        ccd = list(ccd) # don't use the same memory space
    if isinstance(ccd,list):
        for i in range(len(ccd)):
            ccd[i] = np.flipud(ccd[i])
            if angle_CCDtoDMD !=0: ccd[i] = rotate(ccd[i], -angle_CCDtoDMD, order=order)
            if flipX: ccd[i] = np.fliplr(ccd[i])
            if flipY: ccd[i] = np.flipud(ccd[i])
    else:
        ccd = np.flipud(ccd)
        if angle_CCDtoDMD !=0: ccd = rotate(ccd, -angle_CCDtoDMD, order=order)
        if flipX: ccd = np.fliplr(ccd)
        if flipY: ccd = np.flipud(ccd)

    return ccd

##########################
## LOAD DATA FROM FILES ##
##########################

# TODO put gui_interface.getScanData in script and get delete gui_interface.py
def getScanData(scanFile='save.p'):
    '''
    (1) If scanFile is a pickled output of the GUI (e.g. save.p or data_correction.p), call gui_interface.getScanData to save it to a .npz file readable by Python 2 or 3 without importing the DLLs
    (2) Prints n_step, step, radius, scan_size, (x0, y0), and (<X>, <Y>)
    (3) Returns scanGridShape = (scan_size, scan_size) to compute the simulation grid

    Returns: scanGridShape, x0, y0
    '''
    if scanFile.endswith('.p'): # scanFile is an output of the GUI
        npzFile = scanFile.replace('.p','.npz')
        print('Saving scan parameters from {} to {} (ignore the DLL import errors)...'.format( scanFile, npzFile ))
        script = '''
            python2 -c 'import sys; sys.path.append("{}"); import gui_interface; gui_interface.getScanData("{}")'
            '''.format(root, scanFile)
        os.system(script)
        scanFile = npzFile
    elif not scanFile.endswith('.npz'):
        raise ValueError('{} is an unsupported file type!'.format(scanFile))

    print('From scan {}:'.format(scanFile))
    with np.load(scanFile) as data: # .npz file already existed or was just produced
        n_step      = data['n_step'].astype(int).item()
        step        = data['step'].astype(int).item()
        radius      = data['radius'].astype(int).item()
        scan_size   = data['scan_size'].astype(int).item()
        y0          = data['y0'].astype(int).item()
        x0          = data['x0'].astype(int).item()
        Y           = data['Y'].astype(int)
        X           = data['X'].astype(int)

    print('   n_step: {}'.format(n_step))
    print('     step: {}'.format(step))
    print('   radius: {}'.format(radius))
    print('scan_size: {}'.format(scan_size))
    print('  (x0,y0): ({:d},{:d})'.format(x0, y0))
    print('(<X>,<Y>): ({:.1f},{:.1f})'.format(X.mean(), Y.mean()))
    print('')

    return (scan_size, scan_size), x0, y0 # scanGridShape (include the mirrors one radius outside the grid)



# TODO use data['zernike_coeff'] with h.zernike(noll) instead of data['phase_map'] to compute phi_dmd
# TODO add parameter smearWaist to convolve a_dmd with normalized gaussian (0 by default so no smearing)
def DMDfromScan(grid_dmd, scanFile='save.npz', order=3):
    '''
    Loads the amplitude a_dmd=|u_dmd| and the phi_dmd=arg(u_dmd) from scanFile that contains the data from wavefront scans conducted by fourierTransform_GUI.py. Specifically:
    (1) exctract intensities and phase_map from the scanFile (generating a .npz file if scanFile is a .p file)
    (2) create interpolants of the amplitude and wavefront on the DMD scan grid (X, Y)
    (3) evaluates the interpolants over the simulation grid grid_dmd=(x, y)
    (4) return the DMD amplitude profile a_dmd and the wavefront (phase in units of wavelengths)

    Returns: a_dmd, phi_dmd
    '''
    print('Extracting a_dmd and wavefront from {}...'.format(scanFile))
    t0 = time.time()

    x, y = grid_dmd
    if x.ndim==1: x, y = np.meshgrid(x, y)

    if scanFile.endswith('.p'): # getScanData hasn't yet been run
        getScanData(scanFile)   # to produce the .npz file
        scanFile = scanFile.replace('.p','.npz')
    elif not scanFile.endswith('.npz'):
        raise ValueError('{} is an unsupported file type!'.format(scanFile))

    with np.load(scanFile) as data:
        n_step      = data['n_step'].astype(int).item()
        radius      = data['radius'].astype(int).item()
        scan_size   = data['scan_size'].astype(int).item()
        y0          = data['y0'].astype(int).item()
        x0          = data['x0'].astype(int).item()
        Y           = data['Y'].astype(int)
        X           = data['X'].astype(int)
        intensities = data['intensities']
        phase_map   = data['phase_map']
        if phase_map.ndim>2: phase_map = phase_map.sum(axis=0) # sum all iterations of the scan

    # fit scan grid to simulation grid (center both at (0, 0))
    Y = Y[:,0] - y0 # centers of each patch on the
    X = X[0,:] - x0 # DMD in units of dmdPixel

    # interpolate intensities to get a_dmd
    # TODO use intensity_target_map on the border of the scan grid (one radius outside the grid) instead of constant value at the edge
    a_dmd = RectBivariateSpline( dmdPixel*Y, dmdPixel*X, np.sqrt(intensities), kx=order, ky=order )(y[:,0], x[0,:], grid=True)

    # interpolate phase_map to get wavefront (phase_map includes one radius from the edges of the scan grid)
    y_scan = dmdPixel*np.arange( -np.ceil(scan_size/2), np.floor(scan_size/2)) # centers of each DMD mirror within the
    x_scan = dmdPixel*np.arange( -np.ceil(scan_size/2), np.floor(scan_size/2)) # scan grid (plus one radius from the edges)
    phi_dmd = RectBivariateSpline(y_scan, x_scan, 2*np.pi*phase_map, kx=order, ky=order)(y[:,0], x[0,:], grid=True)

    # turn off the mirrors outside the scan grid
    a_dmd   *= (y_scan[0]-dmdPixel/2<=y) * (y<=y_scan[-1]+dmdPixel/2)
    a_dmd   *= (x_scan[0]-dmdPixel/2<=x) * (x<=x_scan[-1]+dmdPixel/2)
    phi_dmd *= (y_scan[0]-dmdPixel/2<=y) * (y<=y_scan[-1]+dmdPixel/2)
    phi_dmd *= (x_scan[0]-dmdPixel/2<=x) * (x<=x_scan[-1]+dmdPixel/2)

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    return a_dmd, phi_dmd



# TODO use a better way to scale the target other than scale parameter to control the actual length in meters the field should be
# TODO add smearWaist parameter similar to DMDfromScan
def targetFromFile(fileName, grid_trap=None, scale=0.5, length=None, angle=0, order=1):
    '''
    Returns a_targ given the name of a serialized (output from pickle.dump, np.save, or np.savez) or an image (png, jpeg, tiff, ...) file. Assumes serialized files contain a variable named a_targ and image files contain the target intensity a_targ**2. If the trap grid is fed to the optional parameter as a tuple of 2D arrays (x,y), a_targ is interpolated (linearly by default) and padded with zeros to make it the same shape. The fraction of the grid dimension fitting the image best that the image occupies is determined by the optional parameter scale (e.g. scale=0.5 means the image will occupy half the grid along its long axis). Length supersedes scale and sets the length in meters of the long dimension of a_targ. Rotate the image by angle degrees counter-clockwise

    Returns: a_targ
    '''
    print('Loading a_targ from {}...'.format(fileName))
    t0 = time.time()

    if fileName.endswith('p'): # pickle.dump
        a_targ = pickle.load(open(fileName,'rb')).a_targ
    elif fileName.endswith('npy'): # np.save
        a_targ = np.load(fileName)
    elif fileName.endswith('npz'): # np.savez
        a_targ = np.load(filename)['a_targ']
    else: # raster image file containing the intensity, not amplitude
        a_targ = np.sqrt(np.flipud(imread(fileName).astype(float))) # image y axis is oriented opposite to the grid's y axis
        # a_targ = np.sqrt(np.flipud(imread(fileName, True))) # image y axis is oriented opposite to the grid's y axis

    if grid_trap is not None: # scale and interpolate to grid
        print('Resizing a_targ to the simulation grid...')
        x_new, y_new = grid_trap # grid_trap = (x_trap, y_trap)
        if x_new.ndim==2: x_new, y_new = x_new[0,:], y_new[:,0]
        imgAspectRatio = a_targ.shape[0]/a_targ.shape[1]
        gridAspectRatio = (y_new[-1]-y_new[0])/(x_new[-1]-x_new[0])

        if imgAspectRatio > gridAspectRatio: # fit image height to grid
            y_old = np.linspace(y_new[0], y_new[-1], a_targ.shape[0])
            x_old = np.linspace(y_new[0], y_new[-1], a_targ.shape[1])/imgAspectRatio
        elif imgAspectRatio < gridAspectRatio: # fit image width to grid
            y_old = np.linspace(x_new[0], x_new[-1], a_targ.shape[0])*imgAspectRatio
            x_old = np.linspace(x_new[0], x_new[-1], a_targ.shape[1])
        else: # image perfectly fits the grid by scaling width and height by the same amount
            y_old = np.linspace(y_new[0], y_new[-1], a_targ.shape[0])
            x_old = np.linspace(x_new[0], x_new[-1], a_targ.shape[1])

        if length is not None: # replace scale
            if imgAspectRatio >= gridAspectRatio:
                scale = length/(y_new[-1] - y_new[0])
            else:
                scale = length/(x_new[-1] - x_new[0])

        y_new, x_new = np.ogrid[ # prepare for RectBivariateSpline
            y_new[0]/scale : y_new[-1]/scale : 1j*y_new.size,
            x_new[0]/scale : x_new[-1]/scale : 1j*x_new.size ]
        y_old += y_new.mean() - y_old.mean() # center image
        x_old += x_new.mean() - x_old.mean() # on the grid

        a_targ = RectBivariateSpline(y_old, x_old, a_targ, kx=order, ky=order)(y_new, x_new)

        x_new, y_new = np.meshgrid(x_new.ravel(), y_new.ravel())
        a_targ *= (x_old[0]<=x_new)*(x_new<=x_old[-1]) # a_targ = 0 outside
        a_targ *= (y_old[0]<=y_new)*(y_new<=y_old[-1]) # its original definition
        # a_targ = np.flipud(a_targ) # image y axis is oriented opposite to the grid's y axis

    if angle != 0: # rotate image counter-clockwise by angle degrees
        a_targ = rotate(a_targ, -angle, order=order)

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    return a_targ



# TODO define pictureFromFile(fileName) like targetFromFile


# main experiment: dx=1µm, dy=4µm, sx=sy=1µm
def sawtoothLattice(grid, nCells=4, angle=0, dx=11e-6, dy=44e-6, sx=11e-6, sy=11e-6, V1=1):
    '''
    Equations 18 and 19 Peotta, Torma, Tovmasyan, Huber. Fractional nCells works

    Returns: a_targ
    '''
    x, y = grid
    angle = np.deg2rad(angle)
    n = np.arange(int(nCells*4)) # indices of lattice sites
    y0 = dy*n/2; y0 -= (y0.max()+y0.min())/2 # y coordinates of lattice site centers (centered at y=0)
    x0 = np.where(n%4==1, +dx, 0) + np.where(n%4==3, -dx, 0) # x coordinates of lattice site centers
    coeff = np.where( n%2==0, V1, 1 ) # V1 on center points, V0=1 on displaced
    R = lambda x0, y0: ( +x0*np.cos(angle) - y0*np.sin(angle),  # rotate center point
                         +x0*np.sin(angle) + y0*np.cos(angle) ) # counter-clockwise
    f = lambda r0: np.exp( -(x-r0[0])**2/(2*sx**2) - (y-r0[1])**2/(2*sy**2) ) # lattice site centered at r0
    V = sum( coeff[ni] * f(R(x0[ni], y0[ni])) for ni in n )
    return np.sqrt(V)



# main experiment: dx=1.4µm, dy=1.4µm, s=0.95µm
# preserve space-bandwidth product: L_test = 11 * L_main
def zigzagLattice(grid, nCells=3, angle=0, dx=15e-6, dy=15e-6, s=10e-6):
    '''
    lattice_zigzag in move_beam.py
    N.B. returns the amplitude (proportional to the square root of the potential)

    Returns: a_targ
    '''
    x, y = grid
    angle = np.deg2rad(angle)
    x, y = ( x*np.cos(angle) + y*np.sin(angle),  # rotate
            -x*np.sin(angle) + y*np.cos(angle) ) # counter-clockwise
    a = np.sin(np.pi*np.where(np.abs(y) < nCells*dy, y+nCells*dy, 0)/dy)
    a = np.sin(np.pi*y/dy) * (np.abs(y)<dy*nCells)
    a = ( np.where(a < 0, a*np.exp(-( (x-dx/2)/s )**2), 0)
        + np.where(a > 0, a*np.exp(-( (x+dx/2)/s )**2), 0) )
    return np.abs(a)


dy     = 1.5e-6
dx     = 1.573e-6
sx     = 0.63e-6
sy     = 0.4e-6
V1     = 32.28/40




def simpleLattice(grid, nSites=5, angle=0, d=3e-6, sx=0.63e-6, sy=0.4e-6):
    '''
    Gaussian wells in a line along the transport direction

    Returns: a_targ
    '''
    x, y = grid
    angle = np.deg2rad(angle)
    n = np.arange(int(nSites)) # indices of lattice sites
    y0 = d*n; y0 -= (y0.max()+y0.min())/2 # y coordinates of lattice site centers (centered at y=0)
    R = lambda y0: ( -y0*np.sin(angle), y0*np.cos(angle) ) # rotate center point counter-clockwise
    f = lambda r0: np.exp( -(x-r0[0])**2/(2*sx**2) - (y-r0[1])**2/(2*sy**2) ) # lattice site centered at r0
    V = sum( f(R(y0[ni])) for ni in n )
    return np.sqrt(V)

######################################
## PREPARE FOR HOLOGRAM COMPUTATION ##
######################################

def prepareDMD(grid, scanGridShape, a_dmd, dmd=None):
    '''
    Impose finite DMD on a_dmd (and all other arrays in dmd) by zeroing them outside the scan grid and normalize a_dmd

    Returns: a_dmd    or    ( a_dmd, dmd )
    '''
    print('Preparing DMD...')
    t0 = time.time()

    if isinstance(dmd,tuple):
        dmd = list(dmd)

    # zero a_dmd outside the scan grid and normalize power
    idx = computeScanIdx(grid, scanGridShape)
    tmp = a_dmd[idx]
    a_dmd = np.zeros_like(a_dmd)
    a_dmd[idx] = tmp
    a_dmd /= np.sqrt(np.sum(a_dmd**2)) # normalize

    if dmd is None:
        print('Done! ({:e} sec.)\n'.format(time.time()-t0))
        return a_dmd
    elif isinstance(dmd,list): # zero dmd[i] outside the scan grid
        for i in range(len(dmd)):
            tmp = dmd[i][idx]
            dmd[i] = np.zeros_like(dmd[i])
            dmd[i][idx] = tmp
    else:
        tmp = dmd[idx]
        dmd = np.zeros_like(dmd)
        dmd[idx] = tmp

    print('Done! ({:e} sec.)\n'.format(time.time()-t0))
    return a_dmd, dmd



def prepareTarget(grid_trap, a_targ, ctrl_scale=(0.5,0.5), ctrl_length=None, offset=0.15):
    '''
    Add Gaunt & Hadzibabic's offset as a fraction of the peak amplitude of a_targ in the control region and normalize a_targ

    Returns: a_targ
    '''
    print('Preparing a_targ...')
    t0 = time.time()

    # add offset (Gaunt & Hadzibabic Eq. 3 in methods)
    a_targ = a_targ.copy() # don't modify the original memory space
    ctrl = computeControlIdx(grid_trap, ctrl_scale, ctrl_length)
    a_targ[ctrl] = np.sqrt( a_targ[ctrl]**2 + (offset*a_targ[ctrl].max())**2 )

    a_targ /= np.sqrt(np.sum(a_targ**2)) # normalize power in trap plane to incident power

    print('Done! ({:e} sec.)\n'.format(time.time()-t0))
    return a_targ



def computeMoments(x, u, beamParameters=False):
    '''
    Computes the center and effective exp(-2) beam waist of the field u along the grid direction x

    Returns: x0, w
    '''
    I = np.abs(u)**2/np.sum(np.abs(u)**2) # normalized intensity
    x1 = np.sum( x*I ) # first moment (center)
    x2 = np.sum( x**2*I ) # second moment
    w = 2*np.sqrt( x2 - x1**2 ) # exp(-2) waist
    return x1, w



def parabolicPhaseGuess(grid_dmd, a_dmd, a_targ, grid_trap=None):
    '''
    Approximate the amplitudes of u_dmd and u_trap/u_targ (back and front focal plane of Fourier lens) as gaussian beams and compute the parabolic phase on u_dmd that produces the correct amplitude of u_trap (Gaunt & Hadzibabic):
        u_dmd ~ exp( -(x_dmd-x0_dmd)**2*(w_dmd**-2 + 2j*pi/R**2) + 2j*pi*B*(x_dmd-x0_dmd) )
        u_trap ~ exp( -(x_trap-x0_trap)**2/w_trap**2 )
        R = w_dmd * np.sqrt(2*np.pi*waveLength*focalLength) / ( (np.pi*w_dmd*w_trap)**2 - (waveLength*focalLength)**2 )**0.25
        B = x0_trap/(waveLength*focalLength)

    Returns: ( kinoformGuess, waist_lens )    or    kinoformGuess
    '''
    print('Computing the parabolic phase guess...')
    t0 = time.time()

    if grid_trap is None: grid_trap = grid_dmd # use Helmholtz grid
    x_dmd, y_dmd = grid_dmd
    x_trap, y_trap = grid_trap

    x0_dmd, wX_dmd = computeMoments(x_dmd, a_dmd)
    y0_dmd, wY_dmd = computeMoments(y_dmd, a_dmd)
    x0_trap, wX_trap = computeMoments(x_trap, a_targ)
    y0_trap, wY_trap = computeMoments(y_trap, a_targ)

    if np.pi*wX_dmd*wX_trap <= waveLength*focalLength: # avoid imaginary phase curvature
        print('Warning: no x phase curvature in parabolic phase initial guess')
        print('Should satisfy: pi * waist_dmd * waist_targ > waveLength * focalLength')
        Rx = np.inf
    else:
        Rx = wX_dmd * np.sqrt(2*np.pi*waveLength*focalLength) / ( (np.pi*wX_dmd*wX_trap)**2 - (waveLength*focalLength)**2 )**0.25

    if np.pi*wY_dmd*wY_trap <= waveLength*focalLength:
        print('Warning: no y phase curvature in parabolic phase initial guess')
        print('Should satisfy: pi * waist_dmd * waist_targ > waveLength * focalLength')
        Ry = np.inf
    else:
        Ry = wY_dmd * np.sqrt(2*np.pi*waveLength*focalLength) / ( (np.pi*wY_dmd*wY_trap)**2 - (waveLength*focalLength)**2 )**0.25

    Bx = x0_trap/(waveLength*focalLength)
    By = y0_trap/(waveLength*focalLength)

    kinoformGuess = 2*np.pi*(
        - (x_dmd-x0_dmd)**2/Rx**2 + Bx*(x_dmd-x0_dmd)
        - (y_dmd-y0_dmd)**2/Ry**2 + By*(y_dmd-y0_dmd) )

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    return kinoformGuess

######################
## FIGURES OF MERIT ##
######################

def computeError(u, a):
    '''
    Spatially-resolved relative error of the intensity of a complex field u in reference to the intensity of a target amplitude a. mask are indices of the field to remove before computing the error. Note that Gerchberg and Saxton showed that the error
        sum( abs(abs(u) - a)**2 )
    decreases (or remains constant) with each iteration, not the definition used here.

    Returns: error
    '''
    return np.abs(u)**2/np.mean(np.abs(u)**2) - a**2/np.mean(a**2)



def computeFidelity(u, a): # TODO write docstring
    '''
    Returns: fidelity
    '''
    return np.sum(np.abs(u)*a)**2 / np.sum(np.abs(u)**2) / np.sum(a**2)



# TODO def roughness() # Pasienski & DeMarco Eq. 3



def computeCtrlEfficiency(u, ctrl):
    '''
    Computes the ratio of power in the control region (given by the slice or index list ctrl) to the total power in the output field u

    Returns: ctrlEfficiency
    '''
    return np.sum( np.abs(u[ctrl])**2 ) / np.sum( np.abs(u)**2 ) # Pasienski & DeMarco paragraph after Eq. 3



def computeOffsetEfficiency(u, ctrl, offset):
    '''
    Computes the fraction of power in the control region used above the offset (as a fraction of th epeak intensity of a_targ)

    Returns: offsetEfficiency
    '''
    return 1 - offset**2/(1+offset**2) * u[ctrl].size * np.abs(u[ctrl]).max()**2 / np.sum(np.abs(u[ctrl])**2)



def computeFootprint(grid, u, P=0.99):
    '''
    Returns the footprint of a complex field u on the real or reciprocal space grid (x, y): the radius Dr of an area within which a fraction P of the total power is contained (like matlab's obw):
        sum_{sqrt(x**2+y**2)<=Dr}( |u(x,y)|**2 ) = P * sum_{x,y}( |u(x,y)|**2 )

    Returns: Dr
    '''
    x, y = grid # real or reciprocal space grid
    r = np.sqrt( (x-x.mean())**2 + (y-y.mean())**2).ravel() # radial position of each point
    I = np.abs(u.ravel())**2/np.sum(np.abs(u)**2) # intensity at each point
    idx = np.argsort(r)
    r = r[idx] # radial positions sorted in increasing order
    E = np.cumsum(I[idx]) # normalized energy = E(r) = int( I(phi,r'), {phi,0,2*pi}, {r',0,r} )
    idx = np.argwhere(E>=P).ravel()[0] # first point where normalized energy contained within a circular region is greater than P
    return r[idx] # radius at this point (Dr)



def checkSampling(grid, u, plot=False, P=0.99):
    '''
    Check that the field u is sufficiently sampled in real and reciprocal space according to Nyquist's theorem using a footprint fraction P.
    '''
    Ny = 2/np.pi*erfinv(P)**2 # effective Nyquist parameter
    print('Checking whether Nyquist\'s theorem is fulfilled for P={} (Nyquist parameter Ny={:.4f})...'.format(P,Ny))
    t0 = time.time()

    x, y = grid
    nuY = np.fft.fftshift(np.fft.fftfreq( y.shape[0], y[1,0]-y[0,0] )) # reciprocal
    nuX = np.fft.fftshift(np.fft.fftfreq( x.shape[1], x[0,1]-x[0,0] )) # space
    nuX, nuY = np.meshgrid(nuX, nuY)                                   # grid
    dx_grid = max( y[1,0]-y[0,0], x[0,1]-x[0,0] ) # real space grid spacing
    Dx_grid = min( y.max()-y.min(), x.max()-x.min() ) # real space grid length
    dnu_grid = max( nuX[0,1]-nuX[0,0], nuY[1,0]-nuY[0,0] ) # reciprocal space grid spacing
    Dnu_grid = min( nuY.max()-nuY.min(), nuX.max()-nuX.min() ) # reciprocal space grid length
    footprint_x = computeFootprint(grid, u, P) # real space
    footprint_nu = computeFootprint((nuX, nuY), ft(u), P) # reciprocal space
    print('Must fulfill the following inequalities:')
    print('Ny * dx_grid * footprint_nu < 1: {:e}'.format(Ny*dx_grid*footprint_nu))
    print('Ny * dnu_grid * footprint_x < 1: {:e}'.format(Ny*dnu_grid*footprint_x))
    if plot: # overlay the field and its footprint in real and reciprocal space
        c = np.linspace(0,2*np.pi,500)        # boundary
        c = ( footprint_x*np.sin(c) + y.mean(),  # of circular
              footprint_x*np.cos(c) + x.mean() ) # footprint
        pp.figure()
        pp.pcolormesh(x*1e3, y*1e3, np.abs(u)**2/np.max(np.abs(u)**2), norm=LogNorm(), cmap='jet', vmax=1, rasterized=True)
        pp.colorbar()
        pp.plot(c[1]*1e3, c[0]*1e3, 'k')
        pp.title('Real space')
        pp.xlabel('$x$ (mm)')
        pp.ylabel('$y$ (mm)')
        pp.axis('scaled')

        c = np.linspace(0,2*np.pi,500)
        c = ( footprint_nu*np.sin(c) + nuX.mean(),
              footprint_nu*np.cos(c) + nuY.mean() )
        pp.figure()
        pp.pcolormesh(nuX*1e-3, nuY*1e-3, np.abs(ft(u))**2/np.max(np.abs(ft(u))**2), norm=LogNorm(), cmap='jet', vmax=1, rasterized=True)
        pp.colorbar()
        pp.plot(c[1]*1e-3, c[0]*1e-3, 'k')
        pp.title('Reciprocal space')
        pp.xlabel('$\\nu_x$ (mm$^{-1}$)')
        pp.ylabel('$\\nu_y$ (mm$^{-1}$)')
        pp.axis('scaled')

        pp.show()

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))

##########################
## HOLOGRAM COMPUTATION ##
##########################

def onestep(a_targ, opticalSystem):
    '''
    Computes the hologram as the amplitude and phase of the IFFT of the (real) target amplitude

    Returns: a_dmd_targ, kinoform
    '''
    print('Running onestep...')
    t0 = time.time()
    u_dmd_targ = opticalSystem.backPropagate(a_targ)
    print('Done! ({:e} sec.)\n'.format(time.time()-t0))
    return np.abs(u_dmd_targ), np.angle(u_dmd_targ)%(2*np.pi)



def gs(a_dmd, a_targ, kinoformGuess, opticalSystem, nIterations=10, saveFile='gs', nSave=np.inf):
    '''
    Performs the Gerchberg-Saxton algorithm and returns the kinoform, field in the trap plane, and fidelity at each iteration

    Returns: kinoform, u_trap, fidelity
    '''
    if saveFile == 'gs':
        try: # name the backup file after the script calling it
            saveFile = __main__.__file__.split('.')[0] + '-data-' + time.strftime('%m.%d-%H.%M.%S')
        except: # omraf was called without a main file
            saveFile = 'data-' + time.strftime('%m.%d-%H.%M.%S')

    t0 = time.time()
    print('Running Gerchberg-Saxton...')
    print('iteration/{}, fidelity (%), time (min.)'.format(nIterations))

    u_dmd = a_dmd*np.exp(1j*kinoformGuess)
    fidelity = np.zeros(nIterations)
    for i in range(nIterations):
        u_trap = opticalSystem.forwardPropagate(u_dmd)
        fidelity[i] = computeFidelity(u_trap, a_targ)
        u_trap = a_targ*np.exp( 1j*np.angle(u_trap) )
        u_dmd = opticalSystem.backPropagate(u_trap)
        u_dmd = a_dmd*np.exp( 1j*np.angle(u_dmd) )
        if (i+1) % nSave == 0: # save the kinoform every nSave iterations in case the job fails
            np.save( saveFile, np.angle(u_dmd) )
            print('Kinoform from iteration {} saved to {}'.format(i+1, saveFile))
        print('{} {:.2f} {:e}'.format(i+1, fidelity[i]*100, (time.time()-t0)/60))

    kinoform = np.angle(u_dmd) % (2*np.pi)
    kinoform[ np.isclose(a_dmd, 0) ] = 0 # no kinoform outside the DMD
    u_trap = opticalSystem.forwardPropagate(u_dmd)

    if saveFile is not None:
        np.savez( saveFile, kinoform=kinoform, u_trap=u_trap, fidelity=fidelity )
        print('Done! Data saved to {}.npz ({:e} min.)\n'.format( saveFile, (time.time()-t0)/60 ))
    else: print('Done! ({:e} min.)\n'.format( (time.time()-t0)/60 ))
    return kinoform, u_trap, fidelity



# TODO implement mixed and free region with "masks" instead of indices
# TODO implement spatially-varying mixing parameter to have noise "envelopes" instead of control and free regions. Do I need a normalization step in the trap plane each iteration? normalize mixing parameter map before iterating?
# TODO extend to non-rectangular control region (maybe take a list of ctrl slices)
def omraf(a_dmd, a_targ, kinoformGuess, opticalSystem, ctrl_scale=(0.5,0.5), ctrl_length=None, mixingParameter=0.4, nIterations=10, saveFile='omraf', nSave=np.inf):
    '''
    Performs the OMRAF (Offset Mixed Region Amplitude Freedom) algorithm and returns the kinoform, field in the trap plane, spatially-resolved relative intensity, and fidelity at each iteration. Arguments are outputs of prepareInputs with doubleGrid=True. ctrl_scale=(cy,cx) is the fraction of the simulation to control along the 0th (y) and 1st (x) dimensions centered at the origin

    Returns: kinoform, u_trap, fidelity
    '''
    if saveFile == 'omraf':
        try: # name the backup file after the script calling it
            saveFile = __main__.__file__.split('.')[0] + '-data-' + time.strftime('%m.%d-%H.%M.%S')
        except: # omraf was called without a main file
            saveFile = 'data-' + time.strftime('%m.%d-%H.%M.%S')

    t0 = time.time()
    print('Running OMRAF...')
    print('iteration/{}, fidelity (%), time (min.)'.format(nIterations))

    try:    ctrl = computeControlIdx(opticalSystem.grid,      ctrl_scale, ctrl_length)
    except: ctrl = computeControlIdx(opticalSystem.grid_trap, ctrl_scale, ctrl_length)

    u_dmd = a_dmd*np.exp(1j*kinoformGuess)
    fidelity = np.zeros(nIterations)
    for i in range(nIterations):
        u_trap_tmp = opticalSystem.forwardPropagate(u_dmd) # temporary array for manipulating control and free regions separately
        fidelity[i] = computeFidelity(u_trap_tmp[ctrl], a_targ[ctrl])
        u_trap = (1-mixingParameter)*u_trap_tmp # set free region
        u_trap[ctrl] = mixingParameter*a_targ[ctrl]*np.exp( 1j*np.angle(u_trap_tmp[ctrl]) ) # set control region
        # TODO combine two previous lines into this:
        # u_trap = m_ctrl*a_targ*np.exp(1j*np.angle(u_trap_tmp)) + m_free*u_trap_tmp
        u_dmd = opticalSystem.backPropagate(u_trap)
        u_dmd = a_dmd*np.exp( 1j*np.angle(u_dmd) )
        if (i+1) % nSave == 0: # save the kinoform every nSave iterations in case the job fails
            np.save( saveFile, np.angle(u_dmd) )
            print('Kinoform from iteration {} saved to {}'.format(i+1, saveFile))
        print('{} {:.2f} {:e}'.format(i+1, fidelity[i]*100, (time.time()-t0)/60))

    kinoform = np.angle(u_dmd) % (2*np.pi)
    kinoform[ np.isclose(a_dmd, 0) ] = 0 # no kinoform outside the DMD
    u_trap = opticalSystem.forwardPropagate(u_dmd)

    if saveFile is not None:
        np.savez( saveFile, kinoform=kinoform, u_trap=u_trap, fidelity=fidelity )
        print('Done! Data saved to {}.npz ({:e} min.)\n'.format( saveFile, (time.time()-t0)/60 ))
    else: print('Done! ({:e} min.)\n'.format( (time.time()-t0)/60 ))
    return kinoform, u_trap, fidelity

#############################
## HOLOGRAM REPRESENTATION ##
#############################

def computeHologram(grid, kinoform, scanGridShape, supergratingPitch=10, supergratingAngle=0, outside=0, toDMD=True, x0=int(dmdShape[1]/2), y0=int(dmdShape[0]/2), saveFile=None):
    '''
    Returns the hologram/grating function either as an array of size scanGridShape to feed to the DMD or of size kinoform.shape==x.shape for use with the output of gs

    Returns: hologram
    '''
    print('Computing the hologram from the kinoform...')
    t0 = time.time()

    x, y = grid
    if y.ndim>1: y, x = y[:,0], x[0,:]
    if (y[-1]-y[0]<dmdPixel*scanGridShape[0]) or (x[-1]-x[0]<dmdPixel*scanGridShape[1]):
        raise ValueError('The grid is smaller than the scan grid shape. Did you forget to pass scanGridShape as a parameter?')

    y_scan = dmdPixel*np.arange( -np.ceil(scanGridShape[0]/2), np.floor(scanGridShape[0]/2) ) # centers of each
    x_scan = dmdPixel*np.arange( -np.ceil(scanGridShape[1]/2), np.floor(scanGridShape[1]/2) ) # mirror in the scan grid
    y_edge = dmdPixel*np.arange( -np.ceil(scanGridShape[0]/2)-0.5, np.floor(scanGridShape[0]/2)+0.5 ) # edges of each
    x_edge = dmdPixel*np.arange( -np.ceil(scanGridShape[1]/2)-0.5, np.floor(scanGridShape[1]/2)+0.5 ) # mirror in the scan
    y_dmd = dmdPixel*np.arange( -np.ceil(dmdShape[0]/2), np.floor(dmdShape[0]/2) ) # centers of each
    x_dmd = dmdPixel*np.arange( -np.ceil(dmdShape[1]/2), np.floor(dmdShape[1]/2) ) # mirror on the DMD

    # represent phase on the unit circle for interpolation/averaging (wikipedia.org/wiki/Directional_statistics)
    kinoform = np.exp(1j*kinoform)

    # kinoform_bin = kinoform averaged over each mirror in the scan grid (x_scan, y_scan)
    if (x[1]-x[0]>=dmdPixel) or (y[1]-y[0]>=dmdPixel): # interpolate if grid spacing is coarser than the DMD pixel size...
        kinoform_bin = RectBivariateSpline(y, x, kinoform, kx=3, ky=3)(y_scan, x_scan, grid=True) # TODO add kx=ky=order argument?
    else: # ...otherwise average kinoform over each DMD pixel in the scan
        idxY = np.array([ # idxY[iy] = y slice of grid points in mirror at y_scan[iy]
            slice( np.argwhere(y_edge[iy]<=y)[0,0], np.argwhere(y<=y_edge[iy+1])[-1,0]+1 )
            for iy in range(scanGridShape[0]) ])
        idxX = np.array([ # idxX[ix] = x slice of grid points in mirror at x_scan[ix]
            slice( np.argwhere(x_edge[ix]<=x)[0,0], np.argwhere(x<=x_edge[ix+1])[-1,0]+1 )
            for ix in range(scanGridShape[1]) ])
        kinoform_bin = np.array([ np.array([      # average kinoform over
            kinoform[ idxY[iy], idxX[ix] ].mean() # simulation grid points
            for ix in range(scanGridShape[1]) ])  # (idxY[iy], idxX[ix])
            for iy in range(scanGridShape[0]) ])  # within each mirror (iy,ix)
        N = np.array([ np.array([                # number of
            kinoform[ idxY[iy], idxX[ix] ].size  # grid points
            for ix in range(scanGridShape[1]) ]) # within each
            for iy in range(scanGridShape[0]) ]) # pixel
        # TODO weight kinoform by a_dmd at the simulation grid point instead of just an arithmetic average?

    kinoform = np.angle(kinoform)         # transform
    kinoform_bin = np.angle(kinoform_bin) # back to phase

    # check representation
    fid_kinoform = np.array([ np.array([
        np.sum( np.cos( kinoform[ idxY[iy],idxX[ix] ] - kinoform_bin[iy, ix] )**2 )
        for ix in range(scanGridShape[1]) ])
        for iy in range(scanGridShape[0]) ]).sum() / N.sum()
    print('kinoform representation fidelity: {}'.format(fid_kinoform))

    supergratingAngle = np.deg2rad(supergratingAngle)
    ky = 2*np.pi/(supergratingPitch*dmdPixel)*np.sin(supergratingAngle) # supergrating
    kx = 2*np.pi/(supergratingPitch*dmdPixel)*np.cos(supergratingAngle) # wavevector
    gratingPhase = ky*y_scan[:,np.newaxis] + kx*x_scan[np.newaxis,:] # supergrating phase (newaxis to broadcast properly)
    gratingPhase += kinoform_bin/subDiffractionOrder
    g = np.where( np.cos(gratingPhase+1e-15)>=0, 1, 0 ).astype(int) # grating function on the scan grid

    pad = ( int( (dmdShape[0]-scanGridShape[0])/2 ),                  # turn off
            int( (dmdShape[1]-scanGridShape[1])/2 ) )                 # pixels
    pad = ( ( pad[0], dmdShape[0]-scanGridShape[0]-pad[0] ),          # outside
            ( pad[1], dmdShape[1]-scanGridShape[1]-pad[1] ) )         # the scan
    g = np.pad( g, pad, mode='constant', constant_values=(outside,) ) # grid

    if toDMD: # save hologram to .npy
        if saveFile is None:
            try: # name the hologram file after the script calling it
                saveFile = __main__.__file__.split('.py')[0] + '-hologram-' + time.strftime('%m.%d-%H.%M.%S')
            except: # computeHologram was called without a main file
                saveFile = 'hologram-' + time.strftime('%m.%d-%H.%M.%S')
        g_dmd = np.ones(dmdShape, int) - on # physical on (1 or 0) isn't necessarily the same as computational on (1)
        idx1 = ( slice( y0-int(np.ceil(scanGridShape[0]/2.)), y0+int(np.floor(scanGridShape[0]/2.)) ),  # correctly centered slice on the
                 slice( x0-int(np.ceil(scanGridShape[1]/2.)), x0+int(np.floor(scanGridShape[1]/2.)) ) ) # DMD grid over the scan grid
        idx2 = computeScanIdx( (x_dmd, y_dmd), scanGridShape ) # slice of simulation grid over the scan grid
        g_dmd[idx1] = np.where( g[idx2]==1, on, 1-on )
        g = g_dmd # assign for returning g
        np.save(saveFile, g)
        print('Saved GUI-readable hologram to: {}.npy'.format(saveFile))
    else: # g to be used in simulations and should have the same shape as the grid
        print('Interpolating the hologram to fit the grid... ({:e} sec.)'.format( time.time()-t0 ))
        g = RegularGridInterpolator(             # grating function is constant within
            (y_dmd, x_dmd), g, method='nearest', # each mirror so method='nearest'
            bounds_error=False, fill_value=0 ) # reflectivity is zero outside the DMD
        x, y = np.meshgrid(x, y)
        gridPoints = np.stack((y.ravel(), x.ravel()), axis=1)
        g = g(gridPoints).reshape(x.shape).astype(int) # interpolate and shape to the grid

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    return g



def computeFineHologram(grid, kinoform, scanGridShape, supergratingPitch=10, supergratingAngle=0):
    '''
    Compute the hologram of a DMD with the same pixel size as the simulation grid for determining the representation error

    Returns: fineHologram
    '''
    print('Computing the fine hologram from the kinoform...')
    t0 = time.time()

    x, y = grid
    if y.ndim>1: y, x = y[:,0], x[0,:]
    supergratingAngle = np.deg2rad(supergratingAngle)
    kx = 2*np.pi/(supergratingPitch*dmdPixel)*np.cos(supergratingAngle) # supergrating
    ky = 2*np.pi/(supergratingPitch*dmdPixel)*np.sin(supergratingAngle) # wavevector
    gratingPhase = kx*x + ky*y + kinoform/subDiffractionOrder
    G = np.where( np.cos(gratingPhase+1e-15)>=0, 1, 0 ).astype(int)
    bounds = dmdPixel*np.array([
        -scanGridShape[0]-1, scanGridShape[0]-1,
        -scanGridShape[1]-1, scanGridShape[1]-1 ])/2
    G *= (bounds[0]<=y) * (y<=bounds[1])
    G *= (bounds[2]<=x) * (x<=bounds[3])

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    return G



def computeAmplitudePhaseHologram(grid_dmd, kinoform, a_dmd, a_dmd_targ, scanGridShape, supergratingPitch=10, supergratingAngle=0, toDMD=True, x0=512, y0=384, saveFile=None):
    '''
    a = a_dmd is the output of DMDfromScan or or a simple gaussian/plane wave
    a_targ = a_dmd_targ is the output of onestep

    Returns: hologram
    '''
    print('Computing the amplitude and phase hologram from the kinoform and incident/outgoing amplitude...')
    t0 = time.time()

    x, y = grid_dmd
    if y.ndim>1: y, x = y[:,0], x[0,:]
    if (y[-1]-y[0]<dmdPixel*scanGridShape[0]) or (x[-1]-x[0]<dmdPixel*scanGridShape[1]):
        raise ValueError('The grid is smaller than the scan grid shape. Did you forget to pass scanGridShape as a parameter?')

    y_scan = dmdPixel*np.arange( -np.ceil(scanGridShape[0]/2), np.floor(scanGridShape[0]/2) ) # centers of each
    x_scan = dmdPixel*np.arange( -np.ceil(scanGridShape[1]/2), np.floor(scanGridShape[1]/2) ) # mirror in the scan grid
    y_edge = dmdPixel*np.arange( -np.ceil(scanGridShape[0]/2)-0.5, np.floor(scanGridShape[0]/2)+0.5 ) # edges of each
    x_edge = dmdPixel*np.arange( -np.ceil(scanGridShape[1]/2)-0.5, np.floor(scanGridShape[1]/2)+0.5 ) # mirror in the scan
    y_dmd = dmdPixel*np.arange( -np.ceil(dmdShape[0]/2), np.floor(dmdShape[0]/2) ) # centers of each
    x_dmd = dmdPixel*np.arange( -np.ceil(dmdShape[1]/2), np.floor(dmdShape[1]/2) ) # mirror on the DMD

    # compute (kinoform_bin, a_dmd_bin, and a_dmd_targ_bin) = (kinoform, a_dmd, a_targ) averaged over each mirror in the scan grid
    kinoform = np.exp(1j*kinoform) # wikipedia.org/wiki/Directional_statistics
    if (x[1]-x[0]>=dmdPixel) or (y[1]-y[0]>=dmdPixel): # interpolate if grid spacing is coarser than the DMD pixel size...
        kinoform_bin = RectBivariateSpline(y, x, kinoform, kx=3, ky=3)(y_scan, x_scan, grid=True) # TODO add kx=ky=order argument?
        a_dmd_bin = RectBivariateSpline(y, x, a_dmd, kx=3, ky=3)(y_scan, x_scan, grid=True)
        a_dmd_targ_bin = RectBivariateSpline(y, x, a_dmd_targ, kx=3, ky=3)(y_scan, x_scan, grid=True)
    else: # ...otherwise average kinoform over each DMD pixel in the scan
        idxY = np.array([ # idxY[iy] = y slice of grid points in mirror at y_scan[iy]
            slice( np.argwhere(y_edge[iy]<=y)[0,0], np.argwhere(y<=y_edge[iy+1])[-1,0]+1 )
            for iy in range(scanGridShape[0]) ])
        idxX = np.array([ # idxX[ix] = x slice of grid points in mirror at x_scan[ix]
            slice( np.argwhere(x_edge[ix]<=x)[0,0], np.argwhere(x<=x_edge[ix+1])[-1,0]+1 )
            for ix in range(scanGridShape[1]) ])
        kinoform_bin = np.array([ np.array([        # average
            kinoform[ idxY[iy], idxX[ix] ].mean()   # simulation
            for ix in range(scanGridShape[1]) ])    # arrays
            for iy in range(scanGridShape[0]) ])    # over
        a_dmd_bin = np.array([ np.array([           # simulation
            a_dmd[ idxY[iy], idxX[ix] ].mean()      # grid
            for ix in range(scanGridShape[1]) ])    # points
            for iy in range(scanGridShape[0]) ])    # (idxY[iy], idxX[ix])
        a_dmd_targ_bin = np.array([ np.array([      # within
            a_dmd_targ[ idxY[iy], idxX[ix] ].mean() # each
            for ix in range(scanGridShape[1]) ])    # mirror
            for iy in range(scanGridShape[0]) ])    # (iy, ix)
        N = np.array([ np.array([                # number of
            kinoform[ idxY[iy], idxX[ix] ].size  # grid points
            for ix in range(scanGridShape[1]) ]) # within each
            for iy in range(scanGridShape[0]) ]) # pixel

    kinoform = np.angle(kinoform) # transform
    kinoform_bin = np.angle(kinoform_bin) # back to phase
    w = (a_dmd_targ_bin/a_dmd_bin) / np.max(a_dmd_targ_bin/a_dmd_bin) # local width map

    # check representation
    scan = computeScanIdx((x, y), scanGridShape)
    fid_a_dmd = np.sum(a_dmd_bin**2)/np.sum(a_dmd[scan]**2) * int(a_dmd[scan].size/a_dmd_bin.size)
    fid_a_dmd_targ = np.sum(a_dmd_targ_bin**2)/np.sum(a_dmd_targ[scan]**2) * int(a_dmd_targ[scan].size/a_dmd_targ_bin.size)
    fid_kinoform = np.array([ np.array([
        np.sum( np.cos( kinoform[ idxY[iy],idxX[ix] ] - kinoform_bin[iy, ix] )**2 )
        for ix in range(scanGridShape[1]) ])
        for iy in range(scanGridShape[0]) ]).sum() / N.sum()
    print('Representation fidelities')
    print('     A_dmd: {}'.format(fid_a_dmd))
    print('A_dmd_targ: {}'.format(fid_a_dmd_targ))
    print('  kinoform: {}'.format(fid_kinoform))

    supergratingAngle = np.deg2rad(supergratingAngle)
    ky = 2*np.pi/(supergratingPitch*dmdPixel)*np.sin(supergratingAngle) # supergrating
    kx = 2*np.pi/(supergratingPitch*dmdPixel)*np.cos(supergratingAngle) # wavevector
    gratingPhase = ky*y_scan[:,np.newaxis] + kx*x_scan[np.newaxis,:] # supergrating phase (newaxis to broadcast properly)
    gratingPhase += kinoform_bin/subDiffractionOrder
    g = np.where( np.cos(gratingPhase+1e-15)>=np.cos(np.pi*w), 1, 0 ).astype(int) # grating function on the scan grid

    pad = ( int( (dmdShape[0]-scanGridShape[0])/2 ),            # turn off
            int( (dmdShape[1]-scanGridShape[1])/2 ) )           # pixels
    pad = ( ( pad[0], dmdShape[0]-scanGridShape[0]-pad[0] ),    # outside
            ( pad[1], dmdShape[1]-scanGridShape[1]-pad[1] ) )   # the scan
    g = np.pad( g, pad, mode='constant', constant_values=(0,) ) # grid

    if toDMD: # save hologram to .npy
        if saveFile is None:
            try: # name the hologram file after the script calling it
                saveFile = __main__.__file__.split('.py')[0] + '-hologram-' + time.strftime('%m.%d-%H.%M.%S')
            except: # computeHologram was called without a main file
                saveFile = 'hologram-' + time.strftime('%m.%d-%H.%M.%S')
        b = np.ones(dmdShape, int) - on # physical on (1 or 0) of the bitmap isn't necessarily the same as computational on (1)
        idx1 = ( slice( y0-int(np.ceil(scanGridShape[0]/2.)), y0+int(np.floor(scanGridShape[0]/2.)) ),
                 slice( x0-int(np.ceil(scanGridShape[1]/2.)), x0+int(np.floor(scanGridShape[1]/2.)) ) )
        idx2 = computeScanIdx( (x_dmd, y_dmd), scanGridShape )
        b[idx1] = np.where( g[idx2]==1, on, 1-on )
        g = b # assign for returning g
        np.save(saveFile, b.astype(bool))
        print('Saved GUI-readable hologram to: {}.npy'.format(saveFile))
    else: # g to be used in simulations and should have the same shape as the grid
        print('Interpolating the hologram to fit the grid... ({:e} sec.)'.format( time.time()-t0 ))
        g = RegularGridInterpolator(             # grating function is constant within
            (y_dmd, x_dmd), g, method='nearest', # each mirror so method='nearest'
            bounds_error=False, fill_value=0 ) # reflectivity is zero outside the DMD
        x, y = np.meshgrid(x, y)
        gridPoints = np.stack((y.ravel(), x.ravel()), axis=1)
        g = g(gridPoints).reshape(x.shape).astype(int) # interpolate and shape to the grid

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    return g

#########################
## ZERNIKE POLYNOMIALS ##
#########################

def zernike(n, m, x, y, noll=None, rho=None, phi=None, disk=False):
    ''' From "Direct and inverse discrete Zernike transform" by Navarro, Arines, Rivera (2009) '''
    if (n is not None) and (m is not None):
        if (n<0) or (abs(m)>n): raise ValueError

    if noll is not None:
        n = 0
        while (noll>n):
            n += 1
            noll -= n
        m = 2*noll - n

    if (rho is None) or (phi is None):
        rho = np.sqrt((x-x.mean())**2 + (y-y.mean())**2)
        rho *= np.sqrt(2)**disk / rho.max()
        phi = np.arctan2(y-y.mean(), x-x.mean())

    if (n-m)%2: return rho*0.0

    norm = np.sqrt( 2*(n+1) / (1 + (m==0)) )
    coeff = lambda k: (-1)**k * factorial(n-k) / ( factorial(k) * factorial((n+np.abs(m))/2 - k) * factorial((n-np.abs(m))/2 - k) )
    R = np.sum( coeff(k) * rho**(n-2*k) for k in range(int((n-np.abs(m))/2)+1) )
    if disk: R *= (rho<=1)
    if m>=0: return  norm*R*np.cos(m*phi)
    else:    return -norm*R*np.sin(m*phi)

################################################
## PROCESS CCD PICTURES AND SIMULATION ARRAYS ##
################################################

# # TODO add default position of 0th subdiffraction order
# def averagePics(saveDir, supergratingPitch, ctrl_scale=None, ctrl_length=None, x0=default, y0=default, nInterp=10):
#     '''
#     (x0, y0) = center point of the 0th subdiffraction order in units of CCD pixels
#     nInterp = number of grid points per CCD pixel
#     '''
#     supergratingPitch = np.sort(supergratingPitch)
#     # pics = []; for picFile in dir(os.path.join(saveDir,'*.png')): pics.append(imread(picFile))
#     pics = [ imread(p) for p in glob(os.path.join(saveDir,'*.png')).sort() ]
#     y_old = np.arange(0, pics[0].shape[0])
#     x_old = np.arange(0, pics[0].shape[1])
#     y_new = np.arange(0, nInterp*pics[0].shape[0])/nInterp
#     x_new = np.arange(0, nInterp*pics[0].shape[1])/nInterp
#     gridPoints = np.stack((y_new.ravel(), x.ravel()), axis=1)
#     for i in range(len(pics)):
#         p = RegularGridInterpolator(
#             (y_old, x_old), pics[i], method='nearest',
#             bounds_error=False, fill_value=0 )
#         p = p(gridPoints).reshape((y_new.size, x_new.size))
#         pics[i] = p
#     pics = CCDtoDMD(pics)
#     # computeOffsetControlIdx(grid_trap, supergratingPitch, supergratingAngle, ctrl_scale=None, ctrl_length=None)



# def scaleToImg(grid_trap, u, img, ccd=False, ROI=None, mask=None, order=1):
#     '''
#     Returns scaling factor s such that the field u is scaled to the image intensity in img:
#         s*|u|**2 ~ img
#     ccd=False then grid_trap is in absolute units (meters) and if ccd=True, it is in units of CCD pixels
#     ROI = [ymin, ymax, xmin, xmax] in the same units as grid_trap
#     '''
#     x, y = grid_trap
#     if x.ndim==1: x, y = np.meshgrid(x, y)
#     if not ccd:
#         x = x/ccdPixel
#         y = y/ccdPixel
#         if ROI is not None: ROI = (np.array(ROI)/ccdPixel).astype(int)
#
#     y_pic, x_pic = np.indices(img.shape)
#     y_pic = y_pic + y.mean() - y_pic.mean() # center grids with
#     x_pic = x_pic + x.mean() - x_pic.mean() # respect to one another
#     if ROI is not None:
#         idx = np.where( (ROI[0]<=y_pic) * (y_pic<=ROI[1]) * (ROI[2]<=x_pic)*(x_pic<=ROI[3]) )
#         y_pic, x_pic, img = y_pic[idx], x_pic[idx], img[idx]
#         idx = np.where( (ROI[0]<=y) * (y<=ROI[1]) * (ROI[2]<=x)*(x<=ROI[3]) )
#         y, x, u = y[idx], x[idx], u[idx]
#     pdb.set_trace()
#     img = RectBivariateSpline(y_pic[:,0], x_pic[0,:], img, kx=order, ky=order)(y[:,0], x[0,:], grid=True)
#
#     if mask is not None:
#         img = np.ma.masked_array(img, mask)
#         u = np.ma.masked_array(u, mask)
#
#     return np.sum( np.abs(u)**2*img ) / np.sum(np.abs(u)**4)

##############
## PLOTTING ##
##############

# def plotData(grid_dmd, grid_trap=None, a_dmd=None, a_targ=None, )

# TODO https://stackoverflow.com/questions/2553354/how-to-get-a-variable-name-as-a-string-in-python

# def plotPhase(x, y, phase, idx=None, title='Phase'):
#     pp.figure(figsize=(12.8, 6.54))
#     if idx is None:
#         pp.pcolormesh(x, y, phase/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
#     else:
#         pp.pcolormesh(x[idx], y[idx], phase[idx]/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
#     pp.title(title)
#     pp.colorbar(label='$\\phi/2\\pi$')
#     pp.axis('scaled')

# def plotError(x_trap, y_trap, error):
#     '''
#     Plots the output error of gs or omraf
#     '''
#     pp.figure()
#     pp.pcolormesh(error, cmap='bwr', norm=SymLogNorm(np.abs(error).mean()))
#     pp.title('Error')
#     pp.colorbar()
#     pp.axis('scaled')
