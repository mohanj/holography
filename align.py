import numpy as np
import matplotlib.pyplot as pp
from scipy.optimize import leastsq



def argmax(a):
    '''return index of maximum of an array with numbers between -shape/2 and shape/2.'''
    ind = np.unravel_index(np.argmax(a), a.shape) # ind[i] is an integer in [0, shape[i]-1]
    # mapping of value ind in [0, s-1] to [-s/2, s/2]
    ind = [int(((ind[i] + shape[i]/2) % shape[i]) - shape[i]/2) for i in xrange(len(shape))]
    return ind



def estimate_shift(fft_img1, fft_img2):
    '''estimate shift from image 1 to 2 with integer precision given their fft.'''

    # get the relative phase factor between fft_img1 and fft_img2
    # while normalizing the amplitude of each pixel to one
    g = np.conjugate(fft_img1) * fft_img2
    g = g / np.absolute(g)

    # inverse fourier transform
    # ideally this is a delta function centered on the shift
    g = np.fft.irfft2(g)

    # get position of its maximum
    return argmax(g)



def calculate_shift(img1, img2):
    '''calculate shift from image 1 to 2 with subpixel precision.
    precision achieved by a fit in fourier space.'''


    if img1.shape != img2.shape:
        raise ValueError('input arrays should have same shape')

    nx, ny = img1.shape


    # fourier transform images
    fft_img1 = np.fft.rfft2(img1)
    fft_img2 = np.fft.rfft2(img2)

    # frequencies in both dimensions in 1/px
    nu_x = np.fft.fftfreq(nx)
    nu_y = np.fft.rfftfreq(ny)

    nu_x, nu_y = np.meshgrid(nu_x, nu_y, indexing='ij')


    # error function for leastsq with displacement p = [dx, dy]
    def error_func(p):
        dx, dy = p

        print('p', p)

        residuals = np.exp(2j * np.pi * (nu_x * dx + nu_y * dx)) * fft_img2 - fft_img1
        return np.ravel(np.absolute(residuals))

    # initial guess for shift
    pest = estimate_shift(fft_img1, fft_img2)

    # fitting
    popt, pcov, infodict, mesg, ier = leastsq(error_func, pest, full_output=True)

    # see http://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.leastsq.html
    # mesg: a string message giving information about cause of failure.
    #
    # ier: an integer flag. if it is equal to 1, 2, 3 or 4, the solution
    # was found. Otherwise, the solution was not found. In either case, the
    # optional output variable 'mesg' gives more information.
    if ier not in [1, 2, 3, 4]:
        raise RuntimeError(mesg)

    return popt



def shift_image(img, dx, dy):
    '''shift image by the integer vector (dx, dy), while keeping the image size constant.
    the pixels created in the process are filled with zeros.
    '''

    dx, dy = -dx, -dy
    if dx > 0:
        # shift picture to the right by padding on the left
        img = np.pad(img, ((0,0), (dx,0)), mode='constant')[:,:-dx]
    else:
        # shift picture to the left by padding on the right
        img = np.pad(img, ((0,0), (0,-dx)), mode='constant')[:,-dx:]

    if dy > 0:
        # shift picture down by padding from top
        img = np.pad(img, ((dy,0), (0,0)), mode='constant')[:-dy,:]
    else:
        # shift picture up by padding from bottom
        img = np.pad(img, ((0,-dy), (0,0)), mode='constant')[-dy:,:]

    return img



def rescale_and_shift_image(img, dx, dy, scale):
    '''rescale image by factor scale by replacing every element
    with an scale*scale array containing that element value.
    then shift the image by (dx, dy).
    dx and dy are in units of the original picture and can be non-integer.
    '''

    # rescaling is easily done using a kronecker product
    rescaled_img = np.kron(img, np.ones((scale, scale)))

    # shift on the bigger image must be multiplied by scale
    new_dx, new_dy = int(round(scale*dx)), int(round(scale*dy))

    return shift_image(rescaled_img, new_dx, new_dy)
