import numpy as np
import matplotlib.pyplot as pp
import sys
sys.path.append('../') # put holography.py in path
import holography as h

scanGridShape = (384, 384)
x_dmd, y_dmd, x_trap, y_trap = h.computeFresnelGrid(scanGridShape)
a_dmd = np.exp( -(x_dmd**2+y_dmd**2)/(3.47e-3)**2 ) # DMD field has a exp(-2) waist of 3.47 mm
a_target = h.targetFromFile('eth.png', (x_trap,y_trap))
x_dmd, y_dmd, x_trap, y_trap, a_dmd, a_target = h.doubleFresnelGrid(x_dmd, y_dmd, a_dmd, a_target)
a_dmd = h.prepareDMD(x_dmd, y_dmd, a_dmd, scanGridShape)
a_target = h.prepareTarget(a_target)
# h.checkSampling(x_dmd, y_dmd, a_dmd, plot=True)
# h.checkSampling(x_trap, y_trap, a_target, plot=True)
kinoformGuess = h.parabolicPhaseGuess(x_dmd, y_dmd, a_dmd, a_target, x_trap=x_trap, y_trap=y_trap)
opticalSystem = h.Fresnel()
kinoform, u_trap, error, rmsError = h.omraf(a_dmd, a_target, kinoformGuess, opticalSystem, nIterations=1)
hologram = h.computeHologram(x_dmd, y_dmd, kinoform, scanGridShape=scanGridShape, checkRepresentation=True) # hologram output is a viable input to dmd.compilePicture
