import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm
import holography as h

############### BEGIN INPUTS ###############
# scanFile = 'save.p'
scanFile = 'save.npz' # getScanData
ccd = True # computeHelmholtzGrid
supergratingPitch = 6 # computeHologram
supergratingAngle = 0 # computeHologram
saveFigs = False
############### END INPUTS ###############

scanGridShape = h.getScanData(scanFile)
x, y = h.computeHelmholtzGrid(scanGridShape, ccd=ccd)
a_dmd, wavefront, x0, y0 = h.dmdFromScan(x, y, scanFile)
a_dmd, wavefront = h.prepareDMD(x, y, a_dmd, wavefront, scanGridShape)
opticalSystem = h.Fourier(x, y, h.focalLength, h.focalLength)

g1 = h.computeHologram(x, y, -wavefront, scanGridShape, supergratingPitch, supergratingAngle)
with np.load('/Volumes/JEFE/best_scan/data_correction.npz') as data:
    g2 = np.where(data.f.hologram==0, 1, 0).astype(int)
hologram_uncorrected = h.computeHologram(x, y, np.zeros_like(x), scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
hologram_corrected   = h.computeHologram(x, y, -wavefront,       scanGridShape, supergratingPitch, supergratingAngle, toDMD=False, checkRepresentation=True)
fineHologram         = h.computeFineHologram(x, y, -wavefront,     scanGridShape, supergratingPitch, supergratingAngle)

u_ccd_uncorrected = opticalSystem.forwardPropagate( hologram_uncorrected*a_dmd*np.exp(1j*wavefront) )
u_ccd_corrected   = opticalSystem.forwardPropagate( hologram_corrected  *a_dmd*np.exp(1j*wavefront) )
u_ccd_fine        = opticalSystem.forwardPropagate( fineHologram        *a_dmd*np.exp(1j*wavefront) )

idx = h.computeScanIdx(a_dmd)
wavefront /= 2*np.pi
a_dmd /= a_dmd.max()
u_ccd_uncorrected /= np.abs(u_ccd_uncorrected).max()
u_ccd_corrected /= np.abs(u_ccd_corrected).max()
u_ccd_fine /= np.abs(u_ccd_fine).max()
pic = h.DMDtoCCD(np.abs(u_ccd_corrected))**2

# pp.figure()
# pp.pcolormesh(x[idx]/h.dmdPixel, y[idx]/h.dmdPixel, a_dmd[idx], cmap='jet')
# pp.title('$|U_{DMD}|$ from scan')
# pp.colorbar()
# pp.axis('scaled')
#
# pp.figure()
# pp.pcolormesh(x[idx]/h.dmdPixel, y[idx]/h.dmdPixel, wavefront[idx], cmap='bwr', vmin=-np.abs(wavefront).max(), vmax=np.abs(wavefront).max() )
# pp.title('wavefront from scan')
# pp.colorbar()
# pp.axis('scaled')
#
# pp.figure()
# pp.pcolormesh(x/h.ccdPixel, y/h.ccdPixel, np.abs(u_ccd_uncorrected), cmap='jet' )
# pp.title('Uncorrected $|U_{CCD}|$')
# pp.axis('scaled')
# # pp.axis([-50, 200, -50, 50])

# pp.figure()
# pp.pcolormesh(x/h.ccdPixel, y/h.ccdPixel, np.abs(u_ccd_corrected), cmap='jet' )
# pp.title('Corrected $|U_{CCD}|$')
# pp.axis('scaled')
# # pp.axis([-50, 200, -50, 50])

# pp.matshow(pic, cmap='jet', norm=LogNorm() )
pp.matshow(pic, cmap='jet', vmax=0.1 )
pp.title('CCD picture')
pp.show()

# pp.figure()
# pp.pcolormesh(x/h.ccdPixel, y/h.ccdPixel, np.abs(u_ccd_fine), cmap='jet' )
# pp.title('Fine $|U_{CCD}|$')
# pp.axis('scaled')
# # pp.axis([-50, 200, -50, 50])

pp.figure()
pp.pcolormesh(x/h.dmdPixel, y/h.dmdPixel, hologram_corrected, cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( 'fresnel-omraf-fig{}.png'.format(n), bbox_inches='tight', dpi=300)
        pp.close(pp.gcf())
else:
    pp.show()
