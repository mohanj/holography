import numpy as np
import matplotlib.pyplot as pp
import sys
sys.path.append('../') # put holography.py in path
import holography as h

scanGridShape = (384, 384)
x, y = h.computeHelmholtzGrid(scanGridShape)
a_dmd = np.exp( -(x**2+y**2)/(3.47e-3)**2 ) # DMD field has a exp(-2) waist of 3.47 mm
a_target = h.targetFromFile('eth.png', (x,y))
x, y, (a_dmd, a_target) = h.doubleHelmholtzGrid(x, y, (a_dmd, a_target))
a_dmd = h.prepareDMD(x, y, a_dmd)
a_target = h.prepareTarget(a_target, doubled=True)
# h.checkSampling(x, y, a_dmd, plot=True)
# h.checkSampling(x, y, a_target, plot=True)
kinoformGuess = h.parabolicPhaseGuess(x, y, a_dmd, a_target)
opticalSystem = h.Fourier(x, y, h.focalLength, h.focalLength)
kinoform, u_trap, error, rmsError = h.omraf(a_dmd, a_target, kinoformGuess, opticalSystem, nIterations=10)
hologram = h.computeHologram(x, y, kinoform, scanGridShape=scanGridShape, checkRepresentation=True) # hologram output is a viable input to dmd.compilePicture
