import sys
sys.path.append('../') # put holography in path
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm
import holography as h

x, y = h.computeGrid(gridShape=(384,384))
a_dmd = np.exp( -(x**2+y**2)/(3.47e-3)**2 )
a_target = h.targetFromFile('eth.png', (x,y), scale=0.3)
# a_target = h.targetFromFile('curie.jpg', (x,y), scale=0.5)
x, y, nuX, nuY, a_dmd, a_target, _ = h.prepareInputs(x, y, a_dmd, a_target)
kinoformGuess = h.parabolicPhaseGuess(x, y, a_dmd, a_target)
opticsList = [ h.Fourier(h.focalLength, h.focalLength, x, y, nuX, nuY) ]

kinoform, u_trap, error, _ = h.omraf(a_dmd, a_target, kinoformGuess, opticsList, nIterations=2)
hologram = h.computeHologram(x, y, kinoform, toDMD=False, gridShape=(768,768))
# hologram_dmd = h.computeHologram(x, y, kinoform)
u_ccd = h.forwardPropagate( hologram*a_dmd, opticsList )

# f1 = h.ft( np.exp(1j*kinoform) )
# f2 = h.ft( hologram )
# f1 = np.cos(kinoform)
# f2 = hologram
# pp.figure(); pp.pcolormesh(np.abs(f1), norm=LogNorm())
# pp.figure(); pp.pcolormesh(np.abs(f2), norm=LogNorm())
# pp.show()

pp.figure()
pp.pcolormesh(x, y, kinoform/(2*np.pi))
pp.colorbar()
pp.axis('scaled')
pp.title('kinoform')

pp.figure()
pp.pcolormesh(x, y, np.abs(u_trap))
pp.axis('scaled')
pp.title('|u_trap|')

pp.figure()
pp.pcolormesh(x, y, np.abs(u_ccd), norm=LogNorm())
# pp.pcolormesh(x, y, np.abs(u_ccd))
pp.axis('scaled')
pp.title('|u_ccd|')

pp.show()
