'''
Command for submitting the job on Euler (https://scicomp.ethz.ch/lsf_submission_line_advisor/):
bsub -n 4 -W 12:00 -N -B -u gwajusman@gmail.com -J "fresnel-omraf-aberrations" -R "rusage[mem=1024]" -oo fresnel-omraf-aberrations.out 'python3 fresnel-omraf-aberrations.py'
'''

import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm, SymLogNorm
import holography as h

############### BEGIN INPUTS ###############
imgFile            = 'curie.jpg' # targetFromFile
waist_dmd          = np.inf
scanGridShape      = (768, 1024) # Fresnel
nyquistParameter   = 3 # Fresnel
padFactor          = 0 # Fresnel
ccd                = True # Fresnel
scale              = None # 0.1 # targetFromFile
length             = 500e-6 # targetFromFile (overrides scale if not None)
angle              = 0 # targetFromFile
offset             = 0 # prepareTarget
ctrl_scale_offset  = None # (1, 1) # prepareTarget
ctrl_length_offset = (length*1.05,)*2 # prepareTarget (overrides scale if not None)
ctrl_scale         = None # (1, 1) # omraf
ctrl_length        = ctrl_length_offset # (np.inf, np.inf) # ctrl_length_offset # (np.inf, 250e-5) # omraf (overrides scale if not None)
nIterations        = 30 # omraf
mixingParameter    = 0.4 # omraf
supergratingPitch  = 6 # 1.1*h.waveLength*h.focalLength/length/h.dmdPixel # computeHologrm
supergratingAngle  = 0 # computeHologrm
saveFigs           = True
saveFile           = imgFile.split('.')[0]
dataFile           = '' # saveFile+'-data.npz'
############### END INPUTS ###############

############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
o = h.Fresnel(scanGridShape, nyquistParameter, padFactor, ccd)
(x_dmd, y_dmd), (x_trap, y_trap) = o.grid_dmd, o.grid_trap
a_dmd = np.exp( -(x_dmd**2 + y_dmd**2)/waist_dmd**2 )
aspectRatio = h.targetFromFile(imgFile).shape
aspectRatio = aspectRatio[0]/aspectRatio[1]
a_target = h.targetFromFile(imgFile, o.grid_trap, scale, length, angle)

a_dmd = h.prepareDMD(o.grid_dmd, scanGridShape, a_dmd)
a_target = h.prepareTarget(o.grid_trap, a_target, ctrl_scale_offset, ctrl_length_offset, offset)
kinoform = h.parabolicPhaseGuess(o.grid_dmd, a_dmd, a_target, o.grid_trap)

if len(dataFile)>0: # avoid redoing omraf if the data already exists
    with np.load(dataFile) as data:
        kinoform = data['kinoform']
        u_trap   = data['u_trap']
        rmsError = data['rmsError']
        try: error = data['error']
        except: error = h.computeError(u_trap, a_target, np.isclose(a_target, 0))
else:
    kinoform, u_trap, error, rmsError = h.omraf(a_dmd, a_target, kinoform, o, ctrl_scale, ctrl_length, nIterations, saveFile+'-data', mixingParameter=mixingParameter)

g = h.computeHologram(o.grid_dmd, kinoform, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
u_ccd = o.forwardPropagate( g*a_dmd )
scan = h.computeScanIdx(o.grid_dmd, scanGridShape)

norm = a_target.max()
a_target /= norm
u_trap /= norm
u_ccd /= norm
x_trap, y_trap = x_trap/h.ccdPixel, y_trap/h.ccdPixel
x_dmd, y_dmd = x_dmd/h.dmdPixel, y_dmd/h.dmdPixel
length /= h.ccdPixel
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############

############### BEGIN PLOTS ###############
print('Plotting kinoform')
pp.figure(figsize=(10,10))
pp.pcolormesh(x_dmd[scan], y_dmd[scan], (kinoform[scan])/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
pp.title('$\\phi_{DMD}$')
pp.axis('scaled')

print('Plotting trap field')
pp.figure(figsize=(10,10))
pp.pcolormesh(x_trap, y_trap, np.abs(u_trap), cmap='gray', vmin=0, vmax=1)
pp.title('$|U_{trap}|$')
pp.axis('scaled')
Dx, Dy = length/2*min(1,1/aspectRatio), length/2*min(1,aspectRatio)
pp.axis([ -Dx, Dx, -Dy, Dy ])
pp.tight_layout()

print('Plotting discretized trap field')
pp.figure(figsize=(10,10))
pp.pcolormesh(x_trap, y_trap, np.abs(u_ccd), cmap='gray', vmin=0, vmax=1)
# pp.pcolormesh(x_trap, y_trap, np.abs(u_ccd), cmap='gray', norm=LogNorm())
pp.title('Discretized $|U_{trap}|$')
pp.axis('scaled')
Dr = h.waveLength*h.focalLength/(supergratingPitch*h.dmdPixel)/h.ccdPixel
Dx, Dy = length/2*min(1,1/aspectRatio), length/2*min(1,aspectRatio)
pp.axis([ Dr*np.cos(np.deg2rad(supergratingAngle)) - Dx,
          Dr*np.cos(np.deg2rad(supergratingAngle)) + Dx,
          Dr*np.sin(np.deg2rad(supergratingAngle)) - Dy,
          Dr*np.sin(np.deg2rad(supergratingAngle)) + Dy ])
pp.tight_layout()

print('Plotting hologram')
pp.figure(figsize=(10,10))
pp.pcolormesh( x_dmd[scan], y_dmd[scan], g[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')
pp.tight_layout()

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=400)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############
