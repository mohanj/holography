'''
Command for submitting the job on Euler (https://scicomp.ethz.ch/lsf_submission_line_advisor/):
bsub -n 4 -W 12:00 -N -B -u gwajusman@gmail.com -J "helmholtz-omraf-aberrations" -R "rusage[mem=1024]" -oo helmholtz-omraf-aberrations.out 'python3 helmholtz-omraf-aberrations.py'
'''

import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm
import holography as h
from colormaps import greiner

############### BEGIN INPUTS ###############
scanFile           = '../save.npz' # getScanData
dmdLensDistance    = 140e-3 # Fourier
lensTrapDistance   = h.focalLength # Fourier
clearAperture      = 2.54e-2 # Fourier
nyquistParameter   = 2 # Fourier
padFactor          = 0 # Fourier
ccd                = True # Fourier
scale              = None # 0.1 # targetFromFile
length             = 1.5e-3 # targetFromFile (overrides scale if not None)
angle              = -90 # targetFromFile
offset             = 0 # prepareTarget
ctrl_scale_offset  = None # (1, 1) # prepareTarget
buf                = length*0.05
ctrl_length_offset = (length+buf, 97/589*length+buf) # prepareTarget
ctrl_scale         = None # (1, 1) # omraf
ctrl_length        = ctrl_length_offset # (np.inf, np.inf) # ctrl_length_offset # (np.inf, 250e-5) # omraf (overrides scale if not None)
nIterations        = 10 # omraf
mixingParameter    = 0.4 # omraf
supergratingPitch  = 10 # computeHologrm
supergratingAngle  = 0 # computeHologrm
saveFigs           = False
saveFile           = 'helmholtz-omraf-aberrations'
dataFile           = '' # saveFile+'-data.npz'
figsize            = (6.4, 4.8) # (12.8, 6.54)
############### END INPUTS ###############

############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
scanGridShape , x0, y0 = h.getScanData(scanFile)
o = h.Fourier(scanGridShape, dmdLensDistance, lensTrapDistance, clearAperture, nyquistParameter, padFactor, ccd)
x, y = o.grid
a_dmd, wavefront = h.DMDfromScan(o.grid, scanFile)
a_target = h.targetFromFile('eth.png', o.grid, scale, length, angle)

a_dmd, wavefront = h.prepareDMD(o.grid, scanGridShape, a_dmd, wavefront)
a_target = h.prepareTarget(o.grid, a_target, ctrl_scale_offset, ctrl_length_offset, offset)
kinoform = h.parabolicPhaseGuess(o.grid, a_dmd, a_target)

if len(dataFile)>0: # avoid redoing omraf if the data already exists
    with np.load(dataFile) as data:
        kinoform = data['kinoform']
        u_trap   = data['u_trap']
        fidelity = data['fidelity']
        error    = data['error']
else:
    kinoform, u_trap, error, fidelity = h.omraf(a_dmd, a_target, kinoform, o, ctrl_scale, ctrl_length, nIterations, saveFile+'-data', mixingParameter=mixingParameter)

# h.computeHologram(o.grid, kinoform, scanGridShape, supergratingPitch, supergratingAngle, x0=x0, y0=y0, checkRepresentation=True, saveFile=saveFile+'-holo-uncorr')
# h.computeHologram(o.grid, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle, x0=x0, y0=y0, saveFile=saveFile+'-holo-corr')
h.computeHologram(o.grid, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle, x0=x0, y0=y0, saveFile='eth-'+saveFile)
# g_uncorr = h.computeHologram(o.grid, kinoform, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
g_corr = h.computeHologram(o.grid, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
bk = h.binKinoform(o.grid, kinoform-wavefront, scanGridShape)
# u_ccd_uncorr = o.forwardPropagate( g_uncorr*a_dmd*np.exp(1j*wavefront) )
u_ccd_corr = o.forwardPropagate( g_corr*a_dmd*np.exp(1j*wavefront) )
u_lens = h.ift( o.H1*h.ft(g_corr*a_dmd*np.exp(1j*wavefront)) )
u_bin = o.forwardPropagate( np.exp(1j*bk)*a_dmd*np.exp(1j*wavefront) )
scan = h.computeScanIdx(o.grid, scanGridShape)
ctrl = h.computeControlIdx(o.grid, None, ctrl_length)
octrl = h.computeOffsetControlIdx(o.grid, supergratingPitch, supergratingAngle, None, ctrl_length)
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############



############### BEGIN PLOTS ###############
# print('Plotting DMD field')
# pp.figure(figsize=figsize)
# pp.pcolormesh(x[scan]/h.dmdPixel, y[scan]/h.dmdPixel, a_dmd[scan]**2 )
# pp.title('$|U_{DMD}|^2$')
# pp.axis('scaled')
# pp.tight_layout()

print('Plotting kinoform-wavefront')
# pp.figure(figsize=figsize)
pp.figure(figsize=(6.5,6.5))
pp.pcolormesh(x[scan]/h.dmdPixel, y[scan]/h.dmdPixel, (kinoform[scan]-wavefront[scan])/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
pp.title('$\\phi_{DMD}-W_{DMD}$')
pp.axis('scaled')
pp.tight_layout()

print('Plotting binned kinoform-wavefront')
# pp.figure(figsize=figsize)
pp.figure(figsize=(6.5,6.5))
pp.pcolormesh(x[scan]/h.dmdPixel, y[scan]/h.dmdPixel, bk[scan]/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
pp.title('Binned $\\phi_{DMD}-W_{DMD}$')
pp.axis('scaled')
pp.tight_layout()

# print('Plotting trap phase')
# pp.figure()
# pp.pcolormesh(y[ctrl].T*1e6, x[ctrl].T*1e6, np.angle(u_trap[ctrl].T)/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
# pp.title('$\\phi_{trap}$')
# pp.axis('scaled')
# pp.tight_layout()

# print('Plotting target field')
# pp.figure()
# pp.pcolormesh(y[ctrl].T*1e6, x[ctrl].T*1e6, np.fliplr(a_target[ctrl].T)**2, cmap='gray')
# pp.title('$|A_{targ}|^2$')
# pp.axis('scaled')

# print('Plotting lens field')
# pp.figure(figsize=figsize)
# pp.pcolormesh(x*1e6, y*1e6, np.abs(u_lens)**2, cmap=greiner)
# pp.title('$|U_{lens}|^2$')
# pp.axis('scaled')
# pp.tight_layout()

print('Plotting trap field')
pp.figure(figsize=figsize)
pp.pcolormesh(y[ctrl].T*1e6, x[ctrl].T*1e6, np.abs(np.fliplr(u_trap[ctrl].T))**2, cmap='gray', vmin=0)
pp.title('$|U_{trap}|^2$')
pp.axis('scaled')
pp.tight_layout()

print('Plotting binned field')
pp.figure(figsize=figsize)
# pp.pcolormesh((y[octrl].T-y[octrl].mean())*1e6, (x[octrl].T-x[octrl].mean())*1e6, np.abs(np.fliplr(u_bin[ctrl].T))**2, cmap='gray', vmin=0)
pp.pcolormesh(y[ctrl].T*1e6, x[ctrl].T*1e6, np.abs(np.fliplr(u_bin[ctrl].T))**2, cmap='gray', vmin=0)
pp.title('$|U_{bin}|^2$')
pp.axis('scaled')
pp.tight_layout()

print('Plotting discretized field')
pp.figure(figsize=figsize)
pp.pcolormesh((y[octrl].T-y[octrl].mean())*1e6, (x[octrl].T-x[octrl].mean())*1e6, np.abs(np.fliplr(u_ccd_corr[octrl].T))**2, cmap='gray', vmin=0)
pp.title('$|U_{CCD}|^2$')
pp.axis('scaled')
pp.tight_layout()

print('Plotting error')
pp.figure(figsize=figsize)
pp.pcolormesh(y[ctrl].T*1e6, x[ctrl].T*1e6, np.fliplr(error.T), cmap='bwr', vmin=-np.abs(error).max(), vmax=np.abs(error).max() )
pp.title('Error')
pp.colorbar()
pp.axis('scaled')
pp.tight_layout()

print('Plotting hologram')
pp.figure(figsize=figsize)
pp.pcolormesh( x[scan]/h.dmdPixel, y[scan]/h.dmdPixel, g_corr[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')
pp.tight_layout()

# print('Plotting simulated CCD picture')
# pic = h.DMDtoCCD(o.grid, np.abs(u_ccd_corr))
# pp.figure()
# pp.pcolormesh(x/h.ccdPixel, y/h.ccdPixel, pic, vmax=1, cmap='gray')
# pp.title('Simulated corrected CCD picture')
# pp.axis('scaled')
# # pp.axis([-300, 300, -300, 300])
# pp.gca().invert_yaxis()

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=400)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############
