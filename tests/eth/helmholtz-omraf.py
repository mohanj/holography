# -*- coding: utf-8 -*-
'''
bsub -n 4 -W 4:00 -N -B -u gwajusman@gmail.com -J "sawtooth-omraf" -R "rusage[mem=2048]" -oo sawtooth-omraf.out 'python3 sawtooth-omraf.py'
'''

import sys
sys.path.append('../../holography/')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm
import holography as h
from colormaps import greiner

############### BEGIN INPUTS ###############
scanGridShape      = (600, 600)
waist_dmd          = 4e-3
dmdLensDistance    = 140e-3 # Fourier
lensTrapDistance   = h.focalLength # Fourier
clearAperture      = 2.54e-2 # Fourier
nyquistParameter   = 2 # Fourier
padFactor          = 0 # Fourier
ccd                = True # Fourier
scale              = None # 0.1 # targetFromFile
length             = 3e-3 # targetFromFile (overrides scale if not None)
angle              = 0 # targetFromFile
offset             = 0 # prepareTarget
ctrl_scale_offset  = None # (1, 1) # prepareTarget
buf                = length*0.1
ctrl_length_offset = (97/589*length+buf, length+buf) # prepareTarget
ctrl_scale         = None # (1, 1) # omraf
ctrl_length        = ctrl_length_offset # (np.inf, np.inf) # (np.inf, 250e-5) # omraf
crop_length        = (ctrl_length_offset[0]*3, ctrl_length_offset[1]*2)
nIterations        = 20 # omraf
mixingParameter    = 0.4 # omraf
supergratingPitch  = 10 # computeHologrm
supergratingAngle  = -90 # computeHologrm
saveFigs           = False
saveFile           = 'helmholtz-omraf'
dataFile           = '' # saveFile+'-data.npz'
figsize            = (6.4, 4.8) # (12.8, 6.54)
############### END INPUTS ###############

############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
o = h.Fourier(scanGridShape, dmdLensDistance, lensTrapDistance, clearAperture, nyquistParameter, padFactor, ccd)
x, y = o.grid
a_dmd = np.exp( -( x**2 + y**2 )/waist_dmd**2 )
a_target = h.targetFromFile('eth.png', o.grid, scale, length, angle)

a_dmd = h.prepareDMD(o.grid, scanGridShape, a_dmd)
a_target = h.prepareTarget(o.grid, a_target, ctrl_scale_offset, ctrl_length_offset, offset)
kinoform = h.parabolicPhaseGuess(o.grid, a_dmd, a_target)

if len(dataFile)>0: #
    kinoform = data['kinoform']
    u_trap   = data['u_trap']
    fidelity = data['fidelity']
    error    = data['error']
else:
    kinoform, u_trap, error, fidelity = h.omraf(a_dmd, a_target, kinoform, o, ctrl_scale, ctrl_length, nIterations, saveFile+'-data', mixingParameter=mixingParameter)
g = h.computeHologram(o.grid, kinoform, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
u_ccd = o.forwardPropagate( g*a_dmd )
scan = h.computeScanIdx(o.grid, scanGridShape)
ctrl = h.computeControlIdx(o.grid, None, ctrl_length)
crop = h.computeControlIdx(o.grid, None, crop_length)
octrl = h.computeOffsetControlIdx(o.grid, supergratingPitch, supergratingAngle, None, ctrl_length)
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############

############### BEGIN PLOTS ###############
# print('Plotting kinoform')
# pp.figure(figsize=figsize)
# pp.pcolormesh(x[scan]/h.dmdPixel, y[scan]/h.dmdPixel, kinoform[scan]/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
# pp.title('$\\phi_{DMD}$')
# pp.axis('scaled')
# pp.tight_layout()

# print('Plotting target field')
# pp.figure(figsize=(8.6,7))
# pp.pcolormesh(x*1e6, y*1e6, a_target**2, cmap='gray')
# pp.title('$A_{targ}|^2$')
# pp.axis('scaled')
# pp.tight_layout()

print('Plotting trap field')
pp.figure(figsize=figsize)
# pp.pcolormesh(x[ctrl]*1e6, y[ctrl]*1e6, np.abs(u_trap[ctrl])**2, cmap='gray', vmin=0)
# pp.pcolormesh(x[crop]*1e6, y[crop]*1e6, np.abs(u_trap[crop])**2, cmap='gray', vmin=0)
pp.pcolormesh(x[crop]*1e6, y[crop]*1e6, np.abs(u_trap[crop])**2, cmap=greiner, vmin=0)
pp.pcolormesh(x[crop]*1e6, y[crop]*1e6, a_target[crop]**2, cmap=greiner, vmin=0)
# pp.pcolormesh(x*1e6, y*1e6, np.abs(u_trap)**2, cmap='gray', vmin=0)
pp.title('$|U_{trap}|^2$')
pp.xlabel('x (µm)')
pp.xlabel('y (µm)')
pp.axis('scaled')
pp.tight_layout()

print('Plotting discretized field')
pp.figure(figsize=figsize)
pp.pcolormesh(x[octrl]*1e6, y[octrl]*1e6, np.abs(u_ccd[octrl])**2, cmap='gray', vmin=0)
# pp.pcolormesh(x*1e6, y*1e6, np.abs(u_ccd)**2, cmap=greiner, norm=LogNorm())
pp.title('$|U_{CCD}|^2$')
pp.axis('scaled')
pp.tight_layout()

print('Plotting error')
pp.figure(figsize=figsize)
pp.pcolormesh(x[ctrl]*1e6, y[ctrl]*1e6, error, cmap='bwr', vmin=-np.abs(error).max(), vmax=np.abs(error).max())
pp.title('Error')
pp.colorbar()
pp.axis('scaled')
pp.tight_layout()

print('Plotting hologram')
pp.figure(figsize=figsize)
pp.pcolormesh( x[scan]/h.dmdPixel, y[scan]/h.dmdPixel, g[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')
pp.tight_layout()

# try: # plot the picture from the CCD if it exists
#     pic = np.flipud(h.imread('sawtooth-omraf-pic-raw.png'))
#     pp.figure(figsize=(11,7))
#     pp.pcolormesh(pic, cmap='jet', norm=LogNorm())
#     pp.title('CCD Picture')
#     pp.colorbar()
#     pp.axis('scaled')
# except:
#     pass

if saveFigs:
    for n in pp.get_fignums():
        # pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=400)
        pp.savefig( 'fig{}.png'.format(n), bbox_inches='tight', dpi=400)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############
