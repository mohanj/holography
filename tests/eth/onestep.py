import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm, SymLogNorm
import holography as h
import imp; imp.reload(h)

############### BEGIN INPUTS ###############
# scanGridShape     = h.dmdShape
scanGridShape     = (485, 485)
waist_dmd         = 3e-3
nyquistParameter  = 4 # Fresnel
padFactor         = 0 # Fresnel
ccd               = True # Fresnel
length            = 0.5e-3 # targetFromFile
angle             = 0 # targetFromFile
buf               = length*0.05
ctrl_length       = (97/589*length+buf, length+buf) # for plotting
supergratingPitch = 10 # computeHologrm
supergratingAngle = 90 # computeHologrm
saveFigs          = False
saveFile          = 'onestep'
dataFile          = '' # saveFile+'-data.npz'
figsize           = (6.4, 4.8) # (12.8, 6.54)
############### END INPUTS ###############



############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
o = h.Fresnel(scanGridShape, nyquistParameter, padFactor, ccd)
(x_dmd, y_dmd), (x_trap, y_trap) = o.grid_dmd, o.grid_trap
a_dmd = np.exp( -( x_dmd**2 + y_dmd**2 )/waist_dmd**2 )
a_target = h.targetFromFile('eth.png', o.grid_trap, None, length, angle)

a_dmd = h.prepareDMD(o.grid_dmd, scanGridShape, a_dmd)
a_target = h.prepareTarget(o.grid_trap, a_target, None, None, 0)
a_dmd_target, kinoform = h.onestep(a_target, o)

# u_trap = opticalSystem.forwardPropagate( a_dmd*np.exp(1j*kinoform) )
# error = h.computeError(u_trap, a_target, np.isclose(a_target,0))

g = h.computeAmplitudePhaseHologram(o.grid_dmd, kinoform, a_dmd, a_dmd_target, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
u_ccd = o.forwardPropagate( g*a_dmd )

scan = h.computeScanIdx(o.grid_dmd, scanGridShape)
ctrl = h.computeControlIdx(o.grid_trap, None, ctrl_length)
octrl = h.computeOffsetControlIdx(o.grid_trap, supergratingPitch, supergratingAngle, None, ctrl_length)
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############



############### BEGIN PLOTS ###############
# print('Plotting kinoform-wavefront')
# pp.figure(figsize=figsize)
# pp.pcolormesh(x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, (kinoform[scan]-wavefront[scan])/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
# pp.title('$\\phi_{DMD}$')
# pp.axis('scaled')

# print('Plotting target field')
# pp.figure(figsize=figsize)
# ax1=pp.subplot(211); pp.pcolormesh(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, a_target[ctrl].T)
# ax2=pp.subplot(212); pp.contourf(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, a_target[ctrl].T, 20)
# ax1.set_title('$A_{target}$')
# ax1.axis('scaled')
# ax2.axis('scaled')

# print('Plotting trap field')
# pp.figure(figsize=figsize)
# ax1=pp.subplot(211); pp.pcolormesh(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, np.abs(u_trap[ctrl].T))
# ax2=pp.subplot(212); pp.contourf(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, np.abs(u_trap[ctrl].T), 20)
# ax1.set_title('$|U_{trap}|$')
# ax1.axis('scaled')
# ax2.axis('scaled')

print('Plotting simulated CCD amplitude')
pp.figure(figsize=figsize)
pp.pcolormesh(x_trap[octrl]*1e6, y_trap[octrl]*1e6, np.abs(u_ccd[octrl])**2, cmap='gray')
pp.title('Simulated $|U_{CCD}|^2$')
pp.axis('scaled')

print('Plotting hologram')
pp.figure(figsize=figsize)
pp.pcolormesh( x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, g[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')


if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=400)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############
