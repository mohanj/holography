'''
Command for submitting the job on Euler (https://scicomp.ethz.ch/lsf_submission_line_advisor/):
bsub -n 4 -W 12:00 -N -B -u gwajusman@gmail.com -J "fresnel-omraf-aberrations" -R "rusage[mem=1024]" -oo fresnel-omraf-aberrations.out 'python3 fresnel-omraf-aberrations.py'
'''

import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm, SymLogNorm
import holography as h

############### BEGIN INPUTS ###############
scanFile           = 'save.npz' # getScanData
nyquistParameter   = 2 # Fresnel
padFactor          = 0 # Fresnel
ccd                = True # Fresnel
scale              = None # 0.1 # targetFromFile
length             = 1e-3 # targetFromFile (overrides scale if not None)
angle              = -90 # targetFromFile
offset             = 0 # prepareTarget
buf                = length*0.05
ctrl_length_offset = (length+buf, 97/589*length+buf) # prepareTarget
ctrl_length        = ctrl_length_offset # (np.inf, np.inf) # ctrl_length_offset # (np.inf, 250e-5) # omraf (overrides scale if not None)
nIterations        = 100 # omraf
mixingParameter    = 0.4 # omraf
supergratingPitch  = 6 # computeHologrm, computeFineHologram
supergratingAngle  = 0 # computeHologrm, computeFineHologram
saveFigs           = False
saveFile           = 'fresnel-omraf-aberrations'
dataFile           = '' # saveFile+'-data.npz'
figsize            = (6.4, 4.8) # (12.8, 6.54)
############### END INPUTS ###############



############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
scanGridShape, x0, y0 = h.getScanData(scanFile)
o = h.Fresnel(scanGridShape, nyquistParameter, padFactor, ccd)
(x_dmd, y_dmd), (x_trap, y_trap) = o.grid_dmd, o.grid_trap
a_dmd, wavefront = h.DMDfromScan(o.grid_dmd, scanFile)
a_target = h.targetFromFile('eth.png', o.grid_trap, scale, length, angle)

a_dmd, wavefront = h.prepareDMD(o.grid_dmd, scanGridShape, a_dmd, wavefront)
a_target = h.prepareTarget(o.grid_trap, a_target, None, ctrl_length_offset, offset)
kinoform = h.parabolicPhaseGuess(o.grid_dmd, a_dmd, a_target, o.grid_trap)

if len(dataFile)>0: # avoid redoing omraf if the data already exists
    with np.load(dataFile) as data:
        kinoform = data['kinoform']
        u_trap   = data['u_trap']
        rmsError = data['rmsError']
        error = data['error']
else:
    kinoform, u_trap, error, fidelity = h.omraf(a_dmd, a_target, kinoform, o, None, ctrl_length, nIterations, saveFile+'-data', mixingParameter=mixingParameter)

# h.computeHologram(o.grid_dmd, kinoform, scanGridShape, supergratingPitch, supergratingAngle, x0=x0, y0=y0, checkRepresentation=True, saveFile=saveFile+'-holo-uncorr')
h.computeHologram(o.grid_dmd, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle, x0=x0, y0=y0, saveFile='eth-'+saveFile)
# g_uncorr = h.computeHologram(o.grid_dmd, kinoform, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
g_corr = h.computeHologram(o.grid_dmd, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
# u_ccd_uncorr = o.forwardPropagate( g_uncorr*a_dmd*np.exp(1j*wavefront) )
u_ccd_corr = o.forwardPropagate( g_corr*a_dmd*np.exp(1j*wavefront) )
# g_fine = h.computeFineHologram(x_dmd, y_dmd, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle)
# u_ccd_fine = o.forwardPropagate( g_fine*a_dmd*np.exp(1j*wavefront) )
scan = h.computeScanIdx(o.grid_dmd, scanGridShape)
ctrl = h.computeControlIdx(o.grid_trap, None, ctrl_length)
octrl = h.computeOffsetControlIdx(o.grid_trap, supergratingPitch, supergratingAngle, None, ctrl_length)
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############



############### BEGIN PLOTS ###############
print('Plotting kinoform-wavefront')
pp.figure(figsize=figsize)
pp.pcolormesh(x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, (kinoform[scan]-wavefront[scan])/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
pp.title('$\\phi_{DMD}-W_{DMD}$')
pp.axis('scaled')
pp.tight_layout()

# print('Plotting trap phase')
# pp.figure()
# pp.pcolormesh(x_trap/h.dmdPixel, y_trap/h.dmdPixel, np.angle(u_trap)/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
# pp.title('$\\phi_{trap}$')
# pp.axis('scaled')
# try: pp.axis([ -ctrl_length[1]/h.ccdPixel, ctrl_length[1]/h.ccdPixel, -ctrl_length[0]/h.ccdPixel, ctrl_length[0]/h.ccdPixel ])
# except: pass

# print('Plotting target field')
# pp.figure()
# pp.pcolormesh(x_trap/h.ccdPixel, y_trap/h.ccdPixel, a_target, cmap='gray', vmin=0, vmax=1)
# pp.title('$A_{target}$')
# pp.axis('scaled')
# try: pp.axis([ -ctrl_length[1]/h.ccdPixel, ctrl_length[1]/h.ccdPixel, -ctrl_length[0]/h.ccdPixel, ctrl_length[0]/h.ccdPixel ])
# except:
#     try: pp.axis(length/h.ccdPixel*np.array([ -1, 1, -0.2, 0.2 ]))
#     except: pass

print('Plotting trap field')
pp.figure(figsize=figsize)
pp.pcolormesh(x_trap[ctrl]*1e6, y_trap[ctrl]*1e6, np.abs(u_trap[ctrl])**2, cmap='gray')
pp.title('$|U_{trap}|^2$')
pp.axis('scaled')
pp.tight_layout()

print('Plotting simulated CCD field')
pp.figure(figsize=figsize)
pp.pcolormesh((x_trap[octrl]-x_trap[octrl].mean())*1e6, (y_trap[octrl]-y_trap[octrl].mean())*1e6, np.abs(u_ccd_corr[octrl])**2, cmap='gray')
pp.title('Simulated $|U_{CCD}|^2$')
pp.axis('scaled')
pp.tight_layout()

print('Plotting error')
pp.figure(figsize=figsize)
pp.pcolormesh(x_trap[ctrl]*1e6, y_trap[ctrl]*1e6, error, cmap='bwr', vmin=-np.abs(error).max(), vmax=np.abs(error).max() )
pp.title('Error')
pp.colorbar()
pp.axis('scaled')
pp.tight_layout()

print('Plotting hologram')
pp.figure(figsize=figsize)
pp.pcolormesh( x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, g_corr[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')
pp.tight_layout()

# print('Plotting simulated CCD picture')
# pic = h.DMDtoCCD(o.grid_trap, np.abs(u_ccd_corr))
# pp.figure(figsize=(12.8,6.54))
# pp.pcolormesh(x_trap/h.ccdPixel, y_trap/h.ccdPixel, pic, vmax=1, cmap='gray')
# pp.title('Simulated corrected CCD picture')
# pp.axis('scaled')
# pp.axis([-450, 450, -450, 450])
# pp.gca().invert_yaxis()

# try: # plot the picture from the CCD if it exists
#     pic = h.imread('fresnel-omraf-aberrations-pic-corr.png')
#     pp.figure()
#     pp.imshow(pic, cmap='gray', interpolation='none')
#     pp.title('CCD picture')
# except:
#     pass

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=400)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############



############### BEGIN SUBPLOTS ###############
# pp.figure(figsize=(7,7))
#
# pp.subplot(221)
# pic = h.DMDtoCCD(o.grid_trap, np.abs(u_ccd_corr))
# pp.pcolormesh(x_trap/h.ccdPixel, y_trap/h.ccdPixel, pic, vmax=0.4, cmap='gray')
# pp.title('Simulated $|U_{CCD}|$ (corrected)')
# pp.axis('scaled')
# pp.axis([-400, 400, -400, 400])
# pp.gca().invert_yaxis()
#
# pp.subplot(222)
# pic = h.DMDtoCCD(o.grid_trap, np.abs(u_ccd_uncorr))
# pp.pcolormesh(x_trap/h.ccdPixel, y_trap/h.ccdPixel, pic, vmax=0.4, cmap='gray')
# pp.title('Simulated $|U_{CCD}|$ (uncorrected)')
# pp.axis('scaled')
# pp.axis([-400, 400, -400, 400])
# pp.gca().invert_yaxis()
#
# pp.subplot(223)
# pic = np.sqrt(h.imread(saveFile+'-pic-corr.png').astype(float))
# pp.pcolormesh(pic, cmap='gray')
# pp.title('$|U_{CCD}|$ (corrected)')
# pp.axis('scaled')
# pp.axis([200, 1000, 0, 800])
# pp.gca().invert_yaxis()
#
# pp.subplot(224)
# pic = np.sqrt(h.imread(saveFile+'-pic-uncorr.png').astype(float))
# pp.pcolormesh(pic, cmap='gray')
# pp.title('$|U_{CCD}|$ (uncorrected)')
# pp.axis('scaled')
# pp.axis([200, 1000, 0, 800])
# pp.gca().invert_yaxis()
#
# pp.tight_layout()
# pp.savefig(saveFile+'-pics.png', bbox_inches='tight', dpi=500)
# pp.show()
############### END SUBPLOTS ###############
