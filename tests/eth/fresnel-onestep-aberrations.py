import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm, SymLogNorm
import holography as h

############### BEGIN INPUTS ###############
scanFile           = 'save.npz' # getScanData
nyquistParameter   = 2 # Fresnel
padFactor          = 0 # Fresnel
ccd                = True # Fresnel
scale              = None # 0.1 # targetFromFile
length             = 200e-5 # targetFromFile (overrides scale if not None)
angle              = -90 # targetFromFile
offset             = 0.0 # prepareTarget
ctrl_scale_offset  = None #(0.05, 0.15) # prepareTarget
ctrl_length_offset = (250e-5, 50e-5) # prepareTarget (overrides scale if not None)
supergratingPitch  = 6 # computeHologrm, computeFineHologram
supergratingAngle  = 0 # computeHologrm, computeFineHologram
saveFigs           = False
saveFile           = 'fresnel-onestep-aberrations'
############### END INPUTS ###############

############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
scanGridShape = h.getScanData(scanFile)
o = h.Fresnel(scanGridShape, nyquistParameter, padFactor, ccd)
(x_dmd, y_dmd), (x_trap, y_trap) = o.grid_dmd, o.grid_trap
a_dmd, wavefront, x0, y0 = h.DMDFromScan(o.grid_dmd, scanFile)
a_target = h.targetFromFile('eth.png', o.grid_trap, scale, length, angle)
a_dmd, wavefront = h.prepareDMD(o.grid_dmd, scanGridShape, a_dmd, wavefront)
a_target = h.prepareTarget(o.grid_trap, a_target, ctrl_scale_offset, ctrl_length_offset, offset)

a_dmd_target, kinoform = h.oneStep(a_target, o)

h.computeHologram(o.grid_dmd, kinoform, scanGridShape, supergratingPitch, supergratingAngle, x0=x0, y0=y0, checkRepresentation=True, saveFile=saveFile+'-holo-uncorr')
h.computeHologram(o.grid_dmd, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle, x0=x0, y0=y0, saveFile=saveFile+'-holo-corr')
g_uncorr = h.computeHologram(o.grid_dmd, kinoform, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
g_corr = h.computeHologram(o.grid_dmd, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
u_ccd_uncorr = o.forwardPropagate( g_uncorr*a_dmd*np.exp(1j*wavefront) )
u_ccd_corr = o.forwardPropagate( g_corr*a_dmd*np.exp(1j*wavefront) )
# g_fine = h.computeFineHologram(x_dmd, y_dmd, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle)
# u_ccd_fine = o.forwardPropagate( g_fine*a_dmd*np.exp(1j*wavefront) )
scan = h.computeScanIdx(o.grid_dmd, scanGridShape)

norm = a_target.max()
a_target /= norm
u_ccd_uncorr /= norm
u_ccd_corr /= norm
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############

############### BEGIN PLOTS ###############
print('Plotting kinoform-wavefront')
pp.figure()
pp.pcolormesh(x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, (kinoform[scan]-wavefront[scan])/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
pp.title('$\\phi_{DMD}-W_{DMD}$')
pp.axis('scaled')
# print('Plotting target field')
# pp.figure()
# pp.pcolormesh(x_trap/h.ccdPixel, y_trap/h.ccdPixel, a_target, cmap='gray', vmin=0, vmax=1)
# pp.title('$A_{target}$')
# pp.axis('scaled')
# try: pp.axis([ -ctrl_length[1]/h.ccdPixel, ctrl_length[1]/h.ccdPixel, -ctrl_length[0]/h.ccdPixel, ctrl_length[0]/h.ccdPixel ])
# except:
#     try: pp.axis(length/h.ccdPixel*np.array([ -1, 1, -0.2, 0.2 ]))
#     except: pass

pp.figure()
pp.pcolormesh(x_trap/h.ccdPixel, y_trap/h.ccdPixel, np.abs(u_ccd_corr), cmap='gray', vmin=0, vmax=1)
pp.title('Discretized $|U_{trap}|$')
pp.axis('scaled')
# pp.axis([ -450, 450, -350, 350 ])
try:
    pp.axis([ -(ctrl_length[1] + h.waveLength*h.focalLength/(supergratingPitch*h.dmdPixel)*np.cos(supergratingAngle))/h.ccdPixel,
               (ctrl_length[1] + h.waveLength*h.focalLength/(supergratingPitch*h.dmdPixel)*np.cos(supergratingAngle))/h.ccdPixel,
              -(ctrl_length[0] + h.waveLength*h.focalLength/(supergratingPitch*h.dmdPixel)*np.sin(supergratingAngle))/h.ccdPixel,
               (ctrl_length[0] + h.waveLength*h.focalLength/(supergratingPitch*h.dmdPixel)*np.sin(supergratingAngle))/h.ccdPixel ])
except:
    try: pp.axis([ -(    length + h.waveLength*h.focalLength/(supergratingPitch*h.dmdPixel)*np.cos(supergratingAngle))/h.ccdPixel,
                    (    length + h.waveLength*h.focalLength/(supergratingPitch*h.dmdPixel)*np.cos(supergratingAngle))/h.ccdPixel,
                   -(0.2*length + h.waveLength*h.focalLength/(supergratingPitch*h.dmdPixel)*np.sin(supergratingAngle))/h.ccdPixel,
                    (0.2*length + h.waveLength*h.focalLength/(supergratingPitch*h.dmdPixel)*np.sin(supergratingAngle))/h.ccdPixel ])
    except: pass

pp.figure()
pp.pcolormesh( x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, g_corr[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')

pic = h.DMDtoCCD(o.grid_trap, np.abs(u_ccd))**2
pp.figure()
pp.pcolormesh(x_trap/h.ccdPixel, y_trap/h.ccdPixel, pic, vmin=0, vmax=0.4, cmap='gray')
pp.title('Simulated CCD picture')
pp.axis('scaled')
pp.axis([-450, 450, -450, 450])
pp.gca().invert_yaxis()

try: # plot the picture from the CCD if it exists
    pic = h.imread('fresnel-omraf-aberrations-pic-corr.png')
    pp.figure()
    pp.imshow(pic, cmap='gray', interpolation='none')
    pp.title('CCD picture')
except:
    pass

pp.tight_layout()

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=300)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############
