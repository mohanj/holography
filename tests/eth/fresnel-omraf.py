'''
bsub -n 4 -W 12:00 -N -B -u gwajusman@gmail.com -J "fresnel-omraf-small" -R "rusage[mem=2048]" -oo fresnel-omraf-small.out 'python3 fresnel-omraf.py'
'''

import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm, SymLogNorm
import holography as h

############### BEGIN INPUTS ###############
# scanGridShape      = h.dmdShape
scanGridShape      = (485, 485)
waist_dmd          = 3e-3
nyquistParameter   = 2 # Fresnel
padFactor          = 0 # Fresnel
ccd                = True # Fresnel
length             = 1e-3 # targetFromFile
angle              = 0 # targetFromFile
offset             = 0 # prepareTarget
buf                = length*0.05
ctrl_length_offset = (97/589*length+buf, length+buf) # prepareTarget
ctrl_length        = ctrl_length_offset # (50e-5, np.inf) # omraf
nIterations        = 50 # omraf
mixingParameter    = 0.4 # omraf
supergratingPitch  = 6 # computeHologrm, computeFineHologram
supergratingAngle  = 90 # computeHologrm, computeFineHologram
saveFigs           = False
saveFile           = 'fresnel-omraf'
dataFile           = '' # saveFile+'-data.npz'
figsize            = (6.4, 4.8) # (12.8, 6.54)
############### END INPUTS ###############



############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
o = h.Fresnel(scanGridShape, nyquistParameter, padFactor, ccd)
(x_dmd, y_dmd), (x_trap, y_trap) = o.grid_dmd, o.grid_trap
a_dmd = np.exp( -( x_dmd**2 + y_dmd**2 )/waist_dmd**2 )
a_target = h.targetFromFile('eth.png', (x_trap, y_trap), None, length, angle)

a_dmd = h.prepareDMD(o.grid_dmd, scanGridShape, a_dmd)
a_target = h.prepareTarget(o.grid_trap, a_target, None, ctrl_length_offset, offset)
kinoform = h.parabolicPhaseGuess(o.grid_dmd, a_dmd, a_target, o.grid_trap)

if len(dataFile)>0: # avoid redoing omraf if the data already exists
    with np.load(dataFile) as data:
        kinoform = data['kinoform']
        u_trap   = data['u_trap']
        rmsError = data['rmsError']
        try: error = data['error']
        except: error = h.computeError(u_trap, a_target, np.isclose(a_target, 0))
else:
    kinoform, u_trap, error, fidelity = h.omraf(a_dmd, a_target, kinoform, o, None, ctrl_length, nIterations, saveFile+'-data', mixingParameter=mixingParameter)

g = h.computeHologram(o.grid_dmd, kinoform, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False, checkRepresentation=False)
u_ccd = o.forwardPropagate( g*a_dmd )
scan = h.computeScanIdx(o.grid_dmd, scanGridShape)
ctrl = h.computeControlIdx(o.grid_trap, None, ctrl_length)
octrl = h.computeOffsetControlIdx(o.grid_trap, supergratingPitch, supergratingAngle, None, ctrl_length)
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############



############### BEGIN PLOTS figsize=(8.6,7) ###############
print('Plotting kinoform')
pp.figure(figsize=figsize)
pp.pcolormesh(x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, kinoform[scan]/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
pp.title('$\\phi_{DMD}/2\\pi$')
pp.axis('scaled')
pp.tight_layout()

# pp.figure()
# pp.pcolormesh(x_trap[ctrl]/h.ccdPixel, y_trap[ctrl]/h.ccdPixel, a_target[ctrl], cmap='jet', vmin=0, vmax=1)
# pp.title('$|U_{target}|$')
# pp.colorbar()
# pp.axis('scaled')

print('Plotting trap field')
pp.figure(figsize=figsize)
pp.pcolormesh(x_trap[ctrl]*1e6, y_trap[ctrl]*1e6, np.abs(u_trap[ctrl])**2, cmap='gray')
pp.title('$|U_{trap}|^2$')
pp.axis('scaled')
pp.tight_layout()
# pp.savefig('Itrap-offset{}.png'.format(offset), bbox_inches='tight', dpi=400)

# pp.figure()
# pp.pcolormesh(x_dmd[crop]/h.dmdPixel, y_dmd[crop]/h.dmdPixel, np.angle(u_trap[crop])/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
# pp.title('arg($U_{trap}$)')
# pp.colorbar(label='$\\phi_{trap}/2\\pi$')
# pp.axis('scaled')

print('Plotting simulated CCD field')
pp.figure(figsize=figsize)
pp.pcolormesh((x_trap[octrl]-x_trap[octrl].mean())*1e6, (y_trap[octrl]-y_trap[octrl].mean())*1e6, np.abs(u_ccd[octrl])**2, cmap='gray')
pp.title('Simulated $|U_{CCD}|^2$')
pp.axis('scaled')
pp.tight_layout()
# pp.savefig('Iccd-offset{}.png'.format(offset), bbox_inches='tight', dpi=400)

print('Plotting error')
pp.figure(figsize=figsize)
# pp.pcolormesh(x_trap[ctrl]*1e6, y_trap[ctrl]*1e6, error, cmap='bwr', norm=SymLogNorm( np.median(np.abs(error)), vmin=-np.abs(error).max(), vmax=np.abs(error).max() ))
pp.pcolormesh(x_trap[ctrl]*1e6, y_trap[ctrl]*1e6, error, cmap='bwr', vmin=-np.abs(error).max(), vmax=np.abs(error).max() )
pp.title('Error')
pp.colorbar()
pp.axis('scaled')
pp.tight_layout()

print('Plotting hologram')
pp.figure(figsize=figsize)
pp.pcolormesh( x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, g[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')
pp.tight_layout()

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=500)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############
