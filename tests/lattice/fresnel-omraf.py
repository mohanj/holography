'''
bsub -n 4 -W 12:00 -N -B -u gwajusman@gmail.com -J "fresnel-omraf-large" -R "rusage[mem=2048]" -oo fresnel-omraf-large.out 'python3 fresnel-omraf.py'
'''

import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm, SymLogNorm
import holography as h

############### BEGIN INPUTS ###############
scanGridShape      = h.dmdShape
# scanGridShape      = (485, 485)
waist_dmd          = 4e-3
nyquistParameter   = 2 # Fresnel
padFactor          = 0 # Fresnel
ccd                = True # Fresnel
nCells             = 5 # sawtoothLattice, zigzagLattice
angle              = 0 # 45 # sawtoothLattice, zigzagLattice
dx, dy, sx, sy     = 50e-6, 50e-6, 33e-6, 33e-6 # sawtoothLattice, zigzagLattice
# dx, dy, sx, sy     = 15e-6, 15e-6, 10e-6, 10e-6 # default
V1                 = 1 # sawtoothLattice, zigzagLattice
offset             = 0 # prepareTarget
ctrl_scale_offset  = None # (1, 1) # prepareTarget
ctrl_length_offset = (4*dy*nCells+4*sy, 4*(dx+sx)) # prepareTarget (overrides scale if not None)
ctrl_scale         = None # (1, 1) # omraf
ctrl_length        = ctrl_length_offset # (50e-5, np.inf) # omraf (overrides scale if not None)
nIterations        = 30 # omraf
supergratingPitch  = 6 # computeHologrm, computeFineHologram
supergratingAngle  = 0 # computeHologrm, computeFineHologram
saveFigs           = False
saveFile           = 'fresnel-omraf'
dataFile           = '' # saveFile+'-data.npz'
figsize            = (12.8, 6.54) # (6.4, 4.8)
############### END INPUTS ###############



############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
o = h.Fresnel(scanGridShape, nyquistParameter, padFactor, ccd)
(x_dmd, y_dmd), (x_trap, y_trap) = o.grid_dmd, o.grid_trap
a_dmd = np.exp( -(x_dmd**2 + y_dmd**2)/waist_dmd**2 )
# a_target = h.sawtoothLattice(o.grid_trap, nCells, angle, dx, dy, sx, sy, V1)
a_target = h.zigzagLattice(o.grid_trap, nCells, angle, dx, dy, sx)

a_dmd = h.prepareDMD(o.grid_dmd, scanGridShape, a_dmd)
a_target = h.prepareTarget(o.grid_trap, a_target, ctrl_scale_offset, ctrl_length_offset, offset)
kinoform = h.parabolicPhaseGuess(o.grid_dmd, a_dmd, a_target, o.grid_trap)

if len(dataFile)>0: # avoid redoing omraf if the data already exists
    with np.load(dataFile) as data:
        kinoform = data['kinoform']
        u_trap   = data['u_trap']
        rmsError = data['rmsError']
        error = data['error']
else:
    kinoform, u_trap, error, rmsError = h.omraf(a_dmd, a_target, kinoform, o, ctrl_scale, ctrl_length, nIterations, saveFile+'-data')

g = h.computeHologram(o.grid_dmd, kinoform, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
u_ccd = o.forwardPropagate( g*a_dmd )
# g_fine = h.computeFineHologram(o.grid_dmd, kinoform, scanGridShape, supergratingPitch, supergratingAngle)
# u_ccd_fine = o.forwardPropagate( g_fine*a_dmd )
scan = h.computeScanIdx(o.grid_dmd, scanGridShape)
ctrl = h.computeControlIdx(o.grid_trap, ctrl_scale, ctrl_length)
octrl = h.computeOffsetControlIdx(o.grid_trap, supergratingPitch, supergratingAngle, ctrl_scale, ctrl_length)

norm = a_target.max()
a_target /= norm
u_trap /= norm
u_ccd /= norm
# u_ccd_fine /= norm
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############



############### BEGIN PLOTS ###############
# print('Plotting kinoform')
# pp.figure(figsize=figsize)
# pp.pcolormesh(x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, kinoform[scan]/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
# pp.title('$\\phi_{DMD}-W_{DMD}$')
# pp.axis('scaled')
# pp.tight_layout()
#
# print('Plotting target field')
# pp.figure(figsize=figsize)
# ax1=pp.subplot(211); pp.pcolormesh(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, a_target[ctrl].T)
# ax2=pp.subplot(212); pp.contourf(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, a_target[ctrl].T, 20)
# ax1.set_title('$A_{target}$')
# ax1.axis('scaled')
# ax2.axis('scaled')
# pp.tight_layout()

print('Plotting trap field')
pp.figure(figsize=figsize)
ax1=pp.subplot(211); pp.pcolormesh(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, np.abs(u_trap[ctrl].T)**2)
ax2=pp.subplot(212); pp.contourf(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, np.abs(u_trap[ctrl].T)**2, 20)
ax1.set_title('$|U_{trap}|^2$')
ax1.axis('scaled')
ax2.axis('scaled')
pp.tight_layout()
pp.savefig('Itrap.png', bbox_inches='tight')

print('Plotting simulated CCD field')
pp.figure(figsize=figsize)
ax1=pp.subplot(211); pp.pcolormesh(y_trap[octrl].T*1e6, x_trap[octrl].T*1e6, np.abs(u_ccd[octrl].T)**2)
ax2=pp.subplot(212); pp.contourf(y_trap[octrl].T*1e6, x_trap[octrl].T*1e6, np.abs(u_ccd[octrl].T)**2, 20)
ax1.set_title('Simulated $|U_{CCD}|^2$')
ax1.axis('scaled')
ax2.axis('scaled')
pp.tight_layout()
pp.savefig('Iccd.png', bbox_inches='tight')

# print('Plotting simulated fine CCD field')
# pp.figure(figsize=figsize)
# ax1=pp.subplot(211); pp.pcolormesh(y_trap[octrl].T*1e6, x_trap[octrl].T*1e6, np.abs(u_ccd_fine[octrl].T))
# ax2=pp.subplot(212); pp.contourf(y_trap[octrl].T*1e6, x_trap[octrl].T*1e6, np.abs(u_ccd_fine[octrl].T), 20)
# ax1.set_title('Fine $|U_{CCD}|$')
# ax1.axis('scaled')
# ax2.axis('scaled')
# pp.tight_layout()

print('Plotting error')
pp.figure(figsize=figsize)
pp.pcolormesh(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, error.T, cmap='bwr', vmin=-np.abs(error).max(), vmax=np.abs(error).max() )
pp.title('Error')
pp.colorbar(orientation='horizontal')
pp.axis('scaled')
pp.tight_layout()

print('Plotting hologram')
pp.figure(figsize=figsize)
pp.pcolormesh( x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, g[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')
pp.tight_layout()

# pp.figure(figsize=figsize)
# pp.pcolormesh( x_dmd[idx]/h.dmdPixel, y_dmd[idx]/h.dmdPixel, fineHologram[idx], cmap='gray' )
# pp.title('Fine Hologram')
# pp.axis('scaled')

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=400)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############
