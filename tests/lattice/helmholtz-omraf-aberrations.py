'''


Command for submitting the job on Euler (https://scicomp.ethz.ch/lsf_submission_line_advisor/):
bsub -n 2 -W 2:00 -N -B -u gwajusman@gmail.com -J "img-aberrations" -R "rusage[mem=1024]" -oo img-aberrations.out 'python3 img-aberrations.py'
'''

import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm, SymLogNorm
import holography as h

############### BEGIN INPUTS ###############
scanFile = 'save.npz' # getScanData
ccd = False # computeHelmholtzGrid
nCells = 5 # sawtoothLattice
angle = 45 # sawtoothLattice
nIterations = 20 # omraf
supergratingPitch = 10 # computeHologrm
supergratingAngle = 45 # computeHologrm
saveFigs = False
############### END INPUTS ###############

############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
scanGridShape = h.getScanData(scanFile)
x, y = h.computeHelmholtzGrid(scanGridShape, ccd=ccd)
a_dmd, wavefront = h.dmdFromScan(x, y, scanFile)
a_target = h.sawtoothLattice(x, y, angle, nCells)
x, y, (a_dmd, a_target, wavefront) = h.doubleHelmholtzGrid(x, y, (a_dmd, a_target, wavefront))

a_dmd, wavefront = h.prepareDMD(x, y, a_dmd, wavefront, scanGridShape)
a_target = h.prepareTarget(a_target, offset=0.01)
kinoform = h.parabolicPhaseGuess(x, y, a_dmd, a_target)
opticalSystem = h.Fourier(x, y, h.focalLength, h.focalLength)

try: # avoid redoing omraf if the data already exists
    with np.load('helmholtz-omraf-aberrations-data.npz') as data:
        kinoform = data['kinoform']
        u_trap   = data['u_trap']
        error    = data['error']
        rmsError = data['rmsError']
except:
    kinoform, u_trap, error, rmsError = h.omraf(a_dmd, a_target, kinoform, opticalSystem, nIterations)
    np.savez('helmholtz-omraf-aberrations-data', kinoform=kinoform, u_trap=u_trap, error=error, rmsError=rmsError)
h.computeHologram(x, y, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle, checkRepresentation=True)
hologram = h.computeHologram(x, y, kinoform-wavefront, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
u_ccd = opticalSystem.forwardPropagate( hologram*a_dmd*np.exp(1j*wavefront) )
idx = h.computeScanIdx(a_dmd)
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############

############### BEGIN PLOTS ###############
pp.figure(figsize=(8.6,7))
pp.pcolormesh(x[idx]/h.dmdPixel, y[idx]/h.dmdPixel, (kinoform[idx]-wavefront[idx])/(2*np.pi)%1, cmap='jet', vmin=0, vmax=1 )
pp.title('Kinoform minus aberrations')
pp.colorbar(label='$(\\phi_{DMD} - \\tilde{\\phi}_{DMD})/2\\pi$')
pp.axis('scaled')

pp.figure(figsize=(8.6,7))
pp.pcolormesh(x/h.ccdPixel, y/h.ccdPixel, np.abs(u_trap), cmap='jet', norm=LogNorm())
pp.title('$|U_{trap}|$')
pp.colorbar()
pp.axis('scaled')

pp.figure(figsize=(8.6,7))
pp.pcolormesh(x/h.ccdPixel, y/h.ccdPixel, np.abs(u_ccd), cmap='jet', norm=LogNorm())
pp.title('$|U_{CCD}|$')
pp.colorbar()
pp.axis('scaled')

pp.figure(figsize=(8.6,7))
pp.pcolormesh(error, cmap='bwr', norm=SymLogNorm(np.abs(error).mean()))
pp.title('Error')
pp.colorbar()
pp.axis('scaled')

pp.figure(figsize=(7,7))
pp.pcolormesh( x[idx]/h.dmdPixel, y[idx]/h.dmdPixel, np.where(hologram[idx]==h.on, 1, 0), cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')

try: # plot the picture from the CCD if it exists
    pic = np.flipud(h.imread('img-aberrations-pic-raw.png'))
    pp.figure(figsize=(11,7))
    pp.pcolormesh(pic, cmap='jet', norm=LogNorm())
    pp.title('CCD Picture')
    pp.colorbar()
    pp.axis('scaled')
except:
    pass

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( 'helmholtz-omraf-aberrations-fig{}.png'.format(n), bbox_inches='tight', dpi=300)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############
