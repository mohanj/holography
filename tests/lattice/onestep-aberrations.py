import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm, SymLogNorm
import holography as h

############### BEGIN INPUTS ###############
scanFile           = '../save.npz' # getScanData
nyquistParameter   = 2 # Fresnel
padFactor          = 0 # Fresnel
ccd                = True # Fresnel
nCells             = 5 # sawtoothLattice, zigzagLattice
angle              = 0 # 45 # sawtoothLattice, zigzagLattice
# default: dx=15e-6, dy=15e-6, s=10e-6
dx, dy, sx, sy     = 100e-6, 100e-6, 67e-6, 67e-6 # sawtoothLattice, zigzagLattice
V1                 = 1 # sawtoothLattice, zigzagLattice
offset             = 0.01 # prepareTarget
ctrl_scale         = None
ctrl_length        = (4*dy*nCells+4*sy, 4*(dx+sx))
supergratingPitch  = 6 # computeHologrm, computeFineHologram
supergratingAngle  = 0 # computeHologrm, computeFineHologram
saveFigs           = False
saveFile           = 'onestep-aberrations'
dataFile           = '' # saveFile+'-data.npz'
figsize            = (6.4, 4.8) # (12.8, 6.54)
crop_scale         = ctrl_scale
crop_length        = ctrl_length
############### END INPUTS ###############



############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
scanGridShape, x0, y0 = h.getScanData(scanFile)
o = h.Fresnel(scanGridShape, nyquistParameter, padFactor, ccd)
(x_dmd, y_dmd), (x_trap, y_trap) = o.grid_dmd, o.grid_trap
a_dmd, wavefront = h.DMDfromScan(o.grid_dmd, scanFile)
# a_target = h.sawtoothLattice(o.grid_trap, nCells, angle, dx, dy, sx, sy, V1)
a_target = h.zigzagLattice(o.grid_trap, nCells, angle, dx, dy, sx)
# a_target = h.zigzagLattice(o.grid_trap, nCells)

a_dmd, wavefront = h.prepareDMD(o.grid_dmd, scanGridShape, a_dmd, wavefront)
a_target = h.prepareTarget(o.grid_trap, a_target, ctrl_scale, ctrl_length, offset)

a_dmd_target, kinoform = h.onestep(a_target, o)
# u_trap = o.forwardPropagate( a_dmd*np.exp(1j*kinoform) )
# error = h.computeError(u_trap, a_target, np.isclose(a_target,0))

# h.computeAmplitudePhaseHologram(grid, kinoform-wavefront, a_dmd, a_dmd_target, scanGridShape, supergratingPitch, supergratingAngle, checkRepresentation=True, toDMD=True, x0=x0, y0=y0, saveFile=saveFile+'-holo')
g = h.computeAmplitudePhaseHologram(o.grid_dmd, kinoform-wavefront, a_dmd, a_dmd_target, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
u_ccd = o.forwardPropagate( g*a_dmd*np.exp(1j*wavefront) )

scan = h.computeScanIdx(o.grid_dmd, scanGridShape)
ctrl = h.computeControlIdx(o.grid_trap, ctrl_scale, ctrl_length)
octrl = h.computeOffsetControlIdx(o.grid_trap, supergratingPitch, supergratingAngle, crop_scale, crop_length)

norm = a_target.max()
a_target /= norm
# u_trap /= norm
u_ccd /= norm
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############



############### BEGIN PLOTS ###############
# print('Plotting kinoform-wavefront')
# pp.figure(figsize=figsize)
# pp.pcolormesh(x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, (kinoform[scan]-wavefront[scan])/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
# pp.title('$\\phi_{DMD}-W_{DMD}$')
# pp.axis('scaled')

print('Plotting target field')
pp.figure(figsize=figsize)
ax1=pp.subplot(211); pp.pcolormesh(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, a_target[ctrl].T)
ax2=pp.subplot(212); pp.contourf(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, a_target[ctrl].T, 20)
ax1.set_title('$A_{target}$')
ax1.axis('scaled')
ax2.axis('scaled')

# print('Plotting trap field')
# pp.figure(figsize=figsize)
# ax1=pp.subplot(211); pp.pcolormesh(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, np.abs(u_trap[ctrl].T))
# ax2=pp.subplot(212); pp.contourf(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, np.abs(u_trap[ctrl].T), 20)
# ax1.set_title('$|U_{trap}|$')
# ax1.axis('scaled')
# ax2.axis('scaled')

print('Plotting simulated CCD amplitude')
pp.figure(figsize=figsize)
ax1=pp.subplot(211); pp.pcolormesh(y_trap[octrl].T*1e6, x_trap[octrl].T*1e6, np.abs(u_ccd[octrl].T))
ax2=pp.subplot(212); pp.contourf(y_trap[octrl].T*1e6, x_trap[octrl].T*1e6, np.abs(u_ccd[octrl].T), 20)
ax1.set_title('Simulated $|U_{CCD}|$')
ax1.axis('scaled')
ax2.axis('scaled')

# pp.figure(figsize=figsize)
# if np.any(np.isnan(error)):
#     pp.pcolormesh(y_trap[octrl].T*1e6, x_trap[octrl].T*1e6, np.abs(error[octrl].T), cmap='jet', norm=LogNorm() )
#     pp.title('|Error|')
# else:
#     pp.pcolormesh(y_trap.T*1e6, x_trap.T*1e6, error.T, cmap='bwr', norm=SymLogNorm( np.median(np.abs(error)), vmin=-np.abs(error).max(), vmax=np.abs(error).max() ))
#     pp.title('Error')
# pp.colorbar()
# pp.axis('scaled')

print('Plotting hologram')
pp.figure(figsize=figsize)
pp.pcolormesh( x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, g[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=400)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############















############### BEGIN PLOTS OLD ###############
# pp.figure(figsize=figsize)
# pp.pcolormesh(x_dmd[octrl]/h.dmdPixel, y_dmd[octrl]/h.dmdPixel, np.angle(u_trap[octrl])/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
# pp.title('arg($U_{trap}$)')
# pp.colorbar(label='$\\phi_{trap}/2\\pi$')
# pp.axis('scaled')
#
# pp.figure(figsize=figsize)
# # pp.pcolormesh(x_trap/h.ccdPixel, y_trap/h.ccdPixel, np.abs(u_ccd), cmap='jet', norm=LogNorm())
# # pp.pcolormesh(x_trap[octrl]/h.ccdPixel, y_trap[octrl]/h.ccdPixel, np.abs(u_ccd[octrl]), cmap='jet', vmin=0, vmax=1)
# pp.pcolormesh(x_trap/h.ccdPixel, y_trap/h.ccdPixel, np.abs(u_ccd), cmap='jet', vmin=0, vmax=1)
# pp.title('DMD-represented $|U_{trap}|$')
# pp.colorbar()
# pp.axis('scaled')
#
# pp.figure(figsize=figsize)
# if isinstance(error, np.ma.masked_array):
#     pp.pcolormesh(np.abs(error[octrl]), cmap='jet', norm=LogNorm() )
#     pp.title('|Error|')
# else:
#     pp.pcolormesh(error[octrl], cmap='bwr', norm=SymLogNorm( np.median(np.abs(error)), vmin=-np.abs(error).max(), vmax=np.abs(error).max() ))
#     pp.title('Error')
# pp.colorbar()
# pp.axis('scaled')
#
# pp.figure(figsize=figsize)
# pp.pcolormesh( x_dmd[idx]/h.dmdPixel, y_dmd[idx]/h.dmdPixel, g[idx], cmap='gray' )
# pp.title('Hologram')
# pp.axis('scaled')
#
# if saveFigs:
#     for n in pp.get_fignums():
#         pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=500)
#         pp.close(pp.gcf())
# else:
#     pp.show()
############### END PLOTS OLD ###############
