import sys
sys.path.append('../../')
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm, SymLogNorm
import holography as h

############### BEGIN INPUTS ###############
scanGridShape      = h.dmdShape
# scanGridShape      = (485, 485)
waist_dmd          = 5e-3
nyquistParameter   = 2 # Fresnel
padFactor          = 0 # Fresnel
ccd                = True # Fresnel
nCells             = 3 # sawtoothLattice, zigzagLattice
angle              = 0 # 45 # sawtoothLattice, zigzagLattice
# dx, dy, sx, sy     = 100e-6, 100e-6, 67e-6, 67e-6 # sawtoothLattice, zigzagLattice
dx, dy, sx, sy     = 50e-6, 50e-6, 33e-6, 33e-6 # sawtoothLattice, zigzagLattice
# dx, dy, sx, sy     = 15e-6, 15e-6, 10e-6, 10e-6 # default
V1                 = 1 # sawtoothLattice, zigzagLattice
offset             = 0 # prepareTarget
ctrl_scale         = None
ctrl_length        = (4*dy*nCells+4*sy, 4*(dx+sx))
supergratingPitch  = 10 # computeHologrm, computeFineHologram
supergratingAngle  = 0 # computeHologrm, computeFineHologram
saveFigs           = False
saveFile           = 'onestep'
dataFile           = '' # saveFile+'-data.npz'
figsize            = (6.4, 4.8) # (12.8, 6.54)
crop_scale         = ctrl_scale
crop_length        = ctrl_length
############### END INPUTS ###############



############### BEGIN KINOFORM COMPUTATION AND REPRESENTATION ###############
o = h.Fresnel(scanGridShape, nyquistParameter, padFactor, ccd)
(x_dmd, y_dmd), (x_trap, y_trap) = o.grid_dmd, o.grid_trap
a_dmd = np.exp( -( x_dmd**2 + y_dmd**2 )/waist_dmd**2 )
# a_target = h.sawtoothLattice(o.grid_trap, nCells, angle, dx, dy, sx, sy, V1)
a_target = h.zigzagLattice(o.grid_trap, nCells, angle, dx, dy, sx)

a_dmd = h.prepareDMD(o.grid_dmd, scanGridShape, a_dmd)
a_target = h.prepareTarget(o.grid_trap, a_target, ctrl_scale, ctrl_length, offset)

a_dmd_target, kinoform = h.onestep(a_target, o)
# u_trap = opticalSystem.forwardPropagate( a_dmd*np.exp(1j*kinoform) )
# error = h.computeError(u_trap, a_target, np.isclose(a_target,0))

g = h.computeAmplitudePhaseHologram(o.grid_dmd, kinoform, a_dmd, a_dmd_target, scanGridShape, supergratingPitch, supergratingAngle, toDMD=False)
u_ccd = o.forwardPropagate( g*a_dmd )

scan = h.computeScanIdx(o.grid_dmd, scanGridShape)
ctrl = h.computeControlIdx(o.grid_trap, ctrl_scale, ctrl_length)
octrl = h.computeOffsetControlIdx(o.grid_trap, supergratingPitch, supergratingAngle, None, crop_length)
############### END KINOFORM COMPUTATION AND REPRESENTATION ###############



############### BEGIN PLOTS ###############
# print('Plotting kinoform-wavefront')
# pp.figure(figsize=figsize)
# pp.pcolormesh(x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, (kinoform[scan]-wavefront[scan])/(2*np.pi)%1, cmap='gray', vmin=0, vmax=1 )
# pp.title('$\\phi_{DMD}$')
# pp.axis('scaled')

# print('Plotting target field')
# pp.figure(figsize=figsize)
# ax1=pp.subplot(211); pp.pcolormesh(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, a_target[ctrl].T)
# ax2=pp.subplot(212); pp.contourf(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, a_target[ctrl].T, 20)
# ax1.set_title('$A_{target}$')
# ax1.axis('scaled')
# ax2.axis('scaled')

# print('Plotting trap field')
# pp.figure(figsize=figsize)
# ax1=pp.subplot(211); pp.pcolormesh(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, np.abs(u_trap[ctrl].T))
# ax2=pp.subplot(212); pp.contourf(y_trap[ctrl].T*1e6, x_trap[ctrl].T*1e6, np.abs(u_trap[ctrl].T), 20)
# ax1.set_title('$|U_{trap}|$')
# ax1.axis('scaled')
# ax2.axis('scaled')

print('Plotting simulated CCD amplitude')
pp.figure(figsize=figsize)
ax1=pp.subplot(211); pp.pcolormesh(y_trap[octrl].T*1e6, x_trap[octrl].T*1e6, np.abs(u_ccd[octrl].T)**2)
ax2=pp.subplot(212); pp.contourf(y_trap[octrl].T*1e6, x_trap[octrl].T*1e6, np.abs(u_ccd[octrl].T)**2, 20)
ax1.set_title('Simulated $|U_{CCD}|^2$')
ax1.axis('scaled')
ax2.axis('scaled')

print('Plotting hologram')
pp.figure(figsize=figsize)
pp.pcolormesh( x_dmd[scan]/h.dmdPixel, y_dmd[scan]/h.dmdPixel, g[scan], cmap='gray' )
pp.title('Hologram')
pp.axis('scaled')

if saveFigs:
    for n in pp.get_fignums():
        pp.savefig( saveFile+'-fig{}.png'.format(n), bbox_inches='tight', dpi=400)
        pp.close(pp.gcf())
else:
    pp.show()
############### END PLOTS ###############
