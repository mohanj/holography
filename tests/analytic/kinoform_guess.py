'''
Tests holography.parabolicPhaseGuess for gaussian DMD and trap
'''
import numpy as np
import matplotlib.pyplot as pp
import sys
sys.path.append('../')
import holography as h

scanGridShape = (384, 384)
x_dmd, y_dmd, x_trap, y_trap = h.computeFresnelGrid(scanGridShape)
