cd ~/Desktop/s17/proj/code.proj/gs-2d/tests
import numpy as np
import matplotlib.pyplot as pp
import sys
sys.path.append('../')
import optics

#################### BEGIN INPUTS ####################

waveLength = 671e-9
dmdLengthX, dmdLengthY = 512*13.68e-6, 512*13.68e-6
gridLengthX, gridLengthY = 2*dmdLengthX, 2*dmdLengthY
dx, dy = 10*waveLength, 10*waveLength
waist_dmdX, waist_dmdY = dmdLengthX, dmdLengthY # gaussian width (intensity = exp(-2)) of amplitude of beam incident on DMD
waist_targetX, waist_targetY = 2e-3, 2e-3 # gaussian or rectangular width of amplitude of target field
focalLength = 10e-2 # Fourier lens
nIterations = 10
offset = 0.15

gridLengthX, gridLengthY = dx*2**np.floor( np.log2(gridLengthX/dx) ), dy*2**np.floor( np.log2(gridLengthY/dy) ) # gridLength/dx = 2^n
x, y = np.arange(-gridLengthX/2, gridLengthX/2, dx), np.arange(-gridLengthY/2, gridLengthY/2, dy)
nuX, nuY = np.fft.fftshift(np.fft.fftfreq(x.size,dx)), np.fft.fftshift(np.fft.fftfreq(y.size,dy))
x, y = np.meshgrid(x,y)
nuX, nuY = np.meshgrid(nuX, nuY)

opticsList = [
    optics.Helmholtz(waveLength, focalLength, nuX, nuY), # between DMD and Fourier lens
    optics.Lens(waveLength, focalLength, x, y), # Fourier lens
    optics.Helmholtz(waveLength, focalLength, nuX, nuY) # between Fourier lens and CCD
]

a_dmd = np.exp( -(x/waist_dmdX)**2 - (y/waist_dmdY)**2 ) # laser incident on DMD has gaussian intensity profile and flat phase
a_target = np.exp( -(x/waist_targetX)**2 - (y/waist_targetY)**2 ) # target amplitude in trap plane has gaussian intensity profile

#################### END INPUTS ####################

# Gerchberg-Saxton
a_dmd, a_target = optics.prepareInputs(x, y, a_dmd, a_target, dmdLengthX=dmdLengthX, dmdLengthY=dmdLengthY, offset=offset)
kinoform, waist_lens = optics.parabolicPhaseGuess(waveLength, focalLength, x, y, x, y, a_dmd, a_target, returnLensWaist=True)
kinoform, u_trap, intensityError, error = optics.gs(a_dmd, a_target, kinoform, opticsList, nIterations=nIterations, printUpdates=True)
u_dmd = a_dmd*np.exp(1j*kinoform)
u_lens = optics.forwardPropagate( u_dmd, [opticsList[0]] )

# check that sampling theorem holds in real and Fourier space
print('')
print('gridLength >> waist')
print(' gridLengthX: {:e}'.format(gridLengthX))
print(' gridLengthY: {:e}'.format(gridLengthY))
print('   waist_dmd: {:e}'.format(np.sqrt(waist_dmdX*waist_dmdY)))
print('  waist_lens: {:e}'.format(waist_lens))
print('waist_target: {:e}'.format(np.sqrt(waist_targetX*waist_targetY)))
print('')
print('1/(2*dx) >> bandwidth')
print('          1/(2*dx): {:e}'.format(1/(2*dx)))
print('          1/(2*dy): {:e}'.format(1/(2*dy)))
print(' bandwidthX(u_dmd): {:e}'.format(optics.bandwidth(nuX,u_dmd)))
print(' bandwidthY(u_dmd): {:e}'.format(optics.bandwidth(nuY,u_dmd)))
print('bandwidthX(u_lens): {:e}'.format(optics.bandwidth(nuX,u_lens)))
print('bandwidthY(u_lens): {:e}'.format(optics.bandwidth(nuY,u_lens)))
print('bandwidthX(u_trap): {:e}'.format(optics.bandwidth(nuX,u_trap)))
print('bandwidthY(u_trap): {:e}'.format(optics.bandwidth(nuY,u_trap)))
print('')

# plot/save results of Gerchberg-Saxton
# pp.figure(figsize=(19,8))
pp.figure()
pp.subplot(2,2,1); pp.pcolormesh(x*1e3, y*1e3, a_target); pp.title('$|u_{target}|$');
pp.subplot(2,2,2); pp.pcolormesh(x*1e3, y*1e3, np.abs(u_trap)); pp.title('$|u_{trap}|$');
# pp.subplot(2,2,2); pp.pcolormesh(x*1e3, y*1e3, np.abs(u_lens), x*1e3, u_lens.real); pp.title('$|u_{lens}|, Re(u_{lens})$')
pp.subplot(2,2,3); pp.pcolormesh(x*1e3, y*1e3, intensityError); pp.title('Relative intensity error in trapping plane');
pp.subplot(2,2,4); pp.pcolormesh(x*1e3, y*1e3, kinoform/(2*np.pi)); pp.title('$\phi_{dmd}/2\pi$')
pp.show(block=False)

# np.savez_compressed( 'gaussian_gs_helmholtz', x=x, y=y, opticsList=opticsList, kinoform=kinoform, a_dmd=a_dmd, u_trap=u_trap, a_target=a_target, intensityError=intensityError )

# plot results of hologram generation
h = optics.hologram(x, y, kinoform)
u_hologram = optics.forwardPropagate(a_dmd*h, opticsList)
# pp.figure(figsize=(19,8))
pp.figure()
pp.subplot(2,2,1); pp.pcolormesh(x*1e3, y*1e3, a_target); pp.title('$|u_{target}|$');
pp.subplot(2,2,2); pp.pcolormesh(x*1e3, y*1e3, np.abs(u_trap)); pp.title('$|u_{trap}|$');
pp.subplot(2,2,3); pp.pcolormesh(x*1e3, y*1e3, h); pp.title('Hologram')
pp.subplot(2,2,4); pp.pcolormesh(x*1e3, y*1e3, np.abs(u_hologram)); pp.title('$|u_{hologram}|$')
pp.show(block=False)
