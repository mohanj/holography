import sys
sys.path.append('../') # put holography in path
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.colors import LogNorm
import holography as h

x, y = h.computeGrid(gridShape=(384,384), x0=0, y0=0)
a_dmd = np.exp( -( x**2 + y**2 )/(1e-3)**2 )
eth = h.targetFromFile('eth.png', (x,y), scale=0.2)

parabolic = h.parabolicPhaseGuess(x, y, a_dmd, eth)
a_trap_parabolic = np.abs(h.ft( a_dmd*np.exp(1j*parabolic) ))
meangrad_parabolic = 2*np.pi*np.sqrt( np.abs(h.ift(nuX*h.ft(parabolic))**2 + h.ift(nuX*h.ft(parabolic))**2) ).mean()

naive = np.angle( h.ift(eth) )
naive = h.unwrap_phase(naive)
a_trap_naive = np.abs(h.ft( a_dmd*np.exp(1j*naive) ))
meangrad_naive = 2*np.pi*np.sqrt( np.abs(h.ift(nuX*h.ft(naive))**2 + h.ift(nuX*h.ft(naive))**2) ).mean()

gs, u_trap_gs, _, _ = h.gs(a_dmd, eth, parabolic, [h.Fresnel(h.focalLength, x, y)], nIterations=10)
a_trap_gs = np.abs(u_trap_gs)
meangrad_gs = 2*np.pi*np.sqrt( np.abs(h.ift(nuX*h.ft(gs))**2 + h.ift(nuX*h.ft(gs))**2) ).mean()

print('meangrad_parabolic: ', meangrad_parabolic)
print('    meangrad_naive: ', meangrad_naive)
print('       meangrad_gs: ', meangrad_gs)

# a_dmd /= np.sqrt(np.sum(a_dmd**2))
# eth /= np.sqrt(np.sum(eth**2))
# gauss /= np.sqrt(np.sum(gauss**2))
# a_trap /= np.sqrt(np.sum(a_trap**2))

# print( '   dmd (x): ', h.computeMoments(x, a_dmd) )
# print( '   eth (x): ', h.computeMoments(x, eth) )
# print( ' gauss (x): ', h.computeMoments(x, gauss) )
# print( 'a_trap (x): ', h.computeMoments(x, a_trap) )
# print('')
# print( '   dmd (y): ', h.computeMoments(y, a_dmd) )
# print( '   eth (y): ', h.computeMoments(y, eth) )
# print( ' gauss (y): ', h.computeMoments(y, gauss) )
# print( 'a_trap (y): ', h.computeMoments(y, a_trap) )

# pp.figure()
# pp.pcolormesh(x, y, eth)
# pp.colorbar()
# pp.title('a_target')
#
# pp.figure()
# pp.pcolormesh(x, y, gauss)
# pp.colorbar()
# pp.title('gauss_target')
#
# pp.figure()
# pp.pcolormesh(x, y, a_trap)
# pp.colorbar()
# pp.title('a_trap')

# pp.figure()
# d = a_trap-gauss
# pp.pcolormesh(x, y, d, cmap='seismic', vmin=-np.abs(d).max(), vmax=np.abs(d).max())
# pp.colorbar()
# pp.title('a_trap - gauss')

# pp.figure()
# pp.pcolormesh(x, y, a_trap_parabolic)
# pp.colorbar()
# pp.title('a_trap_parabolic')
#
# pp.figure()
# pp.pcolormesh(x, y, a_trap_naive)
# pp.colorbar()
# pp.title('a_trap_naive')
#
# pp.figure()
# pp.pcolormesh(x, y, parabolic)
# pp.colorbar()
# pp.title('parabolic')
#
# pp.figure()
# pp.pcolormesh(x, y, naive)
# pp.colorbar()
# pp.title('naive')
#
# pp.show()
