# %reset
# %matplotlib inline
# %cd ~/Desktop/s17/proj/code.proj/gs-2d/tests
import numpy as np
import matplotlib.pyplot as pp
import sys
sys.path.append('../')
import optics

#################### BEGIN INPUTS ####################

waveLength = 671e-9
gridLengthX, gridLengthY = 20e-3, 20e-3
dx, dy = 10*waveLength, 10*waveLength
waist_dmdX, waist_dmdY = 1e-3, 1e-3 # gaussian width (intensity = exp(-2)) of amplitude of beam incident on DMD
waist_targetX, waist_targetY = 1e-3, 1e-3 # gaussian or rectangular width of amplitude of target field
focalLength = 10e-2 # Fourier lens
nIterations = 10
offset = 0

gridLengthX, gridLengthY = dx*2**np.floor( np.log2(gridLengthX/dx) ), dy*2**np.floor( np.log2(gridLengthY/dy) ) # gridLength/dx = 2^n
x_dmd, y_dmd = np.arange(-gridLengthX/2, gridLengthX/2, dx), np.arange(-gridLengthY/2, gridLengthY/2, dy)
nuX, nuY = np.fft.fftshift(np.fft.fftfreq(x_dmd.size,dx)), np.fft.fftshift(np.fft.fftfreq(y_dmd.size,dy))
x_dmd, y_dmd = np.meshgrid(x_dmd,y_dmd)
nuX, nuY = np.meshgrid(nuX, nuY)

opticsList = [ optics.Fresnel(waveLength, focalLength, x_dmd, y_dmd) ] # DMD to Fourier lens to CCD
x_trap, y_trap = opticsList[0].x_image, opticsList[0].y_image
a_dmd = np.exp( -(x_dmd/waist_dmdX)**2 - (y_dmd/waist_dmdY)**2 ) # laser incident on DMD has gaussian intensity profile and flat phase
a_target = np.exp( -(x_trap/waist_targetX)**2 - (y_trap/waist_targetY)**2 ) # target amplitude in trap plane has gaussian intensity profile

#################### END INPUTS ####################

# Gerchberg-Saxton
a_dmd, a_target = optics.prepareInputs(x_dmd, y_dmd, a_dmd, a_target, dmdLengthX=np.inf, dmdLengthY=np.inf, offset=offset)
kinoform = optics.parabolicPhaseGuess(waveLength, focalLength, x_dmd, y_dmd, x_trap, y_trap, a_dmd, a_target, returnLensWaist=False)
kinoform, u_trap, intensityError, error = optics.gs(a_dmd, a_target, kinoform, opticsList, nIterations=nIterations, printUpdates=True)
u_dmd = a_dmd*np.exp(1j*kinoform)
# pp.figure(figsize=(20,9)); pp.pcolormesh(x_dmd*1e3, y_dmd*1e3, kinoform/(2*np.pi)%1); pp.show(block=False)
# pp.figure(figsize=(20,9)); pp.pcolormesh(x_dmd*1e3, y_dmd*1e3, kinoform); pp.colorbar(); pp.show(block=False)

# check that sampling theorem holds in real and Fourier space
print('')
print('gridLength >> waist')
print(' gridLengthX: {:e}'.format(gridLengthX))
print(' gridLengthY: {:e}'.format(gridLengthY))
print('   waist_dmd: {:e}'.format(np.sqrt(waist_dmdX*waist_dmdY)))
print('waist_target: {:e}'.format(np.sqrt(waist_targetX*waist_targetY)))
print('1/(2*dx) >> bandwidth')
print('         1/(2*dx): {:e}'.format(1/(2*dx)))
print('         1/(2*dy): {:e}'.format(1/(2*dy)))
print(' bandwidth(u_dmd): {:e}'.format(np.sqrt(optics.bandwidth(nuX,u_dmd)*optics.bandwidth(nuY,u_dmd))))
print('bandwidth(u_trap): {:e}'.format(np.sqrt(optics.bandwidth(nuX,u_trap)*optics.bandwidth(nuY,u_trap))))
print('')

# # plot/save results of Gerchberg-Saxton
pp.figure(figsize=(20,9))
pp.subplot(2,2,1); pp.pcolormesh(x_trap*1e3, y_trap*1e3, a_target); pp.title('$|u_{target}|$');
pp.subplot(2,2,2); pp.pcolormesh(x_trap*1e3, y_trap*1e3, np.abs(u_trap)); pp.title('$|u_{trap}|$');
pp.subplot(2,2,3); pp.pcolormesh(x_trap*1e3, y_trap*1e3, intensityError); pp.title('Relative intensity error in trapping plane');
pp.subplot(2,2,4); pp.pcolormesh(x_trap*1e3, y_trap*1e3, kinoform/(2*np.pi)); pp.title('$\phi_{dmd}/2\pi$')
pp.show(block=False)

# plot results of hologram generation
h = optics.hologram(x_dmd, y_dmd, kinoform)
u_hologram = optics.forwardPropagate(a_dmd*h, opticsList)
pp.figure(figsize=(20,9))
pp.subplot(2,2,1); pp.pcolormesh(x_trap*1e3, y_trap*1e3, a_target); pp.title('$|u_{target}|$');
pp.subplot(2,2,2); pp.pcolormesh(x_trap*1e3, y_trap*1e3, np.abs(u_trap)); pp.title('$|u_{trap}|$');
pp.subplot(2,2,3); pp.pcolormesh(x_dmd*1e3, y_dmd*1e3, h); pp.title('Hologram')
pp.subplot(2,2,4); pp.pcolormesh(x_trap*1e3, y_trap*1e3, np.abs(u_hologram)); pp.title('$|u_{hologram}|$')
pp.show(block=False)

# np.savez_compressed( 'gaussian_gs_fresnel', x=x, y=y, opticsList=opticsList, kinoform=kinoform, a_dmd=a_dmd, u_trap=u_trap, a_target=a_target, intensityError=intensityError, hologram=h )
