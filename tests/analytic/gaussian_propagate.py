'''
Test whether optics.Helmholtz produces the same beam profile as the analytically calculated gaussian beam in the paraxial/Fresnel approximation
'''
# %cd ~/Desktop/s17/proj/code.proj/gs-2d/tests
# %matplotlib
import numpy as np
import matplotlib.pyplot as pp
import sys
sys.path.append('../')
import optics

waveLength = 671e-9
gridLengthX, gridLengthY = 1e-1, 1e-1
dx, dy = 100*waveLength, 100*waveLength
w0 = 1e-3

# maximum propagation length z such that the waist of the image field is no larger than w_max: zR*np.sqrt((w_max/w0)**2 - 1)
zR = np.pi*w0**2/waveLength # Rayleigh range
z = [zR/10, zR, 10*zR] # propagation lengths to test
gridLengthX, gridLengthY = dx * 2**np.floor( np.log2(gridLengthX/dx) ), dy * 2**np.floor( np.log2(gridLengthY/dy) ) # gridLength/dx = 2^n
x, y = np.arange(-gridLengthX/2, gridLengthX/2, dx), np.arange(-gridLengthY/2, gridLengthY/2, dy)
nuX, nuY = np.fft.fftshift(np.fft.fftfreq(x.size,dx)), np.fft.fftshift(np.fft.fftfreq(y.size,dy))
x, y = np.meshgrid(x, y)
nuX, nuY = np.meshgrid(nuX, nuY)
u = np.exp( -(x**2 + y**2)/w0**2 )

def gaussianBeam(z):
    w = w0*np.sqrt(1 + (z/zR)**2)
    if z==0: R = np.inf
    else: R = z + zR**2/z
    gouy = np.arctan2(z,zR)
    return w0/w * np.exp(-(x**2+y**2)/w**2) * np.exp( 2j*np.pi/waveLength*(z + (x**2+y**2)/(2*R)) - 1j*gouy )

pp.figure()
for iz, zi in enumerate(z):
    u1 = gaussianBeam(zi)
    h = optics.Helmholtz(waveLength, zi, nuX, nuY)
    u2 = h.forwardPropagate(u)
    pp.subplot(3, 3, 3*iz+1); pp.pcolormesh(x, y, np.abs(u1)-np.abs(u2)); pp.colorbar()
    if iz==0: pp.title('|Gaussian| - |Helmholtz|')
    pp.ylabel('z/zR={}'.format(zi/zR))
    pp.subplot(3, 3, 3*iz+2); pp.pcolormesh(x, y, u1.real)
    if iz==0: pp.title('Re(Gaussian)')
    pp.subplot(3, 3, 3*iz+3); pp.pcolormesh(x, y, u2.real)
    if iz==0: pp.title('Re(Helmholtz)')
pp.show(block=False)

print('gridLength >> waist')
print(' gridLengthX: {:e}'.format(gridLengthX))
print(' gridLengthY: {:e}'.format(gridLengthY))
print('   max waist: {:e}'.format( w0*np.sqrt(1 + (z[-1]/zR)**2) ))
print('1/(2*dx) >> bandwidth')
print('     1/(2*dx): {:e}'.format(1/(2*dx)))
print('     1/(2*dy): {:e}'.format(1/(2*dy)))
print('max bandwidth: {:e}'.format(optics.bandwidth(nuX,u)))
