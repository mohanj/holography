import sys
sys.path.append('../')
import numpy as np
import holography as h
import pickle

#################### BEGIN INPUTS ####################
waveLength = 671e-9
focalLength = 100e-3 # Fourier lens
# nPixelsX, nPixelsY = 1024, 768
# nPixelsX, nPixelsY = 768, 768
nPixelsX, nPixelsY = 512, 512
dmdLengthX, dmdLengthY = nPixelsX*13.68e-6, nPixelsY*13.68e-6
gridLengthX, gridLengthY = 2*dmdLengthX, 2*dmdLengthY
# dx, dy = 8*waveLength, 8*waveLength
dx, dy = focalLength*waveLength/dmdLengthX, focalLength*waveLength/dmdLengthY
waist_dmd = 3.47e-3 # gaussian width (intensity = exp(-2)) of amplitude of beam incident on DMD
waist_targetX, waist_targetY = 0.1e-3, 0.1e-3 # rectangular width of amplitude of target field
x0_target, y0_target = 0., 0. # center point of target field
smoothing_waist = 0.01e-3 # waist of Gaussian that smooths square potential
nIterations = 10

gridLengthX, gridLengthY = dx * 2**np.floor( np.log2(gridLengthX/dx) ), dy * 2**np.floor( np.log2(gridLengthY/dy) ) # gridLength/dx = 2**n
x, y = np.meshgrid( np.arange(-gridLengthX/2, gridLengthX/2, dx), np.arange(-gridLengthY/2, gridLengthY/2, dy) )
a_dmd = np.exp( -(x**2 + y**2)/waist_dmd**2 ) # laser incident on DMD has gaussian intensity profile and flat phase
a_target = np.where( np.logical_and( np.abs(x - x0_target)<waist_targetX, np.abs(y - y0_target)<waist_targetY ), 1., 0. ) # target amplitude in trap plane is an off-center square
a_target = h.ift( h.ft(a_target)*h.ft(np.exp(-(x**2 + y**2)/smoothing_waist**2)) ).real # smooth target profile by convolving with a gaussian
x, y, nuX, nuY, a_dmd, a_target = h.prepareInputs(x, y, a_dmd, a_target, doubleGrid=True)

opticsList = [ h.Fourier(waveLength, focalLength, focalLength, focalLength, x, y, nuX, nuY) ]
#################### END INPUTS ####################



#################### BEGIN OMRAF ####################
kinoformGuess = h.parabolicPhaseGuess(waveLength, focalLength, x, y, a_dmd, x, y, a_target)
kinoform, u_trap, intensityError, error = h.omraf(a_dmd, a_target, kinoformGuess, opticsList, nIterations=nIterations)
u_dmd = a_dmd*np.exp(1j*kinoform)
u_lens = h.forwardPropagate( u_dmd, [h.Helmholtz(waveLength, focalLength, nuX, nuY)] )
#################### END OMRAF ####################



#################### BEGIN CHECKS ####################
print('') # check that sampling theorem holds in direct and Fourier space
print('gridLength >> waist')
print('    gridLength: {:e}'.format( (gridLengthX + gridLengthY)/2 ))
print('     waist_dmd: {:e}'.format( waist_dmd ))
print('    waist_lens: {:e}'.format( h.waist(x,y,u_lens) ))
print('  waist_target: {:e}'.format( np.sqrt(waist_targetX**2 + waist_targetY**2) ))
print('1/(2*dx) >> bandwidth')
print('      1/(2*dx): {:e}'.format( (1/(2*dx) + 1/(2*dy))/2 ))
print(' bandwidth_dmd: {:e}'.format( h.bandwidth(nuX,nuY,u_dmd) ))
print('bandwidth_lens: {:e}'.format( h.bandwidth(nuX,nuY,u_lens) ))
print('bandwidth_trap: {:e}'.format( h.bandwidth(nuX,nuY,u_trap) ))
print('')
#################### BEGIN CHECKS ####################



#################### BEGIN HOLOGRAM REPRESENTATION ####################
# g, kinoform_dmd = h.hologram(x, y, kinoform)
hologram = h.computeHologram(x, y, kinoform, nPixelsX=nPixelsX, nPixelsY=nPixelsY, toDMD=False)
# pp.matshow(kinoform_dmd/(2*np.pi), cmap='gray')
# pp.matshow(g, cmap='gray')
# pp.show()
# pp.figure(); pp.pcolormesh(kinoform/(2*np.pi), cmap='gray'); pp.colorbar()
# h = h.hologram(x, kinoform, pitch=pitch, nPixels=nPixels)
# u_hologram = h.forwardPropagate(a_dmd*h, opticsList)
# pp.figure()
# pp.subplot(2,1,1); pp.plot(x*1e3, a_target, x*1e3, np.abs(u_hologram)); pp.title('$|u_{target}|, |u_{hologram}|$');
# pp.subplot(2,1,2); pp.plot(x*1e3, np.cos(kinoform), x*1e3, h); pp.title('Hologram');
# pp.show(block=False)
#################### END HOLOGRAM REPRESENTATION ####################



#################### BEGIN SAVE RESULTS ####################
data = h.Data(notes='no notes', nIterations=nIterations, x=x, y=y, a_dmd=a_dmd, a_target=a_target, kinoform=kinoform, u_trap=u_trap, u_lens=u_lens, hologram=hologram, intensityError=intensityError, error=error)
pickle.dump(data, __file__[0:-3])
# np.savez_compressed( 'square_omraf_helmholtz', x=x, y=y, a_target=a_target, kinoform=kinoform, u_trap=u_trap, intensityError=intensityError, u_lens=u_lens )
#################### END SAVE RESULTS ####################



#################### BEGIN COLORMAP PLOTS ####################
# pp.figure(); pp.pcolormesh(x*1e3, y*1e3, kinoformGuess); pp.title('kinoformGuess')
# pp.figure(); pp.pcolormesh(x*1e3, y*1e3, kinoform, cmap='gray'); pp.colorbar(); pp.title('kinoform')
# pp.show()

# pp.figure()
#
# pp.subplot(221)
# pp.pcolormesh(x*1e3, y*1e3, a_target)
# pp.title('$|u_{target}|$')
#
# pp.subplot(222)
# pp.pcolormesh(x*1e3, y*1e3, np.abs(u_trap))
# pp.title('$|u_{trap}|$')
#
# # pp.subplot(222)
# # pp.pcolormesh(x*1e3, y*1e3, np.abs(u_lens), x*1e3, u_lens.real)
# # pp.title('$|u_{lens}|, Re(u_{lens})$')
#
# pp.subplot(223)
# pp.pcolormesh(x*1e3, y*1e3, intensityError)
# pp.title('Intensity error in trap plane')
#
# pp.subplot(224)
# pp.pcolormesh(x*1e3, y*1e3, kinoform/(2*np.pi), cmap='gray')
# pp.title('$\phi_{dmd}/2\pi$')
#
# pp.show()
#################### END COLORMAP PLOTS ####################



#################### BEGIN SURFACE PLOTS ####################
# pp.figure()
#
# pp.subplot(221, projection='3d')
# pp.gca().plot_surface(x*1e3, y*1e3, a_target, cmap='viridis')
# pp.title('$|u_{target}|$')
#
# pp.subplot(222, projection='3d')
# pp.gca().plot_surface(x*1e3, y*1e3, np.abs(u_trap), cmap='viridis')
# pp.title('$|u_{trap}|$')
# pp.gca().set_zlim(-1.01, 1.01)
#
# pp.subplot(223, projection='3d')
# pp.gca().plot_surface(x*1e3, y*1e3, intensityError, cmap='coolwarm')
# pp.title('Intensity error in trap plane')
#
# pp.subplot(224, projection='3d')
# pp.gca().plot_surface(x*1e3, y*1e3, kinoform/(2*np.pi), cmap='gray')
# pp.title('$\phi_{dmd}/2\pi$')
#
# pp.show(block=False)
#################### END SURFACE PLOTS ####################
