'''
What happens if we compute the kinoform for finite offset, let it converge, then start damping the offset slowly?
Does the kinoform change smoothly with changing offset in OMRAF? Determine qualitatively by plotting kinoform(x, offset) for all x. If smooth, then plot kinoform(x0, offset) for various random points on the DMD x0.

Use gaussian as target since the "exact" kinoform is known for Fresnel

https://en.wikipedia.org/wiki/Richardson_extrapolation
'''
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.cm import viridis
import holography as h
import time

############### BEGIN INPUTS ###############
scanGridShape = 1024
ccd = False
w0_dmd = np.inf
nCells = 5
x0_target = 0
w0_target = 300e-6
ctrl = 0.2
# offset = np.arange(0.05, 0.15, 0.001)
offset0 = 0.15
nIterations0 = 100
offset = np.logspace( np.log10(0.15), -4, 10 )
nIterations = 10
doubled = True
nIterations = 200
############### END INPUTS ###############

x_dmd, x_trap = h.computeFresnelGrid(scanGridShape, ccd=ccd)
a_dmd = np.exp( -(x_dmd/w0_dmd)**2 )
# a0_target = np.exp( -(x_trap - x0_target)**2/w0_target**2 )
a0_target = h.lattice(x_trap, nCells)
points = np.sort((x_dmd.size*points).astype(int))

if doubled: x_dmd, x_trap, a_dmd, a0_target = h.doubleFresnelGrid(x_dmd, a_dmd, a0_target)
a_dmd = h.prepareDMD(x_dmd, a_dmd, scanGridShape=scanGridShape)
a0_target = h.prepareTarget(a0_target, ctrl, 0, doubled)
kinoform0 = h.parabolicPhaseGuess(x_dmd, a_dmd, a0_target, x_trap=x_trap)
opticalSystem = h.Fresnel()

points_data = np.zeros((offset.size, points.size))
f1 = pp.figure(); pp.title('Kinoform'); pp.xlabel('$x_{DMD}/\\delta x_{DMD}$')
f2 = pp.figure(); pp.title('RMS Error'); pp.xlabel('Iteration')
f3 = pp.figure(); pp.title('$|U_{trap}|^2$'); pp.xlabel('$x_{trap}/\\delta x_{CCD}$')
colors = [ viridis(c) for c in np.linspace(0, 1, offset.size) ]
t0 = time.time()
for i, o in enumerate(offset):
    a_target = h.prepareTarget(a0_target, ctrl, offset=o, doubled=doubled)
    k = h.parabolicPhaseGuess(x_dmd, a_dmd, a_target, x_trap=x_trap)
    k, u_trap, error, rmsError = h.omraf(a_dmd, a_target, k, opticalSystem, ctrl, nIterations)
    points_data[i,:] = k[points]
    f1.gca().plot(x_dmd/h.dmdPixel, k/(2*np.pi), color=colors[i])
    f2.gca().semilogy(rmsError, color=colors[i])
    f3.gca().semilogy(x_dmd/h.dmdPixel, np.abs(u_trap)**2, color=colors[i])
print('Everything took {:e} seconds'.format(time.time()-t0))

# f1.gca().legend(offset)
# f2.gca().legend(offset)
# f3.gca().legend(offset)

f4 = pp.figure(); pp.title('Kinoforom at various points'); pp.xlabel('Offset')
colors = [ viridis(c) for c in np.linspace(0, 1, points.size) ]
for i, c in enumerate(colors):
    # f4.gca().plot(offset, points_data[:,i], color=c)
    # f4.gca().semilogx(offset, points_data[:,i], color=c)
    f4.gca().loglog(offset, points_data[:,i], color=c)
# pp.legend(np.round(points/x_dmd.size, 2))

pp.show()
