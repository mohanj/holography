'''
holography.parabolicPhaseGuess should be exact for gaussian DMD and target fields with Fresnel propagation. Checks correct functioning of:
- computeFresnelGrid
- prepareDMD
- prepareTarget
- parabolicPhaseGuess
- computeHologram
- Fresnel
- computeError
- checkSampling
'''
import numpy as np
import matplotlib.pyplot as pp
import holography as h

x_dmd, x_trap = h.computeFresnelGrid(1024)
a_dmd = np.exp( -(x_dmd - 1e-3)**2/(3.47e-3)**2 )
a_target = np.exp( -(x_trap - 0.5e-3)**2/(50e-6)**2 )
a_dmd = h.prepareDMD(x_dmd, a_dmd, scanGridShape=np.inf)
a_target = h.prepareTarget(a_target, offset=0.)
kinoformGuess, waist_lens = h.parabolicPhaseGuess(x_dmd, a_dmd, a_target, x_trap=x_trap, returnLensWaist=True)
g = h.computeHologram(x_dmd, kinoformGuess, checkRepresentation=True)
opticalSystem = h.Fresnel()
u_trap = opticalSystem.forwardPropagate( a_dmd*np.exp(1j*kinoformGuess) )
u_ccd = opticalSystem.forwardPropagate( g*a_dmd )
error = h.computeError( u_trap, a_target, np.isclose(a_target,0) )

# h.checkSampling(x_dmd, a_dmd, plot=True)
# h.checkSampling(x_trap, a_target, plot=True)
# h.checkSampling(x_trap, np.abs(u_ccd), plot=True)

pp.figure()
pp.plot(x_trap/h.ccdPixel, a_target, x_trap/h.ccdPixel, np.abs(u_trap), x_trap/h.ccdPixel, np.abs(u_ccd))
pp.legend([ '$A_{target}$', '$|U_{trap}|$', '$|U_{CCD}|$' ])
pp.xlabel('CCD pixel number')

pp.figure()
pp.plot(x_trap/h.ccdPixel, error)
pp.title('Error')
pp.xlabel('CCD pixel number')

pp.show()
