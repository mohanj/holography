'''
holography.Helmholtz should produce a similar beam profile as the analytically calculated plane wave sent through a square aperture propagated with Fresnel integrals (Saleh & Teich example 4.3-1). Checks:
- computeHelmholtzGrid
- Helmholtz

High frequency oscillations in Helmholtz solution since the sinc of the square aperture is a slowly-decaying function and contains significant contributions far from zero -- see holograpy.checkSampling on a propagated field with small Fresnel number:
    h.checkSampling(x, h.Helmholtz(15, x).forwardPropagate(u0), plot=True)
'''
import numpy as np
from scipy.special import fresnel
import matplotlib.pyplot as pp
import holography as h

x = h.computeHelmholtzGrid(2500)
D = 1e-3 # slit width
u0 = 1.*( np.abs(x-x.mean())<=D/2 )
NF = np.array([10, 1, 0.5, 0.1])
z = D**2/(h.waveLength*NF)

def squareDiffraction(NF):
    # S = integral( sin(pi/2 * t**2), t=0..z)
    # C = integral( cos(pi/2 * t**2), t=0..z)
    # S, C = fresnel(z)
    X = np.sqrt(NF)*x/D
    S1, C1 = fresnel( np.sqrt(2)*X - np.sqrt(2*NF) )
    S2, C2 = fresnel( np.sqrt(2)*X + np.sqrt(2*NF) )
    return 2**-0.5*( C2-C1 - 1j*(S2-S1) )

pp.figure()
for i, (NFi, zi) in enumerate(zip(NF,z)):
    u1 = squareDiffraction(NFi)
    u2 = h.Helmholtz(zi, x).forwardPropagate(u0)
    pp.subplot(2,2,i+1)
    pp.title('NF={:.1f} (z={:.2e} mm)'.format(NFi, zi*1e3))
    # pp.plot( x*1e3, np.abs(u1), 'b-',
    #          x*1e3, np.abs(u2), 'r-',
    #          x*1e3, u1.real, 'b--',
    #          x*1e3, u2.real, 'r--' )
    # pp.legend(['|Fresnel|', '|Helmholtz|', 'Re(Fresnel)', 'Re(Helmholtz)'])
    pp.plot( x*1e3, np.abs(u1), 'b-',
             x*1e3, np.abs(u2), 'r-' )
    pp.legend(['|Fresnel|', '|Helmholtz|'])
pp.show()
