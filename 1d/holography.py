'''
Each class corresponds to a component of an optical system. Each has the methods forwardPropagate -- which returns the field u2 at the output of the component given the field u1 at the input -- and backPropagate -- returns the field at the input given the field at the output.

=====================================
== Variable names and explanations ==
=====================================
- waveLength (float)
- propagationDistance (float)
- focalLength (float)
- clearAperture (float)
- dmdLensPropagationDistance (float):
- lensTrapPropagationDistance (float):
- x (1D array): The simulation grid. Must be regularly spaced dx = x[n+1]-x[n] == np.diff(x).mean() and have length 2^n (integral n) for FFT
- nu (1D array): Spatial frequencies np.fft.fftshift(np.fft.fftfreq(x.size, x[1]-x[0]))
- x1 (1D array): the simulation grid in the input/object plane
- x2 (1D array): the simulation grid in the Fourier plane for Fresnel x2 = nu1*waveLength*focalLength or output/trap/image plane
- u1 (1D array): the complex field at the input of an optical component or system
- u2 (1D array): the complex field at the output of an optical component or system
- a1 (1D array): the amplitude of the field at the input of an optical component or system abs(u1)
- a2 (1D array): the amplitude of the field at the output of an optical component or system abs(u2)
- a_target (1D array): the target amplitude of the field in the image plane of a multi-component optical system
- a_dmd (1D array): the amplitude of the laser field incident on the DMD
- kinoform (1D array): the phase of the field leaving the DMD (for gs()) or the phase to be imprinted on the wavefront by the DMD (for hologram())
- u_dmd (1D array): the complex field leaving the DMD u_dmd = a_dmd*np.exp(1j*kinoform)
- opticsList (1D list of optics): the sequence of optical components the light passes through from the DMD to the trap plane
- kinoformGuess (1D array): The guess for the kinoform at the start of the Gerchberg-Saxton loop (e.g. parabolicPhaseGuess)
- waist_lens (float): the gaussian width of the field at the Fourier lens (useful for determining grid size according to sampling theorem)
- g (1D array): the grating function of the DMD -- reflectivity of each pixel (1 or 0) such that an incoming plane wave will be reflected with the input kinoform
- pitch (float): grating pitch (mirror pitch)/sqrt(2)
- nPixels (int): number of micromirrors/pixels on the DMD along x

===========
== Notes ==
===========
- uses Goodman/NumPy's convention for the Fourier transform (except in Helmholtz.backPropagate)
- the optics classes cannot accept arrays of waveLength, propagationDistance, focalLength, or clearAperture
- len(x) should be an integral power of 2 with equally spaced points
- gs requires a_target and a_dmd to be normalized such that np.sum(a_target**2) == np.sum(a_dmd**2) == 1 (done by prepareInputs())

===========
== To do ==
===========
- change optics classes to take boolean input that says whether u1 and u2 are in real or Fourier space and define propagate functions accordingly (possibly with convolution)
- shift the Fourier simulaiton space to the center of the target field's spacial frequency spectrum so we can sample a smaller region
- add a step to smooth the kinoform before propagate in gs and omraf
- add incidentAngle and width map argument to hologram?
- add smoothing/randomization option to hologram (Zupapnic thesis Eq. 3.14)
- add gaps between mirrors in hologram
- add padding to x vectors if x.size is not an integer power of two (Numerical Recipes: Treatment of End Effects by Zero Padding)
- write functions to add phase from aberrations and oblique incidence of beam to simplify code between output of gs and input of hologram
- change Lens to ParabolicLens and add Lens that has the exact profile of the lens (Saleh & Teich Eq. 2.4-5)
- do I need to vectorize the classes or methods?
- remove extra fftshift for transfer function?
- how to handel Lens.backPropagate for finite clearAperture? transmission = 1 instead of 0 for x outside clearAperture?
- easy to modify Helmholtz and Fresnel for propagation through dispersive medium (exact profile of lens?)
'''

###################################################################
## INITIALIZE MODULE WITH GLOBAL PARAMETERS OF THE OPTICAL SETUP ##
###################################################################

import numpy as np
from scipy.interpolate import UnivariateSpline, interp1d
from scipy.special import erfinv
import matplotlib.pyplot as pp
import sys, os, time, pickle, pdb, __main__

# location of this file and its dependancies
root = os.path.split( os.path.abspath(__file__) )[0]

# Parameters of the setup with the Vialux DMD
waveLength          = 671e-9
focalLength         = 100e-3 # Fourier lens
dmdShape            = 1024 # x axis of DMD
dmdPixel            = 13.68e-6
# telescope           = -1 # TODO magnification of telescope after DMD with image plane in the back focal plane of the Fourier lens
dmdLength           = dmdPixel*dmdShape
on                  = 0 # the DMD mirror is blazed (on) when the controller passes the value of on to it
diffractionOrder    = -6 # which diffraction order from the fundamental DMD balzed grating is used
subDiffractionOrder = -1 # which diffraction order from the supergrating is used (relative to diffractionOrder)
blazeAngle          = 12 # tilt angle of the DMD mirrors in on or off
dmdAngle            = 45 # DMD is rotated 45 degrees about the optical axis
# incidentAngle       = -np.deg2rad(12.2835789581) # angle the incident optical axis makes with the DMD normal
# reflectedAngle      = -np.deg2rad(11.7164210419) # angle the reflected optical axis makes with the DMD normal
ccdPixel            = 3.75e-6 # size of square CCD pixels

#############
## CLASSES ##
#############

def ft(u):
    '''
    Goodman/NumPy's Fourier transform (Saleh & Teich's inverse Fourier transform) with proper shifting (Schmidt Listing 2.1)
    '''
    return np.fft.fftshift( np.fft.fft( np.fft.fftshift(u), norm='ortho' ) )



def ift(Fu):
    '''
    Goodman/NumPy's inverse Fourier transform (Saleh & Teich's Fourier transform) with proper shifting (Schmidt Listing 2.2)
    '''
    return np.fft.fftshift( np.fft.ifft( np.fft.fftshift(Fu), norm='ortho' ) )



class Helmholtz:
    '''
    Free space propagation as the exact solution to the scalar Helmholtz equation. backPropagate uses conj because it uses Saleh & Teich's Fourier transform (phasor) convention while the input u2 and the output u1 is in Goodman/NumPy's to avoid overflow of exp. Uses the same grid in both the input and output plane
    '''
    def __init__(self, x, propagationDistance):
        nu = np.fft.fftshift(np.fft.fftfreq( x.size, x[1]-x[0] ))
        self.H = np.exp( +2j*np.pi*propagationDistance*np.sqrt(waveLength**-2 - nu**2 + 0j) ) # transfer function
    def forwardPropagate(self, u1):
        return ift( self.H*ft( u1 ) )
    def backPropagate(self, u2):
        return ft( self.H*ift( u2.conj() ) ).conj()



class Fresnel:
    '''
    Free space propagation from back/input/object/1 to front/output/Fourier/2 focal plane of a parabolic lens in Fresnel approximation: Fourier transform
    '''
    def forwardPropagate(self, u1):
        return ft(u1)
    def backPropagate(self, u2):
        return ift(u2)



class ParabolicLens:
    ''' Transmission through a parabolic lens '''
    def __init__(self, x, focalLength, clearAperture=np.inf):
        self.L = np.exp( -1j*np.pi*x**2/(waveLength*focalLength) ) # lens transmission
        self.L[ np.abs(x) > clearAperture/2 ] = 0.                 # function
    def forwardPropagate(self, u1):
        return self.L*u1
    def backPropagate(self, u2):
        return self.L.conj()*u2



class Fourier:
    '''
    Fourier configuration where, ideally, the object is in the back focal plane (dmdLensDistance=focalLength) and the trap/CCD is in the front focal plane (lensTrapDistance=focalLength) shown in Figure 5.5 (b) in Goodman. Uses full Helmholtz propagation. backPropagate uses conj because it uses Saleh & Teich's Fourier transform (phasor) convention while the input u2 and the output u1 is in Goodman/NumPy's to avoid overflow of exp
    '''
    def __init__(self, x, dmdLensDistance, lensTrapDistance, clearAperture=np.inf):
        nu = np.fft.fftshift(np.fft.fftfreq( x.size, x[1]-x[0] ))
        self.H1  = np.exp( +2j*np.pi*dmdLensDistance *np.sqrt(waveLength**-2 - nu**2 + 0j) ) # DMD -> lens
        self.H2 = np.exp( +2j*np.pi*lensTrapDistance*np.sqrt(waveLength**-2 - nu**2 + 0j) ) # lens -> trap
        self.L = np.exp( -1j*np.pi*x**2/(waveLength*focalLength) )
        self.L[ np.abs(x) > clearAperture/2 ] = 0.
    def forwardPropagate(self, u1):
        return ift( self.H2*ft( self.L*ift( self.H1*ft( u1 ) ) ) )
    def backPropagate(self, u2):
        return ft( self.H1*ift( self.L*ft( self.H2*ift( u2.conj() ) ) ) ).conj()



class OpticsList:
    '''
    Propagation through an optical system composed of an arbitrary number of any of the above optical components (Helmholtz, Lens, Fresnel, Fourier). The order that the light encounters the components during forward propagation should be the order of the orguments to OpticsList __init__ function
    '''
    def __init__(self, *opticsList):
        self.opticsList = opticsList
    def forwardPropagate(self, u1):
        for o in self.opticsList: u1 = o.forwardPropagate(u1)
        return u1 # u2
    def backPropagate(self, u2):
        for o in opticsList: u2 = o.backPropagate(u2)
        return u2 # u1

#########################
## GRID INITIALIZATION ##
#########################

def computeFresnelGrid(scanGridShape=dmdShape, nyquistParameter=2, padFactor=0, ccd=False):
    '''
    Returns the grids centered at 0 in both DMD and trap (Fourier) planes x_dmd and x_trap defined by:
        dx_trap = waveLength*focalLength / Dx_dmd
        Dx_trap = waveLength*focalLength / dx_dmd
    for use with Fresnel. gridShape is the number of DMD pixels to be covered by the grid in each dimension and can differ dmdShape. If using data from a wavefront scan performed by fourierTransform_GUI.py, gridShape should be (scan_size, scan_size) (returned by getScanParameters). Enhance the grid size by 2**padFactor along each dimension

    Returns: x_dmd, x_trap
    '''
    print('Computing the Fresnel grid on {:d} DMD pixels...'.format(scanGridShape))
    t0 = time.time()

    dx_dmd = dmdPixel/nyquistParameter # DMD grid spacing
    Dx_dmd = nyquistParameter*dmdPixel*scanGridShape # lower bound of DMD grid length
    if ccd: # ensure the CCD is being adequately sampled in the trap plane (nyquistParameter grid points per CCD pixel)
        Dx_dmd = max( Dx_dmd, nyquistParameter*waveLength*focalLength/ccdPixel )

    Dx_dmd = dx_dmd * 2**np.ceil( np.log2(Dx_dmd/dx_dmd) ) # number of grid points to next power of 2
    dx_dmd /= 2**padFactor # pad in both DMD and trap
    Dx_dmd *= 2**padFactor # planes to avoid aliasing

    N = int(Dx_dmd/dx_dmd/2) # number of grid points in both directions using int to eliminate rounding errors
    x_dmd = dx_dmd*np.arange( -N, N ) # take care to not repeat left boundary on the right

    # trap plane is related to DMD by FFT from Fresnel
    x_trap = waveLength*focalLength*np.fft.fftshift(np.fft.fftfreq( 2*N, dx_dmd ))

    print('Done! The grid size is {} ({:e} sec.)\n'.format( x_dmd.size, time.time()-t0 ))
    return x_dmd, x_trap



def computeHelmholtzGrid(scanGridShape=dmdShape, nyquistParameter=2, padFactor=0, ccd=False):
    '''
    Returns the grid x centered at 0 that sufficiently samples both the DMD and trap planes for use with Helmholtz. scanGridShape is the number of DMD pixels to be covered by the grid in each dimension and can differ dmdShape. Enhance the grid size by 2**padFactor along each dimension

    Returns: x
    '''
    print('Computing the Helmholtz grid on {:d} DMD pixels...'.format(scanGridShape))
    t0 = time.time()

    dx_dmd = dmdPixel/nyquistParameter # DMD grid spacing
    Dx_dmd = dmdPixel*scanGridShape # lower bound of DMD grid length

    dx_trap = waveLength*focalLength/(nyquistParameter*Dx_dmd) # DMD and trap field sizes and
    Dx_trap = waveLength*focalLength/dmdPixel                  # bandwidths are related by Fourier
    if ccd: # ensure the CCD is being adequately sampled
        dx_trap = min( dx_trap, ccdPixel/nyquistprameter )

    dx = min( dx_dmd, dx_trap ) # DMD and trap grids are the
    Dx = max( Dx_dmd, Dx_trap ) # same so sample both sufficiently

    Dx = dx * 2**np.ceil( np.log2(Dx/dx) ) # next power of 2
    Dx *= 2**padFactor # pad to avoid aliasing

    N = int(Dx/dx/2) # number of grid points in both directions using int to eliminate rounding errors
    x = dx*np.arange( -N, N ) # take care to not repeat left boundary on the right

    print('Done! The grid size is {} ({:e} sec.)\n'.format( x.size, time.time()-t0 ))
    return x



def doubleFresnelGrid(x_dmd, dmd, trap, order=3):
    '''
        (1) Pads x_trap with linear ramps
        (2) Computes the new x_dmd using FFT relationship (halves grid spacing)
        (3) Pads each array in trap with zeros
        (3) Interpolates each array in dmd to new grid using spline of order 'order'

    Returns: x_dmd, x_trap, dmd, trap
    '''
    if isinstance(dmd,tuple):  # tuples are
        dmd = list(dmd)        # immutable
    if isinstance(trap,tuple): # but lists
        trap = list(trap)      # aren't

    dx_dmd = x_dmd[1] - x_dmd[0]
    pad = int(x_dmd.size/2)

    # create interpolants for each array in/the array dmd
    if isinstance(dmd,list):
        interpolants = [ UnivariateSpline(x_dmd, a, k=order, s=0, ext=1) for a in dmd ]
    else:
        interpolant = UnivariateSpline(x_dmd, dmd, k=order, s=0, ext=1)

    x_dmd = dx_dmd/2*np.arange( -x_dmd.size, x_dmd.size ) # compute new x_dmd
    x_trap = waveLength*focalLength*np.fft.fftshift(np.fft.fftfreq( x_dmd.size, dx_dmd/2 )) # compute new x_trap

    # pad each array in/the array trap with zeros
    if isinstance(trap,list):
        for i, a in enumerate(trap):
            trap[i] = np.pad( a, (pad,), 'constant', constant_values=(0,) )
    else:
        trap = np.pad( trap, (pad,), 'constant', constant_values=(0,) )

    # interpolate each array in/the array dmd
    if isinstance(dmd,list):
        for i in range(len(dmd)):
            dmd[i] = interpolants[i](x_dmd)
    else:
        dmd = interpolant(x_dmd)

    return x_dmd, x_trap, dmd, trap



def doubleHelmholtzGrid(x, aList):
    '''
    Pads:
        (1) x with a linear ramp
        (2) all arrays in aList with zeros (e.g. a_dmd, a_target, aberrations)

    Returns: x, aList
    '''
    if isinstance(aList,tuple): # tuples are immutable
        aList = list(aList)     # but lists aren't

    # double x
    pad = int(x.size/2)
    x = (x[1]-x[0]) * np.arange( -x.size, x.size )

    # pad each array in/the array aList with zeros
    if isinstance(aList,list):
        for i, a in enumerate(aList):
            aList[i] = np.pad( a, (pad,), 'constant', constant_values=(0,) )
    else:
        aList = np.pad( aList, (pad,), 'constant', constant_values=(0,) )

    return x, aList



def lattice(x, nCells=4, d=30*ccdPixel, s=10*ccdPixel):
    '''
    Equations 18 and 19 in Toerma, Huber, ...
    N.B. returns the amplitude proportional to the square root of the potential

    Returns: a_target
    '''
    f = lambda x0: np.exp( -(x-x0)**2/(2*s**2) )
    V = sum([ f(x0) for x0 in d*np.arange(-nCells/2, nCells/2) ])
    return np.sqrt(V)

######################################
## PREPARE FOR HOLOGRAM COMPUTATION ##
######################################

def prepareDMD(x_dmd, a_dmd, dmd=None, scanGridShape=dmdShape):
    '''
    Impose finite DMD on a_dmd (and all other arrays in dmd) and normalize a_dmd

    Returns: a_dmd    or    ( a_dmd, dmd )
    '''
    if isinstance(dmd,tuple):
        dmd = list(dmd)

    a_dmd = a_dmd.copy()
    
    bounds = dmdPixel*np.array([ -scanGridShape-1, scanGridShape-1 ])/2
    a_dmd *= (bounds[0]<=x_dmd) * (x_dmd<=bounds[1])
    # a_dmd[ np.abs(x_dmd) > dmdPixel*scanGridShape/2 ] = 0 # finite, rectangular DMD centered at (0, 0)
    a_dmd /= np.sqrt(np.sum(a_dmd**2)) # normalize

    if dmd is None:
        return a_dmd
    elif isinstance(dmd,list):
        for i in range(len(dmd)):
            dmd[i] *= (bounds[0]<=x_dmd) * (x_dmd<=bounds[1])
    else:
        dmd *= (bounds[0]<=x_dmd) * (x_dmd<=bounds[1])

    return a_dmd, dmd



def prepareTarget(a_target, ctrl=0.5, offset=0.15, doubled=True):
    '''
    Add Gaunt & Hadzibabic's offset and normalize a_target

    Returns: a_target
    '''
    a_target = a_target.copy()

    # add offset (Gaunt & Hadzibabic Eq. 3 in methods)
    if doubled: # grid has already been doubled (a_target is an output of either doubleFresnelGrid or doubleHelmholtzGrid)
        ctrl = slice( int(a_target.size*(0.5-ctrl/2)), int(a_target.size*(0.5+ctrl/2)) )
        ctrl = slice(int(a_target.size/4), int(a_target.size*3/4)) # indices of control region at the center of the grid
        a_target[ctrl] = np.sqrt( a_target[ctrl]**2 + (offset*a_target.max())**2 )
    else: # grid has not yet been doubled
        a_target = np.sqrt( a_target**2 + (offset*a_target.max())**2 )

    a_target /= np.sqrt(np.sum(a_target**2)) # normalize power in trap plane to incident power

    return a_target



def computeMoments(x, u, beamParameters=False):
    '''
    Computes the center and effective exp(-2) beam waist of the field u

    Returns: x0, w
    '''
    I = np.abs(u)**2/np.sum(np.abs(u)**2) # normalized intensity
    x1 = np.sum( x*I ) # first moment (center)
    x2 = np.sum( x**2*I ) # second moment
    w = 2*np.sqrt( x2 - x1**2 ) # exp(-2) waist
    return x1, w



def parabolicPhaseGuess(x_dmd, u_dmd, u_trap, x_trap=None, returnLensWaist=False):
    '''
    Approximate the amplitudes of u_dmd and u_trap/u_target (back and front focal plane of Fourier lens) as gaussian beams and compute the parabolic phase on u_dmd that produces the correct amplitude of u_trap (Gaunt & Hadzibabic):
        u_dmd ~ exp( -(x_dmd-x0_dmd)**2*(w_dmd**-2 + 2j*pi/R**2) + 2j*pi*B*(x_dmd-x0_dmd) )
        u_trap ~ exp( -(x_trap-x0_trap)**2/w_trap**2 )
        R = w_dmd * np.sqrt(2*np.pi*waveLength*focalLength) / ( (np.pi*w_dmd*w_trap)**2 - (waveLength*focalLength)**2 )**0.25
        B = x0_trap/(waveLength*focalLength)

    Returns: ( kinoformGuess, waist_lens )    or    kinoformGuess
    '''
    print('Computing the parabolic phase guess...')
    t0 = time.time()

    if x_trap is None: x_trap = x_dmd # use Helmholtz grid

    x0_dmd, w_dmd = computeMoments(x_dmd, u_dmd)
    x0_trap, w_trap = computeMoments(x_trap, u_trap)

    if np.pi*w_dmd*w_trap <= waveLength*focalLength: # avoid imaginary phase curvature
        print('Warning: no phase curvature in parabolic phase initial guess')
        print('Should satisfy: pi * waist_dmd * waist_target > waveLength * focalLength')
        R = np.inf
    else:
        R = w_dmd * np.sqrt(2*np.pi*waveLength*focalLength) / ( (np.pi*w_dmd*w_trap)**2 - (waveLength*focalLength)**2 )**0.25

    B = x0_trap/(waveLength*focalLength)

    kinoformGuess = 2*np.pi*( (x_dmd-x0_dmd)**2/R**2 + B*(x_dmd-x0_dmd) )

    waist_lens = np.sqrt( w_dmd**2 + w_trap**2 - 2/np.pi*np.sqrt( (np.pi*w_dmd*w_trap)**2 - (waveLength*focalLength)**2 ) )

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    if returnLensWaist: return kinoformGuess, waist_lens
    else:               return kinoformGuess

######################
## FIGURES OF MERIT ##
######################

def computeError(u, a, mask=np.array([])):
    '''
    Spatially-resolved relative error of the intensity of a complex field u in reference to the intensity of a target amplitude a. mask are indices of the field to remove before computing the error. Note that Gerchberg and Saxton showed that the error
        (np.abs(u) - a) / u.size
    decreases (or remains constant) with each iteration, not the definition used here.

    Returns: error
    '''
    if len(mask) != 0:
        u = np.ma.masked_array(u, mask)
        a = np.ma.masked_array(a, mask)
    U = np.abs(u)**2/np.sum(np.abs(u)**2)
    A = a**2/np.sum(a**2)
    return U/A - 1 # Pasienski & DeMarco Eq. 2
    # return (U - A)/A.mean()



def computeEfficiency(u, idx):
    '''
    Computes the ratio of power in the control region (given by the slice or index list idx) to the total power in the output field u

    Returns: efficiency
    '''
    return np.sum( np.abs(u[idx])**2 ) / np.sum( np.abs(u)**2 ) # Pasienski & DeMarco paragraph after Eq. 3



def computeFootprint(x, u, P=0.99):
    '''
    Returns the footprint of a complex field u on the real or reciprocal space grid x: the length Dx of region within which a fraction P of the total power is contained (like matlab's obw):
        int( |u(x)|**2, x, -Dx/2, Dx/2 ) = P*int( |u(x)|**2, x, -inf, inf )
    or in radial coordinates:
        int( |u(r)|**2, r, 0, Dx ) = P*int( |u(r)|**2, r, 0, inf )

    Returns: Dr
    '''
    r = np.sqrt( (x-x.mean())**2 ) # radial position of each point
    I = np.abs(u.ravel())**2/np.sum(np.abs(u)**2) # intensity at each point
    idx = np.argsort(r)
    r = r[idx] # sorted radial positions
    E = np.cumsum(I[idx]) # normalized energy = E(r) = int( I(phi,r'), {phi,0,2*pi}, {r',0,r} )
    idx = np.argwhere(E>P).ravel()[0] # first point where normalized energy contained within a region is greater than P
    return 2*r[idx] # radius at this point (Dx)



def checkSampling(x, u, plot=False, P=0.99):
    '''
    Check that the field u is sufficiently sampled in real and reciprocal space according to Nyquist's theorem using a footprint fraction P.
    '''
    Ny = 2/np.pi*erfinv(P)**2 # effective Nyquist parameter
    print('Checking whether Nyquist\'s theorem is fulfilled for P={} (Ny={})'.format(P,Ny))
    t0 = time.time()

    nu = np.fft.fftshift(np.fft.fftfreq( x.size, x[1]-x[0] )) # reciprocal space grid
    dx_grid = x[1] - x[0] # real space grid spacing
    Dx_grid = x[-1] - x[0] # real space grid length
    dnu_grid = nu[1] - nu[0] # reciprocal space grid spacing
    Dnu_grid = nu[-1] - nu[0] # reciprocal space grid length
    Dx_field = computeFootprint(x, u, P) # real space
    Dnu_field = computeFootprint(nu, ft(u), P) # reciprocal space
    print('Must fulfill the following inequalities:')
    print('Ny * dx_grid * Dnu_field < 1: {:e}'.format(Ny*dx_grid*Dnu_field))
    print('Ny * dnu_grid * Dx_field < 1: {:e}'.format(Ny*dnu_grid*Dx_field))
    if plot: # overlay the field and its footprint in real and reciprocal space
        pp.figure()
        pp.plot(x*1e3, np.abs(u)**2/np.max(np.abs(u)**2))
        pp.axvline(-Dx_field/2*1e3, color='k')
        pp.axvline(+Dx_field/2*1e3, color='k')
        pp.title('Real space')

        pp.figure()
        pp.plot(nu*1e-3, np.abs(ft(u))**2/np.max(np.abs(ft(u))**2))
        pp.axvline(-Dnu_field/2*1e-3, color='k')
        pp.axvline(+Dnu_field/2*1e-3, color='k')
        pp.title('Reciprocal space')

        pp.show()

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))

##########################
## HOLOGRAM COMPUTATION ##
##########################

def naive(a_target, opticalSystem):
    '''
    Computes the hologram naivly as the phase of the IFFT of the (real) target amplitude

    Returns: kinoform
    '''
    u_dmd = opticalSystem.backPropagate(a_target)
    kinoform = np.unwrap(np.angle(u_dmd))
    kinoform -= kinoform.min()
    return kinoform



def gs(a_dmd, a_target, kinoformGuess, opticalSystem, nIterations=1, printUpdates=False):
    '''
    Performs the Gerchberg-Saxton algorithm and returns the kinoform, field in the trap plane, spatially-resolved relative intensity error, and RMS relative intensity error at each iteration

    Returns: kinoform, u_trap, error, rmsError
    '''

    # check for correct inputs for correct size 2**n
    if (a_dmd.size & (a_dmd.size-1) != 0) or (a_target.size & (a_target.size-1) != 0):
        raise ValueError('Grid size must have size 2**n for integer n')

    print('Running Gerchberg-Saxton...')
    t0 = time.time()
    print('iteration/{}, RMS error, time (sec.)'.format(nIterations))

    mask = np.isclose(a_target, 0) # mask indices in computeError (don't use if possible, fancy indexing is slow)

    u_dmd = a_dmd*np.exp(1j*kinoformGuess)
    rmsError = np.zeros(nIterations) # RMS relative intensity error at each iteration Pasienski & DeMarco Eq. 2
    u_trap = np.zeros_like(u_dmd) + 0j # preallocate for in-place operations
    for i in range(nIterations):
        u_trap[:] = opticalSystem.forwardPropagate(u_dmd)
        rmsError[i] = np.sqrt(np.mean( computeError(u_trap, a_target, mask)**2 ))
        u_trap[:] = a_target*np.exp( 1j*np.angle(u_trap) )
        u_dmd[:] = opticalSystem.backPropagate(u_trap)
        u_dmd[:] = a_dmd*np.exp( 1j*np.angle(u_dmd) )
        if printUpdates: print('{} {:e} {:e}'.format( i+1, rmsError[i], time.time()-t0 ))

    kinoform = np.unwrap(np.angle(u_dmd))
    kinoform[np.isclose(a_dmd, 0)] = 0 # no kinoform outside the DMD
    kinoform -= kinoform.min() # make kinoform positive
    u_trap = opticalSystem.forwardPropagate(u_dmd)
    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    return kinoform, u_trap, computeError(u_trap, a_target, mask), rmsError




def omraf(a_dmd, a_target, kinoformGuess, opticalSystem, ctrl=0.5, nIterations=1, mixingParameter=0.4, printUpdates=False):
    '''
    Performs the OMRAF (Offset Mixed Region Amplitude Freedom) algorithm and returns the kinoform, field in the trap plane, spatially-resolved relative intensity, and RMS error at each iteration. Arguments are outputs of prepareInputs with doubleGrid=True.

    Returns: kinoform, u_trap, error, rmsError
    '''

    # check inputs for correct size 2**n
    if (a_dmd.size & (a_dmd.size-1) != 0) or (a_target.size & (a_target.size-1) != 0):
        raise ValueError('Grid size must have size 2**n for integer n')

    print('Running OMRAF...')
    t0 = time.time()
    print('iteration/{}, RMS error, time (min.)'.format(nIterations))

    # indices of control (canvas) region at the center of the grid
    ctrl = slice( int(a_target.size*(0.5-ctrl/2)), int(a_target.size*(0.5+ctrl/2)) )

    measure = ctrl # a_target>0 everywhere in control region if offset>0
    # measure = np.isclose(a_target, 0) # mask indices in computeError (don't use if possible, fancy indexing is slow)

    u_dmd = a_dmd*np.exp(1j*kinoformGuess)
    rmsError = np.zeros(nIterations) # RMS relative intensity error at each iteration Pasienski & DeMarco Eq. 2
    u_trap = np.zeros_like(u_dmd) + 0j # preallocate for in-place operations
    u_trap_tmp = np.zeros_like(u_dmd) + 0j # temporary array for manipulating control and free regions separately
    for i in range(nIterations):
        u_trap_tmp[:] = opticalSystem.forwardPropagate(u_dmd)
        rmsError[i] = np.sqrt(np.mean( computeError(u_trap_tmp[measure]/mixingParameter, a_target[measure])**2 ))
        u_trap[:] = (1-mixingParameter)*u_trap_tmp # set free region
        u_trap[ctrl] = mixingParameter*a_target[ctrl]*np.exp( 1j*np.angle(u_trap_tmp[ctrl]) ) # set control region
        u_dmd[:] = opticalSystem.backPropagate(u_trap)
        u_dmd[:] = a_dmd*np.exp( 1j*np.angle(u_dmd) )
        if printUpdates: print('{} {:e} {:e}'.format(i+1, rmsError[i], time.time()-t0 ))

    kinoform = np.unwrap(np.angle(u_dmd))
    kinoform[ np.isclose(a_dmd, 0) ] = 0 # no kinoform outside the DMD TODO change this to nan instead?
    kinoform -= kinoform.min() # make kinoform positive
    kinoform[ np.isclose(a_dmd, 0) ] = 0
    u_trap = opticalSystem.forwardPropagate(u_dmd)
    print('Done! ({:e} min.)\n'.format( time.time()-t0 ))
    return kinoform, u_trap, computeError(u_trap[measure]/mixingParameter, a_target[measure]), rmsError

#############################
## HOLOGRAM REPRESENTATION ##
#############################

# TODO handle DMD pixels that are off on the DMD
# TODO for simulation grid pixels whose averaged phasor is zero, interpolate between neighboring points
# TODO add Philipp's randomization
def computeHologram(x, kinoform, scanGridShape=dmdShape, order=3, supergratingPitch=10, checkRepresentation=False):
    '''
    Returns the hologram/grating function either as an array of size scanGridShape to feed to the DMD or of size kinoform.shape==x.shape for use with the output of gs

    Returns: hologram
    '''
    print('Computing the hologram from the kinoform...')
    t0 = time.time()

    if x[-1]-x[0] < dmdPixel*scanGridShape:
        raise ValueError('The grid is smaller than the scan grid shape. Did you forget to pass scanGridShape as a parameter?')

    # positions of the centers of each mirror in the scan grid centered on the simulation grid
    x_scan = dmdPixel*np.arange( -scanGridShape/2, scanGridShape/2 )

    # positions of the edges of each mirror in the scan centered on the simulation and scan grids
    x_edge = dmdPixel*np.arange( -(scanGridShape+1)/2, (scanGridShape+1)/2 )

    # positions of the centers of each mirror on the DMD centered on the simulation and scan grids
    x_dmd = dmdPixel*np.arange( -dmdShape/2, dmdShape/2)

    kinoform = np.exp(1j*kinoform) # represent phase on the unit circle for interpolation/averaging

    # kinoform_scan = kinoform fitted to the scan grid (x_scan, y_scan)
    if x[1]-x[0]>=dmdPixel: # interpolate if grid spacing is coarser than the DMD pixel size...
        kinoform_scan = UnivariateSpline(x, kinoform, k=order, s=0, ext=1)(x_dmd)
    else: # ...otherwise average kinoform over each DMD pixel in the scan
        idx = np.array([ # idx[i] = slice of grid points x in mirror at x_scan[i]
            slice( np.argwhere(x_edge[i]<=x)[0,0], np.argwhere(x<=x_edge[i+1])[-1,0]+1 )
            for i in range(scanGridShape) ])
        # average kinoform over simulation grid points idx[i] within each mirror i
        kinoform_scan = np.array([ kinoform[ idx[i] ].mean() for i in range(scanGridShape) ])

    if checkRepresentation: # plot the kinoform spread over each mirror
        dphi = np.sqrt(-2*np.log( np.abs(kinoform_scan) ))/(2*np.pi) # dphi = sqrt( -2*log(|<exp(i*phi)>|) )
        pp.figure()
        pp.plot(x_scan/dmdPixel, dphi)
        pp.title('kinoform stdev on each mirror\n$\\langle\\delta\\phi/2\\pi\\rangle = {:.4f}$'.format( np.nanmean(dphi) ))
        pp.xlabel('DMD pixel number')
        pp.show()
        print('Representation error = <dphi/2pi> = {:.4f}'.format( np.nanmean(dphi) ))

    kinoform_scan = np.angle(kinoform_scan) # transform back to phase

    kg = 2*np.pi/(supergratingPitch*dmdPixel) # supergrating wavevector
    gratingPhase = kg*x_scan + kinoform_scan
    g = np.where( np.cos(gratingPhase/subDiffractionOrder)>=0, on, 1-on ).astype(int) # grating function on the scan grid
    pad = int((dmdShape-scanGridShape)/2)                          # turn off pixels
    pad = ( pad, dmdShape-scanGridShape-pad )                      # outside the
    g = np.pad( g, pad, mode='constant', constant_values=(1-on,) ) # scan grid

    print('Interpolating the hologram to fit the grid... ({:e} sec.)'.format( time.time()-t0 ))
    g = interp1d(                 # grating function is constant within
        x_dmd, g, kind='nearest', # each mirror so method='nearest'
        bounds_error=False, fill_value=(~on)) # reflectivity is zero (off=~on) outside the DMD
    g = g(x).astype(int)

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    return g



def computeFineHologram(x, kinoform, scanGridShape=dmdShape, supergratingPitch=10):
    '''
    Compute the hologram of a DMD with the same pixel size as the simulation grid for determining the representation error

    Returns: fineHologram
    '''
    print('Computing the fine hologram from the kinoform...')
    t0 = time.time()

    kg = 2*np.pi/(supergratingPitch*dmdPixel) # supergrating wavevector
    gratingPhase = kg*x + kinoform
    g = np.where( np.cos(gratingPhase/subDiffractionOrder)>=0, 1, 0 ).astype(int)
    bounds = dmdPixel*np.array([ -scanGridShape-1, scanGridShape-1 ])/2
    g *= (bounds[0]<=x) * (x<=bounds[1])

    print('Done! ({:e} sec.)\n'.format( time.time()-t0 ))
    return g
