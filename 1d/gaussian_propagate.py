'''
holography.Helmholtz should produce a similar beam profile as the analytically calculated gaussian beam in the paraxial approximation. Checks:
- computeHelmholtzGrid
- Helmholtz

maximum propagation length z such that the waist of the image field is no larger than w_max:
    z = zR*np.sqrt((w_max/w0)**2 - 1)
'''
import numpy as np
import matplotlib.pyplot as pp
import holography as h

x = h.computeHelmholtzGrid(1024)
w0 = 1e-4 # waist of field at z=0
u0 = np.exp( -(x/w0)**2 )
zR = np.pi*w0**2/h.waveLength
z = zR*np.array([0.1, 1, 10, 30])

def gaussianBeam(z):
    w = w0*np.sqrt(1 + (z/zR)**2)
    if z==0: R = np.inf
    else: R = z + zR**2/z
    gouy = np.arctan2(z,zR)
    # np.sqrt(w0/w) instead of w0/w since there is only one transverse direction, not two
    return np.sqrt(w0/w) * np.exp(-(x/w)**2) * np.exp( 2j*np.pi/h.waveLength*(z + x**2/(2*R)) - 1j*gouy )

pp.figure()
for iz, zi in enumerate(z):
    u1 = gaussianBeam(zi)
    u2 = h.Helmholtz(x, zi).forwardPropagate(u0)
    pp.subplot(2,2,iz+1)
    pp.plot( x*1e3, np.abs(u1), 'b-',
             x*1e3, np.abs(u2), 'r-',
             x*1e3, u1.real, 'b--',
             x*1e3, u2.real, 'r--')
    pp.title('z/zR={:.1f}'.format(zi/zR))
    pp.legend(['|Gaussian|', '|Helmholtz|', 'Re(Gaussian)', 'Re(Helmholtz)'])
pp.show()
