'''
Does the kinoform change smoothly with changing offset in OMRAF? Determine qualitatively by plotting kinoform(x, offset) for all x. If smooth, then plot kinoform(x0, offset) for various random points on the DMD x0.

Use gaussian as target since the "exact" kinoform is known for Fresnel

https://en.wikipedia.org/wiki/Richardson_extrapolation
'''
import numpy as np
import matplotlib.pyplot as pp
from matplotlib.cm import viridis
import holography as h
import time

############### BEGIN INPUTS ###############
scanGridShape = 1024
ccd = False
w0_dmd = 3.47e-3
nCells = 5
x0_target = 0
w0_target = 300e-6
ctrl = 0.2
# offset = np.arange(0.05, 0.15, 0.001)
offset = np.logspace(-5, -0.7, 1000)
points = np.linspace(0.5-ctrl/2, 0.5+ctrl/2, 20)
doubled = True
nIterations = 200
# supergratingPitch = 8
############### END INPUTS ###############

x_dmd, x_trap = h.computeFresnelGrid(scanGridShape, ccd=ccd)
a_dmd = np.exp( -(x_dmd/w0_dmd)**2 )
a0_target = np.exp( -(x_trap - x0_target)**2/w0_target**2 )
# a0_target = h.lattice(x_trap, nCells)
points = np.sort((x_dmd.size*points).astype(int))

if doubled: x_dmd, x_trap, a_dmd, a0_target = h.doubleFresnelGrid(x_dmd, a_dmd, a0_target)
a_dmd = h.prepareDMD(x_dmd, a_dmd, scanGridShape=scanGridShape)
a0_target = h.prepareTarget(a0_target, ctrl, 0, doubled)
kinoform0 = h.parabolicPhaseGuess(x_dmd, a_dmd, a0_target, x_trap=x_trap)
opticalSystem = h.Fresnel()

points_data = np.zeros((offset.size, points.size))

f1 = pp.figure(); pp.title('Kinoform');       pp.xlabel('DMD pixel number')
f2 = pp.figure(); pp.title('RMS Error');      pp.xlabel('OMRAF iteration number')
f3 = pp.figure(); pp.title('$|U_{trap}|^2$'); pp.xlabel('CCD pixel number')
colors = [ viridis(c) for c in np.linspace(0, 1, offset.size) ]

t0 = time.time()
for i, o in enumerate(offset):
    a_target = h.prepareTarget(a0_target, ctrl, offset=o, doubled=doubled)
    k = h.parabolicPhaseGuess(x_dmd, a_dmd, a_target, x_trap=x_trap)
    k, u_trap, error, rmsError = h.omraf(a_dmd, a_target, k, opticalSystem, ctrl, nIterations)
    points_data[i,:] = k[points]
    # label='$\\Delta={:.2f}$'.format(o)
    f1.gca().plot(x_dmd/h.dmdPixel, k/(2*np.pi), color=colors[i], zorder=offset.size-i)
    f2.gca().semilogy(rmsError, color=colors[i], zorder=offset.size-i)
    f3.gca().semilogy(x_dmd/h.dmdPixel, np.abs(u_trap)**2, color=colors[i], zorder=offset.size-i)
    print('{:.1f}% Estimated time until completion: {:e} seconds'.format( i/offset.size*100., (time.time()-t0)/(i+1)*(offset.size-i-1) ))
print('Everything took {:e} seconds'.format(time.time()-t0))

f1.gca().plot(x_dmd/h.dmdPixel, kinoform0/(2*np.pi), 'k', zorder=offset.size+1, label='Exact')
f1.gca().set_xlim(-scanGridShape/2, scanGridShape/2)
f1.gca().legend()
# f2.gca().legend(offset)
# f3.gca().legend(offset)

f4 = pp.figure(); pp.title('Kinoforom at various points'); pp.xlabel('Offset')
colors = [ viridis(c) for c in np.linspace(0, 1, points.size) ]
for i, c in enumerate(colors):
    # label = '{:.2f}'.format(points/x_dmd.size)
    f4.gca().loglog(offset, points_data[:,i], color=c)

pp.show()
