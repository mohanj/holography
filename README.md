# Holography

This package implements various algorithms to compute both amplitude and phase holograms that a DMD (or phase-only SLM like liquid crystal) must imprint on a beam to generate arbitrary fields in the output plane of an optical system.

The algorithms are
- onestep [Zupancic et al., Opt. Express 24, 13 (2016)]
- Gerchberg-Saxton [Gerchberg & Saxton, Optik 35, 237 (1972)]
- OMRAF [Gaunt & Hadzibabic, Scientific Reports 2, 721 (2012)]

The modular propagation methods are motivated by the operator notation in Goodman 5.4

