#!/usr/bin/env python
# -*- coding: utf8 -*-

'''
This module contains functions we use to :
--> scan a wavefront :
-------- create a grid
-------- create 'DMD Aperture' images
-------- send these images to the DMD
-------- run a sequence of display aperture on DMD - take a picture with CCD

--> fit CCD images from the scan :
-------- fit a spot with a 2d gaussian to find its center position and the intensities

--> calculate the wavefront (phase profile) :
-------- reconstruct wavefront from tangential plane
-------- fit wavefront with Zernike polynomials

--> calculate the intensity profile :
-------- fit intensities measurement with a 2d gaussian
-------- define a perfect 2d gaussian target from the intensity profile

--> create holograms :
-------- create phase map
-------- create local width map
-------- create hologram from the previous two

--> beam shapping :
-------- create HG (Hermite-Gaussian) modes
-------- create LG (Laguerre-Gaussian) modes

--> various plotting...

--> save all data for further use or archiving

Written by S. de Leseleuc (sylvain.dldk(at)gmail(dot)com)
Modified by Jeff Mohan
Last Modified : 26 October 2017
'''


#Misc
import numpy as np
import time # to time different functions
import datetime # to create new folders named with the current datetime
import pickle # to serialize object (notably our Data object) and save them
import os
import sys
import progressbar # to represent progression of long-time processes in the console
import pdb

import copy # to copy objects

#Reading image
from scipy.misc import imread, imsave
from PIL import Image, ImageStat # to fast statistics on a PIL object

#Scipy methods
from scipy.optimize import leastsq # for fitting
from scipy import interpolate # for interpolation of data on a larger grid (phase map, intensity map ...)
from scipy.misc import factorial as fac

#Plotting
from matplotlib import pyplot as plt
from matplotlib.pyplot import quiver # special module for arrows plot. useful for plotting the displacement of the spots
from matplotlib import cm # import colormap for plots
from mpl_toolkits.mplot3d import Axes3D # module for 3d plotting
plt.ioff() # ensure that pyplot is not in interactive mode

#DMD
sys.path.append('C:\\Users\\quantumoptics\\Documents\\DMD\\DMD_programming\\dmdcom\\') # put fourierTransform_functions in the path
import communication  #  import the script interfacing with the DMD
import imgFunc # some image functions
import imgFunc_FT # some image functions specific to Fourier work

#Camera
import camera # import the script interfacing with the CCD

#Hermite and laguerre
from scipy.special import hermite # hermite functions
from scipy.special import genlaguerre # generalized laguerre functions

#For plots of complex numbers
from colorsys import hls_to_rgb

#For Debugging
##import ipdb
##from ipdb import launch_ipdb_on_exception
##from memory_profiler import profile

# =======================
# = Constants =
# =======================

# dataPath = 'C:\\Users\\VP-DMD\\Desktop\\data\\'
dataPath = 'C:\\Users\\quantumoptics\\Documents\\DMD\\DMD_programming\\wavefront_correction\\data\\'
saveFormat = '.png'

wavelength = 671e-9 #m
CCDpixelSize = 3.75e-6 #m
mirrorSize = 13.68e-6 #m
focal_length = 100e-3 #m
telescope = 1 #1/Magnification
diff_order = 1 # which subdiffraction order of the supergrating is used
#Beam
waist_beam = 254. # approximate waist of the beam on the DMD
on = 0 # what value must be passed to the DMD for a pixel to be on (blazed)

# k = M/diff_order*Dccd*2*pi/(wavelength*f)
# Dccd = (diff_order/M)*wavelength*f*k/(2*pi)

# x_ccd and y_ccd are CCD coordinates as the beam profiler/python GUI reads them
# x_dmd and y_dmd are DMD coordinates as accessed by the python api
# R(phi) is a rotation by the phi counter-clockwise
# transformation: [x_dmd, y_dmd].T = [[(-1)**flipX, 0], [0, (-1)**flipY]] * R(phi) * [x_ccd, y_ccd].T
angle_CCDtoDMD = 135 # rotate CCD by angle_CCDtoDMD to get to DMD coordinates before doing reflections
flipX = True # True if number of reflections that preserve x is odd, False if even (not including the telescope)
flipY = False # True if number of reflections that preserve y is odd, False if even (not including the telescope)

# ======================
# = Random functions =
# ======================

def calculate_FT_radius(radius):
	'''
	Given the radius of a spot, return the radius of the Fourier-transformed spot by the Fourier lens

	Parameter :
	--> radius : in DMD mirror unit

	Return :
	--> FT_radius, FT_radius_pixel : the Fourier-transformed radius in meters and in CCD pixel unit

	Method :
	--> translate the radius (mirror unit) in meters unit
	--> add the effect of the telescope on the radius
	--> obtain the radius after Fourier Transform FT by : w(FT) = wavelength*focal_length/(pi*w)
	--> translate the FT_radius in CCD pixel unit
	'''
	radius_m = radius*mirrorSize #m
	radius_m_after_telescope = radius_m/abs(telescope)
	# FT_radius = wavelength*focal_length/np.pi/radius_m_after_telescope
	FT_radius = 3.83*wavelength*focal_length/(2*np.pi*radius_m_after_telescope)
	FT_radius_pixel = FT_radius/CCDpixelSize
	return FT_radius, FT_radius_pixel

def calculate_iFT_radius(FT_radius):
	'''
	Do the inverse operation of calculate_FT_radius
	'''
	radius_m_before_telescope = wavelength*focal_length/np.pi/FT_radius
	radius_m = radius_m_before_telescope*abs(telescope)
	radius = radius_m/mirrorSize
	return radius

def calculate_grid_span(x0, y0, n_step, step, radius):
	X, Y= init_grid(x0, y0, n_step, step)
	return X.min()-radius, Y.min()-radius, X.max()+radius, Y.max()+radius

def calculate_separation_order(spacing):
	'''
	Given a grating spacing on the DMD, return the separation between 0 and 1st order of diffraction

	Parameter :
	--> spacing : in mirror number

	Return :
	--> dtetha, dx, dpixel : separation in (angle, meter on CCD, pixel on CCD)
	'''

	dtheta = np.arcsin(wavelength/spacing/mirrorSize)
	dx = dtheta * abs(telescope) *focal_length
	dpixel = dx/CCDpixelSize
	return dtheta, dx, dpixel

def calculate_resolution_limit(step, binning, resolution_factor):
	'''
	Given the grid step, the binning number of the CCD and the resolution_factor use before fitting, return the resolution of the wavefront

	Parameter :
	--> step : grid step. distance between two sampling apertures
	--> binning : binning number of the CCD. depends on its mode.
	In 1280x960 mode : binning = 1
	In 640x480 mode : binning = 2
	--> resolution_factor : before fitting, the resolution of the image will be decreased by this factor to fasten the fitting

	Return :
	--> dfit, dtheta, dwavefront : resolution of (single fit in meters, wavefront normal in angle, wavefront in wavelength unit)
	'''

	dfit = binning*resolution_factor*CCDpixelSize
	dtheta = dfit/abs(telescope)/focal_length
	dwavefront = dtheta * step * mirrorSize / wavelength
	return dfit, dtheta, dwavefront

def updateFolderPath(dataPath):
	'''
	Create a new folder to save data.

	Parameters :
	--> dataPath : path to where you want all your datas to be saved

	Return :
	--> the updated folderPath

	Do :
	--> check if the daily folder is created. if not, create it
	--> create a new folder by giving it the hour - minute name. if one already exist, create one with the name : hour - minute - second
	'''
	folderPath = dataPath

	today = datetime.datetime.now()
	todayPath= today.strftime("%m")+'_'+today.strftime("%d")+"\\"
	folderPath += todayPath

	if not os.path.exists(folderPath):
		os.makedirs(folderPath)

	minutePath = today.strftime("%H")+"-"+today.strftime("%M")+"\\"
	secondPath = today.strftime("%H")+"-"+today.strftime("%M")+"-"+today.strftime("%S")+"\\"

	if not os.path.exists(folderPath+minutePath):
		folderPath += minutePath
		os.makedirs(folderPath)
	else :
		folderPath += secondPath
		os.makedirs(folderPath)

	return folderPath

# ======================
# = Scanning functions =
# ======================

def init_grid(x0, y0, n_step, step):
	'''
	Create a grid for scanning the wavefront.

	Arguments:
	-> x0, y0 -- grid center position
	-> n_step -- we create a square grid, n_step is the number of sample along one axis of the grid
	-> step -- spacing (in mirror numbers) between two samples

	Return:
	-> X, Y -- the grid
	'''

	n_step = int(n_step)
	X = x0 + step*np.linspace(-n_step/2+1, n_step/2, n_step)
	Y = y0 + step*np.linspace(-n_step/2+1, n_step/2, n_step)
	X, Y = np.meshgrid(X, Y)
	return X, Y

def scan(X, Y, radius, hologram, folderPath, mode_cam, shutter_cam, gain_cam, saveImage=False, ROI=None, resolution_factor=2, binning=1):
	'''
	Scan the wavefront by displaying images stored on the DMD and taking images from the CCD

	Parameters :
	--> folderPath : where to save images
	--> X, Y : the grid containing the center's position of all the sampling aperture
	--> shutter_cam_init : shutter time for the camera
	--> gain_cam : gain for the camera
	--> cameraMode : mode of the CCD
	--> saveImage : True or False. it is not recommended to save the images of all the spot. But can be useful for debugging.
	'''

	t1 = 0.7 # buffer time between startProj and beginning of takePicture sequence [sec.]
	t2 = 0.4 # duration of takePicture sequence where pictures are taken continuously [sec.]
	t3 = 0.1 # buffer time after takePicture sequence [sec.]
	t4 = 1e-5 # buffer time between each takePicture [sec.]

	if t2<shutter_cam*1.1e-3: # ensure enough time for at least one picture
		t2 = shutter_cam*1.1e-3

	t = int( (t1+t2+t3)*1e6 ) # total time in micro sec. for timingSeq

	dmd = communication.DMD()
	cam = camera.Camera(shutter=shutter_cam, gain=gain_cam, cameraMode=mode_cam)
	n_step = X.shape[0]

	if saveImage: #If asked, create a directory to save images
		if (folderPath+'img') in os.listdir(folderPath): os.rmtree(folderPath+'img')
		os.makedirs(folderPath+'img')

	intensities = np.zeros((n_step, n_step), dtype=float)
	over_exposed = np.zeros((n_step, n_step), dtype=bool)
	positions = np.zeros((n_step, n_step, 2), dtype=float) # positions[i,j] = (x_ccd[i,j], y_ccd[i,j])
	widths = np.zeros((n_step, n_step, 2), dtype=float) # widths[i,j] = (waistX_ccd[i,j], waistY_ccd[i,j])

	print '==================================='
	print 'Taking and fitting images'
	pbar = progressbar.ProgressBar(maxval = n_step**2)
	pbar.start()
	t0 = time.time()
	for idx in np.ndindex(n_step, n_step):
		# create sequence
		name = str(idx)
		hologram_patch = imgFunc_FT.quickAperture(hologram, X[idx], Y[idx], radius, on)
		hologram_patch = dmd.compilePicture(hologram_patch)
		dmd.allocSeq(name, 1)
		dmd.putSeq(name, hologram_patch)
		dmd.timingSeq(name, t) # play for shutter time plus buffer before and after taking picture

		# play sequence and take pictures
		img_list = [] # initialize lists for accumulating statistics
		t0 = time.time()
		dmd.startProj(name)
		time.sleep(t1)
		while time.time()-t0 < t1+t2:
			img_list.append(camera.takePicture(cam))
			time.sleep(t4)
		dmd.waitProj()
		dmd.freeSeq(name)
		print '{} pictures taken'.format(len(img_list))

		# optionally crop and save CCD picture
		# ROI = None # uncomment this line to avoid cropping
		if ROI is not None:
			img_list = [ img[ ROI[0]:ROI[1], ROI[2]:ROI[3] ] for img in img_list ]

		# compute with moments to get intensity, over_exposed, position, and width
		img_list = [ img.astype(float) for img in img_list ] # img dtype is uint8 by default
		img = sum(img_list)/len(img_list) # averaged image
		intensities[idx] = img.mean() # average counts per pixel (intensity of average equals average of intensities)
		over_exposed[idx] = np.sum(np.isclose(img, 255)>0.01*img.size) # true if more than 1% of pixels are saturated
		if saveImage: imsave(folderPath+'img\\'+name+'.png', img) # save averaged image

		y, x = np.indices(img.shape) # image grid on the CCD in pixel units
		img_list = [ img/img.max() for img in img_list ] # normalize for computing moments
		x0 = np.mean([ np.average(x, weights=img**10) for img in img_list ]) # robustly find the point
		y0 = np.mean([ np.average(y, weights=img**10) for img in img_list ]) # of maximum intensity
		positions[idx] = x0, y0 # center of the DMD's Airy spot on teh CCD
		wx = np.mean([ np.sqrt(np.average( (x-x0)**2, weights=img) ) for img in img_list ])
		wy = np.mean([ np.sqrt(np.average( (y-y0)**2, weights=img) ) for img in img_list ])
		widths[idx] = wx, wy

		pbar.update( idx[0]*n_step + idx[1] + 1)

	camera.finishCam(cam)
	dmd.free()
	dmd = None
	pbar.finish()
	print 'Taking and fitting all images took {:e} seconds'.format(time.time()-t0)

	return positions, widths, intensities, over_exposed

# =======================
# = Analyzing functions =
# =======================

def gauss2dSym(params, X, Y):
	'''
	Define a symmetric 2d Gaussian (widthX = widthY)

	Parameters :
	--> params : centerX, centerY, width, height, offset
	--> X, Y : sequences of value
	'''

	centerX, centerY, width, height, offset = params
	return offset + height*np.exp(-2*(X-centerX)**2/(width**2)-2*(Y-centerY)**2/(width**2))

# TODO add w0 = np.sqrt(widths[:,:,0]**2+widths[:,:,1]**2) as another measure for exclusion
# normalize w0 /= w0.min() and use plt.errorbar(r0.ravel(), yerr=w0.ravel())
def getCorruptedMeasurements(positions, intensities, over_exposed):
	'''
	Check for obvious failures of fittings

	-> look if positions is not out of range of the image
	'''

	w_cam = 1280
	h_cam = 960

	# manually get points that are within the CCD but obviously wrong
	x0, y0 = positions[:,:,0], positions[:,:,1]
	n_step = x0.shape[0]
	r0 = np.sqrt( (x0-np.median(x0))**2 + (y0-np.median(y0))**2)
	plt.plot(r0.ravel(), 'k')
	plt.scatter(range(r0.size), r0.ravel(), s=30)
	plt.title('Find the position cutoff for corrupted measurements')
	plt.ylabel('Radial position (CCD pixels)')
	plt.xlabel('Measurement index')
	plt.gca().set_xlim(np.array([-0.05, 1.05])*n_step**2)
	plt.show()

	print ''
	cutoff = raw_input('Enter the UPPER limit of acceptable positions (blank for no limit): ')
	print ''
	try: cutoff = float(cutoff)
	except: cutoff = np.inf
	badIdx = np.where( r0>=cutoff )

	c = np.where( over_exposed, 'r', 'b')
	c[badIdx] = 'r'
	c = c.ravel()
	plt.plot(intensities.ravel(), 'k')
	plt.scatter(range(intensities.size), intensities.ravel(), s=30, c=c)
	plt.title('Find the intensity cutoffs for corrupted measurements\n(saturated or out of position bounds in red)')
	plt.ylabel('Mean counts per pixel on ROI')
	plt.xlabel('Measurement index')
	plt.gca().set_xlim(np.array([-0.05, 1.05])*n_step**2)
	plt.show()

	print ''
	cutoff1 = raw_input('Enter the LOWER limit of aceptible intensities (blank for no limit): ')
	cutoff2 = raw_input('Enter the UPPER limit of aceptible intensities (blank for no limit): ')
	print ''
	try: cutoff1 = float(cutoff1)
	except: cutoff1 = 0
	try: cutoff2 = float(cutoff2)
	except: cutoff2 = np.inf
	badIdx = np.where(np.logical_or.reduce(( r0>cutoff, intensities<=cutoff1, cutoff2<=intensities )))

	corrupted_measurements = np.zeros(x0.shape, dtype=bool)
	corrupted_measurements[badIdx] = True
	if len(badIdx[0]) == 0:
		print 'No bad measurements'
	else:
		print 'Bad measurements:'
		print badIdx

	return corrupted_measurements

def displacement(X, Y, positions, corrupted_measurements):
	'''
	Translate positions of image into a local wavefront normal coordinates

	-> calculate mean positions of the fitted spot.
	--- this mean position is our reference.
	--- it gives a reference wavefront normal

	-> calculate displacement from mean position
	--- a displacement means that the wavefront is at angle compared to the reference wavefront
	'''

	#Delete corrupted measurements to calculate the mean value
	clean_positions = np.ma.masked_array(positions, corrupted_measurements[:,:,None]*np.ones((1,1,2)))

	#Calculate the mean position of the images. it will be our reference direction for the wavefront.
	x_mean = clean_positions[:,:,0].mean()
	y_mean = clean_positions[:,:,1].mean()

	displacements = np.zeros_like(positions)
	angle_distortions = np.zeros_like(positions)
	phi = np.deg2rad(angle_CCDtoDMD)
	for idx in np.ndindex(positions[:,:,0].shape):
		#Translate position on CCD into displacement from mean position in DMD coordinates
		dx_cam = positions[idx][0] - x_mean
		dy_cam = positions[idx][1] - y_mean
		dx_dmd = (-1)**flipX*( np.cos(phi)*dx_cam - np.sin(phi)*dy_cam )
		dy_dmd = (-1)**flipY*( np.sin(phi)*dx_cam + np.cos(phi)*dy_cam )
		displacements[idx] = dx_dmd, dy_dmd

		#Translate displacement into an angle_distortion
		angle_x = dx_dmd * CCDpixelSize / telescope / focal_length # telescope = 1/magnification (e.g. -2)
		angle_y = dy_dmd * CCDpixelSize / telescope / focal_length
		angle_distortions[idx] = angle_x, angle_y
	displacements[corrupted_measurements] = angle_distortions[corrupted_measurements] = 0

	# return displacements, angle_distortions, reference_displacement
	return displacements, angle_distortions

# TODO add corrupted_measurements as an argument to make angular_destortions a masked array?
def frankotChellappa(n_step, step, angle_distortions):
	''' use Frankot Chellappa 1988 Eq. 21 to build the wavefront from the angular distortions (wavefront gradient) '''
	def ft(u):  return np.fft.fftshift( np.fft.fft2( np.fft.ifftshift(u)))
	def ift(U): return np.fft.ifftshift(np.fft.ifft2(np.fft.fftshift(U)))
	# angle_distortions = np.ma.masked_array(angle_distortions, corrupted_measurements[:,:,None]*np.ones((1,1,2)))
	kx = angle_distortions[:,:,0]/wavelength/diff_order # phase gradient in
	ky = angle_distortions[:,:,1]/wavelength/diff_order # cycles/meter (wavelengths)
	kx = np.pad(kx, (n_step,), 'constant') # zero-pad to
	ky = np.pad(ky, (n_step,), 'constant') # avoid aliasing
	nuX = np.fft.fftshift(np.fft.fftfreq(3*n_step, mirrorSize*step))
	nuX, nuY = np.meshgrid(nuX, nuX) # Fourier space grid
	wavefront = ift( (nuX*ft(kx) + nuY*ft(ky)) / (nuX**2 + nuY**2 + 1e-15) / (2j*np.pi) ).real # 1e-15 to avoid division by 0
	wavefront *= np.sqrt( np.sum(np.abs(wavefront)**2) / np.sum(np.abs(wavefront[n_step:2*n_step, n_step:2*n_step])**2) ) # put power in the scan grid
	wavefront = wavefront[n_step:2*n_step, n_step:2*n_step] # crop to original grid
	# TODO use linear 2d interpolation on corrupted measurements?
	wavefront -= (wavefront.max()+wavefront.min())/2. # center on 0
	return wavefront # phase in units of wavelengths (cycles)

def fitGaussianIntensity(x0, y0, scan_size, n_step, step, X, Y, intensities):
	'''
	Fit a 2d gaussian on the measured intensities to find the best parameter for gaussian correction
	Interpolate sampled intensities to estimate intensity on every mirror
	'''

	###Fitting
	# l = np.sqrt(len(X))
	# x = np.reshape(X,(l,l)).astype(np.float64)
	# y = np.reshape(Y,(l,l)).astype(np.float64)
	# intensities_2d = np.reshape(intensities,(l,l)).astype(np.float64)
	l = X.shape[0]
	x = X.copy()
	y = Y.copy()
	intensities_2d = intensities.copy()

	#Center estim
	i_max, j_max = np.unravel_index(intensities_2d.argmax(), intensities_2d.shape)
	y_max = y[i_max,j_max]
	x_max = x[i_max,j_max]

	#Height estim
	height_estim = intensities_2d[i_max, j_max]

	#offser estim
	offset_estim = np.mean(intensities_2d[0:2,0:2])

	#width estim
	width_estim = 200.

	p0_gauss_2d=[x_max, y_max, width_estim, height_estim, offset_estim]
	#print p0_gauss_2d
	p_res_gauss_2d, cov, ifo, msg, ier = leastsq(lambda params: -(gauss2dSym(params,x,y)-intensities_2d).ravel(), p0_gauss_2d, full_output=1)
	p_res_gauss_2d[-1]=0 #force an offset of 0
	#print p_res_gauss_2d

	###Intensity_target_map
	x_new = np.arange(x0-scan_size/2,x0+scan_size/2, 1)
	y_new = np.arange(y0-scan_size/2,y0+scan_size/2, 1)
	X_new, Y_new = np.meshgrid(x_new, y_new)

	intensity_target_map = gauss2dSym(p_res_gauss_2d, X_new, Y_new)

	###Interpolation of measured intensities over the scanned area
	x =  np.arange(-n_step/2+1,n_step/2+1,1)*step+x0
	y =  np.arange(-n_step/2+1,n_step/2+1,1)*step+y0

	interpolation = interpolate.RectBivariateSpline(x, y, intensities_2d)
	intensity_interpolated_map = interpolation(x_new, y_new)

	return p_res_gauss_2d, intensity_interpolated_map, intensity_target_map

# =======================
# = Zernike functions =
# =======================

def zernike_rad(m, n, rho):
	'''
	Calculate the radial component of Zernike polynomial (m, n)
	given a grid of radial coordinates rho.

	Parameter :
	--> n : the radial degree
	--> m : the azymuthal degree
	--> rho : a radius number or array

	Return :
	--> radial_components : the radial_component of the Zernike polynomial Z(n,m) for the input rho array

	Method :
	--> 1) ensure that the input radial and azymuthal degree makes sense.
	------ they should be positive
	------ m should be equal or smaller than n in absolute value

	--> 2) if n-m is odd, the Zernike polynomial is not define and set to 0.

	--> 3) generate pre-factor formula. see https://en.wikipedia.org/wiki/Zernike_polynomials

	--> 4) calculate radial_components. see https://en.wikipedia.org/wiki/Zernike_polynomials
	'''

	# 1)
	if (n < 0 or m < 0 or abs(m) > n):
		raise ValueError

	# 2)
	if ((n-m) % 2):
		return rho*0.0

	# 3)
	pre_fac = lambda k: (-1.0)**k * fac(n-k) / ( fac(k) * fac( (n+m)/2.0 - k ) * fac( (n-m)/2.0 - k ) )

	# 4)
	radial_component = sum(pre_fac(k) * rho**(n-2.0*k) for k in xrange((n-m)/2+1))

	return radial_component

def zernike(m, n, rho, phi):
	'''
	Calculate Zernike polynomial (m, n) given a grid of radial
	coordinates rho and azimuthal coordinates phi.

	Parameters :
	--> n : the radial degree
	--> m : the azimuthal degree


	Return :
	--> the zernike polynomial

	Method :
	--> if m > 0, multiply the radial components by cos(m * phi)
	--> if m < 0, multiply the radial components by sin(m * phi)
	--> if m = 0, no azimuthal dependence

	see https://en.wikipedia.org/wiki/Zernike_polynomials
	'''


	if (m > 0): return zernike_rad(m, n, rho) * np.cos(m * phi)
	if (m < 0): return zernike_rad(-m, n, rho) * np.sin(-m * phi)
	return zernike_rad(0, n, rho)

def zernikel(j, rho, phi):
	'''
	Calculate Zernike polynomial with Noll coordinate j given a grid of radial
	coordinates rho and azimuthal coordinates phi:

	Parameters :
	--> j : Noll index.
	--> rho : a radius number or array
	--> phi : an azimuthal number or array

	Return :
	--> the Zernike polynomial Z_j where j is the Noll index

	Method :
	--> link between Noll index and radial/azimuthal degree :
	see https://en.wikipedia.org/wiki/Zernike_polynomials for an easy visualisation of the link between the two
	'''

	n = 0
	while (j > n):
		n += 1
		j -= n

	m = -n+2*j
	return zernike(m, n, rho, phi)

def fitWavefront(X, Y, n_step, radius, scan_size, wavefront, number_zernike_polynomial):
	'''
	Fit the input wavefront with the Zernike polynomial.

	Parameters :
	--> scan_size : size of the scanned area
	--> wavefront : 2d numpy array to be fitted
	--> scan_size : size of the scanned area.
	it is also the size where a correcting hologram would be displayed
	the correcting cologram will be calculated from the result of this fit

	--> number_zernike_polynomial : number of Zernike polynomial to use to fit the wavefront.

	Return :
	--> zernike_coeff : a list of the zernike coefficient

	Method :
	--> 1) make a grid
	---- each grid point is a sampled point of the wavefront
	---- the grid has to be scaled so the

	--> 2) create the Zernike polynomial on this grid

	--> 3) calculate covariance between Zernike polynomial
	if calculated on the unit disk, with normalized Zernike polynomial, the covariance matrix should be the identity matrix
	in our case :
	----- Zernike polynomial were not normalized
	----- we calculate on a grid, and not on a continuous space
	----- we calculate the covariance matrix on a square inscribed in the unit disk and not on the full unit disk
	so the covariance matrix is not the identity

	--> 4) We invert the covariance matrix

	--> 5) We calculate the inner product of the wavefront with the Zernike polynomial

	--> 6) We retrieve the Zernike coefficients of the wavefront by multiplying the inverted covariance matrix with the inner product vector.
	See my report for more details of the calculation.
	'''

	#1) Make coordinate grid for rho and phi

	#Reshape the grid as a 2d numpy array
	# X = np.array(np.reshape(X, (n_step,n_step)), dtype = np.float32)
	# Y = np.array(np.reshape(Y, (n_step,n_step)), dtype = np.float32)

	#Center the grid
	X = X - (X.max() + X.min())/2.
	Y = Y - (Y.max() + Y.min())/2.

	#Scale the grid so that the unit disk wo
	scaling_factor = 2. / (np.sqrt(2)*(scan_size-2*radius))
	X *= scaling_factor
	Y *= scaling_factor

	#Create the radius,azimuthal and mask grid
	grid_rho = (X**2.0+Y**2)**0.5
	grid_phi = np.arctan2(X, Y)
	# grid_mask = grid_rho <= 0.5

	#2) Calculate zernike polynomials
	zern_list = [zernikel(i, grid_rho, grid_phi) for i in xrange(number_zernike_polynomial)]

	#3) Calculate covariance between all Zernike polynomials
	cov_mat = np.array([[np.sum(zerni * zernj) for zerni in zern_list] for zernj in zern_list])

	#4) Invert covariance matrix using SVD
	cov_mat_in = np.linalg.pinv(cov_mat)

	# 5) Inner product
	wf_zern_inprod = np.array([np.sum(wavefront * zerni) for zerni in zern_list]) # wavefront[corrupted_measurements] = 0
	# wf_zern_inprod = np.array([np.nansum(wavefront * zerni) for zerni in zern_list]) # nansum for compatibility with masked arrays

	#6) calculate zernike coefficient
	zernike_coeff = np.dot(cov_mat_in, wf_zern_inprod)

	return zernike_coeff

# =======================
# = Repairing functions =
# =======================

def createPhaseMap(zernike_coeff, scan_size, with012 = True, unit_disk_mask = False):
	'''
	Create the  phase map from Zernike fit.

	Parameters :
	--> zernike_coeff : list of zernike_coefficient
	--> scan_size : size of the scanned area. size of the hologram to be displayed.
	--> scaling_factor : scaling factor of the Zernike fit.
	--> with012 : include piston, tip and tilt coefficient ?
	--> mask : include a mask to create a phase_map only on the 'unit_disk'

	Return :
	--> the phase_map
	--> the grid_mask

	Method :
	--> 1) create the grid
	take care of the scaling_factor relating the disk unit diameter with the grid size (n_step - 1)*step + 2*radius


	--> 2) calculate phase_map
	'''

	#1) Create the grid
	t0_g= time.clock()
	x = np.linspace(-1,1,scan_size)/np.sqrt(2)
	X, Y = np.meshgrid(x,x)
	grid_rho = (X**2.0+Y**2)**0.5
	grid_phi = np.arctan2(X, Y)
	grid_mask = grid_rho <= 1
	t1_g = time.clock()

	#print 'Creating the grid took : %.3f seconds'%(t1_g-t0_g)

	t0_p = time.clock()

	# 2)Calculate the phase map
	if with012:
		phase_map = sum(val * zernikel(i, grid_rho, grid_phi) for (i, val) in enumerate(zernike_coeff))
	else:
		phase_map = sum(val * zernikel(i, grid_rho, grid_phi) for (i, val) in enumerate(zernike_coeff) if i > 2)
	t1_p = time.clock()
	#print 'Creating the phase map took : %.3f seconds'%(t1_p-t0_p)

	if unit_disk_mask:
		phase_map *= grid_mask

	return phase_map, grid_mask

def createLocalWidthMap(intensity_interpolated_map, intensity_target_map, x0, y0, n_step, step, scan_size):
	'''
	-> Calculate ratio between measured and targeted intensities
	-> Rescale the ratio to the minimum value
	-> Relate the ratio to the local width of the grating
	'''

	# plt.ion()

	# fig = plt.figure()
	# ax = fig.add_subplot(111)
	# plt.title('Interpolated Map')
	# clr = ax.matshow(intensity_interpolated_map)
	# fig.colorbar(clr)

	# fig = plt.figure()
	# ax = fig.add_subplot(111)
	# plt.title('Target Map')
	# clr = ax.matshow(intensity_target_map)
	# fig.colorbar(clr)


	###Ratio I_measured/I_target
	ratio_map = intensity_interpolated_map/intensity_target_map

	# fig = plt.figure()
	# ax = fig.add_subplot(111)
	# plt.title('ratio')
	# clr = ax.matshow(ratio_map)
	# fig.colorbar(clr)

	###Rescale the ratio to the minimum value, so that every ratio is smaller than 1 (we can only decrease intensity using the DMD)
	m = np.min(ratio_map.ravel())
	print 'min ratio : ', m

	ratio_rescale_map = m/ratio_map

	# fig = plt.figure()
	# ax = fig.add_subplot(111)
	# plt.title('ratio rescale to min')
	# clr = ax.matshow(ratio_rescale_map, vmax = 1)
	# fig.colorbar(clr)

	###Link ratio and width of the grating.
	local_width_map = np.arcsin(np.sqrt(ratio_rescale_map))

	# fig = plt.figure()
	# ax = fig.add_subplot(111)
	# plt.title('local width map')
	# clr = ax.matshow(local_width_map, vmax = 0.5)
	# fig.colorbar(clr)

	# plt.ioff()

	return ratio_map, ratio_rescale_map, local_width_map

def createHologram(phase_map_list, local_width_map, x0, y0, scan_size, spacing, smoothing=False, smoothing_factor=1, correct_phase=True, correct_intensity=True, unit_disk_mask=False, w=1024, h=768, centered=True, kx=None, ky=None):
	'''
	Phase map :
	-> phase is in wavelength unit. the phase is added to the grating phase.

	Local width map :
	-> intensity correction.

	Scan size:
	-> must be a scalar or a length-2 list/tuple/array

	Return
	-> the hologram
	'''

	if (type(scan_size) is int) or (type(scan_size) is float):
		scan_size = [scan_size, scan_size]


	if (kx is None) or (ky is None):
		#Initial version using spacing
		#create grating phase : increase along the dmd x-axis
		grating_phase = np.asarray(range(scan_size[1])*scan_size[0])*1.0
		grating_phase = np.reshape(grating_phase, (scan_size[1], scan_size[0]))/spacing
	else:
		# Version using 2d grating wavevector
		X, Y = np.meshgrid(np.arange(scan_size[0]), np.arange(scan_size[1]))
		grating_phase = kx*X + ky*Y


	local_phase = grating_phase

	if correct_phase:
		for i, phase_map in enumerate(phase_map_list):
			print 'Adding phase map #', i+1
			local_phase -= phase_map/diff_order #add phase to grating phase #sum over all the phase map (for many iteration case)
		local_phase = local_phase%1
		#local_phase = local_phase%1 - 0.5

	# if centered:
	# 	# Create fringes that look symmetric when their width is varied
	# 	local_phase = 2*np.pi*(local_phase%1 - 0.5)
	# else:
	# 	local_phase = np.pi*(local_phase%1)

	if smoothing:
		print 'Smoothing hologram with factor : ', smoothing_factor
		random_map = np.random.random((scan_size[1], scan_size[0]))
		if correct_intensity :
			print 'Calculating prob_map ...'
			#prob_map = 1./2 * (np.tanh(1./smoothing_factor*(local_phase - 0.5 + local_width_map)) - np.tanh(1./smoothing_factor*(local_phase - 0.5 - local_width_map)))
			prob_map = 0.5 + 0.5*np.tanh(1./smoothing_factor/np.pi*(local_width_map - local_phase))
			# prob_map = 0.5*( np.tanh((local_phase + 0.5)/smoothing_factor) - np.tanh((local_phase - 0.5)/smoothing_factor) )
			print 'Comparing to random map ...'
			hologram_int = random_map <= prob_map
		else :
			print 'Calculating prob_map ...'
			#prob_map = 1./2 * (np.tanh(1./smoothing_factor*((local_phase -0.5)*2 + 0.5)) - np.tanh(1./smoothing_factor*((local_phase -0.5)*2 - 0.5)))
			# prob_map = 0.5 + 0.5*np.tanh(1./smoothing_factor/np.pi*(np.pi - local_phase))
			prob_map = 0.5*( np.tanh((local_phase + 0.5)/smoothing_factor) - np.tanh((local_phase - 0.5)/smoothing_factor) )
			print 'Comparing to random map ...'
			hologram_int = random_map <= prob_map
	else:
		print 'No smoothing of hologram'
		if correct_intensity :
			hologram_int = abs(local_phase) <= local_width_map #compare value to local width map
		else :
			# hologram_int = abs(local_phase) <= np.pi/2
			hologram_int = np.where( np.cos(2*np.pi*local_phase+1e-15)>=0, 1, 0 ).astype(int)


	print 'Unit Disk Mask:', unit_disk_mask
	if unit_disk_mask :
		x = np.linspace(-1,1, min(scan_size[0], scan_size[1]))
		X, Y = np.meshgrid(x,x)
		grid_rho = (X**2.0+Y**2)**0.5
		grid_mask = grid_rho <= 1
		hologram_int *= grid_mask


	#create the full hologram with the DMD dimension and place the intermediate hologram at the good place
	hologram = np.zeros((h,w),int)
	idx = ( slice(y0 - int(np.ceil(scan_size[0]/2.)), y0 + int(np.floor(scan_size[0]/2.)) ),
	 		slice(x0 - int(np.ceil(scan_size[1]/2.)), x0 + int(np.floor(scan_size[1]/2.)) ) )
	# hologram[y0-scan_size[1]/2 : y0+scan_size[1]/2, x0-scan_size[0]/2 : x0+scan_size[0]/2] = hologram_int
	hologram[idx] = hologram_int
	# TODO add field in HologramMenu that says what on pixels should be (0 in current setup)
	hologram = np.where(hologram==1, on, 1-on)
	return hologram


	# return hologram_int

# TODO make sure everything's hunky dory here (referenced in line 921 in GUI.py)
def createHologramFromData(newfolderPath, data, spacing, smoothing, smoothing_factor, correct_phase, correct_intensity, include012 = True, save = True, newDataName = '', unit_disk_mask = False):

	#create new data file
	data_correction = copy.copy(data)
	data_correction.folderPath = newfolderPath

	#Constants
	folderPath = data.folderPath
	x0 = data.x0 #(x0, y0) : center position of the scan. reference for the phase map and hologram
	y0 = data.y0
	X = data.X
	Y = data.Y
	n_step = data.n_step
	step = data.step
	scan_size = data.scan_size # size of the scanned area. used to define the radius of the zernike fit.
	intensity_interpolated_map = data.intensity_interpolated_map
	intensity_target_map = data.intensity_target_map

	if include012:
		phase_map_list = data.phase_map
	else :
		phase_map_list = data.phase_map_without012

	#Create correction map for intensity
	if correct_intensity :
		ratio_map, ratio_rescale_map, local_width_map = createLocalWidthMap(intensity_interpolated_map, intensity_target_map, x0, y0, n_step, step, scan_size)
		data_correction.ratio_map = ratio_map
		data_correction.ratio_rescale_map = ratio_rescale_map
		data_correction.ratio_local_width_map = local_width_map
	else :
		local_width_map = []

	#Define Hologram
	print 'Creating holograms...'
	hologram = createHologram(
		phase_map_list,
		local_width_map,
		x0,
		y0,
		scan_size,
		spacing,
		smoothing,
		smoothing_factor,
		correct_phase,
		correct_intensity,
		unit_disk_mask
		)

	data_correction.hologram = hologram
	data_correction.smoothing_factor = smoothing_factor

	if save:
		print 'Saving correction data to : ', data_correction.folderPath
		saveData(data_correction,newDataName)

	return hologram

def sendHologram(hologram):
	print 'Displaying hologram'
	speakToDMD.sendImage(0,transformArray(hologram))
	speakToDMD.imageOrder(1)
	speakToDMD.playFirst()

# ===========================
# = Beam-shapping functions =
# ===========================

#Hermite Gaussian Beam

def hermiteGaussian(scan_size, m, n, waist):
	'''
	Return a hermite-gaussian mode of order (m,n) : TEM_mn

	TEM_mn(r) = Hm(sqrt(2)*x/w)*Hn(sqrt(2)*y/w)*exp(-(x**2+y**2))/w**2

	-> w is a free parameter
	-> Hm is the Hermite polynomial of order m
	'''

	#grid
	grid = (np.indices((scan_size, scan_size), dtype=float) - scan_size/2.) / float(scan_size/2.)
	grid_rho = (grid**2.0).sum(0)**0.5
	grid_phi = np.arctan2(grid[0], grid[1])
	X = grid[1]
	Y = grid[0]

	#ensure waist is a float number
	waist = float(waist)

	#create hermite gaussian
	hg  = hermite(m)(np.sqrt(2)*X/waist) * hermite(n)(np.sqrt(2)*Y/waist) * np.exp(-(X**2+Y**2)/waist**2)
	hg_norm = hg/hg.max()
	return hg_norm

def hermiteGaussianDrawAll(i_max = 3, j_max = 3, waist = 0.5):
	scan_size = 100

	fig = plt.figure()

	for i in range(i_max):
		for j in range(j_max):
			h = hermiteGaussian(scan_size,i,j,waist)
			ax = fig.add_subplot(i_max,j_max,i+j*i_max+1)
			ax.imshow(h, cmap = cm.gray)
			ax.axis('off')
			ax.set_title(str(i) + ' - ' + str(j))
	show()

def hermiteGaussianHologram(intensity_gauss_params, intensity_interpolated_map, phase_map_list, x0, y0, n_step, step, scan_size, spacing, smooth, smoothing_factor, m, n, waist):
	'''
	Return an hologram generating a hermite gaussian beam
	'''

	#Measured intensity beam parameters
	x_beam, y_beam, width_beam, height, offset = intensity_gauss_params

	#Generate hermite gaussian amplitude map
	h = hermiteGaussian(scan_size,m,n,waist)

	#Phase map
	s = np.shape(h)
	phase_map = np.angle(h)/2/np.pi

	# fig = plt.figure()
	# ax = fig.add_subplot(111)
	# clr = ax.matshow(phase_map, cmap = cm.gray)
	# colorbar(clr)
	# show()

	phase_map_list.append(phase_map) #append the phase map to the existing correcting phase map

	#Local width map
	intensity_target_map = h**2
	intensity_target_map_norm = intensity_target_map * height

	ratio_map, ratio_rescale_map, local_width_map = createLocalWidthMap(intensity_interpolated_map, intensity_target_map_norm, x0, y0, n_step, step, scan_size)
	#Hologram
	hologram =  createHologram(phase_map_list, local_width_map, x0, y0, scan_size, spacing, smooth, smoothing_factor, correct_phase = True, correct_intensity = True, unit_disk_mask = False, w = 1024, h = 768)

	return hologram

def hermiteGaussianHologramFromData(new_folderPath, data, spacing, smooth, smoothing_factor, m, n, waist):
	#New data file
	data_hermiteGaussian = copy.copy(data)
	data_hermiteGaussian.folderPath = new_folderPath

	#Get data
	x0 = data.x0
	y0 = data.y0
	n_step = data.n_step
	step = data.step
	scan_size = data.scan_size
	intensity_interpolated_map = data.intensity_interpolated_map
	phase_map_list = data.phase_map_without012
	intensity_gauss_params = data.p_res_gauss_2d

	#Generate hologram
	hologram = hermiteGaussianHologram(intensity_gauss_params, intensity_interpolated_map, phase_map_list, x0, y0, n_step, step, scan_size, spacing, smooth, smoothing_factor, m, n, waist)
	data_hermiteGaussian.hologram = hologram

	saveData(data_hermiteGaussian,'data_hermiteGaussian.p')

	return hologram

#Laguerre Gaussian beam

def laguerreGaussian(scan_size, p, l, w):
	'''
	Return a laguerre-gaussian mode of order (m,n) : TEM_mn

	LG_lp(r) = r**l*L_lp(2*r**2/w**2)*exp(-r**2/w**2)*exp(i*l*phi)

	-> w is a free parameter
	-> Hm is the Hermite polynomial of order m
	'''

	grid = (np.indices((scan_size, scan_size), dtype=float) - scan_size/2.) / float(scan_size/2.)
	grid_rho = (grid**2.0).sum(0)**0.5
	grid_phi = np.arctan2(grid[0], grid[1])


	lg  = grid_rho**abs(l)*genlaguerre(p,abs(l))(2*grid_rho**2/w**2)*np.exp(-grid_rho**2/w**2)*np.exp(1j*l*grid_phi)
	lg_norm = lg/(abs(lg).max())
	return lg_norm

def colorize(z):
	r = np.abs(z)
	arg = np.angle(z)

	h = (arg + np.pi)  / (2 * np.pi) + 0.5
	l = 1.0 - 1.0/(1.0 + r**0.3)
	s = 0.8

	c = np.vectorize(hls_to_rgb) (h,l,s) # --> tuple
	c = np.array(c)  # -->  array of (3,n,m) shape, but need (n,m,3)
	c = c.swapaxes(0,2)
	return c

def laguerreGaussianDrawAll(p_max = 3, l_max = 3, w = 0.5):
	scan_size = 100

	fig = plt.figure()
	for p in range(p_max):
		for l in range(l_max):
			lg = laguerreGaussian(scan_size, p, l, w)
			ax = fig.add_subplot(p_max,l_max,l+p*l_max+1)
			ax.imshow(colorize(lg))
			ax.axis('off')
			ax.set_title(str(p) + ' - ' + str(l))

	fig = plt.figure()
	for p in range(p_max):
		for l in range(l_max):
			lg = laguerreGaussian(scan_size, p, l, w)
			ax = fig.add_subplot(p_max,l_max,l+p*l_max+1)
			ax.imshow(abs(lg))
			ax.axis('off')
			ax.set_title(str(p) + ' - ' + str(l))

	fig = plt.figure()
	for p in range(p_max):
		for l in range(l_max):
			lg = laguerreGaussian(scan_size, p, l, w)
			ax = fig.add_subplot(p_max,l_max,l+p*l_max+1)
			ax.imshow(angle(lg))
			ax.axis('off')
			ax.set_title(str(p) + ' - ' + str(l))
	show()

def laguerreGaussianHologram(intensity_gauss_params, intensity_interpolated_map, phase_map_list, x0, y0, n_step, step, scan_size, spacing, smooth, smoothing_factor, p, l, waist):
	'''
	Return an hologram generating a hermite gaussian beam
	'''

	#Measured intensity beam parameters
	x_beam, y_beam, width_beam, height, offset = intensity_gauss_params

	#Generate hermite gaussian amplitude map
	lg = laguerreGaussian(scan_size,p,l,waist)

	#Phase map
	phase_map = np.angle(lg)/2/np.pi
	phase_map_list.append(phase_map) #append the phase map to the existing correcting phase map

	#Local width map
	intensity_target_map = abs(lg)**2
	intensity_target_map_norm = intensity_target_map * height

	ratio_map, ratio_rescale_map, local_width_map = createLocalWidthMap(intensity_interpolated_map, intensity_target_map_norm, x0, y0, n_step, step, scan_size)
	#Hologram
	hologram =  createHologram(phase_map_list, local_width_map, x0, y0, scan_size, spacing,  smooth, smoothing_factor, correct_phase = True, correct_intensity = True, unit_disk_mask = False, w = 1024, h = 768)

	return hologram

def laguerreGaussianHologramFromData(new_folderPath, data, spacing, smooth, smoothing_factor, p, l, waist):
	#New data file
	data_laguerreGaussian = copy.copy(data)
	data_laguerreGaussian.folderPath = new_folderPath

	#Get data
	x0 = data.x0
	y0 = data.y0
	n_step = data.n_step
	step = data.step
	scan_size = data.scan_size
	intensity_interpolated_map = data.intensity_interpolated_map
	phase_map_list = data.phase_map_without012
	intensity_gauss_params = data.p_res_gauss_2d

	#Generate hologram
	hologram = laguerreGaussianHologram(intensity_gauss_params, intensity_interpolated_map, phase_map_list, x0, y0, n_step, step, scan_size, spacing, smooth, smoothing_factor, p, l, waist)
	data_laguerreGaussian.hologram = hologram


	saveData(data_laguerreGaussian,'data_laguerreGaussian.p')

	return hologram

# =====================
# = Plotting functions =
# =====================

def plotScatter(positions, n_step, folderPath, saveFormat):
	'''
	Form the map of measured positions using the 'scatter' pyplot method

	Keyword arguments :
	positions -- list of measured positions
	'''

	print '==================================='
	print 'Plotting Positions'

	x, y = positions[:,:,0].ravel(), positions[:,:,1].ravel()

	#define a colormap to help visualize data
	color = range(n_step**2)

	fig, ax = plt.subplots()

	ax.set_title('Measured positions map')

	sca = ax.scatter(x, y, c = color, s = 50, cmap='jet')
	cbar = fig.colorbar(sca)
	cbar.set_label('Image index')

	plt.savefig(folderPath+"position_map"+saveFormat, bbox_inches='tight')
	return fig

def plotQuiverDistortion(X, Y, angle_distortions, wavefront, folderPath, saveFormat):
	'''
	Plot angle distortion of wavefront as arrows

	I am using quiver library (cf matplotlib website...)
	'''

	print '==================================='
	print 'Plotting Angle Distortions'
	fig = plt.figure()

	n_step = int(np.sqrt(X.size))
	plt.pcolormesh(X, Y, wavefront, cmap='bwr', vmin=-np.abs(wavefront).max(), vmax=np.abs(wavefront).max())
	plt.colorbar()
	Q = quiver( X, Y, angle_distortions[:,:,0]/diff_order, angle_distortions[:,:,1]/diff_order, pivot='mid')
	plt.axis('scaled')
	l,r,b,t = plt.axis([X.min(), X.max(), Y.min(), Y.max()])
	dx, dy = r-l, t-b
	plt.axis([l-0.05*dx, r+0.05*dx, b-0.05*dy, t+0.05*dy])

	plt.title('Wavefront Normals')
	plt.xlabel('X (mirrors)')
	plt.ylabel('Y (mirrors)')

	plt.savefig(folderPath+"quiverDistortion"+saveFormat, bbox_inches='tight')
	return fig

def plotMeasuredIntensity(X, Y, intensities, over_exposed, folderPath, saveFormat):
	'''
	Form the intensityMap from raw measurements

	Keyword arguments :
	intensities -- list of measured intensity
	over_exposed -- list of 0 and 1 : 0 = good, 1 = over_exposed
	'''

	print '==================================='
	print 'Plotting Intensity Measured'

	if 1 in over_exposed :
		print 'WARNING : some images have been over_exposed'
		print 'The intensity map will not be correct'

	n_step = X.shape[0]
	fig, ax = plt.subplots()

	ax.set_title('Intensity Measured map')
	ax.set_xlabel('X (mirrors)')
	ax.set_ylabel('Y (mirrors)')

	# cax = ax.pcolormesh(X, Y, intensities, cmap='jet')
	# plt.axis('scaled')
	# plt.axis([X.min(), X.max(), Y.min(), Y.max()])
	cax = ax.imshow(intensities, origin='', extent = [X.min(), X.max(), Y.min(), Y.max()], cmap='jet', interpolation='none')
	cbar = fig.colorbar(cax)
	# plt.axis('scaled')

	plt.savefig(folderPath+"intensity_measured_map"+saveFormat, bbox_inches='tight')
	return fig

def plotInterpolatedIntensity(x0, y0, scan_size, intensity_interpolated_map, folderPath, saveFormat):
	'''
	Form the intensityMap from interpolated data

	Keyword arguments :
	intensity_interpolated_map -- list of measured intensity
	'''

	print '==================================='
	print 'Plotting Intensity Interpolated'

	fig, ax = plt.subplots()

	ax.set_title('Intensity Interpolated map')
	ax.set_xlabel('X (mirrors)')
	ax.set_ylabel('Y (mirrors)')

	# cax = ax.pcolormesh(intensity_interpolated_map, cmap='jet')
	cax = ax.imshow(intensity_interpolated_map, interpolation='none', cmap='jet', extent = [x0-scan_size/2, x0+scan_size/2, y0-scan_size/2, y0+scan_size/2], origin = '')
	cbar = fig.colorbar(cax)

	plt.savefig(folderPath+"intensity_interpolated_map"+saveFormat, bbox_inches='tight')
	return fig

# def plotInterpolatedIntensity2(x0, y0, scan_size, intensity_interpolated_map, folderPath, saveFormat):
# 	print '==================================='
# 	print 'Plotting Intensity Interpolated 2'
#
# 	fig = plt.figure()
# 	ax = fig.gca(projection='3d')
#
# 	x_new = np.arange(x0-scan_size/2,x0+scan_size/2+1, 1)
# 	y_new = np.arange(y0-scan_size/2,y0+scan_size/2+1, 1)
#
# 	X, Y = np.meshgrid(x_new,y_new)
#
# 	surf = ax.plot_surface(X, Y, intensity_interpolated_map, rstride=1, cstride=1, cmap=cm.coolwarm, vmin = intensity_interpolated_map.min(), vmax = intensity_interpolated_map.max())
# 	fig.colorbar(surf)
#
# 	ax.set_title('Intensity Interpolated map')
# 	ax.set_xlabel('X (mirrors)')
# 	ax.set_ylabel('Y (mirrors)')
# 	ax.set_zlabel('Intensity (arbitrary unit)')
#
# 	plt.savefig(folderPath+"intensity_interpolated_map_2"+saveFormat, bbox_inches='tight')
#
# 	return fig

def plotTargetIntensity(x0, y0, scan_size, intensity_target_map, folderPath, saveFormat):
	print '==================================='
	print 'Plotting Target Intensity Map'

	fig, ax = plt.subplots()


	ax.set_title('Intensity Target map')
	ax.set_xlabel('X (mirrors)')
	ax.set_ylabel('Y (mirrors)')

	# cax = ax.pcolormesh(intensity_target_map, cmap='jet')
	cax = ax.imshow(intensity_target_map, interpolation='none', cmap='jet', extent=[x0-scan_size/2, x0+scan_size/2, y0-scan_size/2, y0+scan_size/2], origin='')
	cbar = fig.colorbar(cax)

	plt.savefig(folderPath+"intensity_target_map"+saveFormat, bbox_inches='tight')
	return fig

# def plotWavefront(X, Y, wavefront, folderPath, saveFormat):
# 	print '==================================='
# 	print 'Plotting Wavefront'
# 	fig = plt.figure()
# 	ax = fig.add_subplot(1, 1, 1, projection='3d')
# 	ax.set_title('Wavefront')
# 	ax.set_xlabel('X')
# 	ax.set_ylabel('Y')
# 	ax.set_zlabel('Distorsions (in unit of wavelength)')
# 	ax.set_zlim(bottom = wavefront.min(), top = wavefront.max())
#
# 	#defining positions of the bars
# 	xpos = X
# 	ypos = Y
# 	zpos = np.zeros(len(wavefront)**2)
# 	dx = step*np.ones_like(xpos)
# 	dy = dx.copy()
# 	dz = wavefront.ravel()
#
# 	ax.bar3d(
# 		xpos,
# 		ypos,
# 		zpos,
# 		dx,
# 		dy,
# 		dz
# 		)
#
# 	plt.savefig(folderPath+"wavefront.png", bbox_inches='tight')
#
# 	return fig

def plotWavefront2(X, Y, wavefront, folderPath, saveFormat):
	print '==================================='
	print 'Plotting Wavefront 2'
	fig = plt.figure()
	ax = fig.add_subplot(1, 1, 1)
	ax.set_title('Wavefront')
	ax.set_xlabel('X (mirrors)')
	ax.set_ylabel('Y (mirrors)')

	wavefront_phase = wavefront%1

	# cax = ax.pcolormesh(wavefront_phase, cmap=cm.gray, vmin = 0, vmax = 1)
	cax = ax.imshow(wavefront_phase, interpolation='nearest', cmap=cm.gray, vmin = 0, vmax = 1, extent = [X.min(), X.max(), Y.min(), Y.max()])
	cbar = fig.colorbar(cax)

	plt.savefig(folderPath+"wavefront2"+saveFormat, bbox_inches='tight')
	return fig

def plotWavefront3(X, Y, wavefront, folderPath, saveFormat):
	print '==================================='
	print 'Plotting Wavefront 3'

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')

	ax.set_title('Wavefront')
	ax.set_xlabel('X (mirrors)')
	ax.set_ylabel('Y (mirrors)')
	ax.set_zlabel('Wavefront (in wavelength unit)')

	n_step = X.shape[0]
	ax.plot_surface(X, Y, wavefront, rstride=1, cstride=1, cmap='bwr')

	plt.savefig(folderPath+"wavefront3"+saveFormat, bbox_inches='tight')

	return fig

def plotPhaseSurface(X, Y, radius, phase_map, folderPath, saveFormat):
	print '==================================='
	print 'Plotting Phase Surface'

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')

	ax.set_title('Fitted Phase Wavefront')
	ax.set_xlabel('X (mirrors)')
	ax.set_ylabel('Y (mirrors)')
	ax.set_zlabel('Wavefront (in wavelength unit)')

	y, x = np.mgrid[ Y.min()-radius : Y.max()+radius,
					 X.min()-radius : X.max()+radius ]
	ax.plot_surface(x, y, phase_map, cmap='bwr')

	plt.savefig(folderPath+"phaseSurface"+saveFormat, bbox_inches='tight')

	return fig

def plotPhaseMap(X, Y, radius, phase_map, folderPath, saveFormat):
	fig = plt.figure()
	ax = fig.add_subplot(111)
	ax.set_title('Phase Map (wavelength unit)')

	y, x = np.mgrid[ Y.min()-radius : Y.max()+radius,
					 X.min()-radius : X.max()+radius ]
	clr = ax.imshow(phase_map%1, cmap=cm.gray, vmin=0, vmax=1, origin='', interpolation='none')
	# plt.axis([x0-scan_size/2, x0+scan_size/2, y0-scan_size/2, y0+scan_size/2])

	fig.colorbar(clr)
	plt.savefig(folderPath + "phase_map"+saveFormat, bbox_inches='tight')
	return fig

def plotInterferoMap(x0, y0, scan_size, phase_map, folderPath, saveFormat):
	fig = plt.figure()
	ax = fig.add_subplot(111)

	w = 1024
	h = 768
	img = np.zeros((w,h),float)
	img.shape=h,w

	img[y0 - scan_size/2 : y0 + scan_size/2 , x0 - scan_size/2 : x0 + scan_size/2] += phase_map[:,:]

	ax.set_title('Phase Map')
	clr = ax.imshow(np.sin(img*2*np.pi), cmap=cm.gray, vmin=0, vmax=1, origin='', interpolation='none')
	plt.axis([x0-scan_size/2, x0+scan_size/2, y0-scan_size/2, y0+scan_size/2])

	fig.colorbar(clr)
	plt.savefig(folderPath + "interfero_map"+saveFormat, bbox_inches='tight')
	return fig

def plotHologram(hologram, x0, y0, scan_size, folderPath, saveFormat):
	fig = plt.figure()
	ax = fig.add_subplot(111)

	ax.set_title('Hologram')
	clr = ax.imshow( np.where(hologram==on, 1, 0), cmap='gray', origin='', interpolation='none')
	plt.axis([x0-scan_size/2, x0+scan_size/2, y0-scan_size/2, y0+scan_size/2])

	plt.savefig(folderPath + "hologram" + saveFormat, bbox_inches='tight')
	return fig

def plotZernikeCoeff(zernike_coeff, folderPath, saveFormat):
	fig = plt.figure()
	ax = fig.add_subplot(111)

	x_axis = range(len(zernike_coeff))
	ax.set_title('Zernike Coefficient')
	ax.plot(x_axis[3:], zernike_coeff[3:], 'o')

	ax.set_xlabel('Zernike Polynom number')
	ax.set_ylabel('Arbitrary unit')

	plt.savefig(folderPath + "zernike_coeff"+saveFormat, bbox_inches='tight')
	return fig

# def plotZernikeReconstruct(rec_zernike, folderPath, saveFormat):
# 	fig = plt.figure()
# 	ax = fig.add_subplot(111)
#
# 	surf_pl = ax.imshow(rec_zernike, interpolation='nearest')
# 	plt.colorbar(surf_pl)
#
# 	plt.savefig(folderPath + 'rec_zernike' + saveFormat, bbox_inches='tight')
# 	return fig

def plotData(data, folderPath, saveFormat, showPlot):
	###Plots
	print 'Plotting all datas'
	figure_list = []
	print 'Holo'
	figure_list.append(plotHologram(data.hologram, data.x0, data.y0, data.scan_size, data.folderPath, saveFormat))
	print 'Scatter'
	figure_list.append(plotScatter(data.positions, data.n_step, data.folderPath, saveFormat))
	print 'Quiver'
	figure_list.append(plotQuiverDistortion(data.X, data.Y, data.angle_distortions[-1], data.wavefront[-1], data.folderPath, saveFormat))
	print 'Wavefront'
	figure_list.append(plotWavefront3(data.X, data.Y, data.wavefront[-1], data.folderPath, saveFormat))
	# figure_list.append(plotWavefront2(data.X, data.Y, data.wavefront[-1], data.folderPath, saveFormat))
	print 'Phase Map'
	figure_list.append(plotPhaseMap(data.X, data.Y, data.radius, data.phase_map_without012[-1], data.folderPath, '_without012' + saveFormat))
	# figure_list.append(plotPhaseMap(data.x0, data.y0, data.scan_size, data.phase_map_without012[-1], data.folderPath, '_without012' + saveFormat))
	# figure_list.append(plotInterferoMap(data.x0, data.y0, data.scan_size, data.phase_map_without012[-1], data.folderPath, '_without012' + saveFormat))
	figure_list.append(plotPhaseSurface(data.X, data.Y, data.radius, data.phase_map[-1], data.folderPath, saveFormat))
	print 'Zernike coeff'
	figure_list.append(plotZernikeCoeff(data.zernike_coeff[-1], data.folderPath, saveFormat))
	print 'Measured Intensity'
	figure_list.append(plotMeasuredIntensity(data.X, data.Y, data.intensities, [], data.folderPath, saveFormat))
	print 'Interpolated Intensity'
	figure_list.append(plotInterpolatedIntensity(data.x0, data.y0, data.scan_size, data.intensity_interpolated_map, data.folderPath, saveFormat))
	#figure_list.append(plotInterpolatedIntensity2(x0, y0, scan_size, intensity_interpolated_map, folderPath, saveFormat))
	print 'Target Intensity'
	figure_list.append(plotTargetIntensity(data.x0, data.y0, data.scan_size, data.intensity_target_map, data.folderPath, saveFormat))

	print 'Almost finish...'

	if showPlot :
		print 'Show all plots'
		plt.show()
	else :
		print 'Close all plots'
		plt.close('all')
		# for fig in figure_list:
		# 	plt.close(fig)

# =====================
# = Save Data functions =
# =====================

class Data():
	'''
	Contain all valuable data of a scan
	'''


	def __init__(self, folderPath, hologram = [], smoothing_factor = 0, x0 = 0, y0 = 0, radius = 0, n_step = 0, step = 0, scan_size = 0, X=[], Y =[], intensity_profile = [], mode = 0, gain = 0, shutter = 0, crop_width = 0, resolution_factor = 0, positions = [], widths = [], over_exposed = [], intensities = [], displacements = [], angle_distortions = [], wavefront = [], zernike_coeff = [], phase_map = [], phase_map_without012 = [], p_res_gauss_2d = [], intensity_interpolated_map = [], intensity_target_map = [], ratio_map = [], ratio_rescale_map = [], local_width_map = [], reference_ccd = (0,0), reference_dmd = (0,0), reference_phase = 0):

		#Folder path
		self.folderPath = folderPath

		#Hologram
		self.hologram = hologram
		self.smoothing_factor = smoothing_factor

		#Scan parameters
		self.x0 = x0
		self.y0 = y0
		self.radius = radius
		self.n_step = n_step
		self.step = step
		self.scan_size = scan_size
		self.X = X
		self.Y = Y
		self.intensity_profile = intensity_profile

		#Camera parameters
		self.mode = mode
		self.gain = gain
		self.shutter = shutter

		#Fit parameters
		self.crop_width = crop_width
		self.resolution_factor = resolution_factor

		#Results of fits
		self.positions = positions
		self.widths = widths
		self.over_exposed = over_exposed
		self.intensities = intensities
		self.displacements = displacements
		self.angle_distortions = angle_distortions
		self.wavefront = wavefront

		#Phase stuff
		self.zernike_coeff = zernike_coeff
		self.phase_map = phase_map
		self.phase_map_without012 = phase_map_without012

		#Intensity stuff
		self.p_res_gauss_2d = p_res_gauss_2d
		self.intensity_interpolated_map = intensity_interpolated_map
		self.intensity_target_map = intensity_target_map
		self.ratio_map = ratio_map
		self.ratio_rescale_map = ratio_rescale_map
		self.local_width_map = local_width_map

		#references
		self.reference_ccd = reference_ccd
		self.reference_dmd = reference_dmd
		self.reference_phase = reference_phase

def saveData(data, saveName):
	pickle.dump(data, open(data.folderPath+saveName,"wb"))
	# gradient = phase gradient in cycles (wavelengths)/DMD pixel
	np.savez(data.folderPath+saveName.rstrip('.p'),
		x0=data.x0,
		y0=data.y0,
		n_step=data.n_step,
		step=data.step,
		radius=data.radius,
		scan_size=data.scan_size,
		X=data.X,
		Y=data.Y,
		hologram=data.hologram,
		intensities=data.intensities,
		gradient=np.sqeeze(np.array(data.angle_distortions))/wavelength/diff_order*mirrorSize,
		wavefront=np.squeeze(data.wavefront),
		phase_map=np.squeeze(data.phase_map))
	return data

def readData(filePath):
	print filePath
	data = pickle.load(open(filePath,"rb"))
	return data

def saveBMP(name,array):
	w,h = 1024,768 ##size of the image
	'''Given an array and a name, save this array as a BMP image with the given name'''
	im = Image.new("1",(w,h)) #pixel will be 0 or 1
	im.format = "bmp"
	pix = im.load()

	# Pixel value is equal to the value stored in the input array
	for j in range(w):#j is the colonnes' index
		for i in range(h):#i is the rows' index
			#in an array, array[row,colonne]
			#in an image, pix[x,y]=pix[colonne,row]
			pix[j,i]=array[i,j]

	# Save the BMP in the logo folder with the input name
	im.save(name+".bmp")
	print "Image "+name+" saved"

# ================
# = Main =
# ================

def ft_scan_main(data, saveImage=True, ROI=None):

	#Get all parameters from data object
	folderPath = data.folderPath
	x0 = data.x0
	y0 = data.y0
	n_step = data.n_step
	step = data.step
	radius = data.radius
	mode_cam = data.mode
	gain_cam_init = data.gain
	shutter_cam_init = data.shutter
	crop_width = data.crop_width
	resolution_factor = data.resolution_factor
	hologram = data.hologram

	if mode_cam == 1: binning = 1
	elif mode_cam == 2: binning = 2
	else: print 'Mode non implemented ?'

	## Create Scan
	X, Y = init_grid(x0, y0, n_step, step)
	scan_size = (n_step-1)*step + 2*radius

	#save data
	data.X = X
	data.Y = Y
	data.scan_size = scan_size
	saveData(data,'save.p')

	# TODO get ROI from the GUI with top, bottom, left, right
	positions, widths, intensities, over_exposed = scan(X, Y, radius, hologram, folderPath, mode_cam, shutter_cam_init, gain_cam_init, saveImage, ROI, resolution_factor, binning)
	corrupted_measurements = getCorruptedMeasurements(positions, intensities, over_exposed)

	#save data
	data.positions = positions
	data.widths = widths
	data.intensities = intensities
	data.over_exposed = over_exposed
	saveData(data,'save.p')


	print 'Processing phase data'
	###Calculate the wavefront
	displacements, angle_distortions = displacement(X, Y, positions, corrupted_measurements)
	wavefront = frankotChellappa(n_step, step, angle_distortions)

	#Zernike Fit the wavefront
	print 'Zernike fitting ...'
	number_zernike_polynomial = 30
	zernike_coeff = fitWavefront(X, Y, n_step, radius, scan_size, wavefront, number_zernike_polynomial)

	#Create phase_map
	print 'Calculating phase map ...'
	phase_map , grid_mask = createPhaseMap(zernike_coeff, scan_size)
	phase_map_without012 , grid_mask = createPhaseMap(zernike_coeff, scan_size, with012=False)

	#save data
	print 'Saving phase data'
	data.displacements.append(displacements)
	data.angle_distortions.append(angle_distortions)
	data.wavefront.append(wavefront)
	data.zernike_coeff.append(zernike_coeff)
	data.phase_map.append(phase_map)
	data.phase_map_without012.append(phase_map_without012)
	# data.reference_dmd = reference_dmd
	# data.reference_phase = reference_phase
	saveData(data,'save.p')


	print 'Intensity map fit'
	###Fit measured intensity with a gaussian
	p_res_gauss_2d, intensity_interpolated_map, intensity_target_map = fitGaussianIntensity(x0, y0, scan_size, n_step, step, X, Y, intensities)

	print 'Saving intensity datas'
	data.p_res_gauss_2d = p_res_gauss_2d
	data.intensity_interpolated_map = intensity_interpolated_map
	data.intensity_target_map = intensity_target_map
	saveData(data,'save.p')

	print 'Thank you for waiting : )'
	return data

if __name__ == '__main__' :
	pass
