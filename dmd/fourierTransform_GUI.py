#!/usr/bin/env python
# -*- coding: utf8 -*-

'''
GUI (Graphical User Interface) for :
--> Shack-Hartmann wavefront sensor
--> Holograms creation to correct wavefront, induce controlled aberrations, ...
... beam shapping (HG, LG modes), or various holograms

It uses functions defined in fourierTransform_functions.py

Written by S. de Leseleuc (sylvain.dldk(at)gmail(dot)com)
Modified by J. Mohan to adapt it to the new Vialux DMD
Last Modified : 20 October 2017

PROCEDURE FOR MEASURING THE DMD WAVEFRONT AND INTENSITY DISTRIBUTION
(1) New Folder
(2) Choose grid parameters in units of DMD pixels (default is good):
	x0, y0 = 512, 360
	aperture radius = 20
	n_step = 40
	step = 10
(3) Find a good camera shutter time and gain by using the "Hologram from save file" action to load
	dmd_fft/data/centered_supergrating_patch.p then adjusting the shutter and gainthen viewing the result
	using "Send Hologram" until the peak intensity is around 220 counts (leave a buffer before the CCD saturation 255).
	With r=20, ND=3 filter, 85mA laser current, default is good (shutter=27, gain=0)
(4) If building off previous scans, use "Hologram from save file" to select data_correction.p (output of Repair).
	This does not modify the previous save.p file but just loads the hologram in data_correction.p into the GUI program
	If starting a new scan, go directly to step (5)
(5) Use "Scan" with the desired camera binning (2 is good), whether to add to existing data (if so, choose the previous
	save.p file, which should be copied into the current working directory to avoid overwriting), and whether or not to save images
	TODO use new scan or existing scan? what do the two options do?
(6) Once the scan is complete, plot the results if desired
(7) Open the "Repair" window, repair only phase, include the 0, 1, and 2 Zernike polynomials,
	and select the save.p file from this scan once clicking "Repair"
(8) To give the information from this scan to the next one, copy this save.p file to directory of the new scan
	and begin immediately (while the hologram from this scan is still in memory) or reload the hologram from
	data_correction.p from this scan before beginning the next

TODO
- add option to automatically do the N (~3) iterations of scan, plot, repair
- add unwrapping to the wavefront heatmaps since the wavefront aberration is definitely more than 1 wavelength
- change camera integration time in each iteration of the main scan loop to collect low intensity patches
- ensure the right DMD parameters are being used (number of pixels, pitch, blaze angle, ROI)
- in current configuration, on pixels are set to 0 instead of 1 (add this as a field in GUI)
- add grating angle as a field in GUI
- what are good values for scan parameters step and n_step?
- why does the beam profiler get weird and freez the top?
- add parameter in scan menu for changing the amount of time for which each image is displayed on the DMD
- end blocking behavior of plt.show() maybe by launching it as a thread
- make data folder so the save functions work
- solve all TODOs in communcation.py, fourierTransform_functions.py, and this file
- fix progress bar updating in scan
- add command on GUI to send hologram to DMD and display the image on the CCD (augment sendHologram)
- changing spacing in Hologram menu doesn't seem to change the hologram in memory
- remove new scan/old scan option since we always have to do a new scan
- catch error for invalid grid size (if any of the patches extend beyond the DMD)
- add boolean parameter in scan menu whether to crop images to the ROI or not
- fix error 'only the main thread can process Windows messages' when closing plot windows
- on energies plot, overlay expected trace for a gaussian beam with the same waist
'''



import os
import thread
import time
import sys
import numpy as np
import copy
sys.path.append('C:\\Users\\quantumoptics\\Documents\\DMD\\DMD_programming\\dmdcom\\') # put communication.py in the path
from fourierTransform_functions import *
import PIL.Image, PIL.ImageTk

#Tk
from Tkinter import *
import tkFont
import tkFileDialog
import tkMessageBox

#Matplotlib
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from mpl_toolkits.mplot3d import Axes3D


# ============================
# = Initialization variables =
# ============================

#scriptPath = 'C:\\Users\\VP-DMD\\Desktop\\PythonScripts'
#dataPath = 'C:\\Users\\VP-DMD\\Desktop\\data\\'
# dataPath = 'C:\\Users\\quantumoptics\\Documents\\DMD\\DMD_programming\\data\\'
dataPath = os.getcwd()+'\\data\\'

saveFormat = '.png'

showPlot_initialValue = 1

x0_initialValue = 512
y0_initialValue = 360

n_step_initialValue = 40
radius_initialValue = 20
step_initialValue = 10

spacing_initialValue = 6
smoothing_factor_initialValue = 0.1
smoothing_initialValue = 0

# ROI[1]-ROI[0] and ROI[3]-ROI[2] must be an integer multiple of the resolution_factor
# ROI = [top, bottom, left, right] on CCD
ROI = np.array([450, 650, 620, 800])

on = 0 # what value must be passed to the DMD for a pixel to be on (blazed)

# =========
# = Classes =
# =========

# Application

class Application(Tk):

	def __init__(self):
		Tk.__init__(self)
		self.frame = Frame(self)
		self.frame.grid()


		#Fonts
		dummy_label = Label(self.frame, text = 'I am a dummy label')
		global regular_font
		global underline_font
		global italic_font
		global bold_font
		global font_name
		global font_size

		regular_font = tkFont.Font(dummy_label, dummy_label.cget("font"))

		font_name = regular_font.cget('family')
		font_size = regular_font.cget('size')

		underline_font = copy.copy(regular_font)
		underline_font.configure(underline = True)

		italic_font = copy.copy(regular_font)

		#Adding all menus
		self.folderMenu = FolderMenu(self)
		self.gridMenu = GridMenu(self)
		self.hologramMenu = HologramMenu(self)
		self.cameraMenu = CameraMenu(self)
		self.actionMenu = ActionMenu(self)

		self.bind('<Return>', self.updateAll)

	def updateAll(self,event = None):
		self.folderMenu.update()
		self.gridMenu.update()
		self.hologramMenu.update()
		self.cameraMenu.update()

# Menus

class FolderMenu(Tk):

	def __init__(self, root):

		self.root = root

		#Frame
		folderMenu = Frame(root, width = 50)
		folderMenu.grid(row = 0, column=0, sticky = N)

		folderMenu_label = Label(folderMenu, text = 'FOLDER MENU')
		folderMenu_label.grid(row = 0, columnspan = 2)

		#Folder Path
		folderPathMenu = Frame(folderMenu, width = 50)
		folderPathMenu.grid(row = 1, column=0, sticky = N)

		self.folderPath = dataPath
		self.folderPath_label = Label(folderPathMenu, text=self.folderPath)
		self.folderPath_label.grid(row = 0, columnspan=2)

		#Choose folder
		chooseFolder_button = Button(folderPathMenu, text="Go to", command=self.chooseFolder)
		chooseFolder_button.grid(row = 1, column = 0)
		folderUpdate_auto_button = Button(folderPathMenu, text="New Folder", command=self.updateFolder_auto)
		folderUpdate_auto_button.grid(row = 1, column = 1)

		separator1 = Frame(folderPathMenu, height=10, width=50, bd=10, relief='sunken', bg='black')
		separator1.grid(row=2,columnspan=2)

		#Ask for repairing phase map
		repairingPhaseMap = Frame(folderMenu, width = 50)
		repairingPhaseMap.grid(row = 2, sticky = N)

		self.refPhaseMapName = ''
		self.referencePhaseMap_label = Label(repairingPhaseMap ,text = 'Reference Phase Map : \n '+ self.refPhaseMapName)
		self.referencePhaseMap_label.grid(row = 0, columnspan =2)

		chooseRefPhaseMap_button = Button(repairingPhaseMap, text = 'Choose a ref. phase map', command = self.chooseRefPhaseMap)
		chooseRefPhaseMap_button.grid(row = 1, columnspan = 2)

		separator2 = Frame(folderPathMenu, height=10, width=50, bd=10, relief='sunken', bg='black')
		separator2.grid(row=2,columnspan=2)

		#Choose save format
		saveMenu = Frame(folderMenu, width = 50)
		saveMenu.grid(row = 3, column=0, sticky = N)

		input_saveFormat_label = Label(saveMenu, text= 'Save Format : ')
		input_saveFormat_label.grid(row = 0, column = 0)
		self.input_saveFormat = Entry(saveMenu, width = 4 )
		self.input_saveFormat.grid(row = 0, column = 1)
		self.input_saveFormat.insert(0,saveFormat)

		separator3 = Frame(saveMenu, height=10, width=50, bd=10, relief='sunken', bg='black')
		separator3.grid(row=1,columnspan=2)

		#Load parameters
		loadMenu = Frame(folderMenu, width = 50)
		loadMenu.grid(row = 4, column=0, sticky = N)

		load_button = Button(loadMenu, text="Load Params", command=self.loadParams)
		load_button.grid(row = 1, column = 0)

		#Show Plots
		showPlotMenu = Frame(folderMenu, width = 50)
		showPlotMenu.grid(row = 5, column = 0)

		self.showPlot_var = IntVar() #0 = non selected, 1 = selected = display aperture
		self.showPlot_var.set(showPlot_initialValue)
		showPlot_var_check = Checkbutton(showPlotMenu, text = 'Show Plots', variable= self.showPlot_var)
		showPlot_var_check.grid(row = 0, columnspan = 2)

	def chooseFolder(self):
		self.folderPath = tkFileDialog.askdirectory(initialdir = dataPath)+'/'
		self.folderPath_label['text'] = self.folderPath

	def updateFolder_auto(self):
		self.folderPath = updateFolderPath(dataPath)
		self.folderPath_label['text'] = self.folderPath

	def chooseRefPhaseMap(self):
		 self.refPhaseMapName = tkFileDialog.askopenfilename(initialdir = dataPath)
		 self.referencePhaseMap_label['text'] = 'Reference Phase Map : \n '+ self.refPhaseMapName

	def loadParams(self):
		fileName = tkFileDialog.askopenfilename(initialdir = dataPath)
		data = readData(fileName)

		gridMenu = self.root.gridMenu
		cameraMenu = self.root.cameraMenu
		hologramMenu = self.root.hologramMenu
		folderMenu = self.root.folderMenu

		folderMenu.folderPath = data.folderPath
		gridMenu.x0 = data.x0
		gridMenu.y0 = data.y0
		gridMenu.n_step = data.n_step
		gridMenu.step = data.step
		cameraMenu.gain = data.gain
		cameraMenu.shutter = data.shutter
		hologramMenu.hologram = data.hologram
		cameraMenu.reference_ccd = data.reference_ccd

		self.root.updateAll()

	def update(self):
		self.folderPath_label['text'] = self.folderPath

class GridMenu(Tk):

	def __init__(self, root):

		#Frame
		gridMenu = Frame(root, width = 50)
		gridMenu.grid(row = 0, column=1, sticky = N)

		gridMenu_label = Label(gridMenu, text = 'GRID MENU')
		gridMenu_label.grid(columnspan = 2)

		#Grid center position

		self.x0 = x0_initialValue
		self.y0 = y0_initialValue

		centerPosition = Frame(gridMenu, width = 50)
		centerPosition.grid(row = 1, column=0, sticky = N)

		self.centerPosition_label = Label(centerPosition, text='Center Position : (' + str(self.x0)+ ',' + str(self.y0) + ')')
		self.centerPosition_label.grid(row = 0, columnspan=2)

		input_x0_label = Label(centerPosition, text="x0 :")
		input_y0_label = Label(centerPosition, text="y0 :")
		self.input_x0 = Entry(centerPosition, width=4)
		self.input_y0 = Entry(centerPosition, width=4)

		input_x0_label.grid(row = 1, column = 0)
		input_y0_label.grid(row = 2, column = 0)
		self.input_x0.grid(row = 1, column = 1)
		self.input_y0.grid(row = 2, column = 1)

		separator1 = Frame(centerPosition, height=10, width=50, bd=10, relief='sunken', bg='black')
		separator1.grid(row=3,columnspan=2)

		#Radius of aperture

		radiusAperture = Frame(gridMenu, width = 50)
		radiusAperture.grid(row = 2, column=0, sticky = N)

		self.radius = radius_initialValue
		self.FT_radius, self.FT_radius_pixel = calculate_FT_radius(self.radius)

		self.radius_label = Label(radiusAperture, text='Aperture radius : ' + str(self.radius) + ' mirrors')
		self.radius_label.grid(row = 0, columnspan=2)
		self.FT_radius_label = Label(radiusAperture, text='After FT : %.1f um = %d pixels' %(self.FT_radius*1e6, self.FT_radius_pixel))
		self.FT_radius_label.grid(row = 1, columnspan=2)

		input_radius_label = Label(radiusAperture, text="radius :")
		self.input_radius = Entry(radiusAperture, width=4)

		input_radius_label.grid(row = 2, column = 0)
		self.input_radius.grid(row = 2, column = 1)

		separator2 = Frame(radiusAperture, height=10, width=50, bd=10, relief='sunken', bg='black')
		separator2.grid(row=3,columnspan=2)

		#Scanning resolution

		self.n_step = n_step_initialValue #WARNING ONLY ODD NUMBER PLEASE TODO why only odd? can we make it work with even?
		self.step = step_initialValue

		scanningResolution = Frame(gridMenu, width = 50)
		scanningResolution.grid(row = 3, column=0, sticky = N)

		self.n_step_label = Label(scanningResolution, text='Number of step : ' + str(self.n_step))
		self.n_step_label.grid(row = 0, columnspan=2)

		self.step_label = Label(scanningResolution, text='Step size: ' + str(self.step))
		self.step_label.grid(row = 1, columnspan=2)

		self.X0, self.Y0, self.X_1, self.Y_1 =  calculate_grid_span(self.x0, self.y0, self.n_step, self.step, self.radius)
		self.grid_span_label = Label(scanningResolution, text='Grid span : from (%d, %d) to (%d, %d)' %(self.X0, self.Y0, self.X_1, self.Y_1))
		self.grid_span_label.grid(row = 2, columnspan = 2)

		#self.scan_size = self.X_1 - self.X0 + 1 * 2*self.radius

		input_n_step_label = Label(scanningResolution, text="n_step :")
		input_step_label = Label(scanningResolution, text="step :")
		self.input_n_step = Entry(scanningResolution, width=3)
		self.input_step = Entry(scanningResolution, width=3)

		input_n_step_label.grid(row = 3, column = 0)
		input_step_label.grid(row = 4, column = 0)
		self.input_n_step.grid(row = 3, column = 1)
		self.input_step.grid(row = 4, column = 1)

		separator3 = Frame(scanningResolution, height=10, width=50, bd=10, relief='sunken', bg='black')
		separator3.grid(row=5,columnspan=2)

		#Update

		update_button = Button(gridMenu, text="Update", command=self.update)
		update_button.grid(row = 4, columnspan = 2)

	def update(self):

		if len(self.input_x0.get()) != 0:
			self.x0 = int(self.input_x0.get())

		if len(self.input_y0.get()) != 0:
			self.y0 = int(self.input_y0.get())

		if len(self.input_radius.get()) != 0:
			self.radius = int(self.input_radius.get())

		if len(self.input_n_step.get()) != 0:
			self.n_step = int(self.input_n_step.get())

		if len(self.input_step.get()) != 0:
			self.step = int(self.input_step.get())

		self.centerPosition_label['text'] = 'Center Position : ' + str(self.x0)+ ' - ' + str(self.y0)
		self.radius_label['text'] ='Aperture radius : ' + str(self.radius) + ' mirrors'
		self.FT_radius, self.FT_radius_pixel  = calculate_FT_radius(self.radius)
		self.FT_radius_label['text'] = 'After FT : %.1f um = %d pixels' %(self.FT_radius*1e6, self.FT_radius_pixel)
		self.n_step_label['text'] = 'Number of step : ' + str(self.n_step)
		self.step_label['text'] = 'Step size: ' + str(self.step)
		self.X0, self.Y0, self.X_1, self.Y_1 =  calculate_grid_span(self.x0, self.y0, self.n_step, self.step, self.radius)
		self.grid_span_label['text'] = 'Grid span : from (%d, %d) to (%d, %d)' %(self.X0, self.Y0, self.X_1, self.Y_1)

class HologramMenu(Tk):

	def __init__(self, root):

		self.root = root

		#Frame
		hologramMenu = Frame(root, width = 50)
		hologramMenu.grid(row = 0, column=2, sticky = N)

		hologramMenu_label = Label(hologramMenu, text = 'HOLOGRAM MENU')
		hologramMenu_label.grid(row = 0, columnspan = 2)

		#Spacing

		spacingMenu = Frame(hologramMenu, width = 50)
		spacingMenu.grid(row = 1, sticky = N)

		self.spacing = spacing_initialValue
		self.dtheta, self.dx, self.dpixel = calculate_separation_order(self.spacing)

		self.spacing_label = Label(spacingMenu, text='Spacing : ' + str(self.spacing) + ' mirrors')
		self.spacing_label.grid(row = 0, columnspan=2)

		self.separation_label = Label(spacingMenu, text='Separation on CCD: %d um' %(self.dx*1e6))
		self.separation_label.grid(row = 1, columnspan=2)

		input_spacing_label = Label(spacingMenu, text="spacing :")
		self.input_spacing = Entry(spacingMenu, width=4)

		input_spacing_label.grid(row = 2, column = 0)
		self.input_spacing.grid(row = 2, column = 1)

		separator1 = Frame(spacingMenu, height=10, width=50, bd=10, relief='sunken', bg='black')
		separator1.grid(row=3,columnspan=2)

		#Smoothing

		smoothingMenu = Frame(hologramMenu, width = 50)
		smoothingMenu.grid(row = 2, sticky = N)

		self.smoothing_factor = smoothing_factor_initialValue
		self.dtheta, self.dx, self.dpixel = calculate_separation_order(self.smoothing_factor)

		self.smoothing_label = Label(smoothingMenu, text='Smoothing : ' + str(self.smoothing_factor))
		self.smoothing_label.grid(row = 0, columnspan=2)

		input_smoothing_label = Label(smoothingMenu, text="factor :")
		self.input_smoothing = Entry(smoothingMenu, width=4)

		input_smoothing_label.grid(row = 2, column = 0)
		self.input_smoothing.grid(row = 2, column = 1)

		self.smooth_var = IntVar()
		self.smooth_var.set(smoothing_initialValue)
		smooth_var_check = Checkbutton(smoothingMenu, text = 'smooth ?', variable = self.smooth_var)
		smooth_var_check.grid(row = 3, columnspan = 2)

		separator2 = Frame(smoothingMenu, height=10, width=50, bd=10, relief='sunken', bg='black')
		separator2.grid(row=4,columnspan=2)


		#Generate hologram

		generateHologramMenu = Frame(hologramMenu, width = 50)
		generateHologramMenu.grid(row = 3, sticky = N)

		self.hologram = imgFunc_FT.initGratingFast(self.spacing, on)

		#from save
		holoFromSave_button = Button(generateHologramMenu, text="Hologram from save file", command=self.hologramFromSave)
		holoFromSave_button.grid(row = 2, columnspan = 2)

		#from grating
		holoGrating_button = Button(generateHologramMenu, text="Hologram from grating", command=self.hologramGrating)
		holoGrating_button.grid(row = 3, columnspan = 2)

		#see current hologram
		seeHologram_button = Button(generateHologramMenu, text="See Hologram", command=self.seeHologram)
		seeHologram_button.grid(row = 4, columnspan = 2)

		#send current hologram
		sendHologram_button = Button(generateHologramMenu, text="Send Hologram", command=self.sendHologram)
		sendHologram_button.grid(row = 5, columnspan = 2)

		#Update
		update_button = Button(generateHologramMenu, text="Update", command=self.update)
		update_button.grid(row = 6, columnspan = 2)

	def update(self):
		if len(self.input_spacing.get()) != 0:
			self.spacing = int(self.input_spacing.get())

		if len(self.input_smoothing.get()) != 0:
			self.smoothing_factor = float(self.input_smoothing.get())


		self.spacing_label['text'] = 'Spacing : ' + str(self.spacing) + ' mirrors'
		self.dtheta, self.dx, self.dpixel = calculate_separation_order(self.spacing)
		self.separation_label['text'] = 'Separation on CCD: %d um' %(self.dx*1e6)
		self.smoothing_label['text'] = 'Smoothing : ' + str(self.smoothing_factor)
		self.hologram = imgFunc_FT.initGratingFast(self.spacing, on) # TODO don't kill a hologram with update

	def hologramFromSave(self):
		filename = tkFileDialog.askopenfilename(initialdir = self.root.folderMenu.folderPath)
		if filename.split('.')[-1] == 'p': # pickle.dump
			self.hologram = readData(filename).hologram
		elif filename.split('.')[-1] == 'npy': # np.save
			self.hologram = np.load(filename)
		elif filename.split('.')[-1] == 'npz': # np.save
			self.hologram = np.load(filename)['hologram']
		else: # unrecognized file type
			raise ValueError('I can only read pickle, np.save, and np.savez files')
		self.sendHologram()
		self.seeHologram()

	def hologramGrating(self):
		self.hologram = imgFunc_FT.initGratingFast(self.spacing, on)
		self.sendHologram()
		self.seeHologram()

	def seeHologram(self):
		fig = plt.figure()
		ax = fig.add_subplot(111)
		ax.set_title('Hologram')
		ax.matshow( np.where(self.hologram==on, 1, 0), cmap = cm.gray, origin='lower')
		# plt.show(block=False) # TODO want this but the plot window doesn't fully close. use thread?
		plt.show()

	def sendHologram(self): # TODO make sure this works. Does one dmd object need to be passed around?
		name = 'Send Hologram'
		dmd = communication.DMD()
		tHologram = dmd.compilePicture(self.hologram)
		dmd.allocSeq(name, 1)
		dmd.putSeq(name, tHologram)
		dmd.timingSeq(name, int(0.4e6))

		mode = self.root.cameraMenu.cameraMode_var.get()
		shutter = self.root.cameraMenu.shutter
		gain = self.root.cameraMenu.gain
		cam = camera.Camera(shutter=shutter, gain=gain, cameraMode=mode)

		dmd.startProj(name)
		time.sleep(0.35)
		img = camera.takePicture(cam)
		dmd.waitProj()

		dmd.freeSeq(name)
		dmd.free()
		camera.finishCam(cam)

		plt.matshow(img)
		plt.title('Full CCD\nmax: {:d}'.format(img.max()))
		plt.colorbar()
		img = img[ ROI[0]:ROI[1], ROI[2]:ROI[3] ]
		plt.matshow(img)
		plt.title('Region of interest (top, bottom, left, right):\n{}\nmax: {:d}'.format(str(ROI), img.max()))
		plt.colorbar()
		plt.show()

class CameraMenu(Tk):

	def __init__(self, root):

		self.root = root

		#Frame
		cameraMenu = Frame(root, width = 50)
		cameraMenu.grid(row = 0, column=3, sticky = N)

		cameraMenu_label = Label(cameraMenu, text = 'CAMERA MENU')
		cameraMenu_label.grid(columnspan = 2)

		#Take picture
		takePictureMenu = Frame(cameraMenu, width = 50)
		takePictureMenu.grid(row = 1)

		takePicture_button = Button(takePictureMenu, text="Take Picture", command=self.commandTakePicture)
		takePicture_button.grid(row = 0, columnspan = 2)

		#Choose camera mode
		cameraMode = Frame(cameraMenu, width = 50)
		cameraMode.grid(row = 2, sticky = N)

		MODES = [
		("1 : 1280x960Y8 - FPS 15", 1),
		("2 : 640x480Y8 - FPS 30", 2)]

		self.cameraMode_var = IntVar()
		self.cameraMode_var.set(1) # initialize

		row_index = 0
		for text, mode in MODES:
			b = Radiobutton(cameraMode, text=text, variable=self.cameraMode_var, value=mode, command = self.root.updateAll)
			b.grid(row = row_index)
			row_index += 1

		#Gain and shutter_time
		configCamMenu = Frame(cameraMenu, width = 50)
		configCamMenu.grid(row = 3, sticky = N)

		self.gain = 0.0
		self.shutter = 4
		# self.shutter = 25

		self.gain_label = Label(configCamMenu, text='Gain : ' + str(self.gain))
		self.gain_label.grid(row = 0, columnspan=2)
		self.shutter_label = Label(configCamMenu, text='Shutter (ms) : ' + str(self.shutter))
		self.shutter_label.grid(row = 1, columnspan=2)

		input_gain_label = Label(configCamMenu, text="Gain :")
		self.input_gain = Entry(configCamMenu, width=3)
		input_gain_label.grid(row = 2, column = 0)
		self.input_gain.grid(row = 2, column = 1)
		input_shutter_label = Label(configCamMenu, text="Shutter (ms) :")
		self.input_shutter = Entry(configCamMenu, width=3)
		input_shutter_label.grid(row = 3, column = 0)
		self.input_shutter.grid(row = 3, column = 1)

		separator1 = Frame(configCamMenu, height=10, width=50, bd=10, relief='sunken', bg='black')
		separator1.grid(row=4,columnspan=2)

		#Update
		update_button = Button(cameraMenu, text="Update", command=self.update)
		update_button.grid(row = 5, columnspan = 2)

	def update(self):
		if len(self.input_gain.get()) != 0:
			self.gain = float(self.input_gain.get())

		if len(self.input_shutter.get()) != 0:
			self.shutter = float(self.input_shutter.get())

		self.gain_label['text'] = 'Gain : ' + str(self.gain)
		self.shutter_label['text'] = 'Shutter : ' + str(self.shutter)

	def commandTakePicture(self):
		today = datetime.datetime.now()

		mode = self.cameraMode_var.get()
		shutter = self.shutter
		gain = self.gain
		saveName = self.root.folderMenu.folderPath + today.strftime("%M")+"-"+today.strftime("%S")+"_gain-%.1f_shutter-%.3f"%(gain,shutter)
		cam = camera.Camera(shutter=shutter, gain=gain, cameraMode=mode)

		name = 'take picture'
		dmd = communication.DMD()
		h = dmd.compilePicture(self.root.hologramMenu.hologram)
		dmd.allocSeq(name, 1)
		dmd.putSeq(name, h)
		dmd.timingSeq(name, 0.4*1000000)

		dmd.startProj(name)
		time.sleep(0.2)
		array_img = camera.takePicture(cam)
		dmd.waitProj()

		dmd.freeSeq(name)
		dmd.free()
		camera.finishCam(cam)

		pil_img = PIL.Image.fromarray(array_img)
		pil_img.save(saveName+'.png')

		fig = plt.figure()
		ax = fig.add_subplot(111)
		clr = ax.imshow(array_img, interpolation='none')
		plt.colorbar(clr)
		# plt.show(block=False) # TODO want this but the plot window doesn't fully close. use thread?
		plt.show()

class ActionMenu(Tk):

	def __init__(self, root):

		self.root = root

		#Frame
		actionMenu = Frame(root, width = 50)
		actionMenu.grid(row = 0, column=4, sticky = N)

		row_index = 0
		actionMenu_label = Label(actionMenu, text = 'ACTION MENU')
		actionMenu_label.grid(row = row_index)

		#Scanning
		row_index += 1
		scan_Button = Button(actionMenu, text="Scan", command=self.openLaunchScanWindow)
		scan_Button.grid(row = row_index)
		self.scanWindow = None

		#Plot datas
		row_index += 1
		plot_button = Button(actionMenu, text = "Plot data", command = self.plotData)
		plot_button.grid(row = row_index)

		#Repair
		row_index += 1
		repair_Button = Button(actionMenu, text="Repair", command=self.openRepairWindow)
		repair_Button.grid(row = row_index)
		self.repairWindow = None

		#Aberration
		row_index += 1
		openAberrationWindow_button = Button(actionMenu, text = 'Create Aberrations', command = self.openAberrationWindow)
		openAberrationWindow_button.grid(row = row_index)
		self.aberrationWindow = None

		#Beam-shapping
		row_index += 1
		openBeamShappingWindow_button = Button(actionMenu, text = 'Beam-shapping', command = self.openBeamShappingWindow)
		openBeamShappingWindow_button.grid(row = row_index)
		self.beamShappingWindow = None

	def openLaunchScanWindow(self):
		self.scanWindow = ScanWindow(self.root)

	def plotData(self):
		gridMenu = self.root.gridMenu
		cameraMenu = self.root.cameraMenu
		folderMenu = self.root.folderMenu
		hologramMenu = self.root.hologramMenu

		folderPath = folderMenu.folderPath

		filename = tkFileDialog.askopenfilename(initialdir = self.root.folderMenu.folderPath)
		data = readData(filename)


		saveFormat = folderMenu.input_saveFormat.get()
		showPlot = folderMenu.showPlot_var.get()

		plotData(data, folderPath, saveFormat, showPlot)

	def openRepairWindow(self):
		self.repairWindow = RepairWindow(self.root)

	def openAberrationWindow(self):
		self.aberrationWindow = AberrationWindow(self.root)

	def openBeamShappingWindow(self):
		self.beamShappingWindow = BeamShappingWindow(self.root)

#Pop up windows

class ScanWindow(Tk): # TODO maybe take another look here and add delay time fields here

	def __init__(self, root):
		self.root = root

		#Pop up toplevel window
		self.scanPopUpWindow = Toplevel()

		#Main frame
		scanPopUpWindow_frame = Frame(self.scanPopUpWindow)
		scanPopUpWindow_frame.grid(row = 0, column = 0, sticky = N)

		#Title
		row_frame_index = 0
		scanPopUpWindow_title = Label(scanPopUpWindow_frame, text = 'WAVEFRONT SCAN')
		scanPopUpWindow_title.configure(font = (font_name, font_size, 'bold'))
		scanPopUpWindow_title.grid(row = row_frame_index, sticky = N)

		#Send scan or not ?
		row_frame_index += 1
		sendScan_frame = Frame(scanPopUpWindow_frame)
		sendScan_frame.grid(row = row_frame_index, sticky = N)

		row_index = 0

		sendScan_label = Label(sendScan_frame, text = 'Do I Send a Scan ?')
		sendScan_label.configure(font = (font_name, font_size, 'underline'))
		sendScan_label.grid(row = row_index)

		MODES = [
		("0 : Use existing scan", 0),
		("1 : Send new scan", 1)]

		self.sendScanMode_var = IntVar()
		self.sendScanMode_var.set(1) # initialize

		for text, mode in MODES:
			row_index += 1
			b = Radiobutton(sendScan_frame, text=text, variable=self.sendScanMode_var, value=mode)
			b.grid(row = row_index)

		#Resolution factor and binning factor
		row_frame_index += 1
		resolutionScan_frame = Frame(scanPopUpWindow_frame)
		resolutionScan_frame.grid(row = row_frame_index, sticky = N)

		row_index = 0

		resolutionScan_label = Label(resolutionScan_frame, text = 'Resolution')
		resolutionScan_label.configure(font = (font_name, font_size, 'underline'))
		resolutionScan_label.grid(row = row_index)

		row_index += 1
		self.binning = self.root.cameraMenu.cameraMode_var.get() #binning of the CCD is 1 (no binning) in mode 1 (1280x960) and 2 if in mode 2 (640x480)
		self.cameraBinning_label = Label(resolutionScan_frame, text = 'Camera binning : %d'%self.binning)
		self.cameraBinning_label.grid(row = row_index)

		row_index += 1
		# self.resolution_factor = 8
		self.resolution_factor = 2
		self.resolution_label = Label(resolutionScan_frame, text = 'Lower resolution before fitting by :\n %d'%self.resolution_factor)
		self.resolution_label.grid(row = row_index)

		row_index += 1
		self.fit_decrease_resolution_factor_scale  = Scale(resolutionScan_frame, from_=0, to=5, resolution=1, orient = HORIZONTAL, showvalue = 0, command = self.update)
		self.fit_decrease_resolution_factor_scale.set(3)
		self.fit_decrease_resolution_factor_scale.grid(row = row_index)

		row_index += 1
		dfit, dtheta, dwavefront = calculate_resolution_limit(self.root.gridMenu.step, self.binning, self.resolution_factor)
		self.resolution_limit_label = Label(resolutionScan_frame, text = 'res. > %.3f lambda'%dwavefront)
		self.resolution_limit_label.grid(row = row_index)

		#What to do with data ?
		row_frame_index += 1
		dataScan_frame = Frame(scanPopUpWindow_frame)
		dataScan_frame.grid(row = row_frame_index, sticky = N)

		row_index = 0

		dataScan_label = Label(dataScan_frame, text = 'What to do with data (optional)?')
		dataScan_label.configure(font = (font_name, font_size, 'underline'))
		dataScan_label.grid(row = row_index)

		row_index += 1
		self.addToExisting_var = IntVar()
		addToExisting_check = Checkbutton(dataScan_frame, text="Add to existing data", variable=self.addToExisting_var)
		addToExisting_check.grid(row = row_index)

		row_index += 1
		self.saveImage_var = IntVar()
		saveImage_check = Checkbutton(dataScan_frame, text = 'Save all images \n Warning : Huge !' , variable = self.saveImage_var)
		saveImage_check.grid(row = row_index)

		#Launch Scan
		row_frame_index += 1
		scan_Button = Button(scanPopUpWindow_frame, text="Launch Scan", command=self.launchScan)
		scan_Button.grid(row = row_frame_index)

		#Quit
		row_frame_index += 1
		quit_Button = Button(scanPopUpWindow_frame, text = 'Quit', command = self._quit)
		quit_Button.grid(row = row_frame_index)

	def launchScan(self): # TODO especially here
		gridMenu = self.root.gridMenu
		cameraMenu = self.root.cameraMenu
		folderMenu = self.root.folderMenu
		hologramMenu = self.root.hologramMenu

		folderPath = folderMenu.folderPath

		if self.addToExisting_var.get():
			print 'Adding to Existing data file'
			fileName = tkFileDialog.askopenfilename(initialdir = folderPath)
			data = readData(fileName)
			data_new = copy.copy(data)
			data_new.folderPath = folderPath
			data_new.hologram = hologramMenu.hologram
		else :
			print 'Creating new Data file'
			data_new = Data(folderPath,hologram = [], smoothing_factor = 0, x0 = 0, y0 = 0, radius = 0, n_step = 0, step = 0, scan_size = 0, X=[], Y =[], intensity_profile = [], mode = 0, gain = 0, shutter = 0, crop_width = 0, resolution_factor = 0, positions = [], widths = [], over_exposed = [], intensities = [], displacements = [], angle_distortions = [], wavefront = [], zernike_coeff = [], phase_map = [], phase_map_without012 = [], p_res_gauss_2d = [], intensity_interpolated_map = [], intensity_target_map = [], ratio_map = [], ratio_rescale_map = [], local_width_map = [], reference_ccd = (0,0), reference_dmd = (0,0), reference_phase = 0)
			data_new.x0 = gridMenu.x0
			data_new.y0 = gridMenu.y0
			data_new.n_step = gridMenu.n_step
			data_new.step = gridMenu.step
			data_new.radius = gridMenu.radius


		data_new.mode = cameraMenu.cameraMode_var.get()
		data_new.gain = cameraMenu.gain
		data_new.shutter = cameraMenu.shutter
		data_new.resolution_factor = self.resolution_factor
		data_new.hologram = hologramMenu.hologram

		saveData(data_new, 'save.p')

		useNewScan = self.sendScanMode_var.get()
		print 'Use new scan :',useNewScan
		saveImage = self.saveImage_var.get()
		print 'Save Image :',saveImage

		#Scan
		#Test : threading the scan, so the window is not blocked !
		# thread.start_new(ft_scan_main, (data_new, useNewScan, saveImage, ROI))
		thread.start_new(ft_scan_main, (data_new, saveImage, ROI))

	def update(self, event = None):
		#update resolution factor
		self.resolution_factor = 2**int(self.fit_decrease_resolution_factor_scale.get())
		self.resolution_label['text'] =  'Lower resolution before fitting by : \n%d'%self.resolution_factor

		#update binning
		self.binning = self.root.cameraMenu.cameraMode_var.get() #binning of the CCD is 1 (no binning) in mode 1 (1280x960) and 2 if in mode 2 (640x480)
		self.cameraBinning_label['text']= 'Camera binning : %d'%self.binning

		#update calculation of resolution limit
		dfit, dtheta, dwavefront = calculate_resolution_limit(self.root.gridMenu.step, self.binning, self.resolution_factor)
		self.resolution_limit_label['text'] = 'res. > %.3f lambda'%dwavefront

	def _quit(self):
		self.scanPopUpWindow.quit()     # stops mainloop
		self.scanPopUpWindow.destroy()  # this is necessary on Windows to prevent  Fatal Python Error: PyEval_RestoreThread: NULL tstate

class RepairWindow(Tk):

	def __init__(self,root):

		#reference to parent
		self.root = root

		#Toplevel window
		self.repairPopUpWindow = Toplevel()

		#Main frame
		repairPopUpWindow_frame = Frame(self.repairPopUpWindow)
		repairPopUpWindow_frame.grid(row = 0, column = 0, sticky = N)

		#Title
		row_frame_index = 0
		repairPopUpWindow_title = Label(repairPopUpWindow_frame, text = 'WAVEFRONT REPAIR')
		repairPopUpWindow_title.grid(row = row_frame_index, sticky = N)

		#What to repair ?
		row_frame_index += 1
		dataRepair_frame = Frame(repairPopUpWindow_frame)
		dataRepair_frame.grid(row = row_frame_index, sticky = N)

		row_index = 0

		dataRepair_label = Label(dataRepair_frame, text = 'What to repair?')
		dataRepair_label.grid(row = row_index)

		row_index += 1
		self.repairPhase_var = IntVar()
		repairPhase_check = Checkbutton(dataRepair_frame, text="Phase", variable= self.repairPhase_var)
		repairPhase_check.grid(row = row_index)

		row_index += 1
		self.repairIntensity_var = IntVar()
		repairIntensitiy_check = Checkbutton(dataRepair_frame, text="Intensity", variable=self.repairIntensity_var)
		repairIntensitiy_check.grid(row = row_index)

		#Target Width
		row_index += 1
		targetWidth_frame = Frame(dataRepair_frame)
		targetWidth_frame.grid(row = row_index)

		self.useOptimalWidthTarget_var = IntVar()
		useOptimalWidthTarget_check	= Checkbutton(targetWidth_frame, text="Use Optimal Width Target", variable=self.useOptimalWidthTarget_var)
		useOptimalWidthTarget_check.grid(row = 0)


		targetWidth_label = Label(targetWidth_frame, text = 'Target Width (in um)')
		targetWidth_label.grid(row = 1, column = 0)
		self.targetWidth_entry = Entry(targetWidth_frame, width = 4)
		self.targetWidth_entry.insert(0,'100')
		self.targetWidth_entry.grid(row = 1, column = 1)


		#Use 012 Zernike pol ?
		row_index += 1
		self.include012_var = IntVar()
		include012_check = Checkbutton(dataRepair_frame, text='Include 012 \n zernike polynoms', variable = self.include012_var)
		include012_check.grid(row = row_index)

		#Launch repair
		row_frame_index += 1
		repair_Button = Button(repairPopUpWindow_frame, text="Repair", command=self.commandRepair)
		repair_Button.grid(row = row_frame_index)

		#Quit
		row_frame_index += 1
		quit_Button = Button(repairPopUpWindow_frame, text = 'Quit', command = self._quit)
		quit_Button.grid(row = row_frame_index)

	def commandRepair(self):

		hologramMenu = self.root.hologramMenu
		folderPath = self.root.folderMenu.folderPath

		#Load data to repair
		print 'Loading data'
		filename = tkFileDialog.askopenfilename(initialdir = folderPath)

		data = readData(filename)
		spacing = hologramMenu.spacing
		smoothing_factor = hologramMenu.smoothing_factor
		smooth = hologramMenu.smooth_var.get()

		#Look what to repair
		correct_phase = self.repairPhase_var.get()
		correct_intensity = self.repairIntensity_var.get()
		useOptimalWidthTarget = self.useOptimalWidthTarget_var.get()
		if not(useOptimalWidthTarget):
			targetWidthCCD = int(self.targetWidth_entry.get())*1e-6
			targetWidthDMD = calculate_iFT_radius(targetWidthCCD)
			print targetWidthDMD
			data.p_res_gauss_2d[2] = targetWidthDMD
			data.p_res_gauss_2d[-1] = 0
			x_new = np.arange(data.x0-data.scan_size/2,data.x0+data.scan_size/2, 1)
			y_new = np.arange(data.y0-data.scan_size/2,data.y0+data.scan_size/2, 1)
			X_new, Y_new = np.meshgrid(x_new, y_new)
			data.intensity_target_map = gauss2dSym(data.p_res_gauss_2d, X_new, Y_new)

		include012 = self.include012_var.get()

		#Give a name to new data
		saveBool = True
		saveName = 'data_correction.p'

		#TODO Thread the repair
		#thread.start_new(createHologramFromData, (folderPath, data, spacing, smooth, smoothing_factor, correct_phase, correct_intensity, include012, saveBool,saveName))
		print 'Creating hologram ...'
		createHologramFromData(folderPath, data, spacing, smooth, smoothing_factor, correct_phase, correct_intensity, include012, saveBool,saveName)
		print '... finished'

		#Send the new hologram to the DMD and view it
		data_new = readData(folderPath + saveName)

		hologramMenu.hologram = data_new.hologram
		hologramMenu.sendHologram()
		hologramMenu.seeHologram()

	def _quit(self):
		self.repairPopUpWindow.quit()     # stops mainloop
		self.repairPopUpWindow.destroy()  # this is necessary on Windows to prevent  Fatal Python Error: PyEval_RestoreThread: NULL tstate

class AberrationWindow(Tk):

	def __init__(self, root):

		self.root = root

		# Top level window
		self.viewingWindow = Toplevel()

		self.viewingWindow_label = Label(self.viewingWindow, text = 'ABERRATION INTERFACE')
		self.viewingWindow_label.grid(row = 0, columnspan = 2)

		# Select aberrations buttons
		selecAber = Frame(self.viewingWindow, width = 50)
		selecAber.grid(column = 0, row = 1,  sticky = N)

		addToDMD_button = Button(selecAber, text = 'Generate Hologram', command = self.generateHologram)
		addToDMD_button.grid(row = 0, columnspan = 2)

		resetAber_button = Button(selecAber, text = 'Reset', command = self.reset)
		resetAber_button.grid(row = 1, columnspan = 2)

		self.n_zernike = 15
		self.scaleZernike = [0]*self.n_zernike
		scale_label = [0]*self.n_zernike
		self.set_scale = [0]*self.n_zernike
		label_list = ['Piston', 'Tilt (Y)', 'Tip (X)', 'Astygmatism +/-45°', 'Defocus', 'Astygmatism 0/90°', 'Trefoil (Y)', 'Coma (Y)', 'Coma (X)', 'Trefoil (X)','Tetrafoil pi/8','2nd Astigm. +/-45°', '3rd Spherical aber.', '2nd Astigm 0/90°', 'Tetrafoil 0' ]
		row_index = 2
		for i in range(self.n_zernike) :
			self.scaleZernike[i] = Scale(selecAber, from_=-1, to=1, resolution=0.05, orient = HORIZONTAL, command = self.viewPhaseMap)
			scale_label[i] = Label(selecAber, text = label_list[i])
			self.set_scale[i] = Entry(selecAber, width = 3)

			scale_label[i].grid(row = row_index + 1, column = 0)
			self.set_scale[i].grid(row = row_index + 2, column = 0)
			self.scaleZernike[i].grid(row = row_index + 1, column = 1, rowspan = 2)

			row_index += 2

		self.phase_map = []

		# # Help menu
		press_enter_label = Label(selecAber, text = "Press 'ENTER' to update values")
		press_reset_label = Label(selecAber, text = "Press 'R' to reset values")
		press_send_label = Label(selecAber, text = "Press 'S' to send to DMD")


		press_enter_label.grid(row = row_index+1 , columnspan = 2)
		press_reset_label.grid(row = row_index+2 , columnspan = 2)
		press_send_label.grid(row = row_index+3 , columnspan = 2)

		row_index += 3


		# Embedding matplotlib plot
		viewingFrame = Frame(self.viewingWindow)
		viewingFrame.grid(column = 1, row = 1, sticky = N)


		self.fig = Figure(figsize=(5,4), dpi=100)
		self.ax = self.fig.add_subplot(111)

		self.canvas = FigureCanvasTkAgg(self.fig, master=viewingFrame)
		self.canvas.show()
		self.canvas.get_tk_widget().pack(side= TOP, fill= BOTH, expand=1)

		toolbar = NavigationToolbar2TkAgg( self.canvas, viewingFrame )
		toolbar.update()
		self.canvas._tkcanvas.pack(side= TOP, fill= BOTH, expand=1)

		button_quit = Button(master= viewingFrame, text='Quit', command=self._quit)
		button_quit.pack(side = BOTTOM)

		# self.canvas.mpl_connect('key_press_event', self.on_key_event)

		self.viewingWindow.bind('<Return>', self.update)
		self.viewingWindow.bind('r', self.reset)
		self.viewingWindow.bind('s', self.generateHologram)

	def generateHologram(self, event = None):
		gridMenu = self.root.gridMenu
		hologramMenu = self.root.hologramMenu
		folderMenu = self.root.folderMenu

		# File path
		filename = folderMenu.refPhaseMapName
		newFolderPath = folderMenu.folderPath

		# Create phase map
		data = readData(filename)
		spacing = hologramMenu.spacing
		smoothing_factor = hologramMenu.smoothing_factor
		smooth = hologramMenu.smooth_var.get()
		scan_size = (gridMenu.n_step-1)*gridMenu.step+2*gridMenu.radius
		phaseMap = self.viewPhaseMap(scan_size, False)
		data.phase_map_without012.append(phaseMap)
		correct_phase = True
		correct_intensity = False
		include012 = False

		# Create hologram and send to DMD
		hologramMenu.hologram = createHologramFromData(newFolderPath, data, spacing, smooth, smoothing_factor, correct_phase, correct_intensity, include012, save = False, unit_disk_mask = True)
		hologramMenu.sendHologram()

	def update(self,event = None):
		for i in range(self.n_zernike) :
			if len(self.set_scale[i].get()) !=0:
				self.scaleZernike[i].set(float(self.set_scale[i].get()))
		self.viewPhaseMap()

	def reset(self, event = None):
		for i in range(self.n_zernike):
			self.scaleZernike[i].set(0)

	def viewPhaseMap(self, scan_size = 200, view = True, event = None):
		zernikeCoeff = np.zeros(self.n_zernike)
		for i in range(self.n_zernike):
			zernikeCoeff[i] = float(self.scaleZernike[i].get())

		if view :
			scan_size = 200
			self.phase_map, grid_mask = createPhaseMap(zernikeCoeff, scan_size, with012 = True, unit_disk_mask = True)
			self.ax.clear()
			self.ax.matshow((self.phase_map+0.5)%1, cmap = cm.gray, vmin = 0, vmax = 1)
			self.fig.canvas.draw()
		else :
			self.phase_map, grid_mask = createPhaseMap(zernikeCoeff, scan_size, with012 = True, unit_disk_mask = True)

		return self.phase_map

	def on_key_event(self,event):
		print('you pressed %s'%event.key)
		key_press_handler(event, canvas, toolbar)

	def _quit(self):
		self.viewingWindow.quit()     # stops mainloop
		self.viewingWindow.destroy()  # this is necessary on Windows to prevent  Fatal Python Error: PyEval_RestoreThread: NULL tstate

class BeamShappingWindow(Tk):

	def __init__(self, root):
		self.root = root

		# Top level window
		self.viewingWindow = Toplevel()

		self.viewingWindow_label = Label(self.viewingWindow, text = 'BEAM SHAPPING INTERFACE')
		self.viewingWindow_label.grid(row = 0, columnspan = 2)

		# Beam-shapping options
		optionMenu = Frame(self.viewingWindow, width = 50)
		optionMenu.grid(row = 1, column = 0, sticky = N)

		# Hermite Gaussian menu
		hermiteGaussianMenu = Frame(optionMenu)
		hermiteGaussianMenu.grid(row = 0, column = 0, sticky = N)

		hermiteGaussianMenu_label = Label(hermiteGaussianMenu, text = 'HERMITE GAUSSIAN MENU')
		hermiteGaussianMenu_label.grid(row = 0, columnspan = 2)

		viewHermiteGaussProfile_button = Button(hermiteGaussianMenu, text = 'View Hermite-Gaussian profile', command = self.viewHermiteGaussProfile)
		viewHermiteGaussProfile_button.grid(row = 1, columnspan = 2)

		generateHologram_HG_button = Button(hermiteGaussianMenu, text = 'Generate Hologram', command = self.generateHologram_HG)
		generateHologram_HG_button.grid(row = 2, columnspan = 2)

		m_order_label = Label(hermiteGaussianMenu, text = 'm : ')
		n_order_label = Label(hermiteGaussianMenu, text = 'n : ')
		waist_hg_label = Label(hermiteGaussianMenu, text = 'waist : ')
		self.m_order = Entry(hermiteGaussianMenu, width = 2)
		self.n_order = Entry(hermiteGaussianMenu, width = 2)
		self.waist_hg = Entry(hermiteGaussianMenu, width = 4)
		m_order_label.grid(row = 3, column = 0)
		n_order_label.grid(row = 4, column = 0)
		waist_hg_label.grid(row = 5, column = 0)
		self.m_order.grid(row = 3, column = 1)
		self.n_order.grid(row = 4, column = 1)
		self.waist_hg.grid(row = 5, column = 1)

		# Laguerre Gaussian Menu
		laguerreGaussianMenu = Frame(optionMenu)
		laguerreGaussianMenu.grid(row = 1, column = 0, sticky = N)

		laguerreGaussianMenu_label = Label(laguerreGaussianMenu, text = 'LAGUERRE GAUSSIAN MENU')
		laguerreGaussianMenu_label.grid(row = 0, columnspan = 2)

		viewLaguerreGaussProfile_button = Button(laguerreGaussianMenu, text = 'View Laguerre-Gaussian profile', command = self.viewLaguerreGaussProfile)
		viewLaguerreGaussProfile_button.grid(row = 1, columnspan = 2)

		generateHologram_LG_button = Button(laguerreGaussianMenu, text = 'Generate Hologram', command = self.generateHologram_LG)
		generateHologram_LG_button.grid(row = 2, columnspan = 2)

		p_order_label = Label(laguerreGaussianMenu, text = 'p : ')
		l_order_label = Label(laguerreGaussianMenu, text = 'l : ')
		waist_lg_label = Label(laguerreGaussianMenu, text = 'waist : ')
		self.p_order = Entry(laguerreGaussianMenu, width = 2)
		self.l_order = Entry(laguerreGaussianMenu, width = 2)
		self.waist_lg = Entry(laguerreGaussianMenu, width = 4)
		p_order_label.grid(row = 3, column = 0)
		l_order_label.grid(row = 4, column = 0)
		waist_lg_label.grid(row = 5, column = 0)
		self.p_order.grid(row = 3, column = 1)
		self.l_order.grid(row = 4, column = 1)
		self.waist_lg.grid(row = 5, column = 1)

		# Embedding matplotlib plot
		viewingFrame = Frame(self.viewingWindow)
		viewingFrame.grid(column = 1, row = 1, sticky = N)


		self.fig = Figure(figsize=(5,4), dpi=100)
		self.ax = self.fig.add_subplot(111)

		self.canvas = FigureCanvasTkAgg(self.fig, master=viewingFrame)
		self.canvas.show()
		self.canvas.get_tk_widget().pack(side= TOP, fill= BOTH, expand=1)

		toolbar = NavigationToolbar2TkAgg( self.canvas, viewingFrame )
		toolbar.update()
		self.canvas._tkcanvas.pack(side= TOP, fill= BOTH, expand=1)

		button_quit = Button(master= viewingFrame, text='Quit', command=self._quit)
		button_quit.pack(side = BOTTOM)

		# self.canvas.mpl_connect('key_press_event', self.on_key_event)

		# self.viewingWindow.bind('<Return>', self.update)
		# self.viewingWindow.bind('r', self.reset)
		# self.viewingWindow.bind('s', self.addToDMD)

	def viewHermiteGaussProfile(self):
		# hermiteGaussianDraw()

		gridMenu = self.root.gridMenu
		hologramMenu = self.root.hologramMenu
		folderMenu = self.root.folderMenu

		# Generate hermite gauss profile
		scan_size = 200
		m = int(self.m_order.get())
		n = int(self.n_order.get())
		waist = float(self.waist_hg.get())
		h = hermiteGaussian(scan_size, m, n, waist)

		# View hermite gauss profile
		self.ax.clear()
		self.ax.matshow(h, cmap = cm.gray)
		self.fig.canvas.draw()

	def generateHologram_HG(self):
		gridMenu = self.root.gridMenu
		hologramMenu = self.root.hologramMenu
		folderMenu = self.root.folderMenu

		# File path
		filename = folderMenu.refPhaseMapName
		newFolderPath = folderMenu.folderPath

		# Get data
		data = readData(filename)
		spacing = hologramMenu.spacing
		smoothing_factor = hologramMenu.smoothing_factor
		smooth = hologramMenu.smooth_var.get()
		m = int(self.m_order.get())
		n = int(self.n_order.get())
		waist = float(self.waist_hg.get())

		# Create hologram
		hologram = hermiteGaussianHologramFromData(newFolderPath, data,spacing, smooth, smoothing_factor, m, n, waist)
		hologramMenu.hologram = hologram

		# View hologram
		self.ax.clear()
		self.ax.matshow(np.where(hologram==on, 1, 0), cmap = cm.gray, vmin = 0, vmax = 1, origin='lower')
		self.fig.canvas.draw()

		# Send hologram
		hologramMenu.sendHologram()

	def viewLaguerreGaussProfile(self):
		# hermiteGaussianDraw()

		gridMenu = self.root.gridMenu
		hologramMenu = self.root.hologramMenu
		folderMenu = self.root.folderMenu

		# Generate hermite gauss profile
		scan_size = 200
		p = int(self.p_order.get())
		l = int(self.l_order.get())
		waist = float(self.waist_lg.get())
		lg = laguerreGaussian(scan_size, p, l, waist)

		# View hermite gauss profile
		self.ax.clear()
		self.ax.imshow(colorize(lg))
		self.fig.canvas.draw()

	def generateHologram_LG(self):
		gridMenu = self.root.gridMenu
		hologramMenu = self.root.hologramMenu
		folderMenu = self.root.folderMenu

		# File path
		filename = folderMenu.refPhaseMapName
		newFolderPath = folderMenu.folderPath

		# Get data
		data = readData(filename)
		spacing = hologramMenu.spacing
		smoothing_factor = hologramMenu.smoothing_factor
		smooth = hologramMenu.smooth_var.get()
		p = int(self.p_order.get())
		l = int(self.l_order.get())
		waist = float(self.waist_lg.get())

		# Create hologram
		hologram = laguerreGaussianHologramFromData(newFolderPath, data,spacing, smooth, smoothing_factor, p, l, waist)
		hologramMenu.hologram = hologram

		# View hologram
		self.ax.clear()
		self.ax.matshow(np.where(hologram==on, 1, 0), cmap = cm.gray, vmin = 0, vmax = 1)
		self.fig.canvas.draw()

		# Send hologram
		hologramMenu.sendHologram()

	def on_key_event(self,event):
		print('you pressed %s'%event.key)
		key_press_handler(event, canvas, toolbar)

	def _quit(self):
		self.viewingWindow.quit()     # stops mainloop
		self.viewingWindow.destroy()  # this is necessary on Windows to prevent  Fatal Python Error: PyEval_RestoreThread: NULL tstate



def main():
	app = Application()
	app.mainloop() # pop-up window, enter the tk event loop

if __name__ == '__main__':
	main()
