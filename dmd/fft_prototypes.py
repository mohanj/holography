import numpy as np
import matplotlib.pyplot as plt
import math
import pickle

from numpy import fft
from fourierTransform_functions import createHologram, createLocalWidthMap, zernikel


### Auxiliary functions to define a pattern on the DMD ###
# TODO: Make the Fourier transform faster by using rfft2 and symmetrisation

def circular_aperture(radius=30, h=768, w=1024):
    """
    Create a cropped circular aperture.
    """
    img = np.zeros((h, w))
    r = np.linspace(-radius, radius, 2*radius+1)
    X, Y = np.meshgrid(r, r)
    circle = np.where(X*X + Y*Y <= radius*radius, 1., 0.)
    img_dmd = np.where(X <= 10, circle, 0.)
    return img_dmd


def hologram_from_file(kx=0.1, ky=0, filename=None, correct_intensity=False, correct_phase=True, include012=True):
    
    # Load parameters from datafile
    data = pickle.load(open(filename, 'rb'))
    
    # Center position of the scan. Reference for the phase map and hologram
    x0 = data.x0
    y0 = data.y0
    
    # Scan parameters
    n_step = data.n_step
    step = data.step
    scan_size = data.scan_size
    
    #Create correction map for phase
    if correct_phase:
        if include012:
            phase_map_list = data.phase_map
        else:
            phase_map_list = data.phase_map_without012
    else:
        phase_map_list = []
    
    #Create correction map for intensity
    if correct_intensity:
        intensity_interpolated_map = data.intensity_interpolated_map
        intensity_target_map = data.intensity_target_map
        ratio_map, ratio_rescale_map, local_width_map = createLocalWidthMap(intensity_interpolated_map, intensity_target_map, x0, y0, n_step, step, scan_size)
    else:
        local_width_map = []
    
    h = createHologram(phase_map_list, local_width_map, x0, y0, scan_size,
        spacing=0, smoothing=False, smoothing_factor=1,
        correct_phase=correct_phase, correct_intensity=correct_intensity,
        unit_disk_mask=False, kx=kx, ky=ky
        )
    
    return h


# def phase_width_from_function(func, args=(), kx=0.1, ky=0., n_res=1000, w=500, h=500, plot=False):
#     """
#     Return a phase and width map from a function that defines the far field image.
#     See hologram_from_function for parameters.
#     """
#     # Get the image to project from `func`
#     k_array = np.linspace(-0.5, 0.5, n_res, endpoint=False)
#     kx_array, ky_array = np.meshgrid(k_array, k_array)
#     img_far_field = func(kx_array, ky_array, *args)

#     # Apply inverse Fourier transform on the far field picture to get the pattern to display on the DMD
#     img_dmd = fft.fftshift(fft.fft2((fft.ifftshift(img_far_field))))

#     # Crop the pattern
#     crop = n_res/2-h/2, n_res/2+(h+1)/2, n_res/2-w/2, n_res/2+(w+1)/2
#     img_dmd = img_dmd[crop[0]:crop[1], crop[2]:crop[3]]
#     # intensity_map = np.abs(img_dmd)**2
#     intensity_map = np.abs(img_dmd)
#     phase_map = np.angle(img_dmd)/(2*np.pi) + 0.01 # needed to avoid rounding errors in `createHologram`

#     # Create a local width map from the intensity map
#     norm_intensity_map = intensity_map/np.max(intensity_map)
#     local_width = np.arcsin(norm_intensity_map)
#     # local_width = 2*np.arcsin(norm_intensity_map)
#     # local_width = np.pi/2 * norm_intensity_map

#     if plot:
#         plt.matshow(img_far_field, cmap='gray')
#         plt.colorbar()
#         plt.matshow(intensity_map, cmap='gray')
#         plt.colorbar()
#         plt.matshow(phase_map, cmap='hsv')
#         plt.colorbar()
#         plt.show()

#     return phase_map, local_width


def phase_amplitude_from_function(func, args=(), kx=0.1, ky=0., n_res=1000, w=500, h=500, plot=False):
    """
    Return a phase and amplitude map from a function that defines the far field image.
    See hologram_from_function for parameters.
    """
    # Get the image to project from `func`
    k_array = np.linspace(-0.5, 0.5, n_res, endpoint=False)
    kx_array, ky_array = np.meshgrid(k_array, k_array)
    img_far_field = func(kx_array, ky_array, *args)

    # Apply inverse Fourier transform on the far field picture to get the pattern to display on the DMD
    img_dmd = fft.fftshift(fft.fft2((fft.ifftshift(img_far_field))))

    # Crop the pattern
    crop = n_res/2-h/2, n_res/2+(h+1)/2, n_res/2-w/2, n_res/2+(w+1)/2
    img_dmd_roi = img_dmd[crop[0]:crop[1], crop[2]:crop[3]]

    amplitude_map = np.abs(img_dmd_roi)
    phase_map = np.angle(img_dmd_roi)/(2*np.pi) + 0.01 # needed to avoid rounding errors in `createHologram`

    part_on_roi = float(np.sum(amplitude_map**2)/np.sum(np.abs(img_dmd)**2))
    if math.isnan(part_on_roi):
        part_on_roi = 1.

    warning_threshold = 0.8

    if part_on_roi < warning_threshold:
        print ('WARNING: only {0:.1f} % of the hologram is located inside the ROI on the DMD, '.format(100*part_on_roi) +
            'expect power losses (warning threshold: {0:.0f} %)'.format(100*warning_threshold))
    else:
        print '{0:.0f} % of the hologram is located inside the ROI on the DMD.'.format(100*part_on_roi)


    if plot:
        plt.matshow(img_far_field, cmap='gray')
        plt.colorbar()
        plt.matshow(amplitude_map, cmap='gray')
        plt.colorbar()
        plt.matshow(phase_map, cmap='hsv')
        plt.colorbar()
        plt.show()

    return phase_map, amplitude_map


# def hologram_from_function(func, args=(), kx=0.1, ky=0., n_res=1000, w=500, h=500, add_phase=[], plot=False):
#     """
#     Return a hologram from a function that defines the far field image.
#     `func(k1, k2, *args)` should return the intensity and phase of this image at transverse wavevector
#     (2*pi/DMD_pixel_size)*(k1, k2). The tuple `args` contains extra arguments to be passed to func.
#     The function is evaluated on the domain [-1/2, 1/2] x [-1/2, 1/2],
#     since the finite pixel size imposes a cutoff of +/-(pi/DMD_pixel_size) in wavevector space.
#     The first diffracted order in the far field image is located at wavevector `(kx, ky)`
#     (controlling the orientation and spacing of the grating underlying the hologram).
#     For numerical purposes, the evaluation is performed on a discrete grid of dimensions n_res*n_res.
#     The array obtained after FFT (which also has dimensions n_res*n_res)
#     is then cropped to dimensions w*h (those of the DMD area that will display the hologram),
#     and a hologram is computed from the intensity and phase.
#     `add_phase` may contain additional phase maps to imprint on the DMD hologram
#     (to correct DMD aberrations or to introduce new ones like defocus for example).
#     """

#     phase_map, local_width = phase_width_from_function(func, args, kx, ky, n_res, w, h, plot)

#     total_phase_map = sum([phase_map]+add_phase)
    
#     # Generate a hologram from the phase and width map
#     img_dmd = createHologram([total_phase_map], local_width, x0=w/2, y0=h/2, scan_size=(w, h),
#         spacing=0, smoothing=False, smoothing_factor=0.1, correct_phase=True, correct_intensity=True,
#         unit_disk_mask=False, centered=True, kx=kx, ky=ky
#         )

#     return img_dmd

def hologram_from_function(func, args=(), kx=0.1, ky=0., n_res=1000, w=500, h=500, ref_ampl_DMD=0., add_phase=[], plot=False):
    """
    Return a hologram from a function that defines the far field image.
    `func(k1, k2, *args)` should return the intensity and phase of this image at transverse wavevector
    (2*pi/DMD_pixel_size)*(k1, k2). The tuple `args` contains extra arguments to be passed to func.
    The function is evaluated on the domain [-1/2, 1/2] x [-1/2, 1/2],
    since the finite pixel size imposes a cutoff of +/-(pi/DMD_pixel_size) in wavevector space.
    The first diffracted order in the far field image is located at wavevector `(kx, ky)`
    (controlling the orientation and spacing of the grating underlying the hologram).
    For numerical purposes, the evaluation is performed on a discrete grid of dimensions n_res*n_res.
    The array obtained after FFT (which also has dimensions n_res*n_res)
    is then cropped to dimensions w*h (those of the DMD area that will display the hologram),
    and a hologram is computed from the intensity and phase.
    `add_phase` may contain additional phase maps to imprint on the DMD hologram
    (to correct DMD aberrations or to introduce new ones like defocus for example).
    """

    phase_map, amplitude_map = phase_amplitude_from_function(func, args, kx, ky, n_res, w, h, plot)

    # Create a local width map from the intensity map
    print 'Maximum FFT amplitude (a.u.):', np.max(amplitude_map)
    norm_amp = max(ref_ampl_DMD, np.max(amplitude_map))
    norm_ampl_map = amplitude_map/norm_amp
    # local_width = np.arcsin(norm_ampl_map)
    local_width = 2*np.arcsin(norm_ampl_map)

    # Issue a warning if the maximum local width <= 1.5 pixel (1 after rounding)
    max_width_pix = np.max(local_width)/(np.sqrt(kx**2+ky**2)*np.pi)
    print max_width_pix
    if max_width_pix < 1.5:
        print 'WARNING: the hologram fringes are thinner than 1 pixel, beam shaping will not be great.'


    # local_width = 2*np.arcsin(norm_intensity_map)
    # local_width = np.pi/2 * norm_intensity_map

    total_phase_map = sum([phase_map]+add_phase)
    
    # Generate a hologram from the phase and width map
    img_dmd = createHologram([total_phase_map], local_width, x0=w/2, y0=h/2, scan_size=(w, h),
        spacing=0, smoothing=False, smoothing_factor=0.1, correct_phase=True, correct_intensity=True,
        unit_disk_mask=False, centered=True, kx=kx, ky=ky
        )

    return img_dmd


def normalized_hologram(func, args=(), kx=0.1, ky=0., n_res=1000, w=500, h=500, ref_intensity=0, add_phase=[], plot=False):
    img_dmd = hologram_from_function(func, args, kx, ky, n_res, w, h, plot)
    img_fft = far_field(img_dmd, padding=500)
    img_fft_crop = crop_order(img_fft, 0.1, 0, n_order=1)
    crop_size = float(img_fft_crop.shape[0])/img_fft.shape[0]
    intensity = np.sum(np.abs(img_fft_crop)**2)/((crop_size*hologram_size)**2)
    ref_ampl_DMD = ref_intensity
    return hologram_from_function(func, args=(), kx=kx, ky=ky, n_res=n_res, w=w, h=h, ref_ampl_DMD=0., add_phase=add_phase, plot=plot)



def normalized_hologram_from_function(func, args=(), kx=0.1, ky=0., n_res=1000, w=500, h=500, ref_intensity=0., add_phase=[], plot=False):
    """
    Return a hologram from a function that defines the far field image.
    `func(k1, k2, *args)` should return the intensity and phase of this image at transverse wavevector
    (2*pi/DMD_pixel_size)*(k1, k2). The tuple `args` contains extra arguments to be passed to func.
    The function is evaluated on the domain [-1/2, 1/2] x [-1/2, 1/2],
    since the finite pixel size imposes a cutoff of +/-(pi/DMD_pixel_size) in wavevector space.
    The first diffracted order in the far field image is located at wavevector `(kx, ky)`
    (controlling the orientation and spacing of the grating underlying the hologram).
    For numerical purposes, the evaluation is performed on a discrete grid of dimensions n_res*n_res.
    The array obtained after FFT (which also has dimensions n_res*n_res)
    is then cropped to dimensions w*h (those of the DMD area that will display the hologram),
    and a hologram is computed from the intensity and phase.
    `add_phase` may contain additional phase maps to imprint on the DMD hologram
    (to correct DMD aberrations or to introduce new ones like defocus for example).
    """

    phase_map, amplitude_map = phase_amplitude_from_function(func, args, kx, ky, n_res, w, h, plot)

    # Create a local width map from the intensity map
    print 'Maximum FFT amplitude (a.u.):', np.max(amplitude_map)
    norm_amp = max(ref_ampl_DMD, np.max(amplitude_map))
    norm_ampl_map = amplitude_map/norm_amp
    # local_width = np.arcsin(norm_ampl_map)
    local_width = 2*np.arcsin(norm_ampl_map)

    # Issue a warning if the maximum local width <= 1.5 pixel (1 after rounding)
    max_width_pix = np.max(local_width)/(np.sqrt(kx**2+ky**2)*np.pi)
    print max_width_pix
    if max_width_pix < 1.5:
        print 'WARNING: the hologram fringes are thinner than 1 pixel, beam shaping will not be great.'


    # local_width = 2*np.arcsin(norm_intensity_map)
    # local_width = np.pi/2 * norm_intensity_map

    total_phase_map = sum([phase_map]+add_phase)
    
    # Generate a hologram from the phase and width map
    img_dmd = createHologram([total_phase_map], local_width, x0=w/2, y0=h/2, scan_size=(w, h),
        spacing=0, smoothing=False, smoothing_factor=0.1, correct_phase=True, correct_intensity=True,
        unit_disk_mask=False, centered=True, kx=kx, ky=ky
        )

    return img_dmd




def far_field(img_dmd, padding=500):
    """
    Compute the far field image of the DMD pattern.
    Zeros are added around the image `img_dmd` before fast Fourier transform to reduce spectral folding,
    so that the padded image is square.
    """
    # Pad the array with zeros
    ny, nx = img_dmd.shape
    if ny >= nx:
        img = np.lib.pad(img_dmd, ((padding-(ny-nx)/2, padding-(ny-nx)/2), (padding, padding)), 'constant', constant_values=0)
    else:
        img = np.lib.pad(img_dmd, ((padding, padding), (padding-(nx-ny)/2, padding-(nx-ny)/2)), 'constant', constant_values=0)
    return fft.fftshift(fft.fft2(fft.ifftshift(img)))


def create_aberrations(zj, w, h, include012=False, unit_circle_radius=500):
    
    M = np.mgrid[0:h, 0:w]
    X = (M[1] - w/2.)/unit_circle_radius
    Y = (M[0] - h/2.)/unit_circle_radius
    grid_rho = np.sqrt(X*X+Y*Y)
    grid_phi = np.arctan2(X, Y)

    j_list = xrange(0, len(zj)) if include012 else xrange(3, 3+len(zj))

    phase_map = np.zeros((h, w))
    for j, z in zip(j_list, zj):
        phase_map += z*zernikel(j, grid_rho, grid_phi)

    return phase_map


def create_defocus(z, w, h):
    return create_aberrations([0, z], w, h, include012=False)


if __name__ == '__main__':
    
    from scipy.special import hermite, genlaguerre # hermite and generalized laguerre functions

    def f(kx, ky, w):
        dkx, dky = 0.0, 0.05
        m, n = 0, 1
        # return np.exp(-(kx**2 + ky**2)/(w**2))
        # return np.exp(-((kx+dkx/2.)**2 + (ky+dky/2.)**2)/(w**2)) + np.exp(-((kx-dkx/2.)**2 + (ky-dky/2.)**2)/(w**2))
        hg = hermite(m)(np.sqrt(2)*kx/w) * hermite(n)(np.sqrt(2)*ky/w) * np.exp(-(kx**2+ky**2)/w**2)
        return hg


    w = 0.01
    defocus = 0.0

    add_phase = create_defocus(defocus, 1024, 768)
    # add_phase = create_aberrations([0, 0, 0, 5], 1024, 768)

    img_dmd = hologram_from_function(func=f, args=(w,), kx=0.1, ky=0., n_res=5000, w=1024, h=768, add_phase=add_phase, plot=True)
    img_fft = far_field(img_dmd, padding=500)

    # Plot DMD image
    plt.matshow(img_dmd, cmap='gray')
    plt.colorbar()

    # Plot norm of FFT
    plt.matshow(np.abs(img_fft), cmap='gray')
    plt.colorbar()

    # Plot phase of FFT
    plt.matshow(np.angle(img_fft), cmap='hsv')
    plt.colorbar()

    plt.show()
