#!/usr/bin/env python
# -*- coding: utf8 -*-

'''
Wrapper of the FlyCapture2 dynamical libraries for Point Grey cameras
Written by T. Uehlinger
Modified by S. de Leseleuc and M. Lebrat
Last modified: 15.06.2015
'''

import ctypes
from ctypes import pointer
import numpy as np
import os

os.putenv("KMP_DUPLICATE_LIB_OK", "TRUE")

MAX_STRING_LENGTH = 512

import platform

if platform.system() == 'Linux':
    flydll_path = "libflycapture-c.so.2"
else:
    #flydll_path = """C:\Program Files\Point Grey Research\FlyCapture2\bin\FlyCapture2_C.dll"""
    #flydll_path = "C:\\Users\\qo\\Documents\\DMD\\shared\\ccd_lib\\FlyCapture2_C.dll"
    flydll_path = r"C:\Program Files (x86)\Point Grey Research\FlyCapture2\bin\FlyCapture2_C.dll"



class Version(ctypes.Structure):
    _fields_ = [
        ("major", ctypes.c_int),
        ("minor", ctypes.c_int),
        ("type", ctypes.c_int),
        ("build", ctypes.c_int)
    ]

class Guid(ctypes.Structure):
    _fields_ = [
        ("value", ctypes.c_uint * 4),
    ]

class CameraInfo(ctypes.Structure):
    _fields_ = [
        ("serialNumber", ctypes.c_uint),
        ("interfaceType", ctypes.c_int),
        ("isColorCamera", ctypes.c_int),
        ("modelName", ctypes.c_char * MAX_STRING_LENGTH),
        ("vendorName", ctypes.c_char * MAX_STRING_LENGTH),
        ("sensorInfo", ctypes.c_char * MAX_STRING_LENGTH),
        ("sensorResolution", ctypes.c_char * MAX_STRING_LENGTH),
        ("driverName", ctypes.c_char * MAX_STRING_LENGTH),
        ("firmwareVersion", ctypes.c_char * MAX_STRING_LENGTH),
        ("firmwareBuildTime", ctypes.c_char * MAX_STRING_LENGTH),
        ("iidcVer", ctypes.c_uint),
        ("maximumBusSpeed", ctypes.c_int),
        ("configROM", ctypes.c_int),
        ("bayerTileFormat", ctypes.c_int),
        ("reserved", ctypes.c_uint * 16),
        ("fill", ctypes.c_int * 1000)  # FIX: this helps to avoid segmentation faults

    ]

class Image(ctypes.Structure):
    _fields_ = [
	("rows", ctypes.c_uint),
	("cols", ctypes.c_uint),
	("stride", ctypes.c_uint),
	("pData", ctypes.POINTER(ctypes.c_char)),
	("dataSize", ctypes.c_uint),
	("receivedDataSize", ctypes.c_uint),
	("format", ctypes.c_int),
	("bayerFormat", ctypes.c_int),
	("imageImpl", ctypes.c_void_p)
    ]

class PropertyInfo(ctypes.Structure):
    _fields_ = [
        ("type", ctypes.c_int),
        ("present", ctypes.c_int),
        ("autoSupported", ctypes.c_int),
        ("manualSupported", ctypes.c_int),
        ("onOffSupported", ctypes.c_int),
        ("onePushSupported", ctypes.c_int),
        ("absValSupported", ctypes.c_int),
        ("readOutSupported", ctypes.c_int),
        ("min", ctypes.c_uint),
        ("max", ctypes.c_uint),
        ("absMin", ctypes.c_float),
        ("absMax", ctypes.c_float),
        ("pUnits", ctypes.c_char * MAX_STRING_LENGTH),
        ("pUnitAbbr", ctypes.c_char * MAX_STRING_LENGTH),
        ("reserved", ctypes.c_uint * 8)
    ]

class Property(ctypes.Structure):
    _fields_ = [
        ("type", ctypes.c_int),
        ("present", ctypes.c_int),
        ("absControl", ctypes.c_int),
        ("onePush", ctypes.c_int),
        ("onOff", ctypes.c_int),
        ("autoManualMode", ctypes.c_int),
        ("valueA", ctypes.c_uint),
        ("valueB", ctypes.c_uint),
        ("absValue", ctypes.c_float),
        ("reserved", ctypes.c_char * 8)
    ]

class TriggerModeInfo(ctypes.Structure):
    _fields_ = [
        ("present", ctypes.c_int),
        ("readOutSupported", ctypes.c_int),
        ("onOffSupported", ctypes.c_int),
        ("polaritySupported", ctypes.c_int),
        ("valueReadable", ctypes.c_int),
        ("sourceMask", ctypes.c_uint),
        ("softwareTriggerSupported", ctypes.c_int),
        ("modeMask", ctypes.c_uint),
        ("reserved", ctypes.c_uint * 8)
    ]

class TriggerMode(ctypes.Structure):
    _fields_ = [      
        ("onOff", ctypes.c_int),
        ("polarity", ctypes.c_uint),
        ("source", ctypes.c_uint),
        ("mode", ctypes.c_uint),
        ("parameter", ctypes.c_uint),      
        ("reserved", ctypes.c_uint * 8)
    ]


FC2_BRIGHTNESS = 0
FC2_AUTO_EXPOSURE = 1
FC2_SHARPNESS = 2
FC2_WHITE_BALANCE = 3
FC2_HUE = 4
FC2_SATURATION = 5
FC2_GAMMA = 6
FC2_IRIS = 7
FC2_FOCUS = 8
FC2_ZOOM = 9
FC2_PAN = 10
FC2_TILT = 11
FC2_SHUTTER = 12
FC2_GAIN = 13
FC2_TRIGGER_MODE = 14
FC2_TRIGGER_DELAY = 15
FC2_FRAME_RATE = 16
FC2_TEMPERATURE = 17
FC2_UNSPECIFIED_PROPERTY_TYPE = 18

FC2_FRAMERATE_1_875 = 0 # < 1.875 fps.
FC2_FRAMERATE_3_75 = 1 # < 3.75 fps.
FC2_FRAMERATE_7_5 = 2 # < 7.5 fps.
FC2_FRAMERATE_15 = 3 # < 15 fps.
FC2_FRAMERATE_30 = 4 # < 30 fps.
FC2_FRAMERATE_60 = 5 # < 60 fps.
FC2_FRAMERATE_120 = 6 # < 120 fps.
FC2_FRAMERATE_240 = 7 # < 240 fps.
FC2_FRAMERATE_FORMAT7 = 8 # < Custom frame rate for Format7 functionality.

FC2_VIDEOMODE_160x120YUV444 = 0 # < 160x120 YUV444.
FC2_VIDEOMODE_320x240YUV422 = 1 # < 320x240 YUV422.
FC2_VIDEOMODE_640x480YUV411 = 2 # < 640x480 YUV411.
FC2_VIDEOMODE_640x480YUV422 = 3 # < 640x480 YUV422.
FC2_VIDEOMODE_640x480RGB = 4 # < 640x480 24-bit RGB.
FC2_VIDEOMODE_640x480Y8 = 5 # < 640x480 8-bit.
FC2_VIDEOMODE_640x480Y16 = 6 # < 640x480 16-bit.
FC2_VIDEOMODE_800x600YUV422 = 7 # < 800x600 YUV422.
FC2_VIDEOMODE_800x600RGB = 8 # < 800x600 RGB.
FC2_VIDEOMODE_800x600Y8 = 9 # < 800x600 8-bit.
FC2_VIDEOMODE_800x600Y16 = 10 # < 800x600 16-bit.
FC2_VIDEOMODE_1024x768YUV422 = 11 # < 1024x768 YUV422.
FC2_VIDEOMODE_1024x768RGB = 12 # < 1024x768 RGB.
FC2_VIDEOMODE_1024x768Y8 = 13 # < 1024x768 8-bit.
FC2_VIDEOMODE_1024x768Y16 = 14 # < 1024x768 16-bit.
FC2_VIDEOMODE_1280x960YUV422 = 15 # < 1280x960 YUV422.
FC2_VIDEOMODE_1280x960RGB = 16 # < 1280x960 RGB.
FC2_VIDEOMODE_1280x960Y8 = 17 # < 1280x960 8-bit.
FC2_VIDEOMODE_1280x960Y16 = 18 # < 1280x960 16-bit.
FC2_VIDEOMODE_1600x1200YUV422 = 19 # < 1600x1200 YUV422.
FC2_VIDEOMODE_1600x1200RGB = 20 # < 1600x1200 RGB.
FC2_VIDEOMODE_1600x1200Y8 = 21 # < 1600x1200 8-bit.
FC2_VIDEOMODE_1600x1200Y16 = 22 # < 1600x1200 16-bit.
FC2_VIDEOMODE_FORMAT7 = 23 # < Custom video mode for Format7 functionality.


class Camera(object):

	#Load the CCD API
	try:
		flydll = ctypes.CDLL(flydll_path) 
		print 'CCD API successfully loaded'
	except Exception, e: 
		print "Error while downloading the CCD library: %s" %e
	
	def __init__(self, index=0, serial=None, gain=0, shutter=1, cameraMode=1):
		self.c = ctypes.c_int()
		self.g = Guid()
		self.img = None
		self.bytes = 1
		self.table16bit = [ i/256 for i in xrange(65536) ]

		# Create context
		self.flydll.fc2CreateContext(pointer(self.c))

		# Search for camera
		if serial == None:
			num_cameras = ctypes.c_int()
			self.flydll.fc2GetNumOfCameras(self.c, pointer(num_cameras))

			if index >= num_cameras:
				print 'Camera not found'
				return
			
			self.flydll.fc2GetCameraFromIndex(self.c, int(index), pointer(self.g))
		else:
			ret = self.flydll.fc2GetCameraFromSerialNumber(self.c, int(serial), pointer(self.g))
			
			if ret != 0:
				print 'Camera not found'
				return

		# Connect to camera
		self.flydll.fc2Connect(self.c, pointer(self.g))

		# Get info from camera
		self.caminfo = CameraInfo()
		self.flydll.fc2GetCameraInfo(self.c, pointer(self.caminfo))
                
                
		# Set mode
		if cameraMode == 1: 
			self.setVideoModeAndFrameRate(FC2_VIDEOMODE_1280x960Y8,  FC2_FRAMERATE_15)
			print 'Camera set to mode 1: 1280x960Y8 - FPS 15'
		elif cameraMode == 2: 
			self.setVideoModeAndFrameRate(FC2_VIDEOMODE_640x480Y8,  FC2_FRAMERATE_30)
			print 'Camera set to mode 2: 640x480Y8 - FPS 30'
		else:
			print 'Selected camera mode unknown, set to mode 1 by default: 1280x960Y8 - FPS 15'
		
		
		#Set Gain
		self.setGain(gain)
		# print 'Gain:', self.getGain()
		
		#Set Shutter
		self.setShutter(shutter)
		# print 'Shutter:', self.getShutter()
		
		self.startCapture()

	def startCapture(self):
		self.img = Image()
		self.flydll.fc2CreateImage(pointer(self.img)) 
		self.flydll.fc2StartCapture(self.c)

	def stopCapture(self):
		self.flydll.fc2StopCapture(self.c)
		if self.img:
			self.flydll.fc2DestroyImage(pointer(self.img))
		
	def destroy(self):
		if self.flydll:
			#self.stopCapture()
			self.flydll.fc2DestroyContext(self.c)
	  
	def getVideoModeAndFrameRate(self):
		videomode = ctypes.c_int()
		framerate = ctypes.c_int()
		
		#Get Video Mode and Frame Rate from camera
		self.flydll.fc2GetVideoModeAndFrameRate(self.c,  pointer(videomode),  pointer(framerate))

		#Adjust bytes mode 
		if videomode.value in [FC2_VIDEOMODE_640x480Y16, FC2_VIDEOMODE_1024x768Y16,  FC2_VIDEOMODE_1280x960Y16,  FC2_VIDEOMODE_1600x1200Y16 ]:
			self.bytes = 2
		else:
			self.bytes = 1

		return (videomode.value,  framerate.value)

	def setVideoModeAndFrameRate(self,  videomode,  framerate):
		self.flydll.fc2SetVideoModeAndFrameRate(self.c,  videomode,  framerate)
		
		return self.getVideoModeAndFrameRate()

	def dim(self):
		return (self.img.cols, self.img.rows)

	def grabImage(self):
		self.flydll.fc2RetrieveBuffer(self.c, pointer(self.img))

		d = self.img.pData
		b = d[0:self.img.dataSize]

		# if self.bytes == 2:
			# array_img = np.fromstring(b,  dtype=np.uint16).reshape(self.dim())
			# array_img = array_img.reshape(self.dim()[1], self.dim()[0])
		# else:	
		array_img = np.fromstring(b,  dtype=np.uint8).reshape(self.dim()[1], self.dim()[0])
			
		#self.np_array = array_img

		return array_img 

	def getPropertyInfo(self, type):
		i = PropertyInfo()
		i.type = type
		self.flydll.fc2GetPropertyInfo(self.c, pointer(i))
		return i

	def getProperty(self, type):
		p = Property()
		p.type = type
		self.flydll.fc2GetProperty(self.c, pointer(p))
		return p

	def setProperty(self, p):
		self.flydll.fc2SetProperty(self.c, pointer(p))

	def setShutter(self, shutter):
		p = self.getProperty(FC2_SHUTTER)
		p.absControl = True
		p.absValue = shutter
		p.autoManualMode = False
		#print 'Shutter set to: ',shutter
		self.setProperty(p)

	def getShutter(self):
		p = self.getProperty(FC2_SHUTTER)
		return p.absValue

	def setGain(self, gain):
		p = self.getProperty(FC2_GAIN)
		p.absControl = True
		p.absValue = gain
		p.autoManualMode = False
		self.setProperty(p)

	def getGain(self):
	       p = self.getProperty(FC2_GAIN)
	       return p.absValue
	
	def setExposure(self, exposure):
	       p = self.getProperty(FC2_AUTO_EXPOSURE)
	       p.absControl = True
	       p.absValue = exposure
	       p.autoManualMode = False
	       self.setProperty(p)
	
	def getTriggerMode(self):
            t = TriggerMode()
	    self.flydll.fc2GetTriggerMode(self.c, pointer(t))
	    return {
	       "on_off": t.onOff,
	       "polarity": t.polarity,
	       "source": t.source,
	       "mode": t.mode,
	       "parameter": t.parameter
            }
	
	def setTriggerMode(self, mode, source, parameter, on_off, polarity):
	    t = TriggerMode()
            t.onOff = on_off
            t.polarity = polarity
            t.source = source
            t.mode = mode
            t.parameter = parameter
            
	    err = self.flydll.fc2SetTriggerMode(self.c, pointer(t))
	    return err
	
	def setExternalTrigger(self):
	    return self.setTriggerMode(0, 0, 0, 1, 1)



def initCam(shutter_i=1, gain_i=0, cameraMode_i=1, serial=None):    
	print "----------------"
	print "Trying to create camera..."
	cam = Camera(serial=serial, shutter=shutter_i, gain=gain_i, cameraMode=cameraMode_i)

	return cam

def takePicture(cam):
	array_img = cam.grabImage()
	
	return array_img
		
def finishCam(cam):
	print "stopCapture..."
	cam.stopCapture()
	print "destroying..."
	cam.destroy()
	print "----------------"

def main():	
	pass
	
if __name__ == "__main__":
	main()