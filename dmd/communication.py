# -*- coding: utf-8 -*-
"""
Module to interface with the DMD

DMD Model: Vialux GmbH V 7000 VIS (XGA-resolution)
Created on Tue Jun 02 10:07:52 2015

@author: Matthias Müller-Schrader (matthias.mueller-schrader(at)gmx.de)


The class DMD defined in this module allows to communicate with the DMD.
The additional modules

    - geom_obj 
    - frames
    - sequence  
    - control_document_er 
   
provide extra functions and allow to define sequences
for the dmd abstractly and to render them.

In the directory of this module there should be a folder 'libDMD' 
containing the dll of the dmd.
"""

import ctypes
import numpy as np


### Constants or controlling arguments, see documentation of ALP-4.2 high speed
ALP_DEFAULT=ctypes.c_int(0)
ALP_DEVICE_NUMBER = ctypes.c_int(2000)
ALP_VERSION = ctypes.c_int(2001)
ALP_TRIGGER_EDGE = ctypes.c_int(2005)
ALP_DEV_DISPLAY_HEIGHT = ctypes.c_int(2057)
ALP_DEV_DISPLAY_WIDTH = ctypes.c_int(2058)
ALP_USB_CONNECTION = ctypes.c_int(2016)
ALP_PBC_TEMPERATURE = ctypes.c_int(2052)
ALP_BITPLANES = ctypes.c_int(2200)
ALP_BITNUM = ctypes.c_int(2103)
ALP_BIN_MODE = ctypes.c_int(2104)
ALP_PICNUM = ctypes.c_int(2201)
ALP_PICTURE_TIME = ctypes.c_int(2203)
ALP_ILLUMINATE_TIME = ctypes.c_int(2204)
ALP_ON_TIME = ctypes.c_int(2214)
ALP_OFF_TIME = ctypes.c_int(2215)
ALP_MIN_ILLUMINATE_TIME = ctypes.c_int(2212)
ALP_DATA_FORMAT = ctypes.c_int(2110)

ALP_DATA_BINARY_TOPDOWN = ctypes.c_int(2)
BIT_PLANES = ctypes.c_long(1)           # Preventing Greyscales

### importing dll (should be in folder libDMD which is itself in the current working directory)
try:
    #dmd_dll = ctypes.CDLL('libDMD/x64/alpV42.dll')
    dmd_dll = ctypes.CDLL(r'C:\Users\quantumoptics\Documents\DMD\DMD_programming\dmdcom\libDMD\alpV42.dll')
except Exception, e:
    print "Error occured while loading DMD-ddl"
    print "Error: %s",e

ALP_ERR = {                           #giving better error messages, see otherwise documentation
    0: 'ALP_OK', 
    1001: 'ALP_NOT_ONLINE', 
    1002: 'ALP_NOT_IDLE',
    1003: 'ALP_NOT_AVAILABLE',
    1004: 'ALP_NOT_READY',
    1005: 'ALP_PARM_INVALID',
    1006: 'ALP_ADDR_INVALID',
    1007: 'ALP_MEMORY_FULL',
    1008: 'ALP_SEQ_IN_USE',
    1009: 'ALP_HALTED',
    1010: 'ALP_ERROR_INIT',
    1011: 'ALP_ERROR_COMM',
    1012: 'ALP_DEVICE_REMOVED',
    1013: 'ALP_NOT_CONFIGURED',
    1014: 'ALP_LOADER_VERSION',
    1018: 'ALP_ERROR_POWER_DOWN',
    
    }

ALP_CONTRL_ARGS = {     # Arguments for AlpSeqControl() and AlpProjControl
    'ALP_BIN_MODE'          : ctypes.c_int(2104),
    'ALP_DATA_FORMAT'       : ctypes.c_int(2110),
    'ALP_FIRSTFRAME'        : ctypes.c_int(2101),
    'ALP_LASTFRAME'         : ctypes.c_int(2102),
    'ALP_SEQ_REPEAT'        : ctypes.c_int(2100),
    'ALP_PROJ_MODE'         : ctypes.c_int(2300),
    'ALP_PROJ_INVERSION'    : ctypes.c_int(2306),
    'ALP_PROJ_UPSIDE_DOWN'  : ctypes.c_int(2307),
    'ALP_MASTER'            : ctypes.c_int(2301),
    'ALP_SLAVE'             : ctypes.c_int(2302),
    'ALP_DEFAULT'           : ctypes.c_int(0),
    'NOT_ALP_DEFAULT'       : ctypes.c_int(1),
    'ALP_EDGE_FALLING'      : ctypes.c_int(2008),
    'ALP_EDGE_RISING'       : ctypes.c_int(2009),
    'ALP_BIN_NORMAL'        : ctypes.c_int(2105),
    'ALP_BIN_UNINTERRUPTED' : ctypes.c_int(2106),

}

ALP_INQ_ARGS = {
    1200      : 'ALP_PROJ_ACTIVE',
    1201      : 'ALP_PROJ_IDLE',
    2301      : 'ALP_MASTER',
    2302      : 'ALP_SLAVE',
    2008      : 'ALP_EDGE_FALLING',
    2009      : 'ALP_EDGE_RISING',
    2105      : 'ALP_BIN_NORMAL',
    2106      : 'ALP_BIN_UNINTERRUPTED',
    0         : 'ALP_DATA_MSB_ALIGN',
    1         : 'ALP_DATA_LSB_ALIGN',
    2         : 'ALP_DATA_BINARY_TOPDOWN',
    3         : 'ALP_DATA_BINARY_BOTTOMUP',
     
}

class DMD():
    """ Class to communicate with the DMD.
        
    Each instance of this class can communicate with one DMD.
    During the initialisation, it tries to connect to the next aviable DMD.
    It is also possible to connect to a special DMD, specified by its serial number.
    
    After the usage, the DMD should be released using the DMD.free() method.
    
    """
    
    def __init__(self,serial_number = ALP_DEFAULT):
            #searching for DMD
        print 'searching for DMD'
        self.DevID = ctypes.c_int()       ### To store the device ID to communicate with DMD
        self.seq_ids = {}       ### To store sequenceIDs 
        ret = dmd_dll.AlpDevAlloc(serial_number,ALP_DEFAULT,ctypes.byref(self.DevID))
        if ret != 0:
            raise Exception('Communication with DMD failed. Error %s'%ALP_ERR[ret])
        else: 
            print 'Connected to DMD'
        ### determining resulution of DMD (for transforming pictures)
        self.disp_height = ctypes.c_int()   
        ret = dmd_dll.AlpDevInquire(self.DevID,ALP_DEV_DISPLAY_HEIGHT,ctypes.byref(self.disp_height))
        self.disp_height = self.disp_height.value   # used c_int.value to get normal py int
        if ret != 0:
            raise Exception('Inspecting height failed. Error %s'%ALP_ERR[ret])
        self.disp_width = ctypes.c_int()
        ret = dmd_dll.AlpDevInquire(self.DevID,ALP_DEV_DISPLAY_WIDTH,ctypes.byref(self.disp_width))
        if ret != 0:
            raise Exception('Inspecting width failed. Error %s'%ALP_ERR[ret])
        self.disp_width = self.disp_width.value
        self.last_added_seq = None
        
#    def __del__(self):
#        
#        #ejecting DMD to prevent some problems
#        print 'cleaning up... =)'
#        ret = dmd_dll.AlpDevHalt(self.DevID)
#        if ret != 0:
#            raise Exception('Halting DMD failed. Error %s'%ALLOC_ERR[ret])
#        ret = dmd_dll.AlpDevFree(self.DevID)
#        if ret != 0:
#            raise Exception('Freeing DMD failed. Error %s'%ALLOC_ERR[ret])
#        print 'cleaned up successfully :)'    
       
    def controlDev(self,tr_edge):
        """ Allows to set some properties to the DMD.
        
        Actually, it is only possible to change the trigger_edge, if the DMD is in the 
        slave mode. 
        
        **Implementation of ``AlpDevControl`` from the DLL.
        
        Parameters
        -----------------
        tr_edge : *int or str*
            Specifies the trigger edge. Can either be a number as specified in the
            DMD documentation or the string ``ALP_EDGE_RISING`` or ``ALP_EDGE_FALLING``.
        """
        if isinstance(tr_edge,basestring):
            c_tr_edge = ALP_CONTRL_ARGS[tr_edge]
        else:
            c_tr_edge = ctypes.c_int(tr_edge)
        ret = dmd_dll.AlpDevControl(self.DevID,ALP_TRIGGER_EDGE,c_tr_edge)        
        if ret:         # ret == 0 is everything is ok.
            raise Exception('Changing trigger edge failed. Error %s'%ALP_ERR[ret])
            
    def free(self):
        """Allows to eject the DMD manually. Should always be done.
        
        **Implementation of ``AlpDevHalt`` and ``AlpDevFree`` from the DLL.
        
        Raises
        ----------------
        Exception :  
            - If either ``AlpDevHalt`` or ``AlpDevFree`` returns an error
        """
        #ejecting the DMD manually        
        print 'Freedom to all DMDs!!!!!!' ### Thats a dream, but don't worry, it will just free this dmd ;)
        ret1 = dmd_dll.AlpDevHalt(self.DevID)
        ret2 = dmd_dll.AlpDevFree(self.DevID)        
        if(ret1+ret2==0): 
            print 'DMD is free :)'
        elif ret1 != 0:
            raise Exception('Halting the DMD failed! Error %s'%ALP_ERR[ret1])
        else:
            raise Exception('Freeing the DMD failed! Error %s'%ALP_ERR[ret2])
            
    def inquireDev(self,conv=True):
        """ Helps inspecting the DMD. 
        
        ** Implementation of some parts of ``AlpDevInquire`` from the DLL.
        
        By default, the values are returned a converted form.
        If ``converted`` is False, the values (except from display height and width)
        will be returned as they come from the DMD, i.e. as ctypes.c_int.
    
        Returns
        ----------------
        propts : *dict* 
                Dictionary containing the properties. Keys are (as string):
                    -``Device_Number``:
                        Serial number of the DMD (can be used later to connect
                        to specific DMD  by handling it to the initialization routine).           
                    -``ALP_Version_Number``:
                        The version number of the ALP device.
                    -``Temperature_PBC``:
                        The internal temperature of the DMD.
                        *If ``converted`` is True, the temperature will be stated
                        in degree celsius.*
                    -``Trigger_Edge``:
                        Whether the DMD reacts to rising or falling triggers.
                        *If ``converted`` is True, the entry will be a string
                        'ALP_EDGE_FALLING' or 'ALP_EDGE_RISING'.*
                    -``USB_Connection``:
                        Whether the connection is ok or removed.
                        *If ``converted`` is True, the entry will be a string
                        'ALP_OK' or 'ALP_DEVICE_REMOVED'.*
                    -``Display_Height``
                        Height of the DMD (type python [sic] int).
                    -``Display_Width``
                        Width of the DMD  (type python [sic] int).
                        
        Raises
        ------------------------
        Exception:
            - If one of the calls of ``AlpDevInquire`` returns an error.
        """
        
        ditc = {'Display_Height':self.disp_height,'Display_Width':self.disp_width}
        ditc['Device_Number'] = ctypes.c_int(0)
        ret = dmd_dll.AlpDevInquire(self.DevID,ALP_DEVICE_NUMBER,ctypes.byref(ditc['Device_Number']))
        ditc['ALP_Version_Number'] = ctypes.c_int(0)
        ret += dmd_dll.AlpDevInquire(self.DevID,ALP_VERSION,ctypes.byref(ditc['ALP_Version_Number']))
        ditc['Trigger_Edge'] = ctypes.c_int(0)
        ret = dmd_dll.AlpDevInquire(self.DevID,ALP_TRIGGER_EDGE,ctypes.byref(ditc['Trigger_Edge']))
        if conv:   # See ALP documentation for the numbers
            ditc['Trigger_Edge']= ALP_INQ_ARGS[ditc['Trigger_Edge'].value]
        ditc['USB_Connection'] = ctypes.c_int(0)
        ret += dmd_dll.AlpDevInquire(self.DevID,ALP_USB_CONNECTION,ctypes.byref(ditc['USB_Connection']))
        if conv:
            ditc['USB_Connection'] = ALP_ERR[ditc['USB_Connection'].value]
        ditc['Temperature_PBC'] = ctypes.c_int(0)
        ret += dmd_dll.AlpDevInquire(self.DevID,ALP_PBC_TEMPERATURE,ctypes.byref(ditc['Temperature_PBC']))
        if conv:           
           ditc['Temperature_PBC'] = ditc['Temperature_PBC'].value/256.
        if ret != 0:
            raise Exception('Error occured while inspecting DMD')
        return ditc
                        
    def allocSeq(self,name,picNum,data_format = 2):
        """ Allocates memory to store later a sequence of pictures
        
        ** Implementation of ``AlpSeqAlloc`` and party of ``AlpSeqControl`` from the DLL.
        
        Parameters
        -----------------
        name : *any type that can be key for a dict*
             Name for the sequence. It can be accessed by DMD.seq_ids[name]
        picNum :  *int*
             The number of XGA pictures belonging to the sequence.
             Could be limited by memory (but unlikely).
        data_format : *opt, int from {0,1,2,3}*
            Specifies the data format for the pictures of the sequence.
            Other modules are designed for the default (Bitplanes, row 0 first).
            See the documentation of the ALP library for more details (default is
            ALP_DATA_BINARY_TOPDOWN). Integers will be converted to ctypes.c_int
            
        Raises
        -------------
        Exception:
            - If either ``AlpSecAlloc`` or ``AlpSecControl`` returns an error.
        """
        
        seqID = ctypes.c_int()
        ret = dmd_dll.AlpSeqAlloc(self.DevID,BIT_PLANES,picNum,ctypes.byref(seqID))
        if ret != 0:
            raise Exception('Allocation of memory failed. Error %s'%ret)
        else: 
            print 'Successfully allocated memory for sequence %s'%name
        self.seq_ids[name] = seqID              ### All sequence IDs are stored in this dict.
        self.last_added_seq = name        
        c_data_format = ctypes.c_int(data_format)       # See ALP Documentation for other formates
        ret = dmd_dll.AlpSeqControl(self.DevID,seqID,ALP_DATA_FORMAT,c_data_format)
        if ret != 0:
            raise Exception('Changing data format to allocate sequence %s failed. Error %s'%(name,ret))
            
    def freeSeq(self,name):
        """ Releases a sequence and releases therby the memory allocated by the sequence.
        
        ** Implementation of ``AlpSeqFree`` from the DLL.
        
        Raises
        ---------------------
        Exception:
            - If ``AlpSeqFree`` returns an error.
        """
        
        ret = dmd_dll.AlpSeqFree(self.DevID,self.seq_ids[name])
        if ret != 0:
            raise Exception('Releasing sequence %s failed. Error %s' %(str(name), ALP_ERR[ret]))
        else: 
            del self.seq_ids[name]
            print 'Released sequence %s :)'%name    
            self.last_added_seq = None
            
    def inquireSeq(self,name,conv=True):
        """ Allows to inquire a sequence and returns a dict with the most important properties.
        
        ** Implementation of parts of ``AlpSeqInquire`` from the DLL.
        
        By default, the values are returned a converted form.
        If ``converted`` is False, the values will be returned as they come 
        from the DMD, i.e. as ctypes.c_int.
        
        Returns
        ----------------
        propts : *dict* 
                Dictionary containing the properties. Keys are (as string):
                    -``Seq_Bitplanes``:
                        Bit depth of the pictures in the sequence. Should be 1.
                    -``Seq_Bitnum``:
                        The bit depth for displaying  (could reduce bitdepth for showing).
                        Should also be 1.
                    -``Seq_Bin_Mode``:
                        If bitplanes or bitnum = 1 (binary mode), it is possible
                        to use a mode without  dark phase. Shows, whether this
                        mode is active.
                    -``Seq_Picnum``:
                        Number of pictures in the sequence.
                    -``Seq_Pic_Time``:
                        Time beween start of two consecutive pictures (in micro s).
                        The illumination time might be smaller but is chosen so that
                        it is maximal.
                    -``Seq_Illum_Time``:
                        Time, one picture is displayed on the DMD. Is <= ``Seq_Pic_Time`` - 44 microseconds.
                        If the DMD is in ``ALP_BIN_UNINTERRUPTED`` mode, it will be set to
                        0 and ignored.
                    -``Seq_Min_Illuminate_Time``:
                        Minimal possible value for ``Seq_Illuminate_Time``. (in mirco s)
                    -``Seq_Data_Format``
                        Data format of the sequence
                    -``Seq_ON_Time`` : 
                        Total active projection time.
                    -``Seq_OFF_Time`` : 
                        Total inactive projection time.
        """
        ditcsq = {}     ### To be read in blocks of 4 lines; init, query, test if conv, convert
        ditcsq['Seq_Bitplanes']=ctypes.c_int()
        ret = dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_BITPLANES,ctypes.byref( ditcsq['Seq_Bitplanes']))
        if conv:
            ditcsq['Seq_Bitplanes'] = ditcsq['Seq_Bitplanes'].value
        ditcsq['Seq_Bitnum']=ctypes.c_int()
        ret += dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_BITNUM,ctypes.byref( ditcsq['Seq_Bitnum']))
        if conv:        
            ditcsq['Seq_Bitnum']=ditcsq['Seq_Bitnum'].value
        ditcsq['Seq_Bin_Mode']=ctypes.c_int()
        ret += dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_BIN_MODE,ctypes.byref( ditcsq['Seq_Bin_Mode']))
        if conv:
            ditcsq['Seq_Bin_Mode'] = ALP_INQ_ARGS[ditcsq['Seq_Bin_Mode'].value]
        ditcsq['Seq_Picnum']=ctypes.c_int()
        ret += dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_PICNUM,ctypes.byref( ditcsq['Seq_Picnum']))
        if conv:
            ditcsq['Seq_Picnum']=ditcsq['Seq_Picnum'].value
        ditcsq['Seq_Pic_Time']=ctypes.c_int()
        ret += dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_PICTURE_TIME,ctypes.byref( ditcsq['Seq_Pic_Time']))
        if conv:
            ditcsq['Seq_Pic_Time'] = str(ditcsq['Seq_Pic_Time'].value/1000.) + ' ms'
        ditcsq['Seq_Illuminate_Time']=ctypes.c_int()
        ret += dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_ILLUMINATE_TIME,ctypes.byref( ditcsq['Seq_Illuminate_Time']))
        if conv:
            ditcsq['Seq_Illuminate_Time'] = str(ditcsq['Seq_Illuminate_Time'].value/1000.) + ' ms'
        ditcsq['Seq_Min_Illum_Time']=ctypes.c_int()
        ret += dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_MIN_ILLUMINATE_TIME,ctypes.byref( ditcsq['Seq_Min_Illum_Time']))
        if conv:
            ditcsq['Seq_Min_Illum_Time'] = str(ditcsq['Seq_Min_Illum_Time'].value/1000.) + ' ms'
        ditcsq['Seq_ON_Time']=ctypes.c_int()
        ret += dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_ON_TIME,ctypes.byref( ditcsq['Seq_ON_Time']))
        if conv:
            ditcsq['Seq_ON_Time'] = str(ditcsq['Seq_ON_Time'].value/1000.) + ' ms'
        ditcsq['Seq_OFF_Time']=ctypes.c_int()
        ret += dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_OFF_TIME,ctypes.byref( ditcsq['Seq_OFF_Time']))
        if conv:
            ditcsq['Seq_OFF_Time'] = str(ditcsq['Seq_OFF_Time'].value/1000.) + ' ms' 
        ditcsq['Seq_Data_Format']=ctypes.c_int()
        ret += dmd_dll.AlpSeqInquire(self.DevID,self.seq_ids[name],ALP_DATA_FORMAT,ctypes.byref( ditcsq['Seq_Data_Format']))
        if conv:
            ditcsq['Seq_Data_Format']=ALP_INQ_ARGS[ditcsq['Seq_Data_Format'].value]
        if ret != 0:
            raise Exception('Error while inquirering sequence %s.'%name)
        return ditcsq
     
    def controlSeq(self,name,arg,num):
        """ Allows to control properties of the sequence.
        
        Parameters
        --------------------
        name : 
            Name the sequence was allocated with.
        arg : *string*
            Property to be changed. One of the following:
             -``ALP_BIN_MODE`` : 
                    Allows to control, wheter the sequence should be displayed
                    normally (0) or in uninterrupted mode (2106).
                    Can also pass ``ALP_BIN_NORMAL`` or ``ALP_BIN_UNINTERRUPTED``
                    as string.
                    **Requires a following call of ``DMD.timingSeq()`` to become active
             -``ALP_DATA_FORMAT`` : 
                     Allows to change the data format.  See ALP-Documentation for further details.
             -``ALP_FIRSTFRAME``  :
                     Allows to restrict the pictures to be shown.
                     Selects the first picture of the sequence to be shown.
             -``ALP_LASTFRAME``   :
                     Allows to restrict the pictures to be shown.
                     Selects the last picture of the sequence to be shown.
             -``ALP_SEQ_REPEAT``  :
                     Sets how often the sequence should be shown when DMD.startProj(seq) is called.
                     Default is 1.
        num : *int*
            A parameter to specify the changement.
        """
        if isinstance(num,basestring):
           c_num = ALP_CONTRL_ARGS[num]
        else:
           c_num = ctypes.c_int(num)
        ret = dmd_dll.AlpSeqControl(self.DevID,self.seq_ids[name],ALP_CONTRL_ARGS[arg],c_num)
        if ret != 0:
            raise Exception('Changing argument %s of sequence %s failed. Error %s' %(ALP_CONTRL_ARGS[arg],name,ALP_ERR[ret]))
    
    def putSeq(self,name,data_array):
        """ Passes a numpy array of length l = (pic_num*display_height*display_width/8) to the DMD
        
        **Implementation of ``AlpSeqPut`` from the DLL.   
        See ALP documentation for further details.
        """
        array_pointer = data_array.ctypes.data         #creates the pointer, a np routine
        ret = dmd_dll.AlpSeqPut(self.DevID,self.seq_ids[name],ALP_DEFAULT,ALP_DEFAULT,array_pointer)
        if ret != 0:
            raise Exception('Putting pictures into sequence failed. Error %s' %ALP_ERR[ret])
        else: 
            print 'loaded data for sequence %s on dmd :)'%name


    def timingSeq(self,name,pic_time=None,illuminate_time=None):
        """ Allows to set the picture time.
          
        **Implementation of parts of ``AlpSeqTiming`` from the DLL.
        Picture time should be in microseconds. Maximum is 10s.
        The picture time is the time between the start of two consecutive pictures.
        Can optionally also change the illumination time for ``ALP_BIN_NORMAL`` mode.
        The Illumination time is the time, the picture is actually viewed.
        
        Parameters
        ------------------------
        pic_time : *int*
            The time between the start of two consecutive pictures.
            If None, it will be set to the smallest possible time compatible with
            illumination time. If both are None, it will be set to
            1/30 second.
        illuminate_time : *int*
            The time a picture will be illuminated.  If None, it will be the 
            maximal possible time; approxemately pic_time - 44 miroseconds.
        """
        if not illuminate_time:
            illuminate_time = 0
        if not pic_time:
            pic_time = 0
        ret = dmd_dll.AlpSeqTiming(self.DevID,self.seq_ids[name],ctypes.c_int(int(illuminate_time)),ctypes.c_int(int(pic_time)),ALP_DEFAULT,ALP_DEFAULT,ALP_DEFAULT)
        if ret != 0:
            raise Exception('Changing time failed. Error %s' %ALP_ERR[ret])
          
    def controlProj(self,cont_type,cont_value):
        """ Allows to control the project. 
        
        **Implementation of ``AlpProjControl`` from the DLL.
        
        The control parameters can also be passed as integers, accoding to the documentation.        
        
        Parameters
        -------------
        cont_type : *str*
            One can change the following properties :
                -``ALP_PROJ_MODE`` :
                    Changes the projection mode. Possible cont_value are:
                    - ``ALP_MASTER`` : The pictures are refreshed by the DMD accoding to the settings by DMD.timingSeq.
                    - ``ALP_SLAVE`` : The transition of a picture follows an external trigger.
                -``ALP_PROJ_INVERSION``:
                    Inverts the image pixels. Possible cont_value are
                    - ``ALP_DEFAULT``
                    - ``NOT_ALP_DEFAULT``
                -``ALP_PROJ_UPSIDE_DOWN``:
                    Flipps the image. Possible cont_value are
                    - ``ALP_DEFAULT``
                    - ``NOT_ALP_DEFAULT``
        """
        if isinstance(cont_type,basestring):
            c_cont_type = ALP_CONTRL_ARGS[cont_type]
        else:
            c_cont_type = ctypes.c_int(cont_type)
        if isinstance(cont_value,basestring):
            c_cont_value = ALP_CONTRL_ARGS[cont_value]
        else:
            c_cont_value = ctypes.c_int(cont_value)
        ret = dmd_dll.AlpProjControl(self.DevID,c_cont_type,c_cont_value)
        if ret != 0:
            raise Exception('Changing properties of Project failed. Error %s' %ALP_ERR[ret]) 
    
    def startProj(self,seq_name=None):
        """ Starts projecting the sequence ``seq_name``.
        
        **Implementation of ``AlpProjStart`` from the DLL.
        
        If no argument is passed, the last_added_seq will be played.
        """
        if not seq_name:
            seq_name = self.last_added_seq
        ret = dmd_dll.AlpProjStart(self.DevID,self.seq_ids[seq_name])
        if ret != 0:
            raise Exception('Playing sequnce %s failed. Error %s.' %(seq_name,ALP_ERR[ret]))
        else: 
            print 'playing sequence %s :)'%seq_name
            
    def startContProj(self,seq_name=None):
        """ Starts continuously playing the sequence ``seq_name``.
        
        If None is given, starts the last inquired sequence.
        **Implementation of ``AlpProjStart`` from the DLL.
        """
        if not seq_name:
            seq_name = self.last_added_seq
        ret = dmd_dll.AlpProjStartCont(self.DevID,self.seq_ids[seq_name])
        if ret != 0:
            raise Exception('Playing continuously sequnce %s failed. Error %s.' %(seq_name,ALP_ERR[ret]))
        else: 
            print 'playing continuously sequence %s :)'%seq_name
    
    def waitProj(self):
        """Pauses the script until the acutally plaing sequence has finished. """        
        ret = dmd_dll.AlpProjWait(self.DevID)
        if ret != 0:
            raise Exception('Waiting for sequnce failed. Error %s.' %ALP_ERR[ret])
            
    def haltProj(self):
        """ Stops the sequence currently running on the DMD.
        
        In fact it finishes the actually playing sequence and stops then.
        See semester thesis of Matthias Mueller-Schrader for details.        
        """
        ret = dmd_dll.AlpProjHalt(self.DevID)            
        if ret:
            raise Exception('Halting project failed. Error %s .' %ALP_ERR[ret])
            
    def inquireProj(self,conv=True):
        """ Returns some information about the project on the DMD. 
        
        ** Implementation of ``AlpProjInquire`` from the DLL.

        By default, the arguments are passed in a converted way. Set conv=False to
        get them as c_int.

        Returns
        ------------------------  
        tmp : *dict*
                Dictionary containing the properties. Keys are:
                
                ``ALP_PROJ_MODE`` : 
                    The projection mode (master or slave).
                ``ALP_PROJ_STATE`` :
                    The actual state of the projection (active or idle).
        """
        tmp = {}
        tmp['ALP_PROJ_MODE'] = ctypes.c_int()
        ret = dmd_dll.AlpProjInquire(self.DevID,ALP_CONTRL_ARGS['ALP_PROJ_MODE'],ctypes.byref(tmp['ALP_PROJ_MODE']))
        if ret:
            raise Exception('Inquireing project failed. Error %s.' %ALP_ERR[ret])
        tmp['ALP_PROJ_STATE'] = ctypes.c_int()
        ret = dmd_dll.AlpProjInquire(self.DevID,ctypes.c_int(2400),ctypes.byref(tmp['ALP_PROJ_STATE']))
        if ret:
            raise Exception('Inquireing project failed. Error %s.' %ALP_ERR[ret])
        if conv:        
            for key in tmp.keys():
                tmp[key] = ALP_INQ_ARGS[tmp[key].value]    ### Convert to right format.
        return tmp  
        
    def compilePicture(self,img):
        """ Convertes pictures, so that their format fits to the DMD.
        
        The DMD expects the pictures as one dimensional byte-array, containing the
        information row for row (from top to bottom and from first to last picture).
        Each Byte contains the information for 8 bits. The MSB the information for 
        the first bit from the left side etc. till the least significant the information
        for the 8 th bit from the left side.
        
        The function takes boolean numpy arrays (or collections of them) and 'packs' them
        the right way
        
        Parameters
        --------------------
        img : *numpy array or something like that*
            The image(s). Each image should be a 2dim boolean numpy array with shapes
            (disp_height,disp_width). Several images can be handeld as list or tuple
            of arrays or  a 3-dim array with the pictures aligned along the axis 0.
            
        Returns
        ------------------
        tArray : *numpy array of dtype Byte*
            The converted image(s). Each Byte contains the information for 8 pixel.
            tArray is a 1-dim array of length heigth*width *num_of_pic / 8
        """
        
        if isinstance(img,np.ndarray) and img.ndim==2 :     
            img = [img]  
            # img returned from sequence (list of arrays)
        tArray= np.zeros(0,dtype='B')   # right format for dmd
        for arr in img:
            arr = np.reshape(arr,self.disp_width*self.disp_height)
            tempArr = np.zeros(self.disp_width*self.disp_height/8,dtype='B')
            pot = 128
            for k in xrange(8):
                tempArr[:]+=arr[k::8]*pot
                pot/=2
            tArray = np.append(tArray,tempArr)
        return tArray
        
    def loadArrToDMD(self,name,img,timing = None,unint=True):
        """ Takes an (collection of) arrays, converts it into the right format
        and transforms it to the DMD.
        
        Parameters
        --------------------------
        name : *int,str... must be hashable*
            The name for the sequence. Is needed to be able to control the sequence later
            and to start it.
        img : *2dim numpy array or collections of it*
            The image(s). Each image should be a 2dim boolean numpy array with shapes
            (disp_height,disp_width). Several images can be handeld as list or tuple
            of arrays or  a 3-dim array with the pictures aligned along the axis 0.
        timing : *opt, float*
            The time each picture shuold be shown [microsecond].
        unint : *opt, bool*
            Whether the uninterrupted mode should be implemented or not.
            (See also documentation of API)
        """
        pckd = self.compilePicture(img)
        picnum = len(pckd) * 8 /(self.disp_height*self.disp_width)       
        self.allocSeq(name,picnum)
        if unint and not timing:        
            timing = self.inquireSeq(name)['Seq_Pic_Time']
        if unint:
            self.controlSeq(name,'ALP_BIN_MODE',2106)
        if timing or unint:
            self.timingSeq(name,timing)
        self.putSeq(name,pckd)
    
    def inspect(self,conv=True):
        """ Inspects the DMD and returns a dict with the most important values.
        
        Combines DMD.inquireDev(), DMD.inquireSeq(lastSeq), DMD.inquireProj() and
        returns a dictionarry containing all the keys from the methods.
        If the last allocated sequence was removed or no sequence was allocated,
        this information will not be added to the dict.
        """
        tmp = self.inquireDev(conv)         ### Infos from the device.
        if self.last_added_seq:                 ### Infos from the last seq (if existing).
            tmp.update(self.inquireSeq(self.last_added_seq,conv))        
            tmp['Name of last alloc Seq'] = self.last_added_seq
        tmp.update(self.inquireProj(conv))  ### Infos from the proj.
        return tmp
        
        