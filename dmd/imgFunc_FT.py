###
# Definition of functions to modify the numpy array
###

from imgFunc import *
import numpy as np
import time


w,h = 1024,768 ##size of the image


def verticalGrid(width, spacing, w = 1024, h = 768):
    '''Vertical Grid
        -> Vertical in lab axis (diagonal in DMD axis)
        -> Spacing & Grid in mirrors number
        -> Spacing : distance between two slits
        -> Width : width of a slit
    '''

    #Check if width is not bigger than spacing
    if width > spacing :
        print 'ERROR : you have given a width bigger than the spacing'
        print 'I need : width < spacing'
        return 0

    img = np.ones((w,h),int)
    img.shape=h,w ## set the array shape to our image shape
    print 'Vertical Grid : '
    print 'spacing = ',spacing
    print 'width = ',width

    #temporary image with grid aligned with DMD axis
    #the temporary image is made bigger than the final image
    # so when we will use it to create the last one by doing a rotation
    # we don't go out of the image
    img_temp = np.zeros((6*w,6*h),int)
    img_temp.shape=6*h, 6*w
    for j in range(width):
        img_temp[:,j::spacing]=1

    #rotation from DMD axis to lab axis
    for i in range(h):
        for j in range(w):
            img[i,j] = img_temp[3*h + i - j, 3*w + i + j]

    return img

def horizontalGrid(width, spacing, w = 1024, h = 768):
    '''Horizontal Grid
        -> Horizontal in lab axis (diagonal in DMD axis)
        -> Spacing & Grid in mirrors number
        -> Spacing : distance between two slits
        -> Width : width of a slit
    '''

    #Check if width is not bigger than spacing
    if width > spacing :
        print 'ERROR : you have given a width bigger than the spacing'
        print 'I need : width < spacing'
        return 0

    img = np.ones((w,h),int)
    img.shape=h,w ## set the array shape to our image shape
    print 'Horizontal Grid : '
    print 'spacing = ',spacing
    print 'width = ',width

    #temporary image with grid aligned with DMD axis
    #the temporary image is made bigger than the final image
    # so when we will use it to create the last one by doing a rotation
    # we don't go out of the image
    img_temp = np.zeros((6*w,6*h),int)
    img_temp.shape=6*h, 6*w
    for j in range(width):
        img_temp[j::spacing,:]=1

    #rotation from DMD axis to lab axis
    for i in range(h):
        for j in range(w):
            img[i,j] = img_temp[3*h + i - j, 3*w + i + j]

    return img

def initGratingFast(spacing, on, phase = 0, w = 1024, h = 768):
	img = np.ones((h,w),int) - on
	for j in range(spacing/2):
		img[:,j+phase::spacing] = on

	return img

def grating(spacing, theta, add_phase = 0, slit_width = 1, w = 1024, h = 768):
	''' Create a grating : binary-zed version of cos(2pi/spacing*(x.cos(theta) + y.sin(theta)))
		-> x and y are relative to DMD basis
		-> spacing
		-> theta : angle of the grating normal with the x axis of the DMD
		-> add_phase : in unit of spacing
		-> slit_width : 1 = maximum outgoing intensity : slit width half of spacing.

		TO DO : implement slit_width and relation between outgoing intensity and width of the slit
	'''


	img = np.zeros((w,h),int)
	img.shape=h,w ## set the array shape to our image shape
	for i in range(h):
		for j in range(w):
			phase = (2*np.pi/spacing*(i*np.sin(theta*np.pi/180) + j*np.cos(theta*pi/180)) + add_phase)%(2.*np.pi) #phase modulo 2.Pi
			if np.pi/2 < abs(phase) < 3*np.pi/2 :
				img[i,j] = 1

	return img

def quickCircularGrating(x_ref, y_ref, radius, spacing, theta, add_phase = 0, slit_width = 1, w = 1024, h = 768):
	'''Implement a faster version to create a circular grating'''

	img = np.zeros((w,h),int)
	img.shape=h,w ## set the array shape to our image shape

	for i in np.arange(-radius-1,radius+1,1):
		for j in np.arange(-radius-1,radius+1,1):
			if i**2+j**2 < radius**2:
				phase = (i*np.sin(theta*pi/180) + j*np.cos(theta*pi/180))/spacing + add_phase
				if 0 <= abs(phase%1) < 0.5 :
					img[y_ref+i,x_ref+j] = 1

	return img

def aperture(img, x_ref, y_ref, radius):
	'''Create an artificial aperture on the DMD'''
	t0 = time.clock()
	h, w = len(img), len(img[0])
	for i in range(h):
		for j in range(w):
			if (i-y_ref)**2+(j-x_ref)**2 > radius**2:
				img[i,j] = 0
	t1 = time.clock()

	print 'Aperture has been added to image in %d ms'%((t1-t0)*1e3)
	return img

def quickAperture(img, x_ref, y_ref, radius, on):
	'''Implement a faster version to create an aperture'''
	img_aperture = img.copy()
	iy, ix = np.indices(img.shape)
	img_aperture[ (iy-y_ref)**2 + (ix-x_ref)**2 > radius**2 ] = 1-on # turn off mirrors outside the aperture
	return img_aperture
