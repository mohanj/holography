###
# Definition of functions to modify the numpy array
###

import numpy as np
import time

w,h = 1024,768 ##size of the image

# ==============================
# = Some Very Basics Functions =
# ==============================

def onPosition():
    '''Define an image with all mirrors in the 'ON' position'''
    img = np.ones((w,h),int)
    img.shape=h,w ## set the array shape to our image shape
    #print "Image is : 'ON' position"
    return img

def offPosition():
    '''Define an image with all mirrors in the 'OFF' position'''
    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape
    print "Image is : 'OFF' position"
    return img

def invert(img):
    '''Invert the value of every pixel. 0 -> 1, 1 -> 0.'''
    newImg = np.zeros((w,h),int)
    newImg.shape=h,w ## set the array shape to our image shape
    newImg[:,:]=1-img[:,:]

    print "Image has been inverted"
    return newImg

# ===========================
# = More Advanced Functions =
# ===========================

def rectangle(x,y,dx,dy):
    t0 = time.clock()
    '''Create a single rectangle of size dx,dy at position x,y'''
    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape
    x = int(np.floor(x))
    y = int(np.floor(y))
    dx = int(dx)
    dy = int(dy)
    img[y:(y+dy),x:(x+dx)] = 1
    print "Rectangle at position : ",x,",",y
    print "Size : ",dx,",",dy
    t1= time.clock()
    print "Creating took",t1-t0,"seconds"
    return img

def rotatedRectangle(x,y,dx,dy):
    t0 = time.clock()
    '''Create a single rectangle of size dx,dy at position x,y'''
    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape
    x = int(np.floor(x))
    y = int(np.floor(y))
    dx = int(dx)
    dy = int(dy)
    for i in range(int(dy/np.sqrt(2))):
        for j in range(int(dx/np.sqrt(2))):
            temp_x = x + i/np.sqrt(2) + j/np.sqrt(2)
            temp_y = y + i/np.sqrt(2) - j/np.sqrt(2)
            img[int(temp_y) , int(temp_x)] = 1
    print "Rectangle at position : ",x,",",y
    print "Size : ",dx,",",dy
    t1= time.clock()
    print "Creating took",t1-t0,"seconds"
    return img

def rotated_rectangle(x, y, dX, dY):
    img_row, img_col = np.zeros((h,w), int), np.zeros((h,w), int)
    x, y, dX, dY = int(x), int(y), int(dX), int(dY)
    range_X = range(-int((dX-1)/2), dX-int((dX-1)/2))
    range_Y = range(-int((dY-1)/2), dY-int((dY-1)/2))
    dM = max(dX, dY)
    range_max = range(-int(dM/2), dM-int(dM/2))
    for n in range_X:
        for m in range_max:
            img_row[y+m, x+n+m] = 1
    for n in range_Y:
        for m in range_max:
            img_col[y-m, x+n+m] = 1
    img = np.logical_and(img_row, img_col).astype(int)
    print "Rotated rectangle at position x, y =", x, y
    print "Size dX, dY =", dX, dY 
    return img


def circle(x, y, D):
    x_arr = np.arange(w)
    y_arr = np.arange(h)
    X, Y = np.meshgrid(x_arr, y_arr, indexing='xy')
    img = ((X-x)**2 + (Y-y)**2 <= (D/2)**2).astype(int)
    print "Circle at position x, y =", x, y
    print "Diameter D =", D
    return img


def small_barrier(x, y):
    '''Create a single rectangle of size dx,dy at position x,y'''
    #t0 = time.clock()
    img = np.zeros((w,h),int)
    img.shape = h, w ## set the array shape to our image shape
    x = int(np.floor(x))
    y = int(np.floor(y))
    img[x,y] = 1
    img[x+1,y-1] = 1
    img[x-1, y+1] = 1
    return img

def periodic_square(a,b):
    '''Define an image with a mirrors on, every b mirrors'''
    '''The separation between two 'ON' square is thus b-a'''
    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape
    for i in range(a):
        for j in range(a):
            img[i::b,j::b]=1
    print "Image is : squares of ",a," x ",a," mirrors, every ",b," mirrors."
    return img

def periodic_colonnes(d_wall,d_site):
    cell_size = d_wall + d_site
    
    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape
    for j in range(d_wall):
        img[:,j::cell_size]=1
    return img

def periodic_colonnes_45_hor(d_wall,d_site):
    cell_size = d_wall + d_site
    
    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape

    #temporary image
    img_temp = np.zeros((6*w,6*h),int)
    img_temp.shape=6*h, 6*w
    for j in range(d_wall):
        img_temp[:,j::cell_size]=1

    #rotation from temp to img
    for i in range(h):
        for j in range(w):
            img[i,j] = img_temp[3*h + i + j, 3*w + i - j]
    return img

def periodic_colonnes_45_vert(d_wall,d_site):
    cell_size = d_wall + d_site
    
    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape

    #temporary image
    img_temp = np.zeros((6*w,6*h),int)
    img_temp.shape=6*h, 6*w
    for j in range(d_wall):
        img_temp[:,j::cell_size]=1

    #rotation from temp to img
    for i in range(h):
        for j in range(w):
            img[i,j] = img_temp[3*h + i - j, 3*w + i + j]
    return img


def lattice(d_wall, d_site, w = 1024, h = 768):
    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape

    cell_size = d_wall + d_site

    #Rows
    for i in range(d_wall):
        img[i::(cell_size),:]=1

    #Colonnes
    for j in range(d_wall):
        img[:,j::(cell_size)]=1

    return img

def lattice_cis(d_wall, d_site, x_cis, l_cis, w = 1024, h = 768):
	img = np.zeros((w,h),int)
	img.shape=h,w ## set the array shape to our image shape

	cell_size = d_wall + d_site

	#Rows
	for i in range(d_wall):
		img[i::(cell_size),0:x_cis]=1
		img[i+l_cis::(cell_size),x_cis:w]=1

	#Colonnes
	for j in range(d_wall):
		img[:,j::(cell_size)]=1

	return img
	
	
def lattice_psf(d_wall, d_site, w = 1024, h = 768, lattice = 'hide'):
	'''
	Return a lattice composed of single mirrors centered on the lattice cell defined by the lattice() function
	
	Parameter : 
	--> lattice : 'hide' or 'show'. if 'show', the lattice will be shown, with the mirror at the center of every lattice cell
	'''
	img = np.zeros((w,h),int)
	img.shape=h,w ## set the array shape to our image shape

	cell_size = d_wall + d_site
	
	#Center 
	if d_site%2 == 0:
		print 'WARNING : mirror would not be centered on lattice cell!'
		print 'So I put a square of 2x2 mirrors centered on lattice cell'
		center = d_wall + d_site/2
		for center_x in [center, center-1]:
			for center_y in [center, center-1]:
				img[center_y :: cell_size, center_x :: cell_size]=1
				
	else:
		print 'Ok : mirror is centered on lattice cell'
		center = d_wall + d_site/2
		img[center :: cell_size, center :: cell_size]=1

	if lattice == 'show':
	#Rows
		for i in range(d_wall):
			img[i::(cell_size),:]=1

		#Colonnes
		for j in range(d_wall):
			img[:,j::(cell_size)]=1

	return img

def wire(length, width, center = (654,456), w = 1024, h = 768):
	'''
	Return a wire of OFF mirror
	-> center of the wire (so that it is displayed of the DMD where the CCD is imaging)
	-> length : length of the wire
	-> width : width of the wire
	'''

	img = np.ones((w,h),int)
	img.shape=h,w ## set the array shape to our image shape
	x_ref, y_ref = center
	for j in np.arange(-width/2+1,width/2+1,1):
		img[(y_ref-length/2):(y_ref+length/2),x_ref + j] = 0

	return img

def wire45(length, direction = 'horizontal', center = (654,456), w= 1024, h = 768):
	'''
	Return a wire of OFF mirror
	-> center of the wire (so that it is displayed of the DMD where the CCD is imaging)
	-> length : length of the wire
	The wire is at angle of 45 with the DMD axis
	'''

	img = np.ones((w,h),int)
	img.shape=h,w ## set the array shape to our image shape
	x_ref, y_ref = center
	
	if direction == 'horizontal':
		x_run = x_ref-length/2
		y_run = y_ref+length/2
		for j in range(length):
			img[y_run:y_run+2,x_run]=0
			y_run+=-1
			x_run+=1
		
	else : 
		x_run = x_ref-length/2
		y_run = y_ref-length/2
		for j in range(length):
			img[y_run:y_run+2,x_run]=0
			y_run+=1
			x_run+=1
	return img
	
def square_wire(side_length, width, center = (654,450), w = 1024, h = 768):
	'''
	Return a wire of OFF mirror in the shape of a square
	-> center of the wire (so that it is displayed of the DMD where the CCD is imaging)
	-> side_length : length of the internal side of the square
	-> width : width of the wire
	'''

	img = np.ones((w,h),int)
	img.shape=h,w ## set the array shape to our image shape
	x_ref, y_ref = center
	side_length+=1
	
	#top side
	for j in range(width):
		img[(y_ref+side_length)+j,(x_ref-width):(x_ref+side_length)]=0
	#bottom side
	for j in range(width):
		img[y_ref-j,(x_ref-width):(x_ref+side_length)]=0
	#left side
	for j in range(width):
		img[y_ref:(y_ref+side_length),x_ref-j-1]=0
	#right side
	for j in range(width):
		img[y_ref:(y_ref+side_length),x_ref+side_length+j-width]=0
	
	return img


def spiral(length, width, spacing, center = (654,450), w=1024, h=768):
	'''
	Return a spiral of OFF mirror
	-> center of the wire (so that it is displayed of the DMD where the CCD is imaging)
	-> ength : length of the spiral
	-> width : width of the spiral
	'''

	img = np.ones((w,h),int)
	img.shape=h,w ## set the array shape to our image shape
	x_ref, y_ref = center
	x_run = x_ref
	y_run = y_ref
	
	spacing+=width
	size = spacing
	dir = 0
	inc = size
	direction_list = []
	while length>0:
		if inc>0:
			direction_list.append(dir)
			length-=1
			inc-=1
		elif inc == 0 : 
			if dir%2 == 0:
				inc = size
				dir+= 1
			elif dir%2 == 1: 
				size+=spacing
				inc = size
				dir+=1
	
	
	img[x_ref,y_ref]=0
	for dir in direction_list:
		if dir%4 == 0:
			x_run+=width
		elif dir%4 == 1:
			y_run+=-width
		elif dir%4 == 2:
			x_run+=-width
		elif dir%4 == 3:
			y_run+=width
		
		img[y_run:(y_run+width),x_run:(x_run+width)]=0
	

		
	return img


def window(a,b,square):
    # a is the size of the unit cell. a unit cell is a square made of a*a mirrors
    # b is the size of the window
    # square is the size of the center square
        
    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape

    # We define the window : b*b units of a*a pixels windows
    for i in range(b*a):
        for j in range(a):
            img[i::(b+1)*a,j::(b+1)*a]=1
    
    for i in range(b*a):
        for j in range(a):
            img[i::(b+1)*a,j+(b-1)*a::(b+1)*a]=1

    for j in range(b*a):
        for i in range(a):
            img[i::(b+1)*a,j::(b+1)*a]=1

    for j in range(b*a):
        for i in range(a):
            img[i+(b-1)*a::(b+1)*a,j::(b+1)*a]=1

    # Square of a*a mirrors at the center of the window
    for i in range(square):
        for j in range(square):
            img[(b/2*a)+i::(b+1)*a,(b/2*a)+j::(b+1)*a]=1

    return img


# ==========
# = Fun functions =
# ==========

def smiley(a,b):
    # a is the size of the unit cell. a unit cell is a square made of a*a mirrors
    # b is the size of the window

    img = np.zeros((w,h),int)
    img.shape=h,w ## set the array shape to our image shape

    # We define the window
    for i in range(b*a):
        for j in range(a):
            img[i::(b+1)*a,j::(b+1)*a]=1

    for i in range(b*a):
        for j in range(a):
            img[i::(b+1)*a,j+(b-1)*a::(b+1)*a]=1

    for j in range(b*a):
        for i in range(a):
            img[i::(b+1)*a,j::(b+1)*a]=1

    for j in range(b*a):
        for i in range(a):
            img[i+(b-1)*a::(b+1)*a,j::(b+1)*a]=1

    # We put an image inside the window

        # smile
    for i in range(a):
        for j in range(a):
            for l in range(4):#largeur sourire
                img[(b-3)*a+j::(b+1)*a,3*a+l*a+i::(b+1)*a]=1
            #img[(b-4)*a+j::(b+1)*a,2*a+i::(b+1)*a]=1
            #img[(b-4)*a+j::(b+1)*a,7*a+i::(b+1)*a]=1
        # nose
    for i in range(a):
        for j in range(a):
            img[5*a+i::(b+1)*a,4*a+a/2+j::(b+1)*a]=1

        # left eye
    for i in range(a): 
        for j in range(a):
            img[3*a+i::(b+1)*a,3*a+j::(b+1)*a]=1
        # right eye
    for i in range(a):
        for j in range(a):
            img[3*a+i::(b+1)*a,(b-4)*a+j::(b+1)*a]=1
    
    
    return img
