'''
This script defines several functions for reading the output files save.p of wavefront scans conducted by fourierTransform_GUI.py and saving the relevent data in a way that is accessible to holography.py

Must run using Python 2
'''

from holography import root
import numpy as np
import sys, pickle, os, pdb
sys.path.append( os.path.join(root,'dmd/') ) # put DMD modules in path
from fourierTransform_functions import Data

def getScanData(scanFile='save.p'):
    '''
    Save scan data from scanFile to a lighter weight .npz file that's readable in Python 2 or 3 without DLLs
    '''
    data = pickle.load(open(scanFile, 'rb'))
    np.savez( scanFile.split('.p')[0],
        n_step      = data.n_step,
        step        = data.step,
        radius      = data.radius,
        scan_size   = data.scan_size,
        y0          = data.y0,
        x0          = data.x0,
        Y           = data.Y,
        X           = data.X,
        intensities = data.intensities,
        phase_map   = 2*np.pi*sum(data.phase_map) ) # convert from wavelengths to radians
